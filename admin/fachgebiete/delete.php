<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php5 - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

if ($_POST["submit"])
{
    if ($_POST["mode"] == "delete_yes")
    {
        $kategoriefac = new Fach();
        $kategoriefac->getById($_POST["id"]);
        $kategoriefac->deleteElement();
        header("Location: ./view.php?sv=1");
    }
}

//  Sicherheitsabfrage
//////////////////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");?>
    <h1>Fachgebiet l�schen</h1>
    <?php
    $kategoriefac = new Fach();
    $kategoriefac->getById($_GET["id"]);
    $kategorie = $kategoriefac->getElement();?>
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
        <fieldset>
            <legend>Soll dieses Fachgebiet wirklich gel�scht werden?</legend>
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
            Fachgebiet: <b><?php echo $kategorie->name;?></b><br/><br />
        </fieldset>
        <br />
        <input type="submit" value="L�schen" name="submit" class="submit" />
        <?php echo $l->makeLink("Zur�ck", "./view.php", "backlink");?>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php"); 
} ?>
