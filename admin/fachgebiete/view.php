<?php
////////////////////////////////////////////////////////////////////////////////
// fachgebiete/view.php - Zeigt Tabelle fachgebiete
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

?>
<h1>Fachgebiete verwalten</h1><br/>
<?php echo '<b>'.$l->makeLink($icon_neu_small." [ neues Fachgebiet anlegen ]",WEBDIR."admin/fachgebiete/edit.php?mode=new&amp;stufe=1","none").'</b>';?>
<br /><br />
<table>
    <tr>
        <th >
            <?php  // Name
                if($_GET['mode']== "desc") echo $l->makeLink('Name '.$icon_sort_asc,WEBDIR."admin/fachgebiete/view.php?mode=asc","none");
                elseif($_GET['mode']== "asc") echo $l->makeLink('Name '.$icon_sort_desc,WEBDIR."admin/fachgebiete/view.php?mode=desc","none");
                else echo $l->makeLink('Name ',WEBDIR."admin/fachgebiete/view.php?mode=asc","none");             
            ?>
        </th>
        <th style="width:40px;">
        </th>
    </tr>
    
    
<?php $kategoriefac = new Fach(20);
if($_GET['mode']== 'asc')
{
 $sort = " ORDER by name DESC";
}
elseif($_GET['mode']== 'desc')
{
 $sort = " ORDER by name ASC";
}
else $sort = " ORDER by name ASC";

$kategoriefac->getAll($sort);
$x=0;
while ($kategorien = $kategoriefac->getElement())
{
$x++;?>

    <tr>
        <td <?php if ($x%2) echo 'class="td1"'; ?>>
            <?php echo $kategorien->name;?>
        </td>
       
        <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
   <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/fachgebiete/edit.php?mode=update&amp;id=".$kategorien->id,"none")." ".$l->makeLink($icon_delete_small,WEBDIR."admin/fachgebiete/delete.php?stufe=".$_GET['stufe']."&amp;id=".$kategorien->id,"none");?>
        </td>
    </tr>
<?php }?>
</table>
<br />
<?php 
if ($x==0)
{
    echo "Keine Fachgebiete vorhanden";
}
else 
{?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten:
        <?php 
            if($_GET['mode'])
            {
                echo $kategoriefac->getHtmlNavi("std", "&amp;mode=".$_GET['mode']);
            }
            else echo $kategoriefac->getHtmlNavi("std","" );
        ?>
    </div>
<?php 
}

include(INCLUDEDIR."footer.inc.php");
?>

