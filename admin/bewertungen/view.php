<?php
////////////////////////////////////////////////////////////////////////////////
// bewertungen/view.php - Zeigt die Tabelle bewertungen
////////////////////////////////////////////////////////////////////////////////


include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<h1>Bewertungen verwalten</h1>
<br/>

<?php
    $kommentarfac = new Bewertung(25);
    // Sortierung
    $mode=strtoupper($_GET['mode']);
    $sortierfelder=array("2"=>'unternehmen.name',
                         "3"=>'bewertungen.userid',
                         "4"=>'bewertungen.kommentar',
                         "5"=>'bewertungen.aktiv');

    if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
    {
        $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
    }
    else $sort = '';

    // Alle Bewertungen ansehen
    //$kommentarfac->getAllBewertung();
    $kommentarfac->getAllBewertungSort($sort);
?>
<br class="clr" /><br />
<table style="width:100%">
    <tr>
        <th style="width:95px;" >
            Bewertung
        </th>
        <th>
        <?php // Unternehmen
            if($_GET['mode']== "desc" && $_GET['sp']==2) echo $l->makeLink('Unternehmen '.$icon_sort_asc,WEBDIR."admin/bewertungen/view.php?mode=asc&amp;sp=2","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==2) echo $l->makeLink('Unternehmen '.$icon_sort_desc,WEBDIR."admin/bewertungen/view.php?mode=desc&amp;sp=2","none");
            else echo $l->makeLink('Unternehmen ',WEBDIR."admin/bewertungen/view.php?mode=asc&amp;sp=2","none");             
        ?>
        </th>
        <th>
        <?php // User
            if($_GET['mode']== "desc" && $_GET['sp']==3) echo $l->makeLink('User '.$icon_sort_asc,WEBDIR."admin/bewertungen/view.php?mode=asc&amp;sp=3","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==3) echo $l->makeLink('User '.$icon_sort_desc,WEBDIR."admin/bewertungen/view.php?mode=desc&amp;sp=3","none");
            else echo $l->makeLink('User ',WEBDIR."admin/bewertungen/view.php?mode=asc&amp;sp=3","none");             
        ?>
        </th>
        <th>
        <?php // Kommentar/Antwort
            if($_GET['mode']== "desc" && $_GET['sp']==4) echo $l->makeLink('Kommentar/Antwort '.$icon_sort_asc,WEBDIR."admin/bewertungen/view.php?mode=asc&amp;sp=4","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==4) echo $l->makeLink('Kommentar/Antwort '.$icon_sort_desc,WEBDIR."admin/bewertungen/view.php?mode=desc&amp;sp=4","none");
            else echo $l->makeLink('Kommentar/Antwort ',WEBDIR."admin/bewertungen/view.php?mode=asc&amp;sp=4","none");             
        ?>            
        </th>
        <th style="width:40px;">
        <?php // Status
            if($_GET['mode']== "desc" && $_GET['sp']==5) echo $l->makeLink('Status '.$icon_sort_asc,WEBDIR."admin/bewertungen/view.php?mode=asc&amp;sp=5","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==5) echo $l->makeLink('Status '.$icon_sort_desc,WEBDIR."admin/bewertungen/view.php?mode=desc&amp;sp=5","none");
            else echo $l->makeLink('Status ',WEBDIR."admin/bewertungen/view.php?mode=asc&amp;sp=5","none");             
        ?>            
        </th>
        <th style="width:40px;">
            Admin
        </th>
    </tr>
    
    
<?php 
$t=0;
while ($kommentar = $kommentarfac->getElement())
{ 
    $t++;
   ?>

   <tr>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php
            // Bewertungsarray()
            $bewertungen=array();
 
            // Bewertung auslesen 
            $bewertungen[]=unserialize($kommentar->bewertung);
                    
            // Gr��e des Bewertungsarrays bestimmen         
            $size_bewertung=sizeof($bewertungen);
                                
            $punktebewertung=0;
                                     
            //Bewertungspunkte addieren 
            for ($x=0;$x<$size_bewertung;$x++)
            {
                $punktebewertung=$punktebewertung+(array_sum($bewertungen[$x])/sizeof($bewertungen[$x]));
            }
                                       
            // Quersumme bilden
            $gesamtbewertung=$punktebewertung/$size_bewertung;
             
            // Ausgabe der Bewertung
            for ($r=1;$r<=5;$r++)
            {
                if ($r<=round($gesamtbewertung,0))
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star.gif" alt="Stern-bewertet-<?php echo $r;?>"/>
                <?php 
                }
                
                else
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star_grey.gif" alt="Stern-unbewertet-<?php echo $r;?>"/>
                <?php
                }
           }
           ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($kommentar->name.', '.$kommentar->vorname,WEBDIR."admin/unternehmen/edit.php?mode=update&amp;id=".$kommentar->unternehmenid); ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($kommentar->username,WEBDIR."admin/benutzer/details.php?mode=update&amp;id=".$kommentar->benutzerid); ?>

       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           Kommentar:<br/>
           <?php echo substr($kommentar->kommentar,0,150);?> ...
           <br/><hr/>
           Antwort:
           <br/>

           <?php echo substr($kommentar->antwort,0,150);?> ...
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php if ($kommentar->status=="1") echo "OK";
           else if ($kommentar->status=="0") echo "K-neu"; 
           else if ($kommentar->status=="2") echo "A-neu"; ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?> style="text-align:center">
       <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/bewertungen/edit.php?id=".$kommentar->id);?>
           <?php echo $l->makeLink($icon_delete_small,WEBDIR."admin/bewertungen/delete.php?id=".$kommentar->id);?>
       </td>
   </tr>

<?php } ?>
</table>
<br/>
Status-Legende: <br/><b>OK</b> = Bewertung freigegeb.; <b>K-neu</b> = neue Bewertung mit Kommentar; <b>A-neu</b> = neue Antwort des Untern.
<br /><br />

<?php if ($t==0) echo "Keine Bewertungen vorhanden";
else {

?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten: 
        <?php 
            if($_GET['sp'])
            {
                echo $kommentarfac->getHtmlNavi("std", "&amp;sp=".$_GET['sp']."&amp;mode=".$_GET['mode']);
            }
            else echo $kommentarfac->getHtmlNavi("std","" );
        ?>
    </div>
<?php 
}

include(INCLUDEDIR."footer.inc.php");
?>

