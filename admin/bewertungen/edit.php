<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php5 - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

$kommentarfac = new Bewertung();
$kommentarfac->getById($_GET["id"]);
$kommentar = $kommentarfac->getElement();

?>
<h1>Bewertung bearbeiten</h1>


<form action="<?php echo $l->makeFormLink(WEBDIR.'admin/bewertungen/update.php?id='.$_GET['id']);?>" method="post">
    <fieldset>
		<legend>Bewertung bearbeiten</legend>
        <br/>
    <div class="headline">
        Bewertung
    </div>
        <?php
            // Bewertungsarray()
            $bewertungen=array();
 
            // Bewertung auslesen 
            $bewertungen[]=unserialize($kommentar->bewertung);
                    
            // Gr��e des Bewertungsarrays bestimmen         
            $size_bewertung=sizeof($bewertungen);
                                
            $punktebewertung=0;
                                     
            //Bewertungspunkte addieren 
            for ($x=0;$x<$size_bewertung;$x++)
            {
                $punktebewertung=$punktebewertung+(array_sum($bewertungen[$x])/sizeof($bewertungen[$x]));
            }
                                       
            // Quersumme bilden
            $gesamtbewertung=$punktebewertung/$size_bewertung;
             
            // Ausgabe der Bewertung
            for ($r=1;$r<=5;$r++)
            {
                if ($r<=round($gesamtbewertung,0))
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star.gif" alt="Stern-bewertet-<?php echo $r;?>"/>
                <?php 
                }
                
                else
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star_grey.gif" alt="Stern-unbewertet-<?php echo $r;?>"/>
                <?php
                }
           }
           ?>
 <br/>
 <label>Kommentar</label>
     <textarea cols="20" rows="20" name="kommentar"><?php echo $kommentar->kommentar;?></textarea><br/>
    <label>Antwort</label>
    
    
    <textarea cols="20" rows="20" name="antwort"><?php echo $kommentar->antwort;?></textarea>
    <br class="clr" /><br />
    <label>
        Status
    </label>
    <select name="aktiv">
        <option value="0" <?php if ($kommentar->aktiv==0) echo 'selected="selected"'  ?>>Kommentar Neu</otpion>
        <option value="1" <?php if ($kommentar->aktiv==1) echo 'selected="selected"'  ?>>Freischalten</option>
        <option value="2" <?php if ($kommentar->aktiv==2) echo 'selected="selected"'  ?>>Antwort neu</option>  
    </select>
    </fieldset><br />
    <input type="submit" value="Daten speichern" class="submit" />
</form>
<?php
include(INCLUDEDIR."footer.inc.php");
?>

