<?php
////////////////////////////////////////////////////////////////////////////////
// bewertungen/update.php - auswerten der Formulardaten
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."std/email.class.php");          // eMail-class


$kommentarfac = new Bewertung();
$kommentarfac->getById($_GET["id"]);


$kommentarfac->update("aktiv='".mysql_real_escape_string($_POST['aktiv'])."', antwort='".mysql_real_escape_string($_POST['antwort'])."', kommentar='".mysql_real_escape_string($_POST['kommentar'])."'");

// Wenn Status auf freigeschalten ge�ndert wurde, Mail an User, vorher
// pr�fen ob User mit Mail in benutzertabelle
if($_POST['aktiv']== 1)
{
    $kommentar = $kommentarfac->getElement();
    
    $userid = $kommentar->userid;
    
    $benutzerfac = new Benutzer();
    $benutzerfac->getBenutzerById($userid);
    if($benutzer = $benutzerfac->getElement())
    {
        $unternehmenfac = new Daten();
        $unternehmenfac->getById($kommentar->unternehmenid);
        $unternehmen = $unternehmenfac->getElement();
        
        if($benutzer->email == '' || !isMail($benutzer->email))
        {
            $_SESSION['err'] .= 'Keine g�ltige Benutzermailadresse vorhanden<br />'; 
        }
        else
        {
            // Mailtext
            $msg = "Hallo ".$benutzer->anrede." ".$benutzer->name.chr(10).chr(10);
            $msg .= "Mitteilung vom ".date("d.m.Y",time()).chr(10).chr(10);
            $msg .= "Ihre Bewertung f�r ".$unternehmen->famname." ".$unternehmen->name." wurde freigeschaltet.";
          
            $emailfac = new Email(1);
            $emailfac->setFrom($CONST_MAIL['from']);
            $emailfac->setTo($benutzer->email);
            $emailfac->setSubject("Bewertung freigeschalten");
            $emailfac->setContent($msg);
            //echo nl2br($msg);
            $emailfac->sendMail();
            // End eMail
        }
    } 
}// end  if($_POST['aktiv']== 1) 

header("Location:view.php?sv=1");
?>

