<?php

////////////////////////////////////////////////////////////////////////////////
// bewertungen/delete.php - l�scht nach nachfrage Tupel aus Tabelle
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."std/email.class.php");          // eMail-class

if ($_POST["mode"] == "delete_yes")
{
    $kommentarfac = new Bewertung();
    $kommentarfac->getById($_POST['id']);
    
    // Wenn Datensatz gel�scht wird, Mail an User, vorher
    // pr�fen ob User mit Mail in benutzertabelle
    $kommentar = $kommentarfac->getElement();
    
    $userid = $kommentar->userid;
    
    $benutzerfac = new Benutzer();
    $benutzerfac->getBenutzerById($userid);
    // user mit id in benutzertabelle
    if($benutzer = $benutzerfac->getElement())
    {
        $unternehmenfac = new Daten();
        $unternehmenfac->getById($kommentar->unternehmenid);
        $unternehmen = $unternehmenfac->getElement();
        
        if($benutzer->email == '' || !isMail($benutzer->email))
        {
            $_SESSION['err'] .= 'Keine g�ltige Benutzermailadresse vorhanden<br />'; 
        }
        else
        {
            // Mailtext
            $msg = "Hallo ".$benutzer->anrede." ".$benutzer->name.chr(10).chr(10);
            $msg .= "Mitteilung vom ".date("d.m.Y",time()).chr(10).chr(10);
            $msg .= "Ihre Bewertung f�r ".chr(10);
            $msg .= $unternehmen->famname." ".$unternehmen->name." wurde gel�scht.".chr(10).chr(10);
            if($_POST['kommentar']) $msg .= "Grund: ".chr(10).chr(10).$_POST['kommentar'];
          
            $emailfac = new Email(1);
            $emailfac->setFrom($CONST_MAIL['from']);
            $emailfac->setTo($benutzer->email);
            $emailfac->setSubject("Bewertung gel�scht");
            $emailfac->setContent($msg);
            //echo nl2br($msg);
            $emailfac->sendMail();
            // End eMail
        }
    } 
    // datensatz l�schen
    $kommentarfac->deleteElement();
    
    header("Location: ./view.php?sv=1");
}

//  Sicherheitsabfrage
////////////////////////////////////////////////////////////////////////////////
   
else
{
    include(INCLUDEDIR."header.inc.php");
    ?>
        <h1>Bewertung l�schen</h1>
            
            
            <?php
             $kommentarfac = new Bewertung();
             $kommentarfac->getById($_GET['id']);
             $kommentar = $kommentarfac->getElement();?>
<div class="headline">
        Bewertung
    </div>
        <?php
            // Bewertungsarray()
            $bewertungen=array();
 
            // Bewertung auslesen 
            $bewertungen[]=unserialize($kommentar->bewertung);
                    
            // Gr��e des Bewertungsarrays bestimmen         
            $size_bewertung=sizeof($bewertungen);
                                
            $punktebewertung=0;
                                     
            //Bewertungspunkte addieren 
            for ($x=0;$x<$size_bewertung;$x++)
            {
                $punktebewertung=$punktebewertung+(array_sum($bewertungen[$x])/sizeof($bewertungen[$x]));
            }
                                       
            // Quersumme bilden
            $gesamtbewertung=$punktebewertung/$size_bewertung;
             
            // Ausgabe der Bewertung
            for ($r=1;$r<=5;$r++)
            {
                if ($r<=round($gesamtbewertung,0))
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star.gif" alt="Stern-bewertet-<?php echo $r;?>"/>
                <?php 
                }
                
                else
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star_grey.gif" alt="Stern-unbewertet-<?php echo $r;?>"/>
                <?php
                }
           }
          
             
             
                echo '<br />Kommentar:<b> "'.substr($kommentar->kommentar,0,300).'..."</b><br /><br />';
                echo '<br />Antwort:<b> "'.substr($kommentar->antwort,0,300).'..."</b><br /><br />';

            ?>
            
            <div class="contentbox">
                <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
                    
                    <input type="hidden" name="mode" value="delete_yes" />
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
                    Soll diese Bewertung wirklich gel�scht werden?
                    <br /><br />
                    <fieldset>
		            <legend>Begr�ndung f�r die L�schung</legend>
                    <label style="width:1px;">
                    Begr�ndung
                    </label>
                    <textarea cols="20" rows="20" name="kommentar"></textarea>
                    <br class="clr" />
                    </legend>
                    <input type="submit" value="L�schen" name="submit" class="submit" /><br class="clr"><BR>
                    <?php echo $l->makeLink("Zur�ck", "./view.php", "login");?>
                
                </form>
            </div>
<?php
include(INCLUDEDIR."footer.inc.php"); }
?>

