<?php
////////////////////////////////////////////////////////////////////////////////
// forum.edit.php - Zum Bearbeiten des Forums / Eintr�ge
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
?>
<script src="<?php echo WEBDIR;?>script/addon.js" language="javascript" type="text/javascript"></script>
<?php
if ($_SESSION["user"])
{
        // Fehlerbox
        ////////////////////////////////////////////////////////////////////////
        if ($_SESSION["err"])
        {?>
            <p class="contentboxheader">Hier stimmt was nicht</p>
            <div class="contentbox">
                <?php echo $_SESSION["err"]; unset($_SESSION["err"]); ?>
            </div>
        <?php 
        }
    
        // Eingabemasken f�r msg und newmsg in user/forum.edit.php
        //////////////////////////////////////////////////////////////////////////////////////////
        /*if ($_GET["mode"] == "msg" || $_GET["mode"] == "newmsg")
        {
        ?>
    
            <p class="contentboxheader"><?php echo ($_GET["mode"]=="msg"?"Beitrag schreiben":"Neues Topic erstellen");?></p>
            <br />
                <form action="<?php echo $l->makeFormLink('./forum.update.php'); ?>" method="post" class="forum">
                    <input type="hidden" name="mode" value="<?php echo $_GET['mode'];?>" />
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
                    <input type="hidden" name="rid" value="<?php echo $_GET['rid'];?>" />
                    <table cellpadding="0" cellspacing="0" class="eintrag">
                        <tr> 
                            <th rowspan="2" class="miniprofil" valign="top">
                              
                                  <?php
                                  $benutzerfac = new Benutzer();
                                  $benutzerfac->getById($_SESSION["user"]->id);
                                  $benutzer = $benutzerfac->getElement();
                                  
                                  $dateifac = new Datei("file");
                                  $dateifac->getByRefAndGroup($benutzer->id, "profil");
                                  
                                  
                                  echo '<p class="thumbbox">';
                                  if ($datei = $dateifac->getElement())
                                  {
                                      $thumbfac = new Thumb($dateifac->getDatei($datei), "files/thumb/", $CONST_THUMB['width'], $CONST_THUMB['height']);
                                      $thumbfac->write();
                                      $bildsize = getimagesize($_SERVER["DOCUMENT_ROOT"].$thumbfac->getDatei());
                                      echo $l->makeLink('<img src="'.$thumbfac->getDatei().'" alt="'.$datei->name.'" style="margin-top:'.round(((60 - $bildsize[1]) / 2)).'px;">', WEBDIR."pages/profil.view.php?id=".$benutzer->id); 
                                  }
                                  else 
                                      echo $l->makeLink('<img src="'.WEBDIR.'images/head_who_thumb.gif" alt="'.$benutzer->name.'">', WEBDIR."user/profil/profil.view.php?id=".$benutzer->id);
                                  echo '</p><br class="clr">';
                                  
                                  echo $benutzer->nickname."<br />";
                                  
                                  $forummsgfac2 = new ForumMsg();
                                  $forummsgfac2->getByUser($benutzer->id);
                                  
                                  echo "Beitr�ge: ".$forummsgfac2->getElementCount()."<br />";
                                  ?>
                                          
                            </th>
                            <td class="headline">
                                  <?php
                                  if ($_GET["mode"] == "newmsg")
                                      echo 'Titel:<br /><input type="text" name="titel" />';
                                  else
                                      echo date("d.m.Y H:i");
                                  ?>
                            </td>
                        </tr> 
                        <tr> 
                            <td class="text">
                                <?php 
                                $htmlarea = new HtmlArea("area");
                                echo $htmlarea->getHtmlButtons();
                                ?>
                            
                                <textarea id="area" name="text" rows="0" cols="0"><?php if ($_GET["zitat"]) echo '[quote msg='.$_GET["zitat"].']'.chr(10);?></textarea>
                            </td>
                            
                        </tr>
                    
                    </table>
                    <br />
                    <?php echo $l->makeLink("Zur�ck", WEBDIR."bewertungen/forum.view.php?id=".$_GET["id"]."&amp;rid=".$_GET["rid"], "backlink");?>
                    <input type="submit" value="Absenden" class="submit" /><br class="clr" />
                
                </form>
            
        
        <?php 
        }*/
        
        // Eingabemaske f�r msg aus adminLink
        ////////////////////////////////////////////////////////////////////////
        if ($_GET["mode"] == "msg" )
        {
              $forummsgfac = new ForumMsg(); 
              $forummsgfac->getBySuperIdFirst($_GET["rid"]);
              $forummsg = $forummsgfac->getElement();
        ?>
    
            <form action="<?php echo $l->makeFormLink('./forum.update.php'); ?>" method="post" class="forum">
                    <input type="hidden" name="mode" value="<?php echo $_GET['mode'];?>" />
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
                    <input type="hidden" name="rid" value="<?php echo $_GET['rid'];?>" />
                    <table cellpadding="0" cellspacing="0" class="eintrag">
                        <tr> 
                            <td class="headline">
                                  Titel:<br /><input type="text" name="titel" value="<?php echo $forummsg->titel;?>" />
                            </td>
                        </tr> 
                        
                    
                    </table>
                    <br />
                    <?php echo $l->makeLink("Zur�ck", WEBDIR."bewertungen/forum.view.php?id=".$_GET["id"]."&amp;rid=".$_GET["rid"], "backlink");?>
                    <input type="submit" value="Absenden" class="submit" /><br class="clr" />
                </form>
        
        <?php 
        }
        
        if ($_GET["mode"] == "forum" && $_SESSION["user"]->typ == "admin" || $_SESSION["rechte"]["forum"][$_GET["id"]])
        {   
            $forumfac = new Forum();
            $forumfac->getById($_GET["id"]);
            $forum = $forumfac->getElement();
            ?>
            
            <p class="contentboxheader">Forum</p>

            <div class="contentbox">
                <form action="<?php echo $l->makeFormLink('./forum.update.php'); ?>" method="post" class="writeset">
                    <input type="hidden" name="mode" value="forum" />
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
                    <label>Typ:</label>
                    <select name="typ">
                        <option value="0" <?php echo ($forum->typ==0?'selected="selected"':'');?>>Unterforum</option>
                        <option value="1" <?php echo ($forum->typ==1?'selected="selected"':'');?>>Hauptforum</option>
                    </select><br class="clr" />
                    <label>Kategorie (Unterforum):</label>
                    <select name="kategorie">
                        <option value="">---</option>
                        <?php
                        $forumkatfac = new Forum();
                        //$forumkatfac->getKategorien();    
                        $forumkatfac->getByTyp(1);          // nur Hautkategorien
                        while ($kat = $forumkatfac->getElement())
                            echo '<option value="'.$kat->id.'" '.($kat->id==$forum->kategorie?'selected="selected"':'').'>'.$kat->name.'</option>';
                        ?>
                    </select><br class="clr" />
                    
                    <label>Neue Kategorie (Hauptforum):</label><input type="text" name="newkategorie" value="<?php echo ($_SESSION['post']->kategorie ? $_SESSION['post']->kategorie : '');?>" /><br class="clr" />
                    <label>Forumname:</label><input type="text" name="name" value="<?php echo ($_SESSION['post']->name ? $_SESSION['post']->name : $forum->name);?>" /><br class="clr" />
                    <label>Moderatoren:</label><input type="text" name="mods" value="<?php echo ($_SESSION['post']->mods ? $_SESSION['post']->mods : $forum->mods);?>" /><br class="clr" />
                    <br />
                    <label>Beschreibung: (optional)</label><textarea name="beschreibung" rows="0" cols="0"><?php echo ($_SESSION['post']->beschreibung ? $_SESSION['post']->beschreibung : $forum->beschreibung);?></textarea><br class="clr" />
                    <br />
                    <?php /*<label>Showrule: ($_SESSION["webbase"])</label><input type="text" name="showrule" value="<?php echo ($_SESSION['post']->showrule ? $_SESSION['post']->showrule : $forum->showrule);?>" /><br class="clr" />*/ ?>
                    <br class="clr" /><br />
                    <?php echo $l->makeLink("Zur�ck", WEBDIR."bewertungen/forum.view.php", "backlink");?>
                    <input type="submit" value="Absenden" class="submit right" /><br class="clr" />
                </form>
            </div>   
        <?php 
        }
        else $_SESSION["err"] = "Sie haben kein Recht f�r dieses Forum<br />";
}// end if ($_SESSION["user"])
else echo "No User!";

include(INCLUDEDIR."footer.inc.php");
?>
