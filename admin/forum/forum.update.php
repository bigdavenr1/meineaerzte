<?php


//////////////////////////////////////////////////////////////////////////////////////////////////
// forum.update.php - Verwaltung des Forums + Eintr�ge
//////////////////////////////////////////////////////////////////////////////////////////////////


include("../../inc/config.php");


if ($_SESSION["user"])
{
    unset($_SESSION["post"]);
    unset($err);
    
    
    
    // Topic erstellen bzw. bearbeiten
    //////////////////////////////////////////////////////////////////////////////////////////////
    //################################################################
    // msg aus adminLink verarbeiten
    // nur Titel der Topic updaten
    if ($_POST["mode"] == "msg")
    {
        if (!$_POST["titel"]) $err .= "Dein Thema brauch einen Titel<br>";
        if (!$_POST["id"]) $err .= "Es ist kein Thema ausgew�hlt<br>";
        if (!$_POST["rid"]) $err .= "Es ist kein Topic ausgew�hlt<br>";
        
        
        if (!$err)
        {

            $forummsgfac = new ForumMsg();
            $forummsgfac->getBySuperIdFirst($_POST["rid"]);
            $forummsgfac->update("titel='".$_POST['titel']."'");
            
            header("Location: ".WEBDIR."bewertungen/forum.view.php?id=".$_POST["id"]."&sv=1");
        }
    }
    //################################################################
    
    // Topic und Beitr�ge erstellen in user/forum.update.php
    /*if ($_POST["mode"] == "newmsg")
    {
        if (!$_POST["titel"]) $err .= "Dein Thema brauch einen Titel<br>";
        if (!$_POST["text"]) $err .= "Dein Thema brauch einen Text<br>";
        if (!$_POST["id"]) $err .= "Es ist kein Forum ausgew�hlt<br>";
        
        
        if (!$err)
        {

            $forummsgfac = new ForumMsg();
            $data[] = "";                                                           // id
            $data[] = $_POST["id"];                                                 // forum_id
            $data[] = $_SESSION["user"]->id;                                        // user_id
            $data[] = md5($_SESSION["user"]->id.$_POST["id"].date("Y-m-d H:i:s"));  // super_id
            $data[] = date("Y-m-d H:i:s");                                          // datum
            $data[] = date("Y-m-d H:i:s");                                          // last_datum
            $data[] = "";                                                           // edit_datum
            $data[] = "";                                                           // edit_times
            $data[] = addslashes(trim(strip_tags($_POST["titel"])));                // titel
            $data[] = nl2br(addslashes(trim(strip_tags($_POST["text"]))));          // text
            $data[] = $_SESSION["user"]->id;                                        // aufrufe
            $forummsgfac->write($data);
            
        
            $_SESSION["punkte"] = "topic";
        
            header("Location: ".WEBDIR."bewertungen/forum.view.php?id=".$_POST["id"]."&sv=1");
        }
        else
        {
            $_SESSION["err"] = $err;
            $_SESSION["post"] = $_POST;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }    
    }
    
    
    
    // Antwort erstellen
    //////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    if ($_POST["mode"] == "msg")
    {
        if (!$_POST["text"]) $err .= "Dein Thema brauch einen Text<br>";
        if (!$_POST["id"]) $err .= "Es ist kein Forum ausgew�hlt<br>";
        if (!$_POST["rid"]) $err .= "Es ist kein Topic ausgew�hlt<br>";
        
        
        if (!$err)
        {

            $forummsgfac = new ForumMsg();
            $data[] = $_POST["mid"];                                                // id
            $data[] = $_POST["id"];                                                 // forum_id
            $data[] = $_SESSION["user"]->id;                                        // user_id
            $data[] = $_POST["rid"];                                                // super_id
            $data[] = $_POST["date"]?$_POST["date"]:date("Y-m-d H:i:s");            // datum
            $data[] = "";                                                           // last_datum
            
            if ($_POST["mid"])
            {
                $data[] = date("Y-m-d H:i:s");                                      // edit_datum
                $data[] = $_POST["edittimes"]+1;                                    // edit_times
            }
            else
            {
                $data[] = "";                                                       // edit_datum
                $data[] = "";                                                       // edit_times
            }
            
            $data[] = $_POST["titel"];                                              // titel
            $data[] = nl2br(addslashes(trim(strip_tags($_POST["text"]))));          // text
            $data[] = $_POST["aufrufe"];                                            // aufrufe
            $forummsgfac->write($data);
            
            
            if (!$_POST["mid"])
            {
                $fmfac = new ForumMsg();
                $fmfac->getTopicByForumIdAndSuperId($_POST["id"], $_POST["rid"]);
                $fmfac->update("last_datum='".date("Y-m-d H:i:s")."'");
            }
            
            
            $_SESSION["punkte"] = "eintrag";
            
            header("Location: ".WEBDIR."bewertungen/forum.view.php?id=".$_POST["id"]."&rid=".$_POST["rid"]."&sv=1&offset0=".$_POST["offset0"]."&offset1=".$_POST["offset1"]);
        }
        else
        {
            $_SESSION["err"] = $err;
            $_SESSION["post"] = $_POST;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }    
    }*/
    
    
    
    // Forum erstellen
    //////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    if ($_POST["mode"] == "forum")
    {
        if (!$_POST["name"]) $err .= "Das Forum braucht einen Namen<br>";
        if ($_POST["typ"]==0 && $_POST["kategorie"]=="") $err .= "Das Forum braucht eine Kategorie<br>";
        if ($_POST["typ"]==1 && $_POST["newkategorie"]=="" && !$_POST["id"]) $err .= "Das neue Forum braucht eine Kategorie<br>";
        
        
        if (!$err)
        {
            $forumfac = new Forum();
            
            if(!$_POST["id"])       // neues Forum anlegen
            {
              $data[] = $_POST["id"];                                                         // id
              $data[] = $_POST["typ"];                                                        // typ
              $data[] = addslashes(trim(strip_tags($_POST["name"])));                         // name
              $data[] = nl2br(addslashes(trim(strip_tags($_POST["beschreibung"]))));          // beschreibung
              $data[] = addslashes(trim(strip_tags($_POST["mods"])));                         // mods
              $data[] = time();                                                               // sort
              $data[] = $_POST["newkategorie"]?$_POST["newkategorie"]:$_POST["kategorie"];    // kategorie
              $forumfac->write($data);
            }
            else                    // Daten �ndern
            {
              $forumfac->getById($_POST["id"]);
              $forumfac->update("name='".addslashes(trim(strip_tags($_POST["name"])))."', beschreibung='".addslashes(trim(strip_tags($_POST["beschreibung"])))."', mods='".addslashes(trim(strip_tags($_POST["mods"])))."', kategorie='".addslashes(trim(strip_tags($_POST["kategorie"])))."'");
            }
        
            header("Location: ".WEBDIR."bewertungen/forum.view.php?sv=1");
        }
        else
        {
            $_SESSION["err"] = $err;
            $_SESSION["post"] = $_POST;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
    }
    
    
    
    // Sorterfunktion
    //////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    if ($_GET["mode"] == "sortup" || $_GET["mode"] == "sortdown")
    {
        $mforumfac = new Forum();
        $mforumfac->getByTyp(1);
        
        $swapindex = false;
        for ($i = 0; $mforum = $mforumfac->getElement(); $i++)
        {
            if ($_GET["mode"] == "sortdown" && $_GET["id"] == $mforum->id) $swapindex = $i-1;
            if ($_GET["mode"] == "sortup" && $_GET["id"] == $mforum->id) $swapindex = $i+1;
        }
        
        if ($swapindex >= 0 && $swapindex < $mforumfac->getElementCount())
        {
            // ID des neuen Platzes
            $mforum = $mforumfac->getElement($swapindex);
        
            // ID des alten Platzes
            $sforumfac = new Forum();
            $sforumfac->getById($_GET["id"]);
            $sforum = $sforumfac->getElement();
        
            // Update
            $mforumfac->update("sort='".$sforum->sort."'");
            $sforumfac->update("sort='".$mforum->sort."'");
        }
        
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }
}
else
{
    header("Location: ".WEBDIR."inc/msg.php?msg=login");
}

?>
