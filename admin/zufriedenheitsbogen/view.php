<?php

////////////////////////////////////////////////////////////////////////////////
// zufriedenheitsbogen/view.php5 - Zeigt die Tabelle zufriedenheit, mit zufriedenheitspunkten
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// Object initialisieren
$zupunktefac = new Zufriedenheitspunkt();
?>
<h1>Zufriedenheitssbogen verwalten</h1><br/>
<?php echo '<b>'.$l->makeLink($icon_neu_small." [ neue Zufriedenheitskategorie anlegen ]",WEBDIR."admin/zufriedenheitsbogen/edit.php?mode=new&amp;item=kat&amp;stufe=1","none").'</b>';?>
<br /><br/>
<?php
$k=1;
$x=0;
$zupunktefac->getBewertungWithKat();
//$zupunktefac->createOwnQuery("SELECT * , katzufriedenheit.name AS katname FROM katzufriedenheit,zufriedenheitspunkte WHERE zufriedenheitspunkte.kat = katzufriedenheit.id ORDER BY katzufriedenheit.id" );
$katname=array();
while($zufriedenheit= $zupunktefac->getElement())
{
    $tabellenende="";
    $k++;

    if (!in_array($zufriedenheit->kat,$katname))
    { 
        $x++;
        if ($x!=1) echo '<tr><td colspan="2">'.$l->makeLink($icon_neu_small." [ neuen Zufriedenheitspunkt anlegen ]",WEBDIR."admin/zufriedenheitsbogen/edit.php?mode=new&amp;item=point&amp;kat=".$lastkat."&amp;stufe=1","none").'</td></tr></table><br/>';
        echo '<h2>'.$zufriedenheit->katname.' '.$l->makeLink($icon_edit_small,WEBDIR."admin/zufriedenheitsbogen/edit.php?mode=update&amp;item=kat&amp;id=".$zufriedenheit->kat,"none").' '.$l->makeLink($icon_delete_small,WEBDIR."admin/zufriedenheitsbogen/delete.php?item=kat&amp;id=".$zufriedenheit->kat,"none").'</h2>'.CHR(10);
        $katname[]=$zufriedenheit->kat;
        $k=1;            
        ?>
        <table>
            <tr>
                <th >
                    Name
                </th>
                <th style="width:40px;">
                </th>
            </tr>
    <?php
        $lastkat=$zufriedenheit->kat;        
    }

    if ($k%2) $class='class="td1"';
    else $class='class="td"';
    echo '<tr><td '.$class.'>'.$zufriedenheit->name.'</td><td>'.$l->makeLink($icon_edit_small,WEBDIR."admin/zufriedenheitsbogen/edit.php?mode=update&amp;item=point&amp;id=".$zufriedenheit->id,"none").' '.$l->makeLink($icon_delete_small,WEBDIR."admin/zufriedenheitsbogen/delete.php?item=point&amp;id=".$zufriedenheit->id,"none").'</td></tr>';
}
?>
    <tr>
        <td colspan="2">
            <?php echo $l->makeLink($icon_neu_small." [ neuen Zufriedenheitspunkt anlegen ]",WEBDIR."admin/zufriedenheitsbogen/edit.php?mode=new&amp;item=point&amp;kat=".$lastkat,"none")?>
        </td>
    </tr>
</table>
<br/>
<hr/>

<h1>Kategorien ohne Zufriedenheitspunkte</h1>
<?php 
$kategoriefac=new Zufriedenheitspunkt();
$kategoriefac->getKat();
$d=0;
while($kategorie=$kategoriefac->getElement())
{
    if (!in_array($kategorie->id,$katname))
    {
        $d++;
        echo '<h2>'.$kategorie->name.' '.$l->makeLink($icon_edit_small,WEBDIR."admin/zufriedenheitsbogen/edit.php?mode=update&amp;item=kat&amp;id=".$kategorie->id,"none").' '.$l->makeLink($icon_delete_small,WEBDIR."admin/zufriedenheitsbogen/delete.php?item=kat&amp;id=".$kategorie->id,"none").'</h2>'.CHR(10);
    }
}

if ($d==0) echo "keine Kategorie ohne Zufriedenheitspunkte gefunden";
?>
<br/><br/>
<hr/>

<h1>Zufriedenheitspunkte ohne g�ltige Kategorie</h1>
<table>
            <tr>
                <th >
                    Name
                </th>
                <th style="width:40px;">
                </th>
            </tr>
<?php 
$h=0;
$zufriedenheitspunktfac=new Zufriedenheitspunkt();
$zufriedenheitspunktfac->getAll();
while($zufriedenheitspunkt=$zufriedenheitspunktfac->getElement())
{
    if (!in_array($zufriedenheitspunkt->kat,$katname))
    {
        $h++;
        echo '<tr><td>'.$zufriedenheitspunkt->name.'</td><td> '.$l->makeLink($icon_edit_small,WEBDIR."admin/zufriedenheitsbogen/edit.php?mode=update&amp;item=point&amp;id=".$zufriedenheitspunkt->id,"none").' '.$l->makeLink($icon_delete_small,WEBDIR."admin/zufriedenheitsbogen/delete.php?item=point&amp;id=".$zufriedenheitspunkt->id,"none").'</td></tr>'.CHR(10);
    }
}

if ($h==0) echo '<tr><td colspan="2">Keine Zufriedenheitspunkte ohne g�ltige Kategoriezuordnung gefunden</td></tr>'

?>

</table><br />
<?php 

include(INCLUDEDIR."footer.inc.php");
?>

