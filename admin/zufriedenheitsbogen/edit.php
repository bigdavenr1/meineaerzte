<?php
////////////////////////////////////////////////////////////////////////////////
// zufriedenheitsbogen/edit.php - Bearbeitungsformular
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

if ($_GET['mode']=="update")
{  
    $zufriedenheitskatfac = new Zufriedenheitspunkt();
    
    if ($_GET['item']=="kat")
    {
        $zufriedenheitskatfac->getKatById($_GET['id']);
        $h1 = "<h1>Zufriedenheitsbogenkategorie bearbeiten</h1>";
    }
    
    if ($_GET['item']=="point")
    {
        $zufriedenheitskatfac->getById($_GET['id']);      
        $h1 = "<h1>Zufriedenheitspunkt bearbeiten</h1>";
    }
    
    $kategorie = $zufriedenheitskatfac->getElement();
}

else 
{
    if ($_GET['item']=="kat") $h1="<h1>Neue Zufriedenheitsbogenkategorie anlegen</h1>";
    if ($_GET['item']=="point") $h1="<h1>Neuen Zufriedenheitsbogenpunkt anlegen</h1>";
}

echo $h1.'<br/>';
if ($_GET['item']=="kat")
{
?>
    <form action="<?php echo $l->makeFormLink(WEBDIR.'admin/zufriedenheitsbogen/update.php?item='.$_GET['item'].'&amp;mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post">
        <fieldset>
		    <legend>Zufriedenheitsbogenkategorie eingeben</legend>
            <label style="width:75px;">
                Name
            </label>
            <input type="text" name="name" value="<?php if ($_GET['mode']=="update") echo $kategorie->name;?>" />
            <br class="clr" /><br />
        </fieldset><br />
        <input type="submit" value="Daten speichern" class="submit" />
    </form>
<?php
}

if ($_GET['item']=="point")
{
?>      
<form action="<?php echo $l->makeFormLink(WEBDIR.'admin/zufriedenheitsbogen/update.php?item='.$_GET['item'].'&amp;mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post">
    <fieldset>
		<legend>Zufriedenheitspunkt eingeben</legend>
        <label style="width:75px;">
            Name
        </label>
        <input type="text" name="name" value="<?php if ($_GET['mode']=="update") echo $kategorie->name;?>" />
        <br class="clr" /><br />
        <select name="kat">
           <?php
               $zufriedenheitskatfac = new Zufriedenheitspunkt();
               $zufriedenheitskatfac->getKat();
               while ($zufriedenheitskat= $zufriedenheitskatfac->getElement())
               {
                  $selected='';
                  if ($_GET['mode']!="update" && $zufriedenheitskat->id == $_GET['kat']) $selected='selected="selected"';
                  if ($_GET['mode']=="update" && $zufriedenheitskat->id == $kategorie->kat) $selected='selected="selected"';
                  echo '<option value="'.$zufriedenheitskat->id.'" '.$selected.'>'.$zufriedenheitskat->name.'</option>'; 
               }
           ?>
        </select>
        <br class="clr" /><br />
    </fieldset><br />
    <input type="submit" value="Daten speichern" class="submit" />
</form> 
<?php
}

include(INCLUDEDIR."footer.inc.php");
?>

