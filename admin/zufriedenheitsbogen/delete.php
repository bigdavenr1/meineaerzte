<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// zufriedenheitsbogen/delete.php - l�schen von Datens�tzen aus zufriedenheit, zufriedenheitspunkten
//                                  und katzufriedenheit
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

if ($_POST["submit"])
{
    if ($_POST["mode"] == "delete_yes")
    {
        $zufriedenheitfac = new Zufriedenheitspunkt();
        if ($_GET['item']=="kat")$zufriedenheitfac->getKatById($_GET["id"]);
        if ($_GET['item']=="point")$zufriedenheitfac->getById($_GET["id"]);
        $zufriedenheitfac->deleteElement();
        header("Location: ./view.php?sv=1");
    }
}

//  Sicherheitsabfrage
//////////////////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");?>
    
    <?php
    $zufriedenheitfac = new Zufriedenheitspunkt();
    if ($_GET['item']=="kat") 
    { 
        $zufriedenheitfac->getKatById($_GET["id"]);
        echo '<h1>Zufriedenheitsbogen Kategorie l�schen</h1>';
    }
    if ($_GET['item']=="point") 
    {
        $zufriedenheitfac->getById($_GET["id"]);
        echo '<h1>Zufriedenheitspunkt l�schen</h1>';
    }
    $kategorie = $zufriedenheitfac->getElement();?>
    <br/>
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF'].'?item='.$_GET['item'].'&amp;id='.$_GET['id']);?>" method="post">
        <fieldset>
            <legend>Soll dieser Eintrag wirklich gel�scht werden?</legend>
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
            Name: <b><?php echo $kategorie->name;?></b><br/>
        </fieldset>
        <br />
        <input type="submit" value="L�schen" name="submit" class="submit" />
        <?php echo $l->makeLink("Zur�ck", "./view.php", "backlink");?>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php"); 
} ?>
