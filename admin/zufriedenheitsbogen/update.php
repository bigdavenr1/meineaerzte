<?php
////////////////////////////////////////////////////////////////////////////////
// zufriedenheitsbogen/update.php - Auswerten der Formulardaten aus edit.php
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
$zufriedenheitsfac = new Zufriedenheitspunkt();

if ($_GET['mode']=="new")
{
    $data[] = "";
    $data[] = $_POST['name'];  
    
    if ($_GET['item']=="kat") $zufriedenheitsfac->writeKat($data);
    
    if ($_GET['item']=="point")
    {
        $data[] = $_POST['kat'];                    // aktiv
        $data[] = 'A';                              // aktiv
        $zufriedenheitsfac->write($data);
    }
}     

if ($_GET['mode']=="update")
{
    if ($_GET['item']=="kat")
    {
        $zufriedenheitsfac->getKatById($_GET["id"]);
        $zufriedenheitsfac->update("name='".mysql_real_escape_string($_POST["name"])."'");
    }      
    
    if ($_GET['item']=="point")
    {
        $zufriedenheitsfac->getById($_GET["id"]);
        $zufriedenheitsfac->update("name='".mysql_real_escape_string($_POST["name"])."', kat='".$_POST['kat']."'");
    }       
} 
header("Location:view.php?sv=1");
?>
