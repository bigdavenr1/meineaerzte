<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php5 - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////


include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");


    $kommentarfac = new Bewertung();
    $kommentarfac->getById($_GET["id"]);
    $kommentar = $kommentarfac->getElement();



?>
<h1>Kommentar bearbeiten</h1>


<form action="<?php echo $l->makeFormLink(WEBDIR.'admin/kommentare/update.php?id='.$_GET['id']);?>" method="post">
    <fieldset>
		<legend>Kommentar bearbeiten</legend>
    <label style="width:1px;">
        Kommentar
    </label>
    <textarea cols="20" rows="20" name="kommentar"><?php echo $kommentar->kommentar;?></textarea>
    <br class="clr" /><br />
    <label>
        Status
    </label>
    <select name="aktiv">
        <option value="0" <?php if ($kommentar->aktiv==0) echo 'selected="selected"'  ?>>NEU</otpion>
        <option value="1" <?php if ($kommentar->aktiv==1) echo 'selected="selected"'  ?>>Freischalten</option> 
    </select>
    </fieldset><br />
    <input type="submit" value="Daten speichern" class="submit" />
</form>
<?php
include(INCLUDEDIR."footer.inc.php");
?>

