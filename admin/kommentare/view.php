<?php
////////////////////////////////////////////////////////////////////////////////
// kommentare/view.php - Zeigt Bewertungen mit Kommentaren an
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<h1>Kommentare freischalten</h1>
<br/>

<?php
    $kommentarfac = new Bewertung(25);
    // Sortierung
    $mode=strtoupper($_GET['mode']);
    $sortierfelder=array("1"=>'bewertungen.kommentar',
                         "2"=>'benutzer.email',
                         "3"=>'unternehmen.name');

    if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
    {
        $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
    }
    else $sort = '';
     
    // Kommentare mit aktiv=0 hohlen
    //$kommentarfac->getNewComment();
    $kommentarfac->getNewComment($sort);
?>
<br class="clr" /><br />
<table style="width:100%">
    <tr>
        <th >
        <?php // Kommentar
            if($_GET['mode']== "desc" && $_GET['sp']==1) echo $l->makeLink('Kommentar '.$icon_sort_asc,WEBDIR."admin/kommentare/view.php?mode=asc&amp;sp=1","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==1) echo $l->makeLink('Kommentar '.$icon_sort_desc,WEBDIR."admin/kommentare/view.php?mode=desc&amp;sp=1","none");
            else echo $l->makeLink('Kommentar ',WEBDIR."admin/kommentare/view.php?mode=asc&amp;sp=1","none");             
        ?> 
        </th>
        <th>
       <?php // Autor
            if($_GET['mode']== "desc" && $_GET['sp']==2) echo $l->makeLink('Autor '.$icon_sort_asc,WEBDIR."admin/kommentare/view.php?mode=asc&amp;sp=2","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==2) echo $l->makeLink('Autor '.$icon_sort_desc,WEBDIR."admin/kommentare/view.php?mode=desc&amp;sp=2","none");
            else echo $l->makeLink('Autor ',WEBDIR."admin/kommentare/view.php?mode=asc&amp;sp=2","none");             
        ?> 
        </th>
        <th>
       <?php // f�r Eintrag
            if($_GET['mode']== "desc" && $_GET['sp']==3) echo $l->makeLink('f�r Eintrag '.$icon_sort_asc,WEBDIR."admin/kommentare/view.php?mode=asc&amp;sp=3","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==3) echo $l->makeLink('f�r Eintrag '.$icon_sort_desc,WEBDIR."admin/kommentare/view.php?mode=desc&amp;sp=3","none");
            else echo $l->makeLink('f�r Eintrag ',WEBDIR."admin/kommentare/view.php?mode=asc&amp;sp=3","none");             
        ?>             
        </th>
        <th style="width:65px;">
            Status
        </th>
        <th style="width:50px;">
            Admin
        </th>
    </tr>
    
    
<?php 
$t=0;
while ($kommentar = $kommentarfac->getElement())
          { 
          $t++;
      ?>

   <tr>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php echo substr($kommentar->kommentar,0,200);?>...
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <a href="mailto:<?php echo $kommentar->username;?>"><?php echo $kommentar->username;?></a>
           <?php echo $l->makeLink('(ID:'.$kommentar->userid.')',WEBDIR."admin/benutzer/details.php?&amp;id=".$kommentar->userid);?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php  echo $l->makeLink($kommentar->name.', '.$kommentar->vorname,WEBDIR."admin/unternehmen/edit.php?mode=update&amp;id=".$kommentar->unternehmenid)?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/kommentare/update.php?mode=changestatus&amp;id=".$kommentar->id);?>
          NEU
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?> style="text-align:center">
       <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/kommentare/edit.php?id=".$kommentar->id);?>
           <?php echo $l->makeLink($icon_delete_small,WEBDIR."admin/kommentare/delete.php?id=".$kommentar->id);?>
       </td>
   </tr>

<?php } ?>
</table>
<br /><br />

<?php if ($t==0) echo "Keine neuen Kommentare vorhanden";
else {

?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten: 
        <?php 
            if($_GET['sp'])
            {
                echo $kommentarfac->getHtmlNavi("std", "&amp;sp=".$_GET['sp']."&amp;mode=".$_GET['mode']);
            }
            else echo $kommentarfac->getHtmlNavi("std","" );
        ?>
    </div>
<?php 
}

include(INCLUDEDIR."footer.inc.php");
?>

