<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php5 - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

$kommentarfac = new Bewertung();
$kommentarfac->getById($_GET["id"]);
$kommentar = $kommentarfac->getElement();

?>
<h1>Antwort zu einem Kommentar bearbeiten</h1>


<form action="<?php echo $l->makeFormLink(WEBDIR.'admin/antworten/update.php?id='.$_GET['id']);?>" method="post">
    <fieldset>
		<legend>Kommentar bearbeiten</legend>
        <br/>
    <div class="headline">
        Kommentar
    </div>
    <div class="td1"><?php echo nl2br($kommentar->kommentar); ?></div><br/>
    <label>Antwort</label>
    
    
    <textarea cols="20" rows="20" name="kommentar"><?php echo $kommentar->antwort;?></textarea>
    <br class="clr" /><br />
    <label>
        Status
    </label>
    <select name="aktiv">
        <option value="0" <?php if ($kommentar->aktiv==2) echo 'selected="selected"'  ?>>NEU</otpion>
        <option value="1" <?php if ($kommentar->aktiv==1) echo 'selected="selected"'  ?>>Freischalten</option> 
    </select>
    </fieldset><br />
    <input type="submit" value="Daten speichern" class="submit" />
</form>
<?php
include(INCLUDEDIR."footer.inc.php");
?>

