<?php
////////////////////////////////////////////////////////////////////////////////
// anmin/antworten/view.php - Zeigt Bewertungen mit Antworten
////////////////////////////////////////////////////////////////////////////////


include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<h1>Antworten zu Kommentaren freischalten</h1>
<br/>

<?php
    $kommentarfac = new Bewertung(25);
    // Sortierung auswerten
    $mode=strtoupper($_GET['mode']);
    $sortierfelder=array("1"=>'bewertungen.antwort',
                         "2"=>'unternehmen.famname');

    if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
    {
        $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
    }
    else $sort = '';
    // Alle Kommentare mit Typ 2
    //$kommentarfac->getNewComment(2);
    $kommentarfac->getNewCommentSort(1,$sort);
?>
<br class="clr" /><br />
<table style="width:100%">
    <tr>
        <th >
        <?php // Antwort
            if($_GET['mode']== "desc" && $_GET['sp']==1) echo $l->makeLink('Antwort '.$icon_sort_asc,WEBDIR."admin/antworten/view.php?mode=asc&amp;sp=1","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==1) echo $l->makeLink('Antwort '.$icon_sort_desc,WEBDIR."admin/antworten/view.php?mode=desc&amp;sp=1","none");
            else echo $l->makeLink('Antwort ',WEBDIR."admin/antworten/view.php?mode=asc&amp;sp=1","none");             
        ?>
        </th>
        <th>
        <?php // Unternehmen
            if($_GET['mode']== "desc" && $_GET['sp']==2) echo $l->makeLink('Unternehmen '.$icon_sort_asc,WEBDIR."admin/antworten/view.php?mode=asc&amp;sp=2","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==2) echo $l->makeLink('Unternehmen '.$icon_sort_desc,WEBDIR."admin/antworten/view.php?mode=desc&amp;sp=2","none");
            else echo $l->makeLink('Unternehmen ',WEBDIR."admin/antworten/view.php?mode=asc&amp;sp=2","none");             
        ?>
        </th>
        <th style="width:65px;">
            Status
        </th>
        <th style="width:50px;">
            Admin
        </th>
    </tr>
    
    
<?php 
$t=0;
while ($kommentar = $kommentarfac->getElement())
{ 
    $t++;
   ?>

   <tr>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php echo substr($kommentar->antwort,0,200);?>...
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php  echo $l->makeLink($kommentar->name.', '.$kommentar->vorname,WEBDIR."admin/unternehmen/edit.php?mode=update&amp;id=".$kommentar->unternehmenid)?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/antworten/update.php?mode=changestatus&amp;id=".$kommentar->id);?>
          NEU
           
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?> style="text-align:center">
       <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/antworten/edit.php?id=".$kommentar->id);?>
           <?php echo $l->makeLink($icon_delete_small,WEBDIR."admin/antworten/delete.php?id=".$kommentar->id);?>
       </td>
   </tr>

<?php } ?>
</table>
<br /><br />

<?php if ($t==0) echo "Keine neuen Antworten vorhanden";
else {

?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten: 
        <?php 
            if($_GET['sp'])
            {
                echo $kommentarfac->getHtmlNavi("std", "&amp;sp=".$_GET['sp']."&amp;mode=".$_GET['mode']);
            }
            else echo $kommentarfac->getHtmlNavi("std","" );
        ?>
    </div>
<?php 
}

include(INCLUDEDIR."footer.inc.php");
?>

