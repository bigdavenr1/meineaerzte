<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// suchwort.view.php - Zeigt die Tabelle suchworte an
//////////////////////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<h1>Meistgenutzten Suchworte verwalten bzw. als Schlagwort �bernehmen</h1>
<table>
<tr>
    <th>
        <?php  // Suchwort
            if($_GET['mode']== "desc" && $_GET['sp']==1) echo $l->makeLink('Suchwort '.$icon_sort_asc,WEBDIR."admin/schlagworte/suchwort.view.php?mode=asc&amp;sp=1","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==1) echo $l->makeLink('Suchwort '.$icon_sort_desc,WEBDIR."admin/schlagworte/suchwort.view.php?mode=desc&amp;sp=1","none");
            else echo $l->makeLink('Suchwort ',WEBDIR."admin/schlagworte/suchwort.view.php?mode=asc&amp;sp=1","none");             
        ?>
    </th>
    <th>
        <?php  // Klicks
          if($_GET['mode']== "desc" && $_GET['sp']==2) echo $l->makeLink('Klicks '.$icon_sort_asc,WEBDIR."admin/schlagworte/suchwort.view.php?mode=asc&amp;sp=2","none");
          elseif($_GET['mode']== "asc" && $_GET['sp']==2) echo $l->makeLink('Klicks '.$icon_sort_desc,WEBDIR."admin/schlagworte/suchwort.view.php?mode=desc&amp;sp=2","none");
          else echo $l->makeLink('Klicks ',WEBDIR."admin/schlagworte/suchwort.view.php?mode=asc&amp;sp=2","none");             
        ?>
    </th>
    <th>Links</th></tr>
<?php
   $suchwortefac = new Suchworte(20);
   
   // Sortierung
    $mode=strtoupper($_GET['mode']);
    $sortierfelder=array("1"=>'suchwort',
                         "2"=>'klicks');
    if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
    {
        $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
    }
    else $sort = " ORDER by klicks DESC";
    
   $suchwortefac->getAll(" WHERE klicks > '4' ".$sort); // Bedingung f�r richtige Anzeige im Navigator
   $count = $suchwortefac->getElementCount();
   $schlagwortefac = new Schlagworte(); // F�r Adminlinkanzeige
    
   if($count > 0)
   {
   ?>
        <?php 
        $x=0;
        while ($suchwort=$suchwortefac->getElement())
        {
            //if ($suchwort->klicks >= 5)
            //{
        $x++;
        ?>
        <tr><td <?php if ($x%2) echo 'class="td1"'; ?>><?php echo $suchwort->suchwort; ?></td>
            <td <?php if ($x%2) echo 'class="td1"'; ?>><?php echo $suchwort->klicks; ?></td> 
            <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center" >
               <?php 
                   $schlagwortefac->getSchlagworteByName($suchwort->suchwort);
                   $schlagwort = $schlagwortefac->getElement();
                   if(!$schlagwort)
                   {
                        echo $l->makeLink($icon_arrow_right_small,WEBDIR."admin/schlagworte/update.php?mode=take&amp;id=".$suchwort->id,"none")." ".$l->makeLink($icon_delete_small,WEBDIR."admin/schlagworte/suchwort.delete.php?id=".$suchwort->id,"none");
                   }
                   else echo"Schon Schlagwort!";
               ?>
            </td></tr>
        <?php
        //}
        }
        ?>
        
   <?php
   }        
?>   
</table>

<?php 
if ($x==0)
{
   echo "Keine Schlagworte eingetragen";
}
else 
{?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten: 
        <?php 
            if($_GET['sp'])
            {
                echo $suchwortefac->getHtmlNavi("std", "&amp;sp=".$_GET['sp']."&amp;mode=".$_GET['mode']);
            }
            else echo $suchwortefac->getHtmlNavi("std","" );
        ?>
    </div>
<?php 
}   
    
include(INCLUDEDIR."footer.inc.php");
?>