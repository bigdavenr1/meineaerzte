<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// delete.php - L�schen von datens�tzen aus der Schlagworttabelle
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

if ($_POST["submit"])
{
    if ($_POST["mode"] == "delete_yes")
    {
        $schlagwortfac = new Schlagworte();
        $schlagwortfac->getById($_POST["id"]);
        $schlagwortfac->deleteElement();
        header("Location: ./view.php?sv=1");
    }
}

//  Sicherheitsabfrage
//////////////////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");?>
    <h1>Schlagwort l�schen</h1>
    <?php
    $schlagwortfac = new Schlagworte();
    $schlagwortfac->getById($_GET["id"]);
    $schlagwort = $schlagwortfac->getElement();?>
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
        <fieldset>
            <legend>Soll dieses Schlagwort wirklich gel�scht werden?</legend>
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
            Schlagwort: <b><?php echo $schlagwort->schlagwort;?></b><br/><br />
        </fieldset>
        <br />
        <input type="submit" value="L�schen" name="submit" class="submit" />
        <?php echo $l->makeLink("Zur�ck", "./view.php", "backlink");?>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php"); 
} ?>