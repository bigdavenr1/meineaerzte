<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// update.php - verarbeiten der Formulardaten f�r Schlagwortbearbeitung aus edit.php
//////////////////////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");

$schlagwortfac = new Schlagworte();

// neuer Datensatz
if ($_GET['mode']=="new")
{
    $data[] = "";
    $data[] = $_POST['name'];                   
    $schlagwortfac->write($data);
}

// neuer Datensatz mit Daten aus Tabelle suchworte
if ($_GET['mode'] == "take")
{
    $suchwortfac = new Suchworte();
    $suchwortfac->getById($_GET["id"]);
    $suchwort = $suchwortfac->getElement();
    
    if($suchwort)
    {
        $data[] = "";
        $data[] = $suchwort->suchwort;                   
        $schlagwortfac->write($data);
    }
}

// Daten updaten
if ($_GET['mode']=="update")
{
    $schlagwortfac->getById($_GET["id"]);
    $schlagwortfac->update("schlagwort='".mysql_real_escape_string($_POST["name"])."'");           
} 

if($_GET['mode'] == "take")header("Location:suchwort.view.php?sv=1");
else header("Location:view.php?sv=1");
?>

