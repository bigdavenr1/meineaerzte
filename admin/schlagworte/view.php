<?php
////////////////////////////////////////////////////////////////////////////////
// schlagworte/view.php - Zeigt die Tabelle schlagworte und suchworte an
////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<h1>Schlagworte verwalten</h1>
<?php echo '<b>'.$l->makeLink($icon_neu_small." [ neues Schlagwort anlegen ]",WEBDIR."admin/schlagworte/edit.php?mode=new&amp;id=0","none").'</b>';?>

<table>
<tr><th>
    <?php  // Schlagwort
        if($_GET['mode']== "desc") echo $l->makeLink('Schlagwort '.$icon_sort_asc,WEBDIR."admin/schlagworte/view.php?mode=asc","none");
        elseif($_GET['mode']== "asc") echo $l->makeLink('Schlagwort '.$icon_sort_desc,WEBDIR."admin/schlagworte/view.php?mode=desc","none");
        else echo $l->makeLink('Schlagwort ',WEBDIR."admin/schlagworte/view.php?mode=asc","none");             
    ?>
    </th><th>Links</th></tr>
<?php
   $schlagwortefac=new Schlagworte(20);
   if($_GET['mode']== 'asc')
    {
     $sort = " ORDER by schlagwort DESC";
    }
    elseif($_GET['mode']== 'desc')
    {
     $sort = " ORDER by schlagwort ASC";
    }
    else $sort = " ORDER by schlagwort ASC";
   
   //$schlagwortefac->getAll();
   $schlagwortefac->getAll($sort);
   
   $count = $schlagwortefac->getElementCount();
   if($count > 0)
   {
        $x=0;
        while ($schlagwort=$schlagwortefac->getElement())
        {
        $x++;
        ?>
            <tr><td <?php if ($x%2) echo 'class="td1"'; ?>>
                <?php echo $schlagwort->schlagwort; ?></td>
            <td <?php if ($x%2) echo 'class="td1"'; ?>>
               <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/schlagworte/edit.php?mode=update&amp;id=".$schlagwort->id,"none")." ".$l->makeLink($icon_delete_small,WEBDIR."admin/schlagworte/delete.php?id=".$schlagwort->id,"none");?>
            </td></tr>
        <?php
        }
    }        
?>
</table>


<?php 
 echo $l->makeLink($icon_neu_small." [ neues Schlagwort anlegen ]",WEBDIR."admin/schlagworte/edit.php?mode=new&amp;id=0").'<br /><br />';
 echo $l->makeLink("zu den Meistgesuchten Suchwörtern",WEBDIR."admin/schlagworte/suchwort.view.php").'<br /><br />';
 

if ($x==0)
{
   echo "Keine Schlagworte eingetragen";
}
else 
{?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten: 
        <?php 
            if($_GET['mode'])
            {
                echo $schlagwortefac->getHtmlNavi("std", "&amp;mode=".$_GET['mode']);
            }
            else echo $schlagwortefac->getHtmlNavi("std","" );
        ?>
    </div>
<?php 
}   
include(INCLUDEDIR."footer.inc.php");
?>