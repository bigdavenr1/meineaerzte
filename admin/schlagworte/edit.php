<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// edit.php - Zeigt Formular f�r Schlagwortbearbeitung
//////////////////////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

if ($_GET['mode']=="update")
{
    $schlagwortfac = new Schlagworte();
    $schlagwortfac->getById($_GET["id"]);
    $schlagwort = $schlagwortfac->getElement();
    $vorgabe = $schlagwort->schlagwort;
    $h1 = "<h1>Schlagwort bearbeiten</h1>";
}
if ($_GET['mode']=="new")
{
    $h1 = "<h1>neues Schlagwort anlegen</h1>";
}

// jetz �bernahme ohne Bearbeiten �ber update.php
/*
if($_GET['mode']=="take")
{
    $suchwortfac = new Suchworte();
    $suchwortfac->getById($_GET["id"]);
    $suchwort = $suchwortfac->getElement();
    $vorgabe = $suchwort->suchwort;
    $h1 = "<h1>Suchwort als Schlagwort anlegen</h1>";
}*/

echo $h1;
?>

<form action="<?php echo $l->makeFormLink(WEBDIR.'admin/schlagworte/update.php?mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post">
    <fieldset>
		<legend>Schlagwort eingeben</legend>
    <label style="width:75px;">
        Schlagwort
    </label>
    <input type="text" name="name" value="<?php if (isset($vorgabe)) echo $vorgabe;?>" />
    <br class="clr" /><br />
        </fieldset><br />
    <?php echo $l->makeLink("Zur�ck", WEBDIR."admin/schlagworte/view.php", "backlink");?>
    <input type="submit" value="Daten speichern" class="submit" />
</form>
<?php
include(INCLUDEDIR."footer.inc.php");
?>

