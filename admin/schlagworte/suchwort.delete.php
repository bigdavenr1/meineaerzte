<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// suchwort.delete.php - L�schen von datens�tzen aus der Suchworttabelle
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

if ($_POST["submit"])
{
    if ($_POST["mode"] == "delete_yes")
    {
        $suchwortfac = new Suchworte();
        $suchwortfac->getById($_POST["id"]);
        $suchwortfac->deleteElement();
        header("Location: ./suchwort.view.php?sv=1");
    }
}

//  Sicherheitsabfrage
//////////////////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");?>
    <h1>Suchwort l�schen</h1>
    <?php
    $suchwortfac = new Suchworte();
    $suchwortfac->getById($_GET["id"]);
    $suchwort = $suchwortfac->getElement();?>
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
        <fieldset>
            <legend>Soll dieses Suchwort wirklich gel�scht werden?</legend>
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
            Suchwort: <b><?php echo $suchwort->suchwort;?></b><br/><br />
        </fieldset>
        <br />
        <input type="submit" value="L�schen" name="submit" class="submit" />
        <?php echo $l->makeLink("Zur�ck", "./suchwort.view.php", "backlink");?>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php"); 
} ?>