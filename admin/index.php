<?php
////////////////////////////////////////////////////////////////////////////////
// admin/index.php -> admin - Index-Seite
////////////////////////////////////////////////////////////////////////////////


include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

  <h1>Willkommen im Admin-Bereich</h1>  
  
  Sie können hier:
  <ul>
      <lI>
          <?php echo $l->makeLink("Fachgebiete verwalten",WEBDIR."admin/fachgebiete/view.php");?>
      </li>
      <lI>
          <?php echo $l->makeLink("Unternehmen verwalten",WEBDIR."admin/unternehmen/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Unternehmensvorschläge verwalten",WEBDIR."admin/unternehmen/view.php?mode=new");?>
      </li>
      <li>
          <?php echo $l->makeLink("Premiumanmeldungen verwalten",WEBDIR."admin/unternehmen/view.php?mode=premium");?>
      </li>
      <li>
          <?php echo $l->makeLink("Bewertungsbogen verwalten",WEBDIR."admin/bewertungsbogen/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Zufriedenheitsbogen verwalten",WEBDIR."admin/zufriedenheitsbogen/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Benutzer verwalten",WEBDIR."admin/benutzer/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Benutzerrechte verwalten",WEBDIR."admin/rechte/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Forum verwalten",WEBDIR."bewertungen/forum.view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Bewertungen verwalten",WEBDIR."admin/bewertungen/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Bewertungskommentare verwalten",WEBDIR."admin/kommentare/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Bewertungsantworten verwalten",WEBDIR."admin/antworten/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Klärungsanfragen einsehen",WEBDIR."admin/klaerung/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Erstbewertungen",WEBDIR."admin/erstbewertung/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Schlagworte verwalten",WEBDIR."admin/schlagworte/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Meistgesuchten Begriffe",WEBDIR."admin/schlagworte/suchwort.view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Newsletter",WEBDIR."admin/newsletter/view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Werbekunden",WEBDIR."admin/werbung/kunden.view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Werbe Anzeigen",WEBDIR."admin/werbung/anzeige.view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Werbe Banner",WEBDIR."admin/werbung/banner.view.php");?>
      </li>
      <li>
          <?php echo $l->makeLink("Feste Werbeplätze",WEBDIR."admin/werbung/festeplaetze.view.php");?>
      </li>
  </ul>
  
Viel Spaß
   

<?include(INCLUDEDIR."footer.inc.php");
?>