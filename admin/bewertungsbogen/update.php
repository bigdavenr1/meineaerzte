<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php5 - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
$bewertungsfac = new Bewertungspunkt();

if ($_GET['mode']=="new")
{
    $data[] = "";
    $data[] = $_POST['name'];  
    
    if ($_GET['item']=="kat") $bewertungsfac->writeKat($data);
    
    if ($_GET['item']=="point")
    {
        $data[] = $_POST['kat'];                    // aktiv
        $data[] = 'A';                              // aktiv
        $bewertungsfac->write($data);
    }
}     

if ($_GET['mode']=="update")
{
    if ($_GET['item']=="kat")
    {
        $bewertungsfac->getKatById($_GET["id"]);
        $bewertungsfac->update("name='".mysql_real_escape_string($_POST["name"])."'");
    }      
    
    if ($_GET['item']=="point")
    {
        $bewertungsfac->getById($_GET["id"]);
        $bewertungsfac->update("name='".mysql_real_escape_string($_POST["name"])."', kat='".$_POST['kat']."'");
    }       
} 
header("Location:view.php?sv=1");
?>
