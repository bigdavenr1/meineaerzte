<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php5 - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

if ($_GET['mode']=="update")
{  
    $bewertungskatfac = new Bewertungspunkt();
    
    if ($_GET['item']=="kat")
    {
        $bewertungskatfac->getKatById($_GET['id']);
        $h1 = "<h1>Bewertungbogenkategorie bearbeiten</h1>";
    }
    
    if ($_GET['item']=="point")
    {
        $bewertungskatfac->getById($_GET['id']);      
        $h1 = "<h1>Bewertungspunkt bearbeiten</h1>";
    }
    
    $kategorie = $bewertungskatfac->getElement();
}

else 
{
    if ($_GET['item']=="kat") $h1="<h1>Neue Bewertungsbogenkategorie anlegen</h1>";
    if ($_GET['item']=="point") $h1="<h1>Neuen Bewertungsbogenpunkt anlegen</h1>";
}

echo $h1.'<br/>';
if ($_GET['item']=="kat")
{
?>
    <form action="<?php echo $l->makeFormLink(WEBDIR.'admin/bewertungsbogen/update.php?item='.$_GET['item'].'&amp;mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post">
        <fieldset>
		    <legend>Bewertungsbogenkategorie eingeben</legend>
            <label style="width:75px;">
                Name
            </label>
            <input type="text" name="name" value="<?php if ($_GET['mode']=="update") echo $kategorie->name;?>" />
            <br class="clr" /><br />
        </fieldset><br />
        <input type="submit" value="Daten speichern" class="submit" />
    </form>
<?php
}

if ($_GET['item']=="point")
{
?>      
<form action="<?php echo $l->makeFormLink(WEBDIR.'admin/bewertungsbogen/update.php?item='.$_GET['item'].'&amp;mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post">
    <fieldset>
		<legend>Bewertungspunkt eingeben</legend>
        <label style="width:75px;">
            Name
        </label>
        <input type="text" name="name" value="<?php if ($_GET['mode']=="update") echo $kategorie->name;?>" />
        <br class="clr" /><br />
        <select name="kat">
           <?php
               $bewertungskatfac = new Bewertungspunkt();
               $bewertungskatfac->getKat();
               while ($bewertungskat= $bewertungskatfac->getElement())
               {
                  $selected='';
                  if ($_GET['mode']!="update" && $bewertungskat->id == $_GET['kat']) $selected='selected="selected"';
                  if ($_GET['mode']=="update" && $bewertungskat->id == $kategorie->kat) $selected='selected="selected"';
                  echo '<option value="'.$bewertungskat->id.'" '.$selected.'>'.$bewertungskat->name.'</option>'; 
               }
           ?>
        </select>
        <br class="clr" /><br />
    </fieldset><br />
    <input type="submit" value="Daten speichern" class="submit" />
</form> 
<?php
}

include(INCLUDEDIR."footer.inc.php");
?>

