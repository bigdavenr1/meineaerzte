<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// bewertungsbogen.view.php - Zeigt die Tabelle bewertungspunkte an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// Object initialisieren
$datenfac = new Bewertungspunkt();
?>
<h1>Bewertungsbogen verwalten</h1><br/>
<?php echo '<b>'.$l->makeLink($icon_neu_small." [ neue Bewertungskategorie anlegen ]",WEBDIR."admin/bewertungsbogen/edit.php?mode=new&amp;item=kat&amp;stufe=1","none").'</b>';?>
<br /><br/>
<?php
$k=1;
$x=0;
//$datenfac->createOwnQuery("SELECT * , katbewertung.name AS katname FROM  katbewertung,bewertungspunkte WHERE bewertungspunkte.kat = katbewertung.id ORDER BY katbewertung.id" );
$datenfac->getBewertungWithKat();
$katname=array();
while($bewertung= $datenfac->getElement())
{
    $tabellenende="";
    $k++;

    if (!in_array($bewertung->kat,$katname))
    { 
        $x++;
        if ($x!=1) echo '<tr><td colspan="2">'.$l->makeLink($icon_neu_small." [ neuen Bewertungspunkt anlegen ]",WEBDIR."admin/bewertungsbogen/edit.php?mode=new&amp;item=point&amp;kat=".$lastkat."&amp;stufe=1","none").'</td></tr></table><br/>';
        echo '<h2>'.$bewertung->katname.' '.$l->makeLink($icon_edit_small,WEBDIR."admin/bewertungsbogen/edit.php?mode=update&amp;item=kat&amp;id=".$bewertung->kat,"none").' '.$l->makeLink($icon_delete_small,WEBDIR."admin/bewertungsbogen/delete.php?item=kat&amp;id=".$bewertung->kat,"none").'</h2>'.CHR(10);
        $katname[]=$bewertung->kat;
        $k=1;            
        ?>
        <table>
            <tr>
                <th >
                    Name
                </th>
                <th style="width:40px;">
                </th>
            </tr>
    <?php
        $lastkat=$bewertung->kat;        
    }

    if ($k%2) $class='class="td1"';
    else $class='class="td"';
    echo '<tr><td '.$class.'>'.$bewertung->name.'</td><td>'.$l->makeLink($icon_edit_small,WEBDIR."admin/bewertungsbogen/edit.php?mode=update&amp;item=point&amp;id=".$bewertung->id,"none").' '.$l->makeLink($icon_delete_small,WEBDIR."admin/bewertungsbogen/delete.php?item=point&amp;id=".$bewertung->id,"none").'</td></tr>';
}
?>
    <tr>
        <td colspan="2">
            <?php echo $l->makeLink($icon_neu_small." [ neuen Bewertungspunkt anlegen ]",WEBDIR."admin/bewertungsbogen/edit.php?mode=new&amp;item=point&amp;kat=".$lastkat,"none")?>
        </td>
    </tr>
</table>
<br/>
<hr/>

<h1>Kategorien ohne Bewertungspunkte</h1>
<?php 
$kategoriefac=new Bewertungspunkt();
$kategoriefac->getKat();
$d=0;
while($kategorie=$kategoriefac->getElement())
{
    if (!in_array($kategorie->id,$katname))
    {
        $d++;
        echo '<h2>'.$kategorie->name.' '.$l->makeLink($icon_edit_small,WEBDIR."admin/bewertungsbogen/edit.php?mode=update&amp;item=kat&amp;id=".$kategorie->id,"none").' '.$l->makeLink($icon_delete_small,WEBDIR."admin/bewertungsbogen/delete.php?item=kat&amp;id=".$kategorie->id,"none").'</h2>'.CHR(10);
    }
}

if ($d==0) echo "keine Kategorie ohne Bewertungspunkte gefunden";
?>
<br/><br/>
<hr/>

<h1>Bewertungspunkte ohne g�ltige Kategorie</h1>
<table>
            <tr>
                <th >
                    Name
                </th>
                <th style="width:40px;">
                </th>
            </tr>
<?php 
$h=0;
$bewertungspunktfac=new Bewertungspunkt();
$bewertungspunktfac->getAll();
while($bewertungspunkt=$bewertungspunktfac->getElement())
{
    if (!in_array($bewertungspunkt->kat,$katname))
    {
        $h++;
        echo '<tr><td>'.$bewertungspunkt->name.'</td><td> '.$l->makeLink($icon_edit_small,WEBDIR."admin/bewertungsbogen/edit.php?mode=update&amp;item=point&amp;id=".$bewertungspunkt->id,"none").' '.$l->makeLink($icon_delete_small,WEBDIR."admin/bewertungsbogen/delete.php?item=point&amp;id=".$bewertungspunkt->id,"none").'</td></tr>'.CHR(10);
    }
}

if ($h==0) echo '<tr><td colspan="2">Keine Bewertungspunkte ohne g�ltige Kategoriezuordnung gefunden</td></tr>'

?>

</table><br />
<?php 

include(INCLUDEDIR."footer.inc.php");
?>

