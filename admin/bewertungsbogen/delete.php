<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php5 - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

if ($_POST["submit"])
{
    if ($_POST["mode"] == "delete_yes")
    {
        $kategoriefac = new Bewertungspunkt();
        if ($_GET['item']=="kat")$kategoriefac->getKatById($_GET["id"]);
        if ($_GET['item']=="point")$kategoriefac->getById($_GET["id"]);
        $kategoriefac->deleteElement();
        header("Location: ./view.php?sv=1");
    }
}

//  Sicherheitsabfrage
//////////////////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");?>
    
    <?php
    $kategoriefac = new Bewertungspunkt();
    if ($_GET['item']=="kat") 
    { 
        $kategoriefac->getKatById($_GET["id"]);
        echo '<h1>Bewertungsbogen Kategorie l�schen</h1>';
    }
    if ($_GET['item']=="point") 
    {
        $kategoriefac->getById($_GET["id"]);
        echo '<h1>Bewertungspunkt l�schen</h1>';
    }
    $kategorie = $kategoriefac->getElement();?>
    <br/>
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF'].'?item='.$_GET['item'].'&amp;id='.$_GET['id']);?>" method="post">
        <fieldset>
            <legend>Soll dieser Eintrag wirklich gel�scht werden?</legend>
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
            Name: <b><?php echo $kategorie->name;?></b><br/>
        </fieldset>
        <br />
        <input type="submit" value="L�schen" name="submit" class="submit" />
        <?php echo $l->makeLink("Zur�ck", "./view.php", "backlink");?>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php"); 
} ?>
