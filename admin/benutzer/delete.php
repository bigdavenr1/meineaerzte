<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// benutzer/delete.php - l�scht Datensatz aus Tabelle benutzer
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

if ($_POST["submit"])
{
    // Benutzer l�schen
    //////////////////////////////////////////////////////////////////////////////////////////////

    if ($_POST["mode"] == "delete_yes")
    {
        $benutzerfac = new Benutzer();
        $benutzerfac->getById($_POST["id"]);
        $benutzerfac->deleteElement();
        header("Location: ./view.php?sv=1");
    }
}

//  Sicherheitsabfrage
//////////////////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");
    ?>
    <h1>Benutzer l�schen</h1>

    <?php
    $benutzerfac = new Benutzer();
    $benutzerfac->getById($_GET["id"]);
    $benutzer = $benutzerfac->getElement();
    
    echo '<br />Benutzer:<b> "'.$benutzer->name.'"</b><br /><br />';
    ?>
    <div class="contentbox">
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
        <input type="hidden" name="mode" value="delete_yes" />
        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
        Soll dieser Benutzer wirklich gel�scht werden?
        
        <br /><br />
        <input type="submit" value="L�schen" name="submit" class="login" /><br class="clr" /><br/>
        <?php echo $l->makeLink("Zur�ck", "./view.php", "login");?>
     </form>
     </div>
<?php
include(INCLUDEDIR."footer.inc.php"); }
?>

