<?php
////////////////////////////////////////////////////////////////////////////////
// benutzer/view.php - Zeigt die Tabelle benutzer an
////////////////////////////////////////////////////////////////////////////////


include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<h1>Benutzer verwalten / sperren / l�schen</h1>
<br/>
<form method="get" action="<?php echo $l->makeFormLink(WEBDIR.'admin/benutzer/details.php');?>">
    <fieldset>
         <legend>
             User �ber User-ID aufrufen
         </legend>
         <label>User-ID</label><input type="text" name="id"/><br class="clr"/>
         <br/>
         <input type="submit" value="User aufrufen" class="submit"/>
    </fieldset>
    
</form>


<?php

    $benutzerfac = new Benutzer(15);
    // Sortierung
    $mode=strtoupper($_GET['mode']);
    $sortierfelder=array("1"=>'name',
                         "2"=>'plz',
                         "3"=>'ort',
                         "4"=>'email',
                         "5"=>'aktiv');

    if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
    {
        $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
    }
    else $sort = " ORDER by name ASC";
    
    $benutzerfac->getBenutzer($sort);
    
?>

<br class="clr" /><br />
<table style="width:100%">
    <tr>
        <th >
        <?php  // Name
            if($_GET['mode']== "desc" && $_GET['sp']==1) echo $l->makeLink('Name '.$icon_sort_asc,WEBDIR."admin/benutzer/view.php?mode=asc&amp;sp=1","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==1) echo $l->makeLink('Name '.$icon_sort_desc,WEBDIR."admin/benutzer/view.php?mode=desc&amp;sp=1","none");
            else echo $l->makeLink('Name ',WEBDIR."admin/benutzer/view.php?mode=asc&amp;sp=1","none");             
        ?>
        </th>
        <th>
        <?php  // PLZ
            if($_GET['mode']== "desc" && $_GET['sp']==2) echo $l->makeLink('PLZ '.$icon_sort_asc,WEBDIR."admin/benutzer/view.php?mode=asc&amp;sp=2","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==2) echo $l->makeLink('PLZ '.$icon_sort_desc,WEBDIR."admin/benutzer/view.php?mode=desc&amp;sp=2","none");
            else echo $l->makeLink('PLZ ',WEBDIR."admin/benutzer/view.php?mode=asc&amp;sp=2","none");             
        ?>
        </th>
        <th>
        <?php  // Ort
            if($_GET['mode']== "desc" && $_GET['sp']==3) echo $l->makeLink('Ort '.$icon_sort_asc,WEBDIR."admin/benutzer/view.php?mode=asc&amp;sp=3","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==3) echo $l->makeLink('Ort '.$icon_sort_desc,WEBDIR."admin/benutzer/view.php?mode=desc&amp;sp=3","none");
            else echo $l->makeLink('Ort ',WEBDIR."admin/benutzer/view.php?mode=asc&amp;sp=3","none");             
        ?>
        </th>
        <th >
        <?php  // Mail
            if($_GET['mode']== "desc" && $_GET['sp']==4) echo $l->makeLink('eMail '.$icon_sort_asc,WEBDIR."admin/benutzer/view.php?mode=asc&amp;sp=4","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==4) echo $l->makeLink('eMail '.$icon_sort_desc,WEBDIR."admin/benutzer/view.php?mode=desc&amp;sp=4","none");
            else echo $l->makeLink('eMail ',WEBDIR."admin/benutzer/view.php?mode=asc&amp;sp=4","none");             
        ?>
        </th>
        <th style="width:75px;">
        <?php  // Status
            if($_GET['mode']== "desc" && $_GET['sp']==5) echo $l->makeLink('Status '.$icon_sort_asc,WEBDIR."admin/benutzer/view.php?mode=asc&amp;sp=5","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==5) echo $l->makeLink('Status '.$icon_sort_desc,WEBDIR."admin/benutzer/view.php?mode=desc&amp;sp=5","none");
            else echo $l->makeLink('Status ',WEBDIR."admin/benutzer/view.php?mode=asc&amp;sp=5","none");             
        ?>
        </th>
        <th style="width:50px;">
            Admin
        </th>
    </tr>
    
<?php 
$x=0;
while ($benutzer = $benutzerfac->getElement())
{
   if ($benutzer->typ=="benutzer" )
   {
   $x++;
?>  
   <tr>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $benutzer->name.", ".$benutzer->vorname;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $benutzer->plz;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $benutzer->ort;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <a href="mailto:<?php echo $benutzer->email;?>"><?php echo $benutzer->email;?></a>      
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/benutzer/update.php?mode=changestatus&amp;id=".$benutzer->id);
            if ($benutzer->aktiv=="0" && $benutzer->aktivierungskey =="0") echo '<span style="color:#F00;">gesperrt</span>'; elseif($benutzer->aktiv=="0" && $benutzer->aktivierungskey !="0") echo '<span style="color:#188E02;">neu</span>';  else  echo "<b>aktiv</b>" ;?>
     
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_info,WEBDIR."admin/benutzer/details.php?&amp;id=".$benutzer->id)." ".$l->makeLink($icon_delete_small,WEBDIR."admin/benutzer/delete.php?id=".$benutzer->id);?>
       </td>
       
   </tr>

<?php 
    }
} 
?>

</table>
<br /><br />
<div class="contentboxsmall" style="text-align:left;">
        Seiten: 
        <?php 
            if($_GET['sp'])
            {
                echo $benutzerfac->getHtmlNavi("std", "&amp;sp=".$_GET['sp']."&amp;sort=".$_GET['sort']);
            }
            else echo $benutzerfac->getHtmlNavi("std","" );
        ?>
    </div>
<?php include(INCLUDEDIR."footer.inc.php");
?>

