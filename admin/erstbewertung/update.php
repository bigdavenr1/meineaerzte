<?php
////////////////////////////////////////////////////////////////////////////////
// erstbewertung/update.php regelt das status-update der Tabelle erstbewertung
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
$erstbewertungfac = new Erstbewertung();

unset($_SESSION['err']);

//////////////////////////////////////////////////////////////////////////////////////////////////
// Bewertungsstatus �ndern
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="changestatus")
{
    // Datensatz holen
    $erstbewertungfac->getByUnternehmenid($_GET["id"]);
    $erstbewertung = $erstbewertungfac->getElement();
    
    // Status bestimmen und Datensatz upaten
    // 1=neu / 2= angesehen/ 3=erneut bewertet / 0=erledigt
    if ($erstbewertung->status == "1") $erstbewertungfac->update("`status`='2'");
    elseif($erstbewertung->status == "3") $erstbewertungfac->update("`status`='0'"); 
} 

 
if(!$_SESSION['err']) header("Location:view.php?sv=1");
?>
