<?php
////////////////////////////////////////////////////////////////////////////////
// erstbewertung/view.php - Zeigt die Tabelle erstbewertung an
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<h1>Unternehmen mit ersten Bewertungen</h1>
<br/>
<?php
// Unternehmen mit Bewertungen hohlen und durchlaufen
$erstbewertungfac = new Erstbewertung();
$erstbewertungfac->getBewerteteUnternehmen();
$liste = $erstbewertungfac->getAllElements();

//echo "<pre>"; print_r($liste); echo "</pre>";
//$count = $erstbewertungfac->getElementCount();
//echo "COUNT: ".$count;

// schauen ob in Tabelle erstbewertung vorhanden
foreach($liste AS $daten_temp)
{
   $erstbewertungfac->getByUnternehmenid($daten_temp->unternehmen);
  
   // wenn nein, dazuspeichern
   if(!$erstdaten = $erstbewertungfac->getElement())
   {
        $erstbewertungfac->insertUnternehmen($daten_temp->unternehmen);
   } 
}
// Tabelle erstbewertung mit Join auf unternehmen ausgeben mit Statusändernlink
$erstbewertungfac1 = new Erstbewertung(20);
$erstbewertungfac1->getUnternehmen();
?>
<table style="width:100%">
    <tr>
        <th>ID</th>
        <th>Vorname</th>
        <th>Name</th>
        <th>Status</th>
      </tr>
<?php
$x=0;
while($erste = $erstbewertungfac1->getElement())
{
    $x++;
?>
    <tr>
        <td <?php if ($x%2) echo 'class="td1"'; ?>><?php echo $erste->unternehmen;?></td>
        <td <?php if ($x%2) echo 'class="td1"'; ?>><?php echo $erste->unternehmenvorname;?></td>
        <td <?php if ($x%2) echo 'class="td1"'; ?>><?php echo $erste->unternehmenname;?></td>
        <td <?php if ($x%2) echo 'class="td1"'; ?>>
            <?php 
              if($erste->erststatus == 1 || $erste->erststatus == 3 )
              {
                 echo $l->makeLink($icon_refresh,WEBDIR."admin/erstbewertung/update.php?id=".$erste->unternehmen."&amp;mode=changestatus").'&nbsp;&nbsp;';
              } 
              //echo $erste->erststatus; 
              if($erste->erststatus == 2) echo 'angesehen';
              elseif($erste->erststatus == 1) echo 'neu';
              elseif($erste->erststatus == 3) echo 'Bewertung nach angesehen';
              ?>
         </td>
    </tr>
<?php
}

?>
</table>
<?php
    if($x > 0)
    {
    ?>
    <div class="contentboxsmall" style="text-align:left;">
        Seiten:
    <?php
        echo $erstbewertungfac1->getHtmlNavi("std", "");
    }
    ?>
    </div>
<?php include(INCLUDEDIR."footer.inc.php");
?>
