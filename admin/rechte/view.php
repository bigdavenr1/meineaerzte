<?php

////////////////////////////////////////////////////////////////////////////////
// admin/rechte/view.php - Zeigt die Rechte der Benutzer an
////////////////////////////////////////////////////////////////////////////////


include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<h1>Benutzerrechte vergeben /l�schen</h1>

<?php

    $benutzerfac = new Benutzer(15);
    // Sortierung
    $mode=strtoupper($_GET['mode']);
    $sortierfelder=array("1"=>'name',
                         "2"=>'plz',
                         "3"=>'ort',
                         "4"=>'email',
                         "5"=>'rechte');

    if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
    {
        $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
    }
    else $sort = ' ORDER by name ASC';
    // alle Benutzer hohlen    
    $benutzerfac->getBenutzer($sort);
?>

<br class="clr" /><br />
<table style="width:100%">
    <tr>
        <th >
        <?php // Name
            if($_GET['mode']== "desc" && $_GET['sp']==1) echo $l->makeLink('Name '.$icon_sort_asc,WEBDIR."admin/rechte/view.php?mode=asc&amp;sp=1","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==1) echo $l->makeLink('Name '.$icon_sort_desc,WEBDIR."admin/rechte/view.php?mode=desc&amp;sp=1","none");
            else echo $l->makeLink('Name ',WEBDIR."admin/rechte/view.php?mode=asc&amp;sp=1","none");             
        ?>
        </th>
        <th>
        <?php  // PLZ
            if($_GET['mode']== "desc" && $_GET['sp']==2) echo $l->makeLink('PLZ '.$icon_sort_asc,WEBDIR."admin/rechte/view.php?mode=asc&amp;sp=2","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==2) echo $l->makeLink('PLZ '.$icon_sort_desc,WEBDIR."admin/rechte/view.php?mode=desc&amp;sp=2","none");
            else echo $l->makeLink('PLZ ',WEBDIR."admin/rechte/view.php?mode=asc&amp;sp=2","none");             
        ?>
        </th>
        <th>
        <?php  // Ort
            if($_GET['mode']== "desc" && $_GET['sp']==3) echo $l->makeLink('Ort '.$icon_sort_asc,WEBDIR."admin/rechte/view.php?mode=asc&amp;sp=3","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==3) echo $l->makeLink('Ort '.$icon_sort_desc,WEBDIR."admin/rechte/view.php?mode=desc&amp;sp=3","none");
            else echo $l->makeLink('Ort ',WEBDIR."admin/rechte/view.php?mode=asc&amp;sp=3","none");             
        ?>
        </th>
        <th >
       <?php  // email
            if($_GET['mode']== "desc" && $_GET['sp']==4) echo $l->makeLink('eMail '.$icon_sort_asc,WEBDIR."admin/rechte/view.php?mode=asc&amp;sp=4","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==4) echo $l->makeLink('eMail '.$icon_sort_desc,WEBDIR."admin/rechte/view.php?mode=desc&amp;sp=4","none");
            else echo $l->makeLink('eMail ',WEBDIR."admin/rechte/view.php?mode=asc&amp;sp=4","none");             
        ?>
        </th>
        <th style="width:75px;">
        <?php // Rechte
            if($_GET['mode']== "desc" && $_GET['sp']==5) echo $l->makeLink('Rechte '.$icon_sort_asc,WEBDIR."admin/rechte/view.php?mode=asc&amp;sp=5","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==5) echo $l->makeLink('Rechte '.$icon_sort_desc,WEBDIR."admin/rechte/view.php?mode=desc&amp;sp=5","none");
            else echo $l->makeLink('Rechte ',WEBDIR."admin/rechte/view.php?mode=asc&amp;sp=5","none");             
        ?>
        </th>
        <th>
            Admin
        </th>
   </tr>
    
<?php 
$x=0;
while ($benutzer = $benutzerfac->getElement())
{
   if ($benutzer->typ=="benutzer")
   {
   $x++;
?>  
   <tr>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($benutzer->name.", ".$benutzer->vorname,WEBDIR."admin/benutzer/details.php?&amp;id=".$benutzer->id);?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $benutzer->plz;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $benutzer->ort;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <a href="mailto:<?php echo $benutzer->email;?>"><?php echo $benutzer->email;?></a>      
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php 
                $ausgabe = "";
                if($benutzer)
                {
                    $rechte =unserialize($benutzer->rechte); // rechtearray erzeugen
                    $isArray = is_array($rechte);            // vor in_array Funktion testen
                    
                    if($isArray)
                    {
                        if($rechte['antworten'])        $ausgabe .= "Antworten /"; 
                        if($rechte['benutzer'])         $ausgabe .= " Benutzer /";
                        if($rechte['bewertungsbogen'])  $ausgabe .= " Bewertungsbogen /";
                        if($rechte['fachgebiete'])      $ausgabe .= " Fachgebiete /"; 
                        if($rechte['forum'])            $ausgabe .= " Forum /";
                        if($rechte['klaerung'])         $ausgabe .= " Klaerung /"; 
                        if($rechte['kommentare'])       $ausgabe .= " Kommentare /"; 
                        if($rechte['rechte'])           $ausgabe .= " Rechte /"; 
                        if($rechte['schlagworte'])      $ausgabe .= " Schlagworte /";
                        if($rechte['unternehmen'])      $ausgabe .= " Unternehmen /"; 
                        if($rechte['zufriedenheitsbogen'])  $ausgabe .= " Zufriedenheitsbogen /";
                    }
                    else $ausgabe .="keins";
                }
                echo $ausgabe;
           ?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php 
                if($isArray)echo $l->makeLink($icon_edit_small,WEBDIR."admin/rechte/edit.php?&amp;id=".$benutzer->id)." ".$l->makeLink($icon_delete_small,WEBDIR."admin/rechte/delete.php?id=".$benutzer->id);
                else echo $l->makeLink($icon_edit_small,WEBDIR."admin/rechte/edit.php?&amp;id=".$benutzer->id);
           ?>
       </td>
       
   </tr>

<?php 
    }
} 
?>

</table>
<br /><br />
<div class="contentboxsmall" style="text-align:left;">
        Seiten: 
        <?php 
            if($_GET['sp'])
            {
                echo $benutzerfac->getHtmlNavi("std", "&amp;sp=".$_GET['sp']."&amp;mode=".$_GET['mode']);
            }
            else echo $benutzerfac->getHtmlNavi("std","" );
        ?>
    </div>

<?php include(INCLUDEDIR."footer.inc.php");
?>

