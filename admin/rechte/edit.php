<?php
////////////////////////////////////////////////////////////////////////////////
// admin/rechte/edit.php - Zeigt die Rechte des gew�hlten Benutzers an
////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<h1>Benutzerrechte editieren</h1>

<?php 
if($_SESSION['user']->typ == "admin" && $_GET['id'])
{
    $benutzerfac = new Benutzer(1);
    $benutzerfac->getById($_GET['id']);
    $benutzer = $benutzerfac->getElement();
    
    if($benutzer)
    {
        $rechte =unserialize($benutzer->rechte); // rechtearray erzeugen
        $isArray = is_array($rechte);            // vor in_array Funktion testen
    }
    else $_SESSION['err'] .= "Benutzer nicht gefunden";
    
    //print_r($rechte);
    
?>
  <form action="<?php echo $l->makeFormLink(WEBDIR.'admin/rechte/update.php?id='.$_GET['id']); ?>" method="post" >
    <fieldset>
        <legend>Rechte des Benutzers</legend>
        
        <?php  // Variablen f�r einzelrechte in array suchen ?>
         <label style="width:200px;"><b>Rechte</b></label><input type="checkbox" name="recht[rechte]" value="ON" <?php if($rechte['rechte']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
         <label style="width:200px;">Unternehmensverwaltung</label><input type="checkbox" name="recht[unternehmen]" value="ON" <?php if($rechte['unternehmen']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
         <label style="width:200px;">Benutzerverwaltung</label><input type="checkbox" name="recht[benutzer]" value="ON" <?php if($rechte['benutzer']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
         <label style="width:200px;">Fachgebiete</label><input type="checkbox" name="recht[fachgebiete]" value="ON" <?php if($rechte['fachgebiete']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
         <label style="width:200px;">Bewertungsverwaltung</label><input type="checkbox" name="recht[bewertungen]" value="ON" <?php if($rechte['bewertungen']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
         <label style="width:200px;">Kommentarverwaltung</label><input type="checkbox" name="recht[kommentare]" value="ON" <?php if($rechte['kommentare']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
         <label style="width:200px;">Antworten</label><input type="checkbox" name="recht[antworten]" value="ON" <?php if($rechte['antworten']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
         <label style="width:200px;">Klaerung</label><input type="checkbox" name="recht[klaerung]" value="ON" <?php if($rechte['klaerung']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
         <label style="width:200px;">Bewertungsbogen</label><input type="checkbox" name="recht[bewertungsbogen]" value="ON" <?php if($rechte['bewertungsbogen']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
         <label style="width:200px;">Schlagworte</label><input type="checkbox" name="recht[schlagworte]" value="ON" <?php if($rechte['schlagworte']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
         <label style="width:200px;">Zufriedenheitsbogen</label><input type="checkbox" name="recht[zufriedenheitsbogen]" value="ON" <?php if($rechte['zufriedenheitsbogen']) echo 'checked='.'"checked"'; ?> style="width:50px;" /><br />
    </fieldset>
    <fieldset>
        <legend>Forumsrechte</legend>
          <?php 
            $forumfac = new Forum();
            $forumfac->getByTyp(0);
            $line = 1;
            
            if($isArray){
                 $forumarray = $rechte[forum];
                 $isForum = is_array($forumarray);            // vor in_array Funktion testen
                 /*if($isForum)
                 {
                        if(in_array("14",$forumarray)) echo "forumrechte da";
                        else echo "Keine Forumsrechte";
                 }*/
                 //print_r($forumarray);
            } 
            
            while($forum = $forumfac->getElement())
            {
                 
            ?>
                <label style="width:200px;"><?php echo $forum->name; ?></label><input type="checkbox" name="<?php echo 'recht[forum]['.$forum->id.']'; ?>" value="ON" <?php if($forumarray[$forum->id]) echo 'checked='.'"checked"'; ?> style="width:50px" /><br /><br />
            <?php
                $line++;
            }
          ?>
    </fieldset>
    <input type="submit" value="Daten speichern" />
    <br class="clr"/><br/>
  </form>  

<?php
}
else echo "nicht eingeloggt";

include(INCLUDEDIR."footer.inc.php");
    
?>