<?php
////////////////////////////////////////////////////////////////////////////////
// klaerung/view.php - Zeigt die Tabelle klaerung an
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
?>
<h1>Klärungsanfragen ansehen / löschen</h1>
<br class="clr" /><br />

<table style="width:100%;">
    <tr>
        <th style="width:20px;">
        <?php // id
            if($_GET['mode']== "desc" && $_GET['sp']==1) echo $l->makeLink('id '.$icon_sort_asc,WEBDIR."admin/klaerung/view.php?mode=asc&amp;sp=1","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==1) echo $l->makeLink('id '.$icon_sort_desc,WEBDIR."admin/klaerung/view.php?mode=desc&amp;sp=1","none");
            else echo $l->makeLink('id ',WEBDIR."admin/klaerung/view.php?mode=asc&amp;sp=1","none");             
        ?>            
        </th>
        <th style="width:100px;" >
        <?php // Bewertung
            if($_GET['mode']== "desc" && $_GET['sp']==2) echo $l->makeLink('Bewertung '.$icon_sort_asc,WEBDIR."admin/klaerung/view.php?mode=asc&amp;sp=2","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==2) echo $l->makeLink('Bewertung '.$icon_sort_desc,WEBDIR."admin/klaerung/view.php?mode=desc&amp;sp=2","none");
            else echo $l->makeLink('Bewertung ',WEBDIR."admin/klaerung/view.php?mode=asc&amp;sp=2","none");             
        ?>            
        </th>
        <th style="width:150px;">
        <?php // User
            if($_GET['mode']== "desc" && $_GET['sp']==3) echo $l->makeLink('User '.$icon_sort_asc,WEBDIR."admin/klaerung/view.php?mode=asc&amp;sp=3","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==3) echo $l->makeLink('User '.$icon_sort_desc,WEBDIR."admin/klaerung/view.php?mode=desc&amp;sp=3","none");
            else echo $l->makeLink('User ',WEBDIR."admin/klaerung/view.php?mode=asc&amp;sp=3","none");             
        ?>              
        </th>
        <th style="width:200px;">
        <?php // Unternehmen
            if($_GET['mode']== "desc" && $_GET['sp']==4) echo $l->makeLink('Unternehmen '.$icon_sort_asc,WEBDIR."admin/klaerung/view.php?mode=asc&amp;sp=4","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==4) echo $l->makeLink('Unternehmen '.$icon_sort_desc,WEBDIR."admin/klaerung/view.php?mode=desc&amp;sp=4","none");
            else echo $l->makeLink('Unternehmen ',WEBDIR."admin/klaerung/view.php?mode=asc&amp;sp=4","none");             
        ?>              
        </th>
        <th >
        <?php // Datum
            if($_GET['mode']== "desc" && $_GET['sp']==5) echo $l->makeLink('Datum '.$icon_sort_asc,WEBDIR."admin/klaerung/view.php?mode=asc&amp;sp=5","none");
            elseif($_GET['mode']== "asc" && $_GET['sp']==5) echo $l->makeLink('Datum '.$icon_sort_desc,WEBDIR."admin/klaerung/view.php?mode=desc&amp;sp=5","none");
            else echo $l->makeLink('Datum ',WEBDIR."admin/klaerung/view.php?mode=asc&amp;sp=5","none");             
        ?>             
        </th>
        <th style="width:30px;">
            Admin
        </th>
    </tr>    
<?php 
$klaerungfac = new Klaerung(15);

// Sortierung verarbeiten
$mode=strtoupper($_GET['mode']);
$sortierfelder=array("1"=>'klaerung.id',
                     "2"=>'bewertungen.id',
                     "3"=>'benutzer.nickname',
                     "4"=>'unternehmen.famname',
                     "5"=>'bewertungen.datum');

if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
{
    $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
}

else $sort = 'ORDER by bewertungen.datum DESC';

// Kommentare mit aktiv=0 hohlen
$klaerungfac->getKlaerungSort($sort);

while ($klaerung = $klaerungfac->getElement())
{
    $x++;
    ?>
    <tr >
       <td <?php if ($x%2) echo 'class="td1"'; ?> >
           <?php echo $klaerung->id;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink("Bewertung Nr.".$klaerung->bewertung,WEBDIR."bewertungen/showeintrag.php?bid=".$klaerung->bewertung."&id=".$klaerung->unternehmen);?>      
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($klaerung->benutzername,WEBDIR."/bewertungen/userprofil.php?id=".$klaerung->benutzer);?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($klaerung->unternehmenname." ".$klaerung->unternehmenvorname,WEBDIR."/bewertungen/showeintrag.php?id=".$klaerung->unternehmen);?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
       <?php echo date("d.m.Y",$klaerung->datum);?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_delete_small,WEBDIR."admin/klaerung/delete.php?id=".$klaerung->id);?>
       </td>
   </tr>

<?php }?>

</table>
<br /><br />
<?php 
if ($x==0)
{
    echo "Keine neuen Klärungsanfragen vorhanden";
}
else 
{?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten: 
        <?php
        if($_GET['sp'])
            {
                echo $klaerungfac->getHtmlNavi("std", "&amp;sp=".$_GET['sp']."&amp;mode=".$_GET['mode']);
            }
            else echo $klaerungfac->getHtmlNavi("std","" );
        ?>
    </div>
<?php 
}
include(INCLUDEDIR."footer.inc.php");
?>
