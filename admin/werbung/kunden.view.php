<?php
////////////////////////////////////////////////////////////////////////////////
// werbung/kunden.view.php - Zeigt die Werbekunden von Tabelle benutzer an
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
?>
    <h1>Werbekunden anlegen / verwalten / freischalten</h1>
    <br/>
    <?php if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>';unset($_SESSION['err']); }?>
    <form method="get" action="<?php echo $l->makeFormLink(WEBDIR.'admin/werbung/kunden.edit.php');?>">
        <fieldset>
             <legend>
                 Werbekunden �ber Werbe-ID aufrufen
             </legend>
             <label>Werbe-ID</label><input type="text" name="id"/><br class="clr"/>
             <label>Kundennummer</label><input type="text" name="nummer"/><br class="clr"/>
             <br/>
             <input type="hidden" name="mode" value="update" />
             <input type="submit" value="Unternehmen aufrufen" class="submit"/>
        </fieldset>
    </form>

<br class="clr" /><br />
<?php echo $l->makeLink($icon_info." <b>[ neuen Werbekunden erstellen ]</b>",WEBDIR."admin/werbung/kunden.edit.php?mode=new");?>
<br/><br/>
<table style="width:100%;">
    <tr>
        <th >
        ID
        <?php
            // Id
        ?>
        </th>
        <th style="width:200px;">
        Name
        <?php
            // Name
        ?>
        </th>
        <th>
        Aktivierungs-Status
        <?php
            // Aktivi.
        ?>
        </th>
        <th style="width:50px;">
            Admin
        </th>
    </tr>    
<?php 
    $kundenfac = new Benutzer(15);
    // Sortierung
    $mode=strtoupper($_GET['mode']);
    $sortierfelder=array("1"=>'name',
                         "2"=>'plz',
                         "3"=>'ort',
                         "4"=>'email',
                         "5"=>'aktiv');

    if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
    {
        $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
    }
    else $sort = " ORDER by id ASC";
    
    $kundenfac->getWerbekunden($sort);

$x=0;
while ($kunde = $kundenfac->getElement())
{
    $x++;
    ?>
    <tr >
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $kunde->id;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php /*echo substr($daten->name,0,100);*/?>
           <?php echo $kunde->name;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/werbung/kunden.update.php?id=".$kunde->id."&amp;mode=changeaktiv")."&nbsp;";
            if ($kunde->aktiv=="0") echo "<i>I</i>"; elseif ($kunde->aktiv=="") echo '<b style="color:green">N</b>'; else echo "<b>A</b>" ;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/werbung/kunden.edit.php?mode=update&amp;id=".$kunde->id)." ".$l->makeLink($icon_delete_small,WEBDIR."admin/werbung/kunden.delete.php?werbeid=".$kunde->id);?>
       </td>
   </tr>

<?php }?>

</table>

<br /><br />
<?php 
if ($x==0)
{
    echo "Keine Werbekunden eingetragen";
}
else 
{?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten:
        <?php 
            echo $kundenfac->getHtmlNavi("std","" );
          ?>
    </div>
<?php 
}
include(INCLUDEDIR."footer.inc.php");
?>

