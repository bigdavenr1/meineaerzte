<?php
////////////////////////////////////////////////////////////////////////////////
// admin/werbung/kunden.delete.php - L�schen von Kunden aus der Tabelle benutzer
//              und aus verbundenen Tabellen 
//              anzeige, anzeigeort, anzeigebanner, anzeigefachgebiete, anzeigeordzeiten
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

if ($_POST["submit"])
{
    if ($_POST["mode"] == "delete_yes")
    {
        $kundenfac = new Benutzer();
        $kundenfac->getWerbekundenById($_POST["werbeid"]);
        
        if($kunde = $kundenfac->getElement()) $werbeid = $kunde->id;
        
        // alle Banner des Werbekunden hohlen und l�schen
        $bannersammelfac = new Anzeigebanner();
        $bannersammelfac->getByWerbeId($werbeid);
        while($foundbanner = $bannersammelfac->getElement())
        {
            $anzeigebannerfac = new Anzeigebanner();
            $anzeigebannerfac->getById($foundbanner->id);
            $anzeigebanner = $anzeigebannerfac->getElement();
            // Bilder von Banner aus Werbeordner l�schen
            if($anzeigebanner->bild != '')
            {
                // Grafiktyp auslesen
                $typ=getimagesize(LOCALDIR."images/werbung/".$anzeigebanner->bild) ;
          
                // je nach typ Endung bestimmen   
                if ($typ[2]==1) $endung=".gif";
                if ($typ[2]==2) $endung=".jpg"; 
                
                // Banner und Halfbanner l�schen
           	    unlink(LOCALDIR."images/werbung/".$anzeigebanner->bild);
                unlink(LOCALDIR."images/werbung/".basename($anzeigebanner->bild,$endung)."_half".$endung);
            }
            
            // Banner l�schen
            $anzeigebannerfac->deleteElement();
        }// end while($foundbanner = $anzeigebannerfac->getElement()) 
          
        // alle Anzeigen des Werbekunden hohlen und l�schen
        $anzeigesammelfac = new Anzeige();    
        $anzeigesammelfac->getByWerbeId($werbeid);
        while($foundanzeige = $anzeigesammelfac->getElement())
        {
            // Anzeige l�schen
            $anzeigefac = new Anzeige();
            $anzeigefac->getById($foundanzeige->id);
            
            // Bilder von Anzeige aus Werbeordner l�schen
            $daten = $anzeigefac->getElement();
            $anzeigeid = $daten->id;                // Datenid hohlen
            
            // Wenn Bilder vorhanden sind, dann als Array einlesen
            if (is_array(unserialize($daten->bild))) 
            {
                $bildname=unserialize($daten->bild);
                // Schauen ob Bilder da sind , wenn ja l�schen
                for ($x=5;$x>-1;$x--)
                {
                    // eventuell altes Bild l�schen
                    if($bildname[$x]['url'] && $bildname[$x]['url'] != '')
                    {
                        // Grafiktyp auslesen
                        $typ=getimagesize(LOCALDIR."images/werbung/".$bildname[$x]['url']) ;
                  
                        // je nach typ Endung bestimmen   
                        if ($typ[2]==1) $endung=".gif";
                        if ($typ[2]==2) $endung=".jpg"; 
                        
                        // Bild und Thumb l�schen
                   	    unlink(LOCALDIR."images/werbung/".$bildname[$x]['url']);
                        unlink(LOCALDIR."images/werbung/".basename($bildname[$x]['url'],$endung)."_thumb".$endung);
                    }  
                }        
            }
            $anzeigefac->deleteElement();
            
            // Datens�tze aus Anzeigeordzeiten l�schen
            $ordzeitenfac = new Anzeigeordzeiten(); 
            $ordzeitenfac->getByAnzeigeId($anzeigeid);
            while($ordzeit = $ordzeitenfac->getElement())
            {
                $ordzeitenfac->deleteElement(); // alte Datens�tze l�schen
            }
            
            // Datens�tze aus Anzeigefachgebiete l�schen
            $anzeigefachfac = new Anzeigefachgebiete();
            $anzeigefachfac->getByAnzeigeId($anzeigeid);
            while($anzeigefach = $anzeigefachfac->getElement())
            {
                $anzeigefachfac->deleteElement(); // alte Datens�tze l�schen
            }
            
            // Datens�tze aus Anzeigeort updaten
            $anzeigeortfac = new Anzeigeort();
            // platz1 updaten
            $anzeigeortfac->getByAnzeigeIdPlatz1($anzeigeid);
            while($anzeigeort = $anzeigeortfac->getElement())
            {
                $anzeigeortfac->update(" `platz1`=''"); // alte Pl�tze �berschreiben
            }
            // platz1 updaten
            $anzeigeortfac->getByAnzeigeIdPlatz2($anzeigeid);
            while($anzeigeort = $anzeigeortfac->getElement())
            {
                $anzeigeortfac->update(" `platz2`=''"); // alte Pl�tze �berschreiben
            }
            // platz1 updaten
            $anzeigeortfac->getByAnzeigeIdPlatz3($anzeigeid);
            while($anzeigeort = $anzeigeortfac->getElement())
            {
                $anzeigeortfac->update(" `platz3`=''"); // alte Pl�tze �berschreiben
            }
            
        }// end  while($foundanzeige = $anzeigefac->getElement())
        
        // werbekunde l�schen
        $kundenfac->deleteElement();
        
        header("Location: ./kunden.view.php?sv=1");
        
    } // end if ($_POST["mode"] == "delete_yes")
}// end if ($_POST["submit"])

//  Sicherheitsabfrage
////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");?>
    <h1>Werbekunden l�schen</h1>
    <?php
    $kundenfac = new Benutzer();
    $kundenfac->getWerbekundenById($_GET["werbeid"]);
    $kunden = $kundenfac->getElement();?>
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
        <fieldset>
            <legend>Soll dieser Werbekunde und dessen Anzeigen, Banner wirklich gel�scht werden?</legend>
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="werbeid" value="<?php echo $_GET['werbeid'];?>" />
            Werbekunde Id: <b><?php echo $kunden->id;?></b><br/><br />
            Daten von : <b><?php echo $kunden->titel.' '.$kunden->name.', '.$kunden->vorname;?></b><br/><br />
        </fieldset>
        <br />
        <input type="submit" value="L�schen" name="submit" class="submit" />
        <?php echo $l->makeLink("Zur�ck", "./kunden.view.php", "backlink");?>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php"); 
} ?>