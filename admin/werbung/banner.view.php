<?php
////////////////////////////////////////////////////////////////////////////////
// werbung/banner.view.php - Zeigt die Tabelle anzeigebanner an
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
?>
    <h1>Anzeigebanner verwalten / freischalten</h1>
    <?php if ($_SESSION['msg']){ echo '<span class="msg">'.$_SESSION['msg'].'</span><br/><br/>'; unset($_SESSION['msg']);}?>
    <br/>
    <form method="get" action="<?php echo $l->makeFormLink(WEBDIR.'admin/werbung/banner.edit.php');?>">
        <fieldset>
             <legend>
                 Anzeigebanner �ber Banner-ID aufrufen
             </legend>
             <label>Banner-ID</label><input type="text" name="id"/><br class="clr"/>
             <br/>
             <input type="hidden" name="mode" value="update" />
             <input type="submit" value="Anzeigebanner aufrufen" class="submit"/>
        </fieldset>
    </form>

<br class="clr" /><br />
<br/><br/>
<table style="width:100%;">
    <tr>
        <th >
            ID
        </th>
        <th style="width:100px;">
            Werbekunde
        </th>
        <th>
            Banner-Typ
        </th>
        <th>
            Status
        </th>
        <th>
            Homepage
        </th>
        <th style="width:50px;">
            Admin
        </th>
    </tr>    
<?php 
    $anzeigebannerfac = new Anzeigebanner(15);
    // Sortierung
    $mode=strtoupper($_GET['mode']);
    $sortierfelder=array("1"=>'id',
                         "2"=>'werbeid',
                         "3"=>'status',
                         "4"=>'home');

    if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
    {
        $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
    }
    else $sort = " ORDER by id ASC";
    
    $anzeigebannerfac->getAll($sort);

$x=0;
while ($anzeigebanner = $anzeigebannerfac->getElement())
{
    $x++;
    ?>
    <tr >
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $anzeigebanner->id;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($anzeigebanner->werbeid,WEBDIR."admin/werbung/kunden.edit.php?mode=update&amp;id=".$anzeigebanner->werbeid);?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
          <?php echo $anzeigebanner->bannertyp;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/werbung/banner.update.php?id=".$anzeigebanner->id."&amp;mode=changestatus")."&nbsp;";
            if ($anzeigebanner->status=="0") echo "<i>I</i>"; elseif ($anzeigebanner->status=="1") echo '<b style="color:green">A</b>'; else echo "<b>unbekannt</b>" ;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $anzeigebanner->homepage;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/werbung/banner.edit.php?mode=update&amp;id=".$anzeigebanner->id)." ".$l->makeLink($icon_delete_small,WEBDIR."admin/werbung/banner.delete.php?id=".$anzeigebanner->id);?>
       </td>
   </tr>

<?php }?>

</table>

<br /><br />
<?php 
if ($x==0)
{
    echo "Keine Werbebanner angelegt";
}
else 
{?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten:
        <?php 
            echo $anzeigebannerfac->getHtmlNavi("std","" );
          ?>
    </div>
<?php 
}
include(INCLUDEDIR."footer.inc.php");
?>