<?php
////////////////////////////////////////////////////////////////////////////////
// admin/werbung/banner.edit.php zeigt das Formular zur �nderung von Werbebannern
////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

unset($stopp);
$anzeigenbannertestfac = new Anzeigebanner();
    
if($_GET['mode']=="new" && (!$_GET['werbeid'] || $_GET['werbeid']==''))
{
    $_SESSION['err'] = '<br/>Keine Werbekunden-Id vorhanden, erst Werbekunde anlegen!<br />';
    $stopp = "ENDE";
}

if ($_GET['mode']=="update")
{

    if($_GET['id'])
    {
        $anzeigenbannertestfac->getById($_GET["id"]);
        
    }
        
    if(!$anzeigebannertest = $anzeigenbannertestfac->getElement())
    {
        $_SESSION['err'] = '<br/>Das von Ihnen aufgerufene Werbebanner existiert nicht!<br />';
        $stopp = "ENDE";
    }
}

if($stopp == "ENDE")
{
    echo '<h1>Fehler</h1>';
    if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}
    echo $l->makeLink("Zur�ck zur Banner-�bersicht", "./banner.view.php", "backlink").'<br /><br />';
    echo $l->makeLink("Zur�ck zur Kunden- �bersicht", "./kunden.view.php", "backlink");
    include(INCLUDEDIR."footer.inc.php");
}
else    // weitermachen
{
    ?>
    <h1>Banner <?php  if($_GET['mode'] == 'update') echo 'bearbeiten'; else echo 'erstellen'; ?></h1>
    <?php if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}?>
    <br />
    
    <form action="<?php echo $l->makeFormLink(WEBDIR.'admin/werbung/banner.update.php?mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>
                Werbeanzeige - Admineinstellungen
            </legend>
            <?php
            $anzeigebannerfac = new Anzeigebanner();
            $anzeigebannerfac->getById($_GET['id']);
            $anzeigebanner = $anzeigebannerfac->getElement();
            ?>
            <input type="hidden" name="werbeid" value="<?php if($_GET['werbeid']) echo $_GET['werbeid']; else echo $anzeigebanner->werbeid; ?>" />
            
            
            <label for="bannerstatus">
                Banner Status
            </label>
            
            <select name="bannerstatus" id="bannerstatus">
                <option value="1" <?php if ($anzeigebanner->status=="1") echo 'selected="selected"';?>>Aktiviert</option>
                <option value="0" <?php if ($anzeigebanner->status=="0") echo 'selected="selected"';?>>Inaktiv</option>
            </select>
            <br class="clr" /><br />
            
            
        </fieldset>
            <br class="clr" /><br />
        <fieldset>
            <legend>
                Banner Daten
            </legend>
            <label>
               Bannertyp
            </label><?php if($_GET['mode']=="new")
            {?> 
               <div style="float:left;">
               <br/>
               <input type="radio" class="left" name="bannertyp" value="half" style="width:40px;height:auto;" checked="checked"/>Half-Size<br class="clr"/><input type="radio" name="bannertyp" value="full" class="left" style="width:40px;height:auto;"/>Full-Size<br class="clr"/>
            </div><?php }
            else echo  '<br>'.$anzeigebanner->bannertyp;
            ?>
            <br class="clr"/><br/>
            <label for="homepage">
                Anzeige Webpage (nach http://)
            </label>
            <input type="text" id="homepage" name="homepage" value="<?php echo $anzeigebanner->homepage;?>" />
            <br /><br />
            <?php
            if($anzeigebanner->bild=='')
            {
                echo 'Kein Bannerbild hochgeladen';
            }
            else 
            {
                // Grafiktyp auslesen
                $typ=getimagesize(LOCALDIR."images/werbung/".$anzeigebanner->bild) ;
      
                // je nach typ Endung bestimmen   
                if ($typ[2]==1) $endung=".gif";
                if ($typ[2]==2) $endung=".jpg"; 
            
                // Reinnamen ohne Endung
                $name=basename($anzeigebanner->bild,$endung);
            
                // Gr��e des Thumbnails auslesen
                $size=getimagesize(LOCALDIR."images/werbung/".$name."_half".$endung);
      
                // HTML Bild schreiben 
                ?>
                <div <?php if ($x%2) echo 'class="td1"';?>>
                <img src="<?php echo WEBDIR;?>images/werbung/<?php echo $name;?>_half<?php echo $endung;?>" height="<?php echo $size[1]; ?>" width="<?php echo $size[0]; ?>" alt="Bild von <?php echo $_SESSION['user']->name." ".$_SESSION['user']->vorname?>"/>
                </div> 
            <?php
            }
            ?>
            <br class="clr"/><br/><hr/><br/>
            
            <label for="bildneu">
                Neues Bild raussuchen:(altes Bild wird gel�scht)
            </label>
            <input id="bildneu" name="bildneu" type="file" /><br class="clr" />
         </fieldset>
         <br class="clr"/><br/>
         <input type="submit" value="Daten speichern" />
         <?php echo $l->makeLink("Zur�ck zur Banner �bersicht", "./banner.view.php", "backlink"); ?>
         <?php echo $l->makeLink("Zur�ck zur Kunden �bersicht", "./kunden.view.php", "backlink"); ?>

        <br class="clr"/><br/>
 </form>
    <?php
    include(INCLUDEDIR."footer.inc.php");
} // end else zu if($stopp == "ENDE")
?>