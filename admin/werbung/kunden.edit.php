<?php

////////////////////////////////////////////////////////////////////////////////
// admin/werbung/kunden.edit.php zeigt das Formular zur �nderung von Werbekunden an
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");



if ($_GET['mode']=="update")
{   
    $kundenfac = new Benutzer();
    if($_GET['id'])
    {
        $kundenfac->getWerbekundenById($_GET["id"]);
    }
    elseif($_GET['nummer'] && !$_GET['id'])
    {
        $kundenfac->getWerbekundenByKuNr($_GET["nummer"]);
    }
    if(!$kunde = $kundenfac->getElement())
    {
        $_SESSION['err']="Der gew�hlte Kunde existiert nicht!<br/>";
        header("LOCATION:kunden.view.php?sv=1");
    }
}    
 
include(INCLUDEDIR."header.inc.php");

    $_GET['id']=$kunde->id;
    ?>
    <h1>Werbekundenseintrag bearbeiten</h1>
    <?php if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>';}?>
    <br />
    
    <form action="<?php echo $l->makeFormLink(WEBDIR.'admin/werbung/kunden.update.php?mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>
                Werbekundendaten
            </legend>
            <label for="titel">
                Titel
            </label>
            <input type="text" name="titel" id="titel" value="<?php echo $kunde->titel;?>" />
            <br class="clr" />
            <label for="anrede">
                Anrede
            </label>
            <select name="anrede" id="anrede">
                <option value="Herr" <?php if($kunde->anrede == "Herr")echo 'selected='.'"selected"'; ?>>Herr</option>
                <option value="Frau" <?php if($kunde->anrede == "Frau")echo 'selected='.'"selected"'; ?>>Frau</option>
            </select>
            <br class="clr" />
            
            <label for="name">
                Nachname
            </label>
            <input type="text" name="name" id="name" value="<?php echo $kunde->name;?>" />
            <br class="clr" />
            <label for="vorname">
                Vorname
            </label>
            <input type="text" id="vorname" name="vorname" value="<?php echo $kunde->vorname;?>" />
            <br class="clr" /><br/>
            <label for="strasse">
                Strasse
            </label>
            <input type="text" id="strasse" name="strasse" value="<?php echo $kunde->adresse;?>" />
            <br class="clr" />
            <label for="plz">
                PLZ / Ort
            </label>
            <input type="text" id="plz" name="plz" value="<?php echo $kunde->plz;?>" class="plz"/>
            <input type="text" id="ort" name="ort" value="<?php echo $kunde->ort;?>" class="ort" alt="ort"/>
            <br class="clr" />
            <label for="land">
                Land
            </label>
            <input type="text" id="land" name="land" value="<?php echo $kunde->land;?>" />
            <br class="clr" /><br/>
            <label for="tel">
                Telefon
            </label>
            <input type="text" id="tel" name="tel" value="<?php echo $kunde->tel;?>" />
            <br class="clr" />
            <label for="fax">
                Fax
            </label>
            <input type="text" id="fax" name="fax" value="<?php echo $kunde->fax;?>" />
            <br class="clr" /><br/>
             <label for="homepage">
                Homepage
            </label>
            <input type="text" id="homepage" name="homepage" value="<?php echo $kunde->homepage;?>" />
        </fieldset>
        <br class="clr" /><br/>
        <br class="clr" /><br />
        <fieldset>
            <legend>
                Admineinstellungen
            </legend>
            <label for="kundennummer">
                Kundennummer
            </label>
            <input type="text" id="kundennummer" name="kundennummer" value="<?php echo $kunde->email;?>" />
            <br class="clr"/><br/>
            <label for="homepage">
                Passwort
            </label>
            <input type="text" id="passwd" name="passwd" value="<?php echo $kunde->nickname;?>" />
            <br class="clr"/><br/>
            <label for="aktiv">
                Aktiv
            </label>
            <select name="aktiv" id="aktiv">
                <option value="1" <?php if ($kunde->aktiv=="1") echo 'selected="selected"';?>>Aktiv</option>
                <option value="0" <?php if ($kunde->aktiv=="0") echo 'selected="selected"';?>>Inaktiv</option>
            </select>
            <br class="clr" /><br />
        </fieldset>
        <br class="clr"/><br/>
        <?php echo $l->makeLink("Zur�ck zur Werbekunden - �bersicht", "./kunden.view.php", "backlink"); ?><input type="submit" value="Daten speichern" />
        <br class="clr"/><br/>
 </form>
         <br class="clr"/><br/>
         <h2>Anzeigen des Werbekunden</h2>
<?php  echo $l->makeLink($icon_info." <b>[ neue Anzeige f�r Werbekunden erstellen ]</b>",WEBDIR."admin/werbung/anzeige.edit.php?mode=new&amp;werbeid=".$_GET['id']);?>
<br/><br/>
<table style="width:100%;">
    <tr>
        <th >
            Anzeigen - ID
        </th>
        <th style="width:200px;">
            Werbekunden - ID
        </th>
        <th>
            Anzeige-Status
        </th>
        <th style="width:50px;">
            Admin
        </th>
    </tr>    
<?php 
    $anzeigefac = new Anzeige();
    $anzeigefac->getByWerbeId($_GET['id']);

$x=0;
while ($anzeige = $anzeigefac->getElement())
{
    $x++;
    ?>
    <tr >
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $anzeige->id;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $anzeige->werbeid;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/werbung/anzeige.update.php?id=".$anzeige->id."&amp;mode=changestatus")."&nbsp;";
            if ($anzeige->status=="0") echo "<i>I</i>"; elseif ($anzeige->status=="1") echo '<b style="color:green">A</b>'; else echo "<b>leer</b>" ;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/werbung/anzeige.edit.php?mode=update&amp;id=".$anzeige->id)." ".$l->makeLink($icon_delete_small,WEBDIR."admin/werbung/anzeige.delete.php?id=".$anzeige->id);?>
       </td>
   </tr>

<?php }?>

</table>

<br />
<?php 
if ($x==0)
{
    echo "Keine Anzeigen angelegt";
}
?>
<br />
    <h2>Banner des Werbekunden</h2>
<?php  echo $l->makeLink($icon_info." <b>[ neues Banner f�r Werbekunden erstellen ]</b>",WEBDIR."admin/werbung/banner.edit.php?mode=new&amp;werbeid=".$_GET['id']);?>
<br/><br/>
<table style="width:100%;">
    <tr>
        <th >
            Banner - ID
        </th>
        <th style="width:200px;">
            Werbekunden - ID
        </th>
        <th>
            Banner-Homepage
        </th>
        <th>
            Banner-Status
        </th>
        <th style="width:50px;">
            Admin
        </th>
    </tr>    
<?php 
    $anzeigebannerfac = new Anzeigebanner();
    $anzeigebannerfac->getByWerbeId($_GET['id']);

$x=0;
while ($anzeigebanner = $anzeigebannerfac->getElement())
{
    $x++;
    ?>
    <tr >
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $anzeigebanner->id;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $anzeigebanner->werbeid;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $anzeigebanner->homepage;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/werbung/banner.update.php?id=".$anzeigebanner->id."&amp;mode=changestatus")."&nbsp;";
            if ($anzeigebanner->status=="0") echo "<i>I</i>"; elseif ($anzeigebanner->status=="1") echo '<b style="color:green">A</b>'; else echo "<b>leer</b>" ;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/werbung/banner.edit.php?mode=update&amp;id=".$anzeigebanner->id)." ".$l->makeLink($icon_delete_small,WEBDIR."admin/werbung/banner.delete.php?id=".$anzeigebanner->id);?>
       </td>
   </tr>

<?php }?>

</table>

<br />
<?php 
if ($x==0)
{
    echo "Keine Banner angelegt";
}


    include(INCLUDEDIR."footer.inc.php");
?>