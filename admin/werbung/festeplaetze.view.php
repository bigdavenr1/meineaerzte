<?php
////////////////////////////////////////////////////////////////////////////////
// werbung/festeplaetze.view.php - Zeigt die Tabelle anzeigeort an
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
?>
    <h1>Feste Anzeigepl�tze erstellen / verwalten </h1>
    <?php if ($_SESSION['msg']){ echo '<span class="msg">'.$_SESSION['msg'].'</span><br/><br/>'; unset($_SESSION['msg']);}?>
    <br/>
    <?php /* 
    <form method="get" action="<?php echo $l->makeFormLink(WEBDIR.'admin/werbung/festeplaetze.edit.php');?>">
        <fieldset>
             <legend>
                 Feste Pl�tze �ber Platz -ID aufrufen
             </legend>
             <label>Feste Pl�tze -ID</label><input type="text" name="id"/><br class="clr"/>
             <br/>
             <input type="hidden" name="mode" value="update" />
             <input type="submit" value="Anzeige aufrufen" class="submit"/>
        </fieldset>
    </form>
    */ ?>
<br class="clr" /><br />
<?php echo $l->makeLink($icon_info." <b>[ neue Anzeigepl�tze erstellen ]</b>",WEBDIR."admin/werbung/festeplaetze.edit.php?mode=new");?>
<br/><br/>
<table style="width:100%;">
    <tr>
        <th >
        Feste Pl�tze - ID
        <?php
            // Id
        ?>
        </th>
        <th>
        Fachid
        <?php
            // Fachid
        ?>
        </th>
        <th>
        Ort
        <?php
            // Ort
        ?>
        </th>
        <th>
        Plz
        <?php
            // Plz
        ?>
        </th>
        <th >
        Platz1
        <?php
            // Platz1
        ?>
        </th>
        <th >
        Platz2
        <?php
            // Platz2
        ?>
        </th>
        <th>
        Platz3
        <?php
            // Platz2
        ?>
        </th>
        <th style="width:50px;">
            Admin
        </th>
    </tr>    
<?php 
    $anzeigeortfac = new Anzeigeort(15);
    // Sortierung
    $mode=strtoupper($_GET['mode']);
    $sortierfelder=array("1"=>'id',
                         "2"=>'fachid',
                         "3"=>'ort',
                         "4"=>'plz',
                         "5"=>'platz1',
                         "6"=>'platz2',
                         "7"=>'platz3');

    if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
    {
        $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
    }
    else $sort = " ORDER by id ASC";
    
    $anzeigeortfac->getAll($sort);

$x=0;
while ($anzeigeort = $anzeigeortfac->getElement())
{
    $x++;
    ?>
    <tr >
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $anzeigeort->id;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $anzeigeort->fachid;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php if($anzeigeort->ort != '') echo $anzeigeort->ort; else echo 'leer';?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php if($anzeigeort->plz != '') echo $anzeigeort->plz; else echo 'leer';?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php if($anzeigeort->platz1 != '') echo $anzeigeort->platz1; else echo 'leer';?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php if($anzeigeort->platz2 != '') echo $anzeigeort->platz2; else echo 'leer';?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php if($anzeigeort->platz3 != '') echo $anzeigeort->platz3; else echo 'leer';?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/werbung/festeplaetze.edit.php?mode=update&amp;id=".$anzeigeort->id)." ".$l->makeLink($icon_delete_small,WEBDIR."admin/werbung/festeplaetze.delete.php?id=".$anzeigeort->id);?>
       </td>
   </tr>

<?php }?>

</table>

<br /><br />
<?php 
if ($x==0)
{
    echo "Keine Anzeigepl�tze angelegt";
}
else 
{?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten:
        <?php 
            echo $anzeigeortfac->getHtmlNavi("std","" );
          ?>
    </div>
<?php 
}
include(INCLUDEDIR."footer.inc.php");
?>