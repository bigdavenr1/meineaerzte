<?php
////////////////////////////////////////////////////////////////////////////////
// werbung/anzeige.view.php - Zeigt die Tabelle anzeigen an
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
?>
    <h1>Anzeigen erstellen / verwalten / freischalten</h1>
    <?php if ($_SESSION['msg']){ echo '<span class="msg">'.$_SESSION['msg'].'</span><br/><br/>'; unset($_SESSION['msg']);}?>
    <br/>
    <form method="get" action="<?php echo $l->makeFormLink(WEBDIR.'admin/werbung/anzeige.edit.php');?>">
        <fieldset>
             <legend>
                 Anzeigen �ber ID aufrufen
             </legend>
             <label>Anzeige-ID</label><input type="text" name="id"/><br class="clr"/>
             <br/>
             <input type="hidden" name="mode" value="update" />
             <input type="submit" value="Anzeige aufrufen" class="submit"/>
        </fieldset>
    </form>

<br class="clr" /><br />
<br/><br/>
<table style="width:100%;">
    <tr>
        <th >
        ID
        <?php
            // Id
        ?>
        </th>
        <th style="width:200px;">
        Werbe- ID
        <?php
            // Werbe-id
        ?>
        </th>
        <th>
        Anzeige-Status
        <?php
            // Status
        ?>
        </th>
        <th style="width:50px;">
            Admin
        </th>
    </tr>    
<?php 
    $anzeigefac = new Anzeige(15);
    // Sortierung
    $mode=strtoupper($_GET['mode']);
    $sortierfelder=array("1"=>'id',
                         "2"=>'werbeid',
                         "3"=>'status',
                         "4"=>'aktiv');

    if ($sortierfelder[$_GET['sp']] && ($mode=="ASC" || $mode=="DESC"))
    {
        $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$mode;
    }
    else $sort = " ORDER by id ASC";
    
    $anzeigefac->getAll($sort);

$x=0;
while ($anzeige = $anzeigefac->getElement())
{
    $x++;
    ?>
    <tr >
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $anzeige->id;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php /*echo substr($daten->name,0,100);*/?>
           <?php echo $anzeige->werbeid;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/werbung/anzeige.update.php?id=".$anzeige->id."&amp;mode=changestatus")."&nbsp;";
            if ($anzeige->status=="0") echo "<i>I</i>"; elseif ($anzeige->status=="") echo '<b style="color:green">N</b>'; else echo "<b>A</b>" ;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/werbung/anzeige.edit.php?mode=update&amp;id=".$anzeige->id)." ".$l->makeLink($icon_delete_small,WEBDIR."admin/werbung/anzeige.delete.php?id=".$anzeige->id);?>
       </td>
   </tr>

<?php }?>

</table>

<br /><br />
<?php 
if ($x==0)
{
    echo "Keine Anzeigen angelegt";
}
else 
{?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten:
        <?php 
            echo $anzeigefac->getHtmlNavi("std","" );
          ?>
    </div>
<?php 
}
include(INCLUDEDIR."footer.inc.php");
?>