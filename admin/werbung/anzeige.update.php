<?php
////////////////////////////////////////////////////////////////////////////////
// werbung/anzeige.update.php ->regelt das updaten der anzeige, anzeigeordzeiten, 
//                              und anzeigefachgebieteTabellen
////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");
$anzeigefac = new Anzeige();

//echo "<pre>"; print_r($_POST); print_r($_GET); echo "</pre>";

// Daten pr�fen
unset($_SESSION['err']);

if(!$_GET['mode']=="changestatus" && (!$_POST['werbeid'] || $_POST['werbeid']=='')) $_SESSION['err'] .= 'Keine Werbeid �bergeben!<br />';
if($_GET['mode']=="update" && ( !$_GET['id'] || $_GET['id'] == '')) $_SESSION['err'] .= 'Keine Anzeigeid �bergeben!<br />';

// Nur drei Fachgebiete ausgew�hlt?
if($_POST['anzeigefachgebiete'])
{
   $count=count($_POST['anzeigefachgebiete']);
   if($count > 3) $_SESSION['err'] .= 'Sie haben zuviele Fachgebiete gew�hlt! Nur drei erlaubt!<br />';
}

// Fachgebiet / Plz Kombination �berpr�fen
for($i=0;$i<10;$i++)
{
  if( $_POST['fachgebiete'][$i]['fach'] != '')
  {
      if( $_POST['fachgebiete'][$i]['plz'] == '')
      {
         $z = $i+1;
         $_SESSION['err'] .= 'Fehler in der '.$z.'ten Fachgebiet-Plz Kombination!<br />';
      }
  }
}

// Daten pr�fen
if(!$_POST['anzeigefirmname'] || $_POST['anzeigefirmname'] == "") $_SESSION['err'] .= "Firmenname ist n�tig!<br/ >";
if(!$_POST['plz'] || $_POST['plz'] == "") $_SESSION['err'] .= "PLZ ist n�tig!<br/ >";
if(!$_POST['ort'] || $_POST['ort'] == "") $_SESSION['err'] .= "Ort ist n�tig!<br/ >";
if(!$_POST['strasse'] || $_POST['strasse'] == "") $_SESSION['err'] .= "Strasse ist n�tig!<br/ >";

// Bei Fehler zur�ck
if($_SESSION['err'])
{
    if($_GET['mode']=='update') header("Location:anzeige.edit.php?mode=update&id=".$_GET['id']."&sv=1");
    elseif($_GET['mode']=='new') header("Location:anzeige.edit.php?mode=new&werbeid=".$_POST['werbeid']."&sv=1");
    else header("Location:anzeige.view.php?sv=1");
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Neuer Datensatz
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="new" && !$_SESSION['err'])
{
    // neue Datens�tze in Tabellen anzeige, anzeigeordzeiten, anzeigefachgebiete
    
    // neues Anzeigeprofil schreiben
    $anzeigefac = new Anzeige();
    $anzeigefac->getLastByWerbeId($_POST['werbeid']);
    if($alteAnzeige = $anzeigefac->getElement()) $last_id = $alteAnzeige->id;
    
    if ($_FILES['bildneu'] != "")// Bild ausgew�hlt
    {                        
        $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);
    
        // �berpr�fung des Bildtyps
        if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
        {
            if ($filetyp[2]==2) $type = '.jpg';
            if ($filetyp[2]==1) $type = '.gif';
      		                
            // neues Bild hochladen
            $key=generateRandomKey(10);
       	    $pic = $anzeigefac->writePic($_FILES['bildneu']["tmp_name"],$key,$type);
            $bildname[]= array("url"=>$key.$type,
                     "bildkommentar"=>$_POST['bildkommentarneu']);
        }
        //Wenn falscher Bildtyp 
        else 
        {    
            $error= $error."[Bild -- falschen Typ]";
        }
    }
        
    // Beschreibung parsen
    $tParser = new TextParser($_POST['text']);
    $tParser->parse();
    
    
    // anzeigearray
    $anzeigedata[] = "";                                                    // id
    $anzeigedata[] = $_POST['werbeid'];                                     // werbeid
    $anzeigedata[] = $_POST['anzeigestatus'];                               // status
    $anzeigedata[] = $_POST['keyanzeige'];                                  // key
    $anzeigedata[] = serialize($bildname);                                  // bild
    $anzeigedata[] = $_POST['anzeigetitel'];                                // titel
    $anzeigedata[] = $_POST['vorname'];                                     // vorname
    $anzeigedata[] = $_POST['name'];                                        // name
    $anzeigedata[] = $_POST['strasse'];                                     // strasse
    $anzeigedata[] = $_POST['plz'];                                         // plz
    $anzeigedata[] = $_POST['ort'];                                         // ort
    $anzeigedata[] = $_POST['land'];                                        // land
    $anzeigedata[] = $_POST['tel'];                                         // tel
    $anzeigedata[] = $_POST['fax'];                                         // fax
    $anzeigedata[] = $_POST['homepage'];                                    // homepage
    $anzeigedata[] = $tParser->getParsedText();                             // text
    $anzeigedata[] = serialize($_POST['suchworte']);                        // suchwort
    $anzeigedata[] = $_POST['anzeigefachgebiete'];                          // fachgebiete
    $anzeigedata[] = $_POST['anzeigefirmname'];                             // firmname
    $anzeigefac->write($anzeigedata);
    
    // neu geschriebenen Datensatz hohlen, f�r AnzeigeId
    if($last_id)// schon alte Anzeige da
    {
        
       $anzeigefac->getLastByWerbeId($_POST['werbeid']);
       $newAnzeige = $anzeigefac->getElement();
       if($last_id != $newAnzeige->id) $AnzeigeId = $newAnzeige->id; // neuer Datensatz f�r Werbeid geschrieben
    }
    else 
    {
       $anzeigefac->getLastByWerbeId($_POST['werbeid']);
       if($newAnzeige = $anzeigefac->getElement()) $AnzeigeId = $newAnzeige->id; // neuer Datensatz f�r Werbeid geschrieben
    }
    
    if($AnzeigeId)
    {
        // Datens�tze in Tabelle anzeigefachgebiete schreiben
        $anzeigefachfac = new Anzeigefachgebiete();
            
        for($i=0;$i<10;$i++)
        {
            if($_POST['fachgebiete'][$i]['fach'] != '')
            {
                // anzeigeid / nr / fachid / plz
                $anzeigefachfac->insertTupel($AnzeigeID, $i, $_POST['fachgebiete'][$i]['fach'], $_POST['fachgebiete'][$i]['plz']);
            }
        }
        
        //Datenarray f�r Anzeigeordzeiten
         // neuer Datensatz bzw. neue Datens�tze schreiben
        for($i=1;$i<8;$i++)
        {
            if($_POST['day'.$i])
            {
                unset($odata);
                unset($ordzeitenfac1);
                $ordzeitenfac1 = new Anzeigeordzeiten(); 
                $odata[] = "";                       // id
                $odata[] = $POST['werbeid'];         // firmid
                $odata[] = $i;                       // daystart
                $odata[] = $i;                       // dayend
                $odata[] = $_POST['time1'.$i];       // timestart
                $odata[] = $_POST['time2'.$i];       // timeend
                $odata[] = $_POST['memo'.$i];        // timeend
                $odata[] = $AnzeigeId;               // anzeigeid
                $ordzeitenfac1->write($odata);
            }
            if($_POST['day'.$i] && $_POST['time3'.$i] != '')
            {  
                unset($o2data);
                unset($ordzeitenfac2);
                $ordzeitenfac2 = new Anzeigeordzeiten(); 
                $o2data[] = "";                       // id
                $o2data[] = $POST['werbeid'];         // firmid
                $o2data[] = $i;                       // daystart
                $o2data[] = $i;                       // dayend
                $o2data[] = $_POST['time3'.$i];       // timestart
                $o2data[] = $_POST['time4'.$i];       // timeend
                $o2data[] = $_POST['memo'.$i];        // timeend
                $o2data[] = $AnzeigeId;               // anzeigeid
                $ordzeitenfac2->write($o2data);
            }
        }   // end for
        $_SESSION['msg'] .= 'Anzeige erfolgreich geschrieben!<br />';
    }// end if($AnzeigeId)
    else $_SESSION['msg'] .= 'Fehler beim Anzeige schreiben!<br />';                                                     
}// if ($_GET['mode']=="new" && !$_SESSION['err'])

//////////////////////////////////////////////////////////////////////////////////////////////////
// Datens�tze �ndern
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="update" && !$_SESSION['err'])
{
    // Update der Anzeige 
    $anzeigefac = new Anzeige();
    $anzeigefac->getById($_GET["id"]);
    $daten = $anzeigefac->getElement();
    
    // Wenn Bilder vorhanden sind, dann als Array einlesen
    if (is_array(unserialize($daten->bild))) $bildname=unserialize($daten->bild);
    else $bildname=array();
    
    // Bildkommentare einlesen;
    $bildkommentar=$_POST['bildkommentar'];
   
    // Schauen ob Bilder gel�scht werden sollen
    for ($x=5;$x>-1;$x--)
    {
        // eventuell altes Bild l�schen
        if($_POST['bildloeschen'.$x])
        {
            // Grafiktyp auslesen
            $typ=getimagesize(LOCALDIR."images/werbung/".$bildname[$x]['url']) ;
      
            // je nach typ Endung bestimmen   
            if ($typ[2]==1) $endung=".gif";
            if ($typ[2]==2) $endung=".jpg"; 
            
            // Bild und Thumb l�schen
       	    unlink(LOCALDIR."images/werbung/".$bildname[$x]['url']);
            unlink(LOCALDIR."images/werbung/".basename($bildname[$x]['url'],$endung)."_thumb".$endung);
            
            // Bildurl und Name l�schen
            unset($bildname[$x]);
            unset($bildkommentar[$x]);
            
            // Arrays neu aufbauen und durchnummerieren
            $bildname=array_values($bildname);     
            $bildkommentar=array_values($bildkommentar);  
        }  
    }        
    // Bildkommentare aktualisieren
    for ($x=0;$x<sizeof($bildname);$x++)
    {
        $bildname[$x]['bildkommentar']=$bildkommentar[$x];
    }
    
    // neue Bilder hochladen     
    if ($_FILES['bildneu'] != "") // Bild ausgew�hlt
    {                        
        // �berpr�fung des Bildtyps
        $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);
 
        // �berpr�fung des Bildtyps
        if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
        {
            if ($filetyp[2]==2) $type = '.jpg';
            if ($filetyp[2]==1) $type = '.gif';
                   
            // neues Bild hochladen
            $key=generateRandomKey(10);
            $pic = $anzeigefac->writePic($_FILES['bildneu']["tmp_name"],$key,$type);
  	        $bildname[]= array("url"=>$key.$type,
                     "bildkommentar"=>$_POST['bildkommentarneu']);
        }

        //Wenn falscher Bildtyp 
        else 
        {    
           $error= $error."[Bild -- falschen Typ]";
        }
    }
        
    // Beschreibung parsen
    $tParser = new TextParser($_POST['text']);
    $tParser->parse();
    
    // Datensatz updaten
    $anzeigefac->update(" `status`='".$_POST['anzeigestatus']."' ,
                          `key`='".$_POST['keyanzeige']."' , 
                          `titel`='".$_POST['anzeigetitel']."' ,
                          `name`='".$_POST['name']."',
                          `vorname`='".$_POST['vorname']."',
                          `strasse`='".$_POST['strasse']."', 
                          `plz`='".$_POST['plz']."',
                          `ort`='".$_POST['ort']."',
                          `land`='".$_POST['land']."',
                          `tel`='".$_POST['tel']."',
                          `fax`='".$_POST['fax']."',
                          `homepage`='".$_POST['homepage']."', 
                          `text`='".$tParser->getParsedText()."' , 
                          `bild`='".serialize($bildname)."' , 
                          `suchwort`='".serialize($_POST['suchworte'])."'  , 
                          `fachgebiete`='".$_POST['anzeigefachgebiete']."' ,
                          `firmname`='".$_POST['anzeigefirmname']."'");
    
    // Update der Anzeigefachgebiete 
    $anzeigefachfac = new Anzeigefachgebiete();
    // Postfelder durchgehen
    for($i=0;$i<10;$i++)
    {
        // Daten da reinschreiben bzw. updaten
        if($_POST['fachgebiete'][$i]['fach'] != '')
        {
            // schon Datensatz da ?
            $anzeigefachfac->getByAnzeigeIdAndNr($_GET["id"],$i);
            if($anzeige = $anzeigefachfac->getElement())
            {
                //updaten
                $anzeigefachfac->update(" `fachid`='".$_POST['fachgebiete'][$i]['fach']."' , `plz`='".$_POST['fachgebiete'][$i]['plz']."'");
            }
            else // reinschreiben
            {
                // anzeigeid / nr / fachid / plz
                $anzeigefachfac->insertTupel($_GET["id"], $i, $_POST['fachgebiete'][$i]['fach'], $_POST['fachgebiete'][$i]['plz']);
            }
        }
        else // bzw. l�schen vorhandener Datens�tze
        {
            $anzeigefachfac->getByAnzeigeIdAndNr($_GET["id"],$i);
            $anzeigefachfac->deleteElement();
        }
    }
        
    // alte Anzeigeordzeiten l�schen neue Datens�tze schreiben
    $ordzeitenfac = new Anzeigeordzeiten(); 
    $ordzeitenfac->getByAnzeigeId($_GET["id"]);
   
    while($ordzeit = $ordzeitenfac->getElement())
    {
        $ordzeitenfac->deleteElement(); // alte Datens�tze l�schen
    }
    
    // neuen Datensatz bzw. neue Datens�tze schreiben
    for($i=1;$i<8;$i++)
    {
        if($_POST['day'.$i])
        {
            unset($odata);
            unset($ordzeitenfac1);
            $ordzeitenfac1 = new Anzeigeordzeiten(); 
            $odata[] = "";                       // id
            $odata[] = $_POST['werbeid'];        // firmid
            $odata[] = $i;                       // daystart
            $odata[] = $i;                       // dayend
            $odata[] = $_POST['time1'.$i];       // timestart
            $odata[] = $_POST['time2'.$i];       // timeend
            $odata[] = $_POST['memo'.$i];        // timeend
            $odata[] = $_GET['id'];              // anzeigeid
            $ordzeitenfac1->write($odata);
        }
        if($_POST['day'.$i] && $_POST['time3'.$i] != '')
        {  
            unset($o2data);
            unset($ordzeitenfac2);
            $ordzeitenfac2 = new Anzeigeordzeiten(); 
            $o2data[] = "";                       // id
            $o2data[] = $_POST['werbeid'];        // firmid
            $o2data[] = $i;                       // daystart
            $o2data[] = $i;                       // dayend
            $o2data[] = $_POST['time3'.$i];       // timestart
            $o2data[] = $_POST['time4'.$i];       // timeend
            $o2data[] = $_POST['memo'.$i];        // timeend
            $o2data[] = $_GET['id'];              // anzeigeid
            $ordzeitenfac2->write($o2data);
        }
    }        
    $_SESSION['msg'] .= 'Anzeige erfolgreich ge�ndert!<br />';
} // end if ($_GET['mode']=="update" && !$_SESSION['err'])

//////////////////////////////////////////////////////////////////////////////////////////////////
// Anzeigestatus �ndern - Aufruf von anzeige.view.php
//////////////////////////////////////////////////////////////////////////////////////////////////
if ($_GET['mode']=="changestatus")
{
    // Datensatz holen
    $anzeigefac = new Anzeige();
    $anzeigefac->getById($_GET["id"]);
    $daten = $anzeigefac->getElement();
    if($daten)
    {
        // Status bestimmen und Datensatz upaten
        if ($daten->status == "1") $anzeigefac->update("status='0'");  
        else $anzeigefac->update("`status`='1'");
    } 
}
   
if(!$_SESSION['err']) header("Location:anzeige.view.php?sv=1");
?>
