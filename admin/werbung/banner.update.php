<?php
////////////////////////////////////////////////////////////////////////////////
// admin/werbung/banner.update.php regelt das update der anzeigebanner Tabelle
////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");

//echo "<pre>"; print_r($_POST); print_r($_GET); print_r($_FILES); echo "</pre>";

// Daten pr�fen
unset($_SESSION['err']);

if (!$_GET['mode']) $_SESSION['err'] .= "Aufruffehler!<br />";

// Bild pr�fen
if ($_FILES['bildneu'] && $_FILES['bildneu']['name'] != "")// Bild ausgew�hlt
{                        
    $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);

    // �berpr�fung des Bildtyps
    if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
    {
       // hier nicht hochladen --- erst sp�ter schreiben
    }
    //Wenn falscher Bildtyp 
    else 
    {    
        $_SESSION['err'] .= "[Bild -- hat falschen Typ]<br/ >";
    }
}

// neuer Datensatz muss Bild ausw�hlen und immer ist Homepage n�tig
if($_FILES['bildneu']['name'] == "" && $_GET['mode']=="new") $_SESSION['err'] .= "Kein Bild ausgew�hlt!<br/ >";
if($_POST['homepage'] == "") $_SESSION['err'] .= "Verlinkte Webseite ist n�tig!<br/ >";

// Bei Fehler zur�ck
if($_SESSION['err'])
{
    if($_GET['mode']=='update') header("Location:./banner.edit.php?mode=update&id=".$_GET['id']."&sv=1");
    elseif($_GET['mode']=='new') header("Location:./banner.edit.php?mode=new&werbeid=".$_POST['werbeid']."&sv=1");
    else header("Location:./banner.view.php?sv=1");
}

////////////////////////////////////////////////////////////////////////////////
// Neuer Datensatz
////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="new" && !$_SESSION['err'])
{
    // neuer Datensatz in Tabelle anzeigebanner
    
    // neues Anzeigeprofil schreiben
    $anzeigebannerfac = new Anzeigebanner();
        
    if ($_FILES['bildneu'] && $_FILES['bildneu']['name'] != "")// Bild ausgew�hlt
    {                        
        $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);
    
        // �berpr�fung des Bildtyps
        if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
        {
            if ($filetyp[2]==2) $type = '.jpg';
            if ($filetyp[2]==1) $type = '.gif';
      		                
            // neues Bild hochladen
            $key=generateRandomKey(10);
       	    $pic = $anzeigebannerfac->writePic($_FILES['bildneu']["tmp_name"],$key,$type);
            $bildname = $key.$type;
        }
    }
    else $bildname = '';
        
    // bannerarray
    $bannerdata[] = "";                                                    // id
    $bannerdata[] = $_POST['werbeid'];                                     // werbeid
    $bannerdata[] = $_POST['bannerstatus'];                                // status
    $bannerdata[] = $bildname;                                             // bild 
    $bannerdata[] = $_POST['homepage'];                                    // homepage
    $bannerdata[] = "";                                                    // show
    $bannerdata[] = "";                                                    // klick
    $bannerdata[] = 2;                                                     // bannerkey    2 = 33%
    $bannerdata[] = $_POST['bannertyp'];                                   // bannertyp  
    $anzeigebannerfac->write($bannerdata);
    
    $_SESSION['msg'] .= 'Werbebanner erfolgreich geschrieben!<br />';
}// if ($_GET['mode']=="new" && !$_SESSION['err'])

////////////////////////////////////////////////////////////////////////////////
// Datens�tze �ndern
////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="update" && !$_SESSION['err'])
{
    // datensatz einlesen
    // Update des Anzeigebanner 
    $anzeigebannerfac = new Anzeigebanner();
    $anzeigebannerfac->getById($_GET["id"]);
    $daten = $anzeigebannerfac->getElement();
    
    // eventuell altes Bild l�schen
    if($_FILES['bildneu'] != "" && $_FILES['bildneu']['name'] != "" && $daten->bild !='')
    {
        // Grafiktyp auslesen
        $typ=getimagesize(LOCALDIR."images/werbung/".$daten->bild) ;
  
        // je nach typ Endung bestimmen   
        if ($typ[2]==1) $endung=".gif";
        if ($typ[2]==2) $endung=".jpg"; 
        
        // Bild und Thumb l�schen
   	    unlink(LOCALDIR."images/werbung/".$daten->bild);
        unlink(LOCALDIR."images/werbung/".basename($daten->bild,$endung)."_half".$endung);
    }  
    
    // neues Bild hochladen     
    if ($_FILES['bildneu'] && $_FILES['bildneu']['name'] != "") // Bild ausgew�hlt
    {                        
        // �berpr�fung des Bildtyps
        $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);
 
        // �berpr�fung des Bildtyps
        if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
        {
            if ($filetyp[2]==2) $type = '.jpg';
            if ($filetyp[2]==1) $type = '.gif';
                   
            // neues Bild hochladen
            $key=generateRandomKey(10);
            $pic = $anzeigebannerfac->writePic($_FILES['bildneu']["tmp_name"],$key,$type);
  	        $bildname = $key.$type;
            
            // Datensatz updaten
            $anzeigebannerfac->update(" `status`='".$_POST['bannerstatus']."' ,
                                        `homepage`='".$_POST['homepage']."', 
                                        `bild`='".$bildname."'"); 
        }
    }
    else    $anzeigebannerfac->update(" `status`='".$_POST['bannerstatus']."' ,
                                        `homepage`='".$_POST['homepage']."'");  // Datensatz updaten ohne Bild
    
         
    $_SESSION['msg'] .= 'Werbebanner erfolgreich ge�ndert!<br />';
} // end if ($_GET['mode']=="update" && !$_SESSION['err'])

////////////////////////////////////////////////////////////////////////////////
// Bannerstatus �ndern - Aufruf von banner.view.php oder aus kunden.edit.php
////////////////////////////////////////////////////////////////////////////////
if ($_GET['mode']=="changestatus")
{
    // Datensatz holen
    $anzeigebannerfac = new Anzeigebanner();
    $anzeigebannerfac->getById($_GET["id"]);
    $daten = $anzeigebannerfac->getElement();
    if($daten)
    {
        // Status bestimmen und Datensatz upaten
        if ($daten->status == "1") $anzeigebannerfac->update("status='0'");  
        else $anzeigebannerfac->update("`status`='1'");
    } 
}
   
if(!$_SESSION['err']) header("Location:banner.view.php?sv=1");

?>
