<?php
////////////////////////////////////////////////////////////////////////////////
// werbung/festeplaetze.edit.php - ZeigtFormular zum �ndern der Tabelle anzeigeort an
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

unset($stopp);

if ($_GET['mode']=="update")
{
    $anzeigeorttestfac = new Anzeigeort();
    if($_GET['id'])
    {
        $anzeigeorttestfac->getById($_GET["id"]);
        
    }
        
    if(!$anzeigeorttest = $anzeigeorttestfac->getElement())
    {
        $_SESSION['err'] = '<br/>Der von Ihnen aufgerufene Datensatz existiert nicht!<br />';
        $stopp = "ENDE";
    }
}

if($stopp == "ENDE")
{
    echo '<h1>Fehler</h1>';
    if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}
    echo $l->makeLink("Zur�ck zur Feste Pl�tze -�bersicht", "./festeplaetze.view.php", "backlink");
    include(INCLUDEDIR."footer.inc.php");
}
else    // weitermachen
{
    ?>
    <h1>Feste Pl�tze <?php  if($_GET['mode'] == 'update') echo 'bearbeiten'; else echo 'erstellen'; ?></h1>
    <?php if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}?>
    <br />
    
    <form action="<?php echo $l->makeFormLink(WEBDIR.'admin/werbung/festeplaetze.update.php?mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post" >
        <fieldset>
            <?php
            $anzeigeortfac = new Anzeigeort();
            $anzeigeortfac->getById($_GET['id']);
            $anzeigeort = $anzeigeortfac->getElement();
            
            $kategoriefac = new Fach();
            $kategoriefac->getAll();
            
            $anzeigefac = new Anzeige();
            $anzeigefac->getAllAktiv();
            
            ?>
            
            <legend>
                Feste Plaetze Daten
            </legend>
            <label for="fachid">
               Fachgebiet
            </label>
            <select name="fachid" id="fachid">
            <option value="">---</option>
            <?php
            while ($kategorien = $kategoriefac->getElement())
            {
                $selected="";
                if ($kategorien->id == $anzeigeort->fachid) $selected='selected="selected"';
                echo '<option value="'.$kategorien->id.'" '.$selected.' >'.$kategorien->name.'</option>';
            }
            ?>
            </select>
            
            <br /><br />
            <label for="ort">
                Ort
            </label>
            <input id="ort" name="ort" type="text" value="<?php echo $anzeigeort->ort; ?>"/>
            <br class="clr" />
            <label for="plz">
                PLZ
            </label>
            <input id="plz" name="plz" type="text" value="<?php echo $anzeigeort->plz; ?>"/>
            <br class="clr" />
            <label for="platz1">
                Platz1
            </label>
            <select name="platz1" id="platz1">
            <option value="">---</option>
            <?php
            while ($anzeige = $anzeigefac->getElement())
            {
                $selected="";
                if ($anzeige->id == $anzeigeort->platz1) $selected='selected="selected"';
                echo '<option value="'.$anzeige->id.'" '.$selected.' >'.$anzeige->id.' '.$anzeige->vorname.' '.$anzeige->name.'</option>';
            }
            $anzeigefac->pointer = 0;    // Pointer auf Anfang der Liste
            ?>
            </select>
            <br class="clr" />
            <label for="platz2">
                Platz2
            </label>
            <select name="platz2" id="platz2">
            <option value="">---</option>
            <?php
            while ($anzeige = $anzeigefac->getElement())
            {
                $selected="";
                if ($anzeige->id == $anzeigeort->platz2) $selected='selected="selected"';
                echo '<option value="'.$anzeige->id.'" '.$selected.' >'.$anzeige->id.' '.$anzeige->vorname.' '.$anzeige->name.'</option>';
            }
            $anzeigefac->pointer = 0;    // Pointer auf Anfang der Liste
            ?>
            </select>
            <br class="clr" />
            <label for="platz3" >
                Platz3
            </label>
            <select name="platz3" id="platz3">
            <option value="">---</option>
            <?php
            while ($anzeige = $anzeigefac->getElement())
            {
                $selected="";
                if ($anzeige->id == $anzeigeort->platz3) $selected='selected="selected"';
                echo '<option value="'.$anzeige->id.'" '.$selected.' >'.$anzeige->id.' '.$anzeige->vorname.' '.$anzeige->name.'</option>';
            }
            $anzeigefac->pointer = 0;    // Pointer auf Anfang der Liste
            ?>
            </select>
            <br class="clr" />
         </fieldset>
         <br class="clr"/><br/>
         <input type="submit" value="Daten speichern" />
         <?php echo $l->makeLink("Zur�ck zur Feste Pl�tze �bersicht", "./festeplaetze.view.php", "backlink"); ?><?php echo $l->makeLink("Zur�ck zur Kunden �bersicht", "./kunden.view.php", "backlink"); ?>
        <br class="clr"/><br/>
 </form>
    <?php
    include(INCLUDEDIR."footer.inc.php");
} // end else zu if($stopp == "ENDE")
?>