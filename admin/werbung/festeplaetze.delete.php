<?php
////////////////////////////////////////////////////////////////////////////////
// admin/werbung/festeplaetze.delete.php - L�schen von Tupel aus der Tabelle anzeigeort
////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");

if ($_POST["submit"])
{
    if ($_POST["mode"] == "delete_yes")
    {
        $anzeigeortfac = new Anzeigeort();
        $anzeigeortfac->getById($_POST["id"]);
        $anzeigeortfac->deleteElement();
        header("Location: ./festeplaetze.view.php?sv=1");
    }
}

//  Sicherheitsabfrage
////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");?>
    <h1>Feste Pl�tze l�schen</h1>
    <?php
    $anzeigeortfac = new Anzeigeort();
    $anzeigeortfac->getById($_GET["id"]);
    $anzeigeort = $anzeigeortfac->getElement();?>
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
        <fieldset>
            <legend>Soll dieser Feste Pl�tze Datensatz wirklich gel�scht werden?</legend>
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
            Datensatz Id: <b><?php echo $anzeigeort->id;?></b><br/><br />
            zur Fachid: <b><?php echo $anzeigeort->fachid.'</b> f�r Ort <b>'.$anzeigeort->ort.'</b>und Plz <b>'.$anzeigeort->plz; ?></b><br/><br />
        </fieldset>
        <br />
        <input type="submit" value="L�schen" name="submit" class="submit" />
        <?php echo $l->makeLink("Zur�ck zur Feste Pl�tze- �bersicht", "./festeplaetze.view.php", "backlink");?>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php"); 
} ?>