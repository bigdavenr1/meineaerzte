<?php
////////////////////////////////////////////////////////////////////////////////
// admin/werbung/banner.delete.php - L�schen von bannern aus der Tabelle
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

if ($_POST["submit"])
{
    if ($_POST["mode"] == "delete_yes")
    {
        $anzeigebannerfac = new Anzeigebanner();
        $anzeigebannerfac->getById($_POST["id"]);
        $anzeigebanner = $anzeigebannerfac->getElement();
        // Bilder von Banner aus Werbeordner l�schen
        if($anzeigebanner->bild != '')
        {
            // Grafiktyp auslesen
            $typ=getimagesize(LOCALDIR."images/werbung/".$anzeigebanner->bild) ;
      
            // je nach typ Endung bestimmen   
            if ($typ[2]==1) $endung=".gif";
            if ($typ[2]==2) $endung=".jpg"; 
            
            // Banner und Halfbanner l�schen
       	    unlink(LOCALDIR."images/werbung/".$anzeigebanner->bild);
            unlink(LOCALDIR."images/werbung/".basename($anzeigebanner->bild,$endung)."_half".$endung);
        }
        
        // Banner l�schen
        $anzeigebannerfac->deleteElement();
        header("Location: ./banner.view.php?sv=1");
    }
}

//  Sicherheitsabfrage
////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");?>
    <h1>Banner l�schen</h1>
    <?php
    $anzeigebannerfac = new Anzeigebanner();
    $anzeigebannerfac->getById($_GET["id"]);
    $anzeigebanner = $anzeigebannerfac->getElement();?>
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
        <fieldset>
            <legend>Soll dieses Banner wirklich gel�scht werden?</legend>
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
            Banner Id: <b><?php echo $anzeigebanner->id;?></b><br/><br />
            zur Webpage: <b><?php echo $anzeigebanner->homepage.' f�r Webkunden mit Id:'.$anzeigebanner->werbeid; ?></b><br/><br />
        </fieldset>
        <br />
        <input type="submit" value="L�schen" name="submit" class="submit" />
        <?php echo $l->makeLink("Zur�ck zur Banner- �bersicht", "./banner.view.php", "backlink");?>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php"); 
} ?>