<?php
////////////////////////////////////////////////////////////////////////////////
// admin/werbung/anzeige.delete.php - L�schen von anzeigen aus der Tabelle
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

if ($_POST["submit"])
{
    if ($_POST["mode"] == "delete_yes")
    {
        // Datens�tze aus Anzeigeordzeiten l�schen
        $ordzeitenfac = new Anzeigeordzeiten(); 
        $ordzeitenfac->getByAnzeigeId($_POST["id"]);
        while($ordzeit = $ordzeitenfac->getElement())
        {
            $ordzeitenfac->deleteElement(); // alte Datens�tze l�schen
        }
        
        // Datens�tze aus Anzeigefachgebiete l�schen
        $anzeigefachfac = new Anzeigefachgebiete();
        $anzeigefachfac->getByAnzeigeId($_POST["id"]);
        while($anzeigefach = $anzeigefachfac->getElement())
        {
            $anzeigefachfac->deleteElement(); // alte Datens�tze l�schen
        }
        
        // Datens�tze aus Anzeigeort updaten
        $anzeigeortfac = new Anzeigeort();
        // platz1 updaten
        $anzeigeortfac->getByAnzeigeIdPlatz1($_POST["id"]);
        while($anzeigeort = $anzeigeortfac->getElement())
        {
            $anzeigeortfac->update(" `platz1`=''"); // alte Pl�tze �berschreiben
        }
        // platz1 updaten
        $anzeigeortfac->getByAnzeigeIdPlatz2($_POST["id"]);
        while($anzeigeort = $anzeigeortfac->getElement())
        {
            $anzeigeortfac->update(" `platz2`=''"); // alte Pl�tze �berschreiben
        }
        // platz1 updaten
        $anzeigeortfac->getByAnzeigeIdPlatz3($_POST["id"]);
        while($anzeigeort = $anzeigeortfac->getElement())
        {
            $anzeigeortfac->update(" `platz3`=''"); // alte Pl�tze �berschreiben
        }
        
        // Anzeige l�schen
        $anzeigefac = new Anzeige();
        $anzeigefac->getById($_POST["id"]);
        
        // Bilder von Anzeige aus Werbeordner l�schen
        $daten = $anzeigefac->getElement();
    
        // Wenn Bilder vorhanden sind, dann als Array einlesen
        if (is_array(unserialize($daten->bild))) 
        {
            $bildname=unserialize($daten->bild);
            // Schauen ob Bilder da sind , wenn ja l�schen
            for ($x=5;$x>-1;$x--)
            {
                // eventuell altes Bild l�schen
                if($bildname[$x]['url'] && $bildname[$x]['url'] != '')
                {
                    // Grafiktyp auslesen
                    $typ=getimagesize(LOCALDIR."images/werbung/".$bildname[$x]['url']) ;
              
                    // je nach typ Endung bestimmen   
                    if ($typ[2]==1) $endung=".gif";
                    if ($typ[2]==2) $endung=".jpg"; 
                    
                    // Bild und Thumb l�schen
               	    unlink(LOCALDIR."images/werbung/".$bildname[$x]['url']);
                    unlink(LOCALDIR."images/werbung/".basename($bildname[$x]['url'],$endung)."_thumb".$endung);
                }  
            }        
        }
        $anzeigefac->deleteElement();
        header("Location: ./anzeige.view.php?sv=1");
    }
}

//  Sicherheitsabfrage
////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");?>
    <h1>Anzeige l�schen</h1>
    <?php
    $anzeigefac = new Anzeige();
    $anzeigefac->getById($_GET["id"]);
    $anzeige = $anzeigefac->getElement();?>
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
        <fieldset>
            <legend>Soll diese Anzeige und die Datens�tze in den verbundenen Tabellen wirklich gel�scht werden?</legend>
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
            Anzeige Id: <b><?php echo $anzeige->id;?></b><br/><br />
            von: <b><?php echo $anzeige->titel.' '.$anzeige->name.', '.$anzeige->vorname;?></b><br/><br />
        </fieldset>
        <br />
        <input type="submit" value="L�schen" name="submit" class="submit" />
        <?php echo $l->makeLink("Zur�ck", "./anzeige.view.php", "backlink");?>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php"); 
} ?>