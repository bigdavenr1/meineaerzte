<?php
////////////////////////////////////////////////////////////////////////////////
// werbung/kunden.update.php regelt das update der benutzer und anzeige Tabellen
////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");
$kundenfac = new Benutzer();

//echo "<pre>"; print_r($_POST); echo "</pre>";


if (!$_GET['mode']) $_GET['mode']="new";

// Daten pr�fen
unset($_SESSION['err']);

// wenn neuer Datensatz, pr�fen auf Kundennummerndopplungen
if($_GET['mode']=='new')
{  
    // Kundennummer da 
    if(!$_POST['kundennummer'] || $_POST['kundennummer'] == "")
    {
        $_SESSION['err'] .= 'Eine Kundennummer ist n�tig!<br />';
    }
    // Kundennummer pr�fen
    if($_POST['kundennummer'] != "")
    {
        $kundenfac->getByMail($_POST['kundennummer']);
        if($pruefdaten = $kundenfac->getElement()) $_SESSION['err'] .= 'F�r diese Kundennummer gibt es schon ein Benutzer!<br />';
    }
}

// wenn update Datensatz und Kundennummer bzw. Mail auf Dopplungen pr�fen
if($_GET['mode']=='update')
{   
   $kundenfac->getById($_GET["id"]);
   $kunde = $kundenfac->getElement(); 
   
   // Kundennummer da 
    if(!$_POST['kundennummer'] || $_POST['kundennummer'] == "")
    {
        $_SESSION['err'] .= 'Eine Kundennummer ist n�tig!<br />';
    }
    // Kundennummer pr�fen
    if($_POST['kundennummer'] != "")
    {
        if($kunde->email != $_POST['kundennummer'])
        {
            $kundenfac->getByMail($_POST['kundennummer']);
            if($pruefdaten = $kundenfac->getElement()) $_SESSION['err'] .= 'F�r diese Kundennummer gibt es schon ein Benutzer!<br />';
        }
    }
}

// Bei Fehler zur�ck
if($_SESSION['err'])
{
    if($_GET['mode']=='update') header("Location:kunden.edit.php?mode=update&id=".$_GET['id']."&sv=1");
    elseif($_GET['mode']=='new') header("Location:kunden.edit.php?mode=new&sv=1");
    else header("Location:kunden.view.php?sv=1");
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Neuer Datensatz
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="new" && !$_SESSION['err'])
{
    // neuer Datensatz in Tabellen benutzer
    
    // Array f�r Benutzerdatenbank
    $salt=generateRandomKey(8);
    $benutzerdata[]="";                                                         //id
    $benutzerdata[]=$_POST['anrede'];                                           //anrede
    $benutzerdata[]=$_POST['vorname'];                                          //vorname
    $benutzerdata[]=$_POST['name'];                                             //nachname
    $benutzerdata[]=md5($_POST['passwd'].$salt);                                //pass
    $benutzerdata[]=$salt;                                                      //salt
    $benutzerdata[]=$_POST['aktiv'];                                            //aktiv
    $benutzerdata[]="werbung";                                                  //typ
    $benutzerdata[]=$_POST['kundennummer'];                                     //email
    $benutzerdata[]=$_POST['strasse'];                                          //adresse
    $benutzerdata[]=$_POST['plz'];                                              //plz
    $benutzerdata[]=$_POST['ort'];                                              //ort
    $benutzerdata[]=$_POST['land'];                                             //land
    $benutzerdata[]=$_POST['tel'];                                              //tel
    $benutzerdata[]=$_POST['fax'];                                              //fax
    $benutzerdata[]="";                                                         //vorwahl
    $benutzerdata[]=$_POST['homepage'];                                         //homepage
    $benutzerdata[]=time();                                                     //anmeldedatum
    $benutzerdata[]="";                                                         //aktivierungskey
    $benutzerdata[]=$_POST['passwd'];                                           //nickname
    $benutzerdata[]="";                                                         //geburtstag
    $benutzerdata[]="";                                                         //bild
    $benutzerdata[]="";                                                         //rechte
    $benutzerdata[]="";                                                         //freigabe
    $benutzerdata[]=$_POST['titel'];                                            //titel

    $benutzerfac= new Benutzer();
    $benutzerfac->write($benutzerdata);
    
}// if ($_GET['mode']=="new" && !$_SESSION['err'])

//////////////////////////////////////////////////////////////////////////////////////////////////
// Datens�tze �ndern
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="update" && !$_SESSION['err'])
{
    // datensatz einlesen
    $kundenfac = new Benutzer(1); 
    $kundenfac->getWerbekundenById($_GET["id"]);
    $kunde = $kundenfac->getElement();
    
    // Update der UserTabelle
    if($kunde)
    {        
        // Update der Logintabelle
        $kundenfac->update("`email`='".$_POST['kundennummer']."' , 
                            `pass`='".md5($_POST['passwd'].$salt)."' , 
                            `salt`='".$salt."' , 
                            `aktiv`='".$_POST['aktiv']."' , 
                            `anrede`='".$_POST['anrede']."' , 
                            `titel`='".$_POST['titel']."',
                            `name`='".$_POST['name']."',
                            `vorname`='".$_POST['vorname']."',
                            `adresse`='".$_POST['strasse']."', 
                            `plz`='".$_POST['plz']."',
                            `ort`='".$_POST['ort']."',
                            `land`='".$_POST['land']."',
                            `tel`='".$_POST['tel']."',
                            `fax`='".$_POST['fax']."',
                            `homepage`='".$_POST['homepage']."',
                            `nickname`='".$_POST['passwd']."'");
     }
    
} // end if ($_GET['mode']=="update" && !$_SESSION['err'])

//////////////////////////////////////////////////////////////////////////////////////////////////
// Werbekunde Sperren und freischalten
//////////////////////////////////////////////////////////////////////////////////////////////////
    
if ($_GET['mode']=="changeaktiv")
{
    // Datensatz holen
    $kundenfac->getById($_GET["id"]);
    $daten = $kundenfac->getElement();
    
    // Status bestimmen und Datensatz upaten
    if ($daten->aktiv == "0") $newstatus="1"; else $newstatus="0"; 
    $kundenfac->update(" `aktiv`='".$newstatus."'");
      
}     
    
if(!$_SESSION['err'] && $_GET['mode']!="changestatus") header("Location:kunden.view.php?sv=1");
?>
