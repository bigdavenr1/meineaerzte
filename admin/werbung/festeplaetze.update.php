<?php
////////////////////////////////////////////////////////////////////////////////
// admin/werbung/festeplaetze.update.php regelt das update der anzeigeort Tabelle
////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");

//echo "<pre>"; print_r($_POST); print_r($_GET); echo "</pre>";

// Daten pr�fen
unset($_SESSION['err']);

if (!$_GET['mode']) $_SESSION['err'] .= "Aufruffehler!<br />";
if ($_GET['mode'] == 'update' && !$_GET['id']) $_SESSION['err'] .= "Keine Id zum updaten mitgegeben!<br />";


if(!$_POST['fachid']) $_SESSION['err'] .= "Fachgebiet muss ausgew�hlt sein!<br />";
if(!$_POST['ort'] && !$_POST['plz']) $_SESSION['err'] .= "Ort oder Plz m�ssen ausgef�llt sein!<br />";
if(!$_POST['platz1'] && !$_POST['platz2'] && !$_POST['platz3']) $_SESSION['err'] .= "Kein fester Platz ausgew�hlt!<br />";

if(!$_SESSION['err'] && $_GET['mode'] == 'new')
{
    // Ort / Plz / Dopplungen pr�fen
    $anzeigeortfac = new Anzeigeort();

    if ($_POST['ort'])
    {                        
        $anzeigeortfac->getByFachidAndOrt($_POST["fachid"],$_POST['ort']);
        if($daten = $anzeigeortfac->getElement()) $_SESSION['err'] .= 'F�r dieses Fachgebiet und Ort gibt es schon feste Pl�tze! Datensatz-Id: '.$daten->id.'<br />';
    }
    
    if ($_POST['plz'])
    {                        
        $anzeigeortfac->getByFachidAndPlz($_POST["fachid"],$_POST['plz']);
        if($daten = $anzeigeortfac->getElement()) $_SESSION['err'] .= 'F�r dieses Fachgebiet und Plz gibt es schon feste Pl�tze! Datensatz-Id: '.$daten->id.'<br />';
    }
}

if(!$_SESSION['err'] && $_GET['mode'] == 'update')
{
    // Ort / Plz / Dopplungen pr�fen
    $anzeigeortfac = new Anzeigeort();
    $anzeigeortfac->getById($_GET['id']);
    $anzeigeort = $anzeigeortfac->getElement();

    if ($_POST['ort'])
    {                        
        $anzeigeortfac->getByFachidAndOrt($_POST["fachid"],$_POST['ort']);
        while($daten = $anzeigeortfac->getElement())
        {
            if($daten->id != $anzeigeort->id) $_SESSION['err'] .= 'F�r dieses Fachgebiet und Ort gibt es schon feste Pl�tze! Datensatz-Id: '.$daten->id.'<br />';
        }
    }
    
    if ($_POST['plz'])
    {                        
        $anzeigeortfac->getByFachidAndPlz($_POST["fachid"],$_POST['plz']);
        while($daten = $anzeigeortfac->getElement())
        {
            if($daten->id != $anzeigeort->id) $_SESSION['err'] .= 'F�r dieses Fachgebiet und Plz gibt es schon feste Pl�tze! Datensatz-Id: '.$daten->id.'<br />';
        }
    }
}

//$_SESSION['err'] .= 'erst mal stoppen!<br />';

// Bei Fehler zur�ck
if($_SESSION['err'])
{
    if($_GET['mode']=='update') header("Location:./festeplaetze.edit.php?mode=update&id=".$_GET['id']."&sv=1");
    elseif($_GET['mode']=='new') header("Location:./festeplaetze.edit.php?mode=new&sv=1");
    else header("Location:./festeplaetze.view.php?sv=1");
}

////////////////////////////////////////////////////////////////////////////////
// Neuer Datensatz
////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="new" && !$_SESSION['err'])
{
    // neuen Datensatz schreiben
    $anzeigeortfac = new Anzeigeort();
    // ortarray
    $ortdata[] = "";                                                    // id
    $ortdata[] = $_POST['fachid'];                                      // fachid
    $ortdata[] = $_POST['ort'];                                         // ort
    $ortdata[] = $_POST['plz'];                                         // plz 
    $ortdata[] = $_POST['platz1'];                                      // platz1
    $ortdata[] = $_POST['platz2'];                                      // platz2
    $ortdata[] = $_POST['platz3'];                                      // platz3
    $anzeigeortfac->write($ortdata);
    
    $_SESSION['msg'] .= 'Feste Pl�tze erfolgreich geschrieben!<br />';
}// if ($_GET['mode']=="new" && !$_SESSION['err'])

////////////////////////////////////////////////////////////////////////////////
// Datens�tze �ndern
////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="update" && !$_SESSION['err'])
{
    // datensatz einlesen
    $anzeigeortfac = new Anzeigeort();
    $anzeigeortfac->getById($_GET["id"]);
    // Update des Datensatzes
    $anzeigeortfac->update(" `fachid`='".$_POST['fachid']."' ,
                             `ort`='".$_POST['ort']."' ,
                             `plz`='".$_POST['plz']."' ,
                             `platz1`='".$_POST['platz1']."' ,
                             `platz2`='".$_POST['platz2']."' ,
                             `platz3`='".$_POST['platz3']."'");  
         
    $_SESSION['msg'] .= 'Fester Platz erfolgreich ge�ndert!<br />';
} // end if ($_GET['mode']=="update" && !$_SESSION['err'])

   
if(!$_SESSION['err']) header("Location:festeplaetze.view.php?sv=1");

?>
