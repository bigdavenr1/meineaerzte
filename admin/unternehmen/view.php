<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// unternehmen.view.php - Zeigt die Tabelle unternehmen an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
if ($_GET['mode']=="new")
{ ?>
    <h1>neue Unternehmensvorschläge bearbeiten / freischalten</h1>
<?php 
}
elseif($_GET['mode']=="premium")
{ ?>
    <h1>neue Premiumanmeldungen bearbeiten / freischalten</h1>
<?php 
}
else
{ ?>
    <h1>Unternehmen anlegen / verwalten / freischalten</h1>
    <br/>
    <form method="get" action="<?php echo $l->makeFormLink(WEBDIR.'admin/unternehmen/edit.php');?>">
        <fieldset>
             <legend>
                 Unternehmen über Firm-ID aufrufen
             </legend>
             <label>Firm-ID</label><input type="text" name="id"/><br class="clr"/>
             <label>Kundennummer</label><input type="text" name="nummer"/><br class="clr"/>
             <br/>
             <input type="hidden" name="mode" value="update" />
             <input type="submit" value="Unternehmen aufrufen" class="submit"/>
        </fieldset>
    </form>
<?php
} ?>
<br class="clr" /><br />
<?php echo $l->makeLink($icon_info." <b>[ neues Unternehmen erstellen ]</b>",WEBDIR."admin/unternehmen/edit.php?mode=new");?>
<br/><br/>
<?php
if(!$_GET['mode'])
            {
                echo "Nur Namen mit Anfangsbuchstaben: ";
                       
                for($i=a;;$i++)
                {
                   $class="";
                   if ($_GET['buchstabe'] == $i) $class='style="color:#f00;font-size:13px;font-weight:bold;"';
                    
                   echo "&nbsp;".$l->makeLink($i,WEBDIR."admin/unternehmen/view.php?buchstabe=".$i ,"none","",$class)."&nbsp;";
                   
                   if($i == 'z') break;
                }
                if ($_GET['buchstabe'] == "") $class2='style="color:#f00;font-size:13px;font-weight:bold;"';
                echo "&nbsp;".$l->makeLink("alle",WEBDIR."admin/unternehmen/view.php" ,"none","",$class2)."&nbsp;";
            }

?>
 anzeigen.<br/><br/>
<table style="width:100%;">
    <tr>
        <th style="width:200px;">
        <?php
            // Name
            // $_GET['mode'] auswerten
            if($_GET['mode']) $modesv = '&amp;mode='.$_GET['mode'];
            else $modesv = '';
            if($_GET['sort']== "desc" && $_GET['sp']==1) echo $l->makeLink('Name '.$icon_sort_asc,WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=1".$modesv,"none");
            elseif($_GET['sort']== "asc" && $_GET['sp']==1) echo $l->makeLink('Name '.$icon_sort_desc,WEBDIR."admin/unternehmen/view.php?sort=desc&amp;sp=1".$modesv,"none");
            else echo $l->makeLink('Name ',WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=1".$modesv,"none");             
        ?>
        </th>
        <th >
        <?php
            // Datum
            if($_GET['sort']== "desc" && $_GET['sp']==2) echo $l->makeLink('Datum '.$icon_sort_asc,WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=2".$modesv,"none");
            elseif($_GET['sort']== "asc" && $_GET['sp']==2) echo $l->makeLink('Datum '.$icon_sort_desc,WEBDIR."admin/unternehmen/view.php?sort=desc&amp;sp=2".$modesv,"none");
            else echo $l->makeLink('Datum ',WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=2".$modesv,"none");             
        ?>
            <br/> <small>Anlage / Premium</small>
        </th>
        <th style="width:150px;">
        <?php
            // Ort
            if($_GET['sort']== "desc" && $_GET['sp']==3) echo $l->makeLink('Ort '.$icon_sort_asc,WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=3".$modesv,"none");
            elseif($_GET['sort']== "asc" && $_GET['sp']==3) echo $l->makeLink('Ort '.$icon_sort_desc,WEBDIR."admin/unternehmen/view.php?sort=desc&amp;sp=3".$modesv,"none");
            else echo $l->makeLink('Ort ',WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=3".$modesv,"none");             
        ?>
        </th>
        <th>
        <?php
            // eMail
            if($_GET['sort']== "desc" && $_GET['sp']==4) echo $l->makeLink('eMail '.$icon_sort_asc,WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=4".$modesv,"none");
            elseif($_GET['sort']== "asc" && $_GET['sp']==4) echo $l->makeLink('eMail '.$icon_sort_desc,WEBDIR."admin/unternehmen/view.php?sort=desc&amp;sp=4".$modesv,"none");
            else echo $l->makeLink('eMail ',WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=4".$modesv,"none");             
        ?>

        </th>
        <?php  if($_GET['mode']!="premium") // Bewer. und Aktivi.
        { ?>
        <th >
        <?php
            // Bewer.
            if($_GET['sort']== "desc" && $_GET['sp']==5) echo $l->makeLink('Bewer. '.$icon_sort_asc,WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=5".$modesv,"none");
            elseif($_GET['sort']== "asc" && $_GET['sp']==5) echo $l->makeLink('Bewer. '.$icon_sort_desc,WEBDIR."admin/unternehmen/view.php?sort=desc&amp;sp=5".$modesv,"none");
            else echo $l->makeLink('Bewer. ',WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=5".$modesv,"none");             
        ?>
        </th>
        <th>
        <?php
            // Aktivi.
            if($_GET['sort']== "desc" && $_GET['sp']==6) echo $l->makeLink('Aktivi. '.$icon_sort_asc,WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=6".$modesv,"none");
            elseif($_GET['sort']== "asc" && $_GET['sp']==6) echo $l->makeLink('Aktivi. '.$icon_sort_desc,WEBDIR."admin/unternehmen/view.php?sort=desc&amp;sp=6".$modesv,"none");
            else echo $l->makeLink('Aktivi. ',WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=6".$modesv,"none");             
        ?>
        </th>
        <?php 
        }
        else // Zahlungsweise
        {
        ?>
        <th >
            Zahlungsweise
        </th>
        <?php
        } ?>
        <th style="width:50px;">
        <?php
            // Status
            if($_GET['sort']== "desc" && $_GET['sp']==7) echo $l->makeLink('Status '.$icon_sort_asc,WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=7".$modesv,"none");
            elseif($_GET['sort']== "asc" && $_GET['sp']==7) echo $l->makeLink('Status '.$icon_sort_desc,WEBDIR."admin/unternehmen/view.php?sort=desc&amp;sp=7".$modesv,"none");
            else echo $l->makeLink('Status ',WEBDIR."admin/unternehmen/view.php?sort=asc&amp;sp=7".$modesv,"none");             
        ?>
        </th>
        <th style="width:50px;">
            Admin
        </th>
    </tr>    
<?php 
$unternehmenfac = new Daten(15);
// Sortierung
$sort=strtoupper($_GET['sort']);
$sortierfelder=array("1"=>'famname',
                     "2"=>'anlagedatum',
                     "3"=>'ort',
                     "4"=>'email',
                     "5"=>'bewertungsstatus',
                     "6"=>'aktiv',
                     "7"=>'status');

if ($sortierfelder[$_GET['sp']] && ($sort=="ASC" || $sort=="DESC"))
{
    $sort = " ORDER by ".$sortierfelder[$_GET['sp']]." ".$sort;
}
elseif($_GET['buchstabe'])
{
    $sort = " WHERE famname LIKE '".$_GET['buchstabe']."%' ".$sort ;
}
else $sort = ' ORDER by name ASC';

if ($_GET['mode']=="new")
{   
    //$unternehmenfac->getNew();
    $unternehmenfac->getNewSort($sort);
}
elseif ($_GET['mode']=="premium")
{
    //$unternehmenfac->getNewPremium();
    $unternehmenfac->getNewPremiumSort($sort);
}
else 
{
    $unternehmenfac->getAll($sort);
}

$x=0;
while ($daten = $unternehmenfac->getElement())
{
    $x++;
    ?>
    <tr >
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php /*echo substr($daten->name,0,100);*/?>
           <?php echo $daten->famname;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           A:<?php echo date("d.m.y",$daten->anlagedatum);?>
           <?php if ($daten->premiumdatum!="" && $daten->status=="P")echo 'P:'.date("d.m.y",$daten->premiumdatum);?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <?php echo $daten->ort;?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?>>
           <a href="mailto:<?php echo $daten->email;?>"><?php echo $daten->email;?></a>
       </td>
        <?php  if($_GET['mode']!="premium") // Bewer. und Aktivi.
        { ?>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/unternehmen/update.php?id=".$daten->id."&amp;mode=changebewertung")."&nbsp;";
            if ($daten->bewertungsstatus=="I") echo "<i>I</i>"; else echo '<b>A</b>'; ?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/unternehmen/update.php?id=".$daten->id."&amp;mode=changeaktiv&amp;email=".$daten->email)."&nbsp;";
            if ($daten->aktiv=="0") echo "<i>I</i>"; elseif ($daten->aktiv=="") echo '<b style="color:green">N</b>'; else echo "<b>A</b>" ;?>
       </td>
        <?php 
        }
        else // Zahlungsweise
        {
        ?>
        <th <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
            <?php  echo $daten->zahlungsweise; ?>
        </th>
        <?php
        } ?>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_refresh,WEBDIR."admin/unternehmen/update.php?id=".$daten->id."&amp;mode=changestatus")."&nbsp;";
            if ($daten->status=="F") echo "<i>F</i>"; elseif ($daten->status=="P") echo 'P'; elseif ($daten->status=="A") echo '<b style="color:green">A</b>'; elseif ($daten->status=="N") echo '<b style="color:red">N</b>';?>
       </td>
       <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
           <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/unternehmen/edit.php?mode=update&amp;id=".$daten->id)." ".$l->makeLink($icon_delete_small,WEBDIR."admin/unternehmen/delete.php?id=".$daten->id);?>
       </td>
   </tr>

<?php }?>

</table>

<br /><br />
<?php 
if ($x==0)
{
    if ($_GET['mode']=="new") echo "Keine neuen Unternehmensvorschläge eingetragen";
    elseif ($_GET['mode']=="premium") echo "Keine neuen Premiumanmeldungen vorhanden";
    elseif ($_GET['buchstabe']) echo "kein Unternehmen mit Name ".$_GET['buchstabe']." vorhanden!";
    else echo "Keine Unternehmen eingetragen";
}
else 
{?>
<div class="contentboxsmall" style="text-align:left;">
        Seiten:
        <?php 
            if($_GET['sp'] && $_GET['mode'])
            {
                echo $unternehmenfac->getHtmlNavi("std", "&amp;sp=".$_GET['sp']."&amp;sort=".$_GET['sort']."&amp;mode=".$_GET['mode']);
            }
            elseif($_GET['sp'] && !$_GET['mode'])
            {
                echo $unternehmenfac->getHtmlNavi("std", "&amp;sp=".$_GET['sp']."&amp;sort=".$_GET['sort']);
            }
            elseif(!$_GET['sp'] && $_GET['mode'])
            {
                echo $unternehmenfac->getHtmlNavi("std", "&amp;mode=".$_GET['mode']);
            }
            elseif($_GET['buchstabe'])
            {
                echo $unternehmenfac->getHtmlNavi("std", "&amp;buchstabe=".$_GET['buchstabe']);
            }
            else echo $unternehmenfac->getHtmlNavi("std","" );
            
            
            
        ?>
    </div>
<?php 
}
include(INCLUDEDIR."footer.inc.php");
?>

