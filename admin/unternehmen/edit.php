<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// unternehmen/edit.php zeigt das Formular zur �nderung von Unternehmen an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

unset($stopp);

if ($_GET['mode']=="update")
{
    $datenfac = new Daten();
    if($_GET['id'])
    {
        $datenfac->getById($_GET["id"]);
    }
    elseif($_GET['nummer'] && !$_GET['id'])
    {
        $datenfac->getByKuNr($_GET["nummer"]);
    }
    
    if(!$daten = $datenfac->getElement())
    {
        $_SESSION['err'] = '<br/>Das von Ihnen aufgerufene Unternehmen existiert nicht!<br />';
        $stopp = "ENDE";
    }
}

if($stopp == "ENDE")
{
    echo '<h1>Fehler</h1>';
    if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}
    echo $l->makeLink("Zur�ck zur �bersicht", "./view.php", "backlink");
    include(INCLUDEDIR."footer.inc.php");
}
else    // weitermachen
{
    $_GET['id']=$daten->id;
    if (is_array(unserialize($daten->fachgebiete))) $fachgebiete=unserialize($daten->fachgebiete);
    else $fachgebiete=array();
    if (is_array(unserialize($daten->suchbegriffe))) $suchbegriffe=unserialize($daten->suchbegriffe);
    else $suchbegriffe=array();
    if (is_array(unserialize($daten->bild))) $bilder=unserialize($daten->bild);
    else $bilder=array();
    // Schlagworte-Array einlesen
    if (is_array(unserialize($daten->schlagworte))) $schlagworte=unserialize($daten->schlagworte);
    else $schlagworte=array();
    
    
    
    ?>
    <script src="<?php echo WEBDIR;?>script/addon.js" language="javascript" type="text/javascript"></script>
    <h1>Unternehmenseintrag bearbeiten</h1>
    <?php if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}?>
    <br />
    
    <form action="<?php echo $l->makeFormLink(WEBDIR.'admin/unternehmen/update.php?mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>
                Unternehmensdaten
            </legend>
            <label for="titel">
                Titel
            </label>
            <input type="text" name="titel" id="titel" value="<?php echo $daten->titel;?>" />
            <br class="clr" />
            <label for="anrede">
                Anrede
            </label>
            <select name="anrede" id="anrede">
                <option value="Herr" <?php if($daten->anrede == "Herr")echo 'selected='.'"selected"'; ?>>Herr</option>
                <option value="Frau" <?php if($daten->anrede == "Frau")echo 'selected='.'"selected"'; ?>>Frau</option>
            </select>
            <br class="clr" />
            
            <label for="famname">
                Nachname
            </label>
            <input type="text" name="famname" id="famname" value="<?php echo $daten->famname;?>" />
            <br class="clr" />
            <label for="vorname">
                Vorname
            </label>
            <input type="text" id="vorname" name="vorname" value="<?php echo $daten->vorname;?>" />
            <br class="clr" /><br/>
            <label for="strasse">
                Strasse
            </label>
            <input type="text" id="strasse" name="strasse" value="<?php echo $daten->strasse;?>" />
            <br class="clr" />
            <label for="plz">
                PLZ / Ort
            </label>
            <input type="text" id="plz" name="plz" value="<?php echo $daten->plz;?>" class="plz"/>
            <input type="text" id="ort" name="ort" value="<?php echo $daten->ort;?>" class="ort" alt="ort"/>
            <br class="clr" />
            <label for="land">
                Land
            </label>
            <input type="text" id="land" name="land" value="<?php echo $daten->land;?>" />
            <br class="clr" /><br/>
            <label for="tel">
                Telefon
            </label>
            <input type="text" id="tel" name="tel" value="<?php echo $daten->tel;?>" />
            <br class="clr" />
            <label for="fax">
                Fax
            </label>
            <input type="text" id="fax" name="fax" value="<?php echo $daten->fax;?>" />
            <br class="clr" /><br/>
            <label for="email">
                eMail
            </label>
            <input type="text" id="email" name="email" value="<?php echo $daten->email;?>" />
            <br class="clr" />
            <label for="homepage">
                Homepage
            </label>
            <input type="text" id="homepage" name="homepage" value="<?php echo $daten->homepage;?>" />
        </fieldset>
        <br class="clr" /><br/>
        <fieldset>
            <legend>
                Beschreibung bearbeiten
            </legend>
            <label for="area">
                Beschreibung
            </label>
            <?php $htmlarea = new HtmlArea("area");
            echo $htmlarea->getHtmlButtons();?>
            <textarea name="text" cols="0" rows="0" id="area"><?php echo $daten->beschreibung;?></textarea>
        </fieldset>
        <br class="clr" /><br />
        <fieldset>
            <legend>
                Fachgebiete
            </legend>
        <?php
            $kategoriefac = new Fach();
            $kategoriefac->getAll();
            while ($kategorien = $kategoriefac->getElement())
            {
                $checked="";
                if (in_array($kategorien->id,$fachgebiete)) $checked='checked="checked"';
                echo '<div style="width:190px;float:left"><input type="checkbox" class="selectbox" '.$checked.' name="fachgebiete['.$kategorien->id.']" value="'.$kategorien->id.'"/>'.$kategorien->name.'</div>';
            }
        ?>
        <br class="clr"/>
        </fieldset>
        <br class="clr" /><br />
        
         <fieldset>
            <legend>
                Suchbegriffe
            </legend>
        <?php
            for($x=0;$x<10;$x++)
            {
                echo '<input type="text" name="suchbegriff[]" value="'.$suchbegriffe[$x].'"/>';
            }
        ?>
        <br class="clr"/>
        </fieldset>
        <br class="clr" /><br />
        <fieldset>
        <legend>
            Schlagworte
        </legend>
        <?php
        for($i=1;$i<4;$i++)
        {
        ?>
        <select name="<?php echo 'schlagwort['.$i.']'; ?>" >
            <option value="">Bitte w�hlen</option>
            <?php 
            $schlagwortefac=new Schlagworte();
            $schlagwortefac->getAll();
            while ($swort=$schlagwortefac->getElement())
            {
                unset($selected); 
                if($schlagworte[$i] == $swort->schlagwort) $selected = 'selected="selected"'; 
                else $teststring = "";
                echo '<option value="'.$swort->schlagwort.'" '.$selected.'>'.$swort->schlagwort.'</option>';
            }     
            ?>
        </select>
        <?php
        }
        ?>
        </fieldset>
        <br class="clr" /><br />
        <fieldset>
            <legend>
                Admineinstellungen
            </legend>
            <label for="kundennummer">
                Kundennummer
            </label>
            <input type="text" id="kundennummer" name="kundennummer" value="<?php echo $daten->kundennummer;?>" />
            <br class="clr"/><br/>
            <label for="homepage">
                Passwort
            </label>
            <input type="text" id="passwd" name="passwd" value="<?php echo $daten->pass_clear;?>" />
            <br class="clr"/><br/>
            <label for="aktiv">
                Aktiv
            </label>
            <select name="aktiv" id="aktiv">
                <option value="1" <?php if ($daten->aktiv=="1") echo 'selected="selected"';?>>Aktiv</option>
                <option value="0" <?php if ($daten->aktiv=="0") echo 'selected="selected"';?>>Inaktiv</option>
            </select>
            <br class="clr" /><br />
            <label for="aktiv">
                Freigabe f�r Bewertungen
            </label>
            <select name="bewertungsstatus" id="bewertungsstatus">
                <option value="A" <?php if ($daten->bewertungsstatus=="A") echo 'selected="selected"';?>>Aktiv</option>
                <option value="I" <?php if ($daten->bewertungsstatus=="I") echo 'selected="selected"';?>>Inaktiv</option>
            </select>
            <br class="clr" /><br />
            <label for="status">
                Neu / Free / Premium / Premiumanfrage
            </label>
            <select name="status" id="status">
                <option value="N" <?php if ($daten->status=="N") echo 'selected="selected"';?>>Neu</option>
                <option value="F" <?php if ($daten->status=="F") echo 'selected="selected"';?>>Free</option>
                <option value="P" <?php if ($daten->status=="P") echo 'selected="selected"';?>>Premium</option>
                <option value="A" <?php if ($daten->status=="A") echo 'selected="selected"';?>>Anfrage</option>
            </select>
        </fieldset>
        <br class="clr"/><br/>
        <fieldset>
            <legend>
                Profilbild
            </legend>
            <?php
            if(sizeof($bilder)==0) echo 'Kein Profilbild hochgeladen';
            
            else 
            {
                for ($x=0; $x<sizeof($bilder);$x++)
                {
                    // Grafiktyp auslesen
                    $typ=getimagesize(LOCALDIR."images/unternehmen/".$bilder[$x]['url']) ;
          
                    // je nach typ Endung bestimmen   
                    if ($typ[2]==1) $endung=".gif";
                    if ($typ[2]==2) $endung=".jpg"; 
                
                    // Reinnamen ohne Endung
                    $name=basename($bilder[$x]['url'],$endung);
                
                    // Gr��e des Thumbnails auslesen
                    $size=getimagesize(LOCALDIR."images/unternehmen/".$name."_thumb".$endung);
          
                    // HTML Bild schreiben 
                    ?>
                    <div <?php if ($x%2) echo 'class="td1"';?>>
                    <img src="<?php echo WEBDIR;?>images/unternehmen/<?php echo $name;?>_thumb<?php echo $endung;?>" height="<?php echo $size[1]; ?>" width="<?php echo $size[0]; ?>" alt="Bild von <?php echo $daten->name." ".$daten->famname?>"/>
                    <input type="checkbox" name="bildloeschen<?php echo $x;?>" class="selectbox"> Bild l�schen  <br class="clr"/>
                    <label>Bildkommentar</label><input type="text" name="bildkommentar[<?php echo $x;?>]" id="bildkommentar" value="<?php echo $bilder[$x]['bildkommentar'];?>"/>
                    </div> 
            <?php
                }
            }
            ?>
            <br class="clr"/><br/><hr/><br/>
            
            <?php if (sizeof($bilder)<5) { ?>
            <label for="bildneu">
                weiteres Bild hinzuf�gen:
            </label>
            <input id="bildneu" name="bildneu" type="file" /><br class="clr" />
            <label for="bildkommentarneu">
                Bildkommentar
            </label>
            <input id="bildkommentarneu" name="bildkommentarneu" type="text" />
            <?php } ?>
        </fieldset>
        <br class="clr"/><br/>
         
        <fieldset>
            <legend>
                Bezahlungsdaten (nur wichtig bei Premium)
            </legend>
            <label for="zahlungsweise">
                Zahlungsweise
            </label>
            <select name="zahlungsweise" id="zahlungsweise">
                <option value="">&nbsp;</option>
                <option value="Vorab" <?php if ($daten->zahlungsweise=="Vorab") echo 'selected="selected"';?>>Vorab</option>
                <option value="Nachnahme" <?php if ($daten->zahlungsweise=="Nachnahme") echo 'selected="selected"';?>>Nachnahme</option>
                <option value="Rechnung" <?php if ($daten->zahlungsweise=="Rechnung") echo 'selected="selected"';?>>Rechnung</option>
            </select>
           <?php  
            /*<br class="clr" />
            <br/><br/>
            <b>Kontodaten (bei Bankeinzug)</b><br/>
            <label for="ktoinh">
                Kontoinhaber
            </label>
            <input type="text" id="ktoinh" name="ktoinh" value="<?php echo $daten->kontoinhaber;?>" />
            <br class="clr" />
            <label for="konto">
                Konto (BIC-/SWIFT-Code)
            </label>
            <input type="text" id="konto" name="konto" value="<?php echo $daten->konto;?>" />
            <br class="clr" /><br/>
            <label for="blz">
                BLZ (IBAN)
            </label>
            <input type="text" id="blz" name="blz" value="<?php echo $daten->blz;?>" />
            <br class="clr" />
            <label for="bank">
                Bank
            </label>
            <input type="text" id="bank" name="bank" value="<?php echo $daten->bank;?>" />
            <br class="clr" />
            <br/><br/>
            <b>Kreditkartendaten (bei Bezahlung per Kreditkarte)</b><br class="clr"/><br/>
            <label for="kreditinh">
                Kreditkarteninhaber
            </label>
            <input type="text" id="kreditinh" name="kreditinh" value="<?php echo $daten->karteninhaber;?>" />
            <br class="clr" />
            <label for="kartentyp">
                Kartentyp
            </label>
            <select name="kartentyp" id="kartentyp">
                <option value="">&nbsp;</option>
                <option value="visa" <?php if ($daten->kartentyp=="visa") echo 'selected="selected"';?>>VISA</option>
                <option value="mastercard" <?php if ($daten->kartentyp=="mastercard") echo 'selected="selected"';?>>MASTERCARD</option>
                <option value="AmericanExpress" <?php if ($daten->kartentyp=="AmericanExpress") echo 'selected="selected"';?>>American Express</option>
            </select>
            <br class="clr" />
            <label for="gueltig">
                G�ltigkeitsdatum
            </label>
            <input type="text" id="gueltig" name="gueltig" value="<?php echo $daten->gueltigkeit;?>" />
            <br class="clr" />
            <label for="kartennummer">
                Kartennummer
            </label>
            <input type="text" id="kartennummer" name="kartennummer" value="<?php echo $daten->kartennummer;?>" />
            <br class="clr" />
            */ ?>
        </fieldset>
        
        <br class="clr" /><br />
           <fieldset>
           <?php 
           $ordzeitenfac = new Ordzeiten();
           $firmenid = $daten->id;              // aus Aufruf am Anfang der Datei
           $ordzeitenfac->getByFirmId($firmenid);
            ?>
            <legend>
               �ffnungszeitenformular
            </legend>
            <table ><tr><th>Tage</th><th colspan="2">Vormittags</th><th colspan="2">Nachmittags</th><th>Ordination/Vereinbarung</th></tr>
            <?php
            for($i=1;$i<8;$i++)
            {
            ?>
             <tr><td <?php if ($i%2) echo 'class="td1"'; ?> >
                <label for="<?php echo 'day'.$i ?>" ><?php echo $ordzeitenfac->parseDay($i,"long"); ?></label>
                <?php 
                    $ordzeitenfac->getByFirmIdAndDay($firmenid,$i); // Datens�tz von Firma und Tag hohlen ORDER by id ASC
                    $counttimes = $ordzeitenfac->getElementCount(); // Anzahl der Datens�tze festhalten
                    $ordzeit = $ordzeitenfac->getElement();
                ?>
                <input type="checkbox" id="<?php echo 'day'.$i; ?>" name="<?php echo 'day'.$i; ?>" value="<?php echo $ordzeitenfac->parseDay($i,"long"); ?>" class="selectbox"  <?php if($ordzeit) echo "checked='checked'"?>/></td>
               <td <?php if ($i%2) echo 'class="td1"'; ?> >
                  von<select id="<?php echo 'time1'.$i ?>" name="<?php echo 'time1'.$i ?>" style="width:75px">
                  <option value =''>---</option>
                  <?php  
                      for($j=1;$j<50;$j++)
                      {
                             
                          unset($teststring); 
                          if($ordzeit->timestart == $j) $teststring = "selected='selected'"; 
                          else $teststring = "";  
                          echo "<option value='".$j."'".$teststring." >".$ordzeitenfac->parseTime($j)."</option>";
                      }  
                   ?>
                  </select>
                </td>
                <td <?php if ($i%2) echo 'class="td1"'; ?> >
                    bis<select id="<?php echo 'time2'.$i ?>" name="<?php echo 'time2'.$i ?>" style="width:75px">
                    <option value =''>---</option>  
                      <?php  
                          for($k=1;$k<50;$k++)
                          {   
                              unset($teststring); 
                              if($ordzeit->timeend == $k) $teststring = "selected='selected'"; 
                              else $teststring = "";  
                              echo "<option value='".$k."'".$teststring." >".$ordzeitenfac->parseTime($k)."</option>";
                          }  
                       ?>
                      </select>
                 </td>
                 <td <?php if ($i%2) echo 'class="td1"'; ?> >
                    von<select id="<?php echo 'time3'.$i ?>" name="<?php echo 'time3'.$i ?>" style="width:75px">
                    <option value =''>---</option>
                    <?php  
                        if($counttimes > 1) // zweiten datensatz holen
                            {
                                $ordzeit = $ordzeitenfac->getElement();
                            }
                        for($m=1;$m<50;$m++)
                        {
                            unset($teststring);
                            if($counttimes > 1)
                            {
                                if($ordzeit->timestart == $m) $teststring = "selected='selected'"; 
                                else $teststring = "";
                            }
                            echo "<option value='".$m."'".$teststring." >".$ordzeitenfac->parseTime($m)."</option>";
                        }  
                     ?>
                  </select>
                 </td>
                 <td <?php if ($i%2) echo 'class="td1"'; ?> >
                    bis<select id="<?php echo 'time4'.$i ?>" name="<?php echo 'time4'.$i ?>" style="width:75px">
                    <option value =''>---</option>
                    <?php  
                        for($n=1;$n<50;$n++)
                        {   
                            unset($teststring);
                            if($counttimes > 1)
                            {
                                if($ordzeit->timeend == $n) $teststring = "selected='selected'"; 
                                else $teststring = "";
                            }    
                            echo "<option value='".$n."'".$teststring." >".$ordzeitenfac->parseTime($n)."</option>";
                        }  
                     ?>
                    </select>
                 </td>
                 <td <?php if ($i%2) echo 'class="td1"'; ?>>
                    <input type="text" id="<?php echo 'memo'.$i; ?>" name="<?php echo 'memo'.$i; ?>" value="<?php echo $ordzeit->memo; ?>" />
                 </td>
              </tr>
              <?php
              }// end Forschleife f�r Tage
           ?>  
           </table>
         </fieldset>
         <br class="clr" /><br />
        <input type="submit" value="Daten speichern" />
        <br class="clr"/><br/>
        <hr/>
        <h1>Bewertungskommentare (nur Premium)</h1>
        <br class="clr"/><br/>
        
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php");
} // end else zu if($stopp == "ENDE")
?>

