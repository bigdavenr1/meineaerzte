<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

    if ($_POST["submit"])
    {
        // Bilder l&ouml;schen
        //////////////////////////////////////////////////////////////////////////////////////////////

        if ($_POST["mode"] == "delete_yes")
        {
            $datenfac = new Daten();
            $datenfac->getById($_POST["id"]);
            $daten = $datenfac->getElement();
            
            $benutzerfac = new Benutzer();
            $benutzerfac->getByMail($daten->email);
            if ($daten->email !="" && $benutzer= $benutzerfac->getElement)
            { 
                $benutzerfac->deleteElement();
            }
            
            else 
            {
                $benutzerfac->getByMail($daten->kundennummer);
                $benutzerfac->deleteElement();
            }

            if($daten->bild != '' && file_exists(LOCALDIR."images/unternehmen/".$daten->bild))
            {
                // Grafiktyp auslesen
                $typ=getimagesize(LOCALDIR."images/unternehmen/".$daten->bild) ;
      
                // je nach typ Endung bestimmen   
                if ($typ[2]==1) $endung=".gif";
                if ($typ[2]==2) $endung=".jpg"; 
            
                // Bild und Thumb l�schen
       	        unlink(LOCALDIR."images/unternehmen/".$daten->bild);
                unlink(LOCALDIR."images/unternehmen/".basename($daten->bild,$endung)."_thumb".$endung);               
            }                
            
            $datenfac->deleteElement();
            
            // �ffnungszeiten der Firma l�schen
            $ordzeitenfac = new Ordzeiten();
            $ordzeitenfac->getByFirmId($_POST["id"]);
            while($ordzeit = $ordzeitenfac->getElement())
            {
                $ordzeitenfac->deleteElement();
            }
                   
            header("Location: ./view.php?sv=1");
        }
     }
    
    //  Sicherheitsabfrage
    //////////////////////////////////////////////////////////////////////////////////////////////
    
    else
    {
        include(INCLUDEDIR."header.inc.php");?>

        <h1>Unternehmen l&ouml;schen best&auml;tigen</h1>
        
        <?php
        $datenfac = new Daten();
        $datenfac->getById($_GET["id"]);
        $daten = $datenfac->getElement();
        
        echo '<br /><b> "'.$daten->name.' '.$daten->famname.' '.$daten->vorname.'"</b><br/><br/>';
        ?>
        <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
            Soll dieses Unternehmen wirklich gel&ouml;scht werden?<br><br>Der L&ouml;schvogang kann nicht r&uuml;ckg&auml;ngig gemacht werden.
            <br /><br />
            <input type="submit" value="L&ouml;schen" name="submit" class="login" /><br class="clr" /><br/>
            <?php echo $l->makeLink("Zur&uuml;ck", "./view.php", "login");?>
        </form>
<?php
include(INCLUDEDIR."footer.inc.php"); }
?>
