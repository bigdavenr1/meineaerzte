<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// unternehmen/update.php regelt das update der Unternehmenstabelle
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
$datenfac = new Daten();

if (!$_GET['mode']) $_GET['mode']="new";

// Daten pr�fen
unset($_SESSION['err']);

// Fachgebiete pr�fen
if($_POST['fachgebiete'])
{
   $count=count($_POST['fachgebiete']);
   if($count > 3) $_SESSION['err'] .= 'Sie haben '.$count.' das sind zuviele Fachgebiete gew�hlt!<br />';
   //echo "ZAHL: ".$count;
}

// wenn neuer Datensatz, pr�fen auf email und Kundennummerndopplungen
if($_GET['mode']=='new')
{  
    // Mail oder Kundennummer da 
    if($_POST['kundennummer'] == "" && $_POST['email'] == "")
    {
        $_SESSION['err'] .= 'Eine eMail-Adresse oder eine Kundennummer sind n�tig!<br />';
    }
    // Mail pr�fen
    if($_POST['email'] != "")
    {
        $datenfac->getByMail($_POST['email']);
        if($pruefdaten = $datenfac->getElement()) $_SESSION['err'] .= 'F�r diese eMail-Adresse gibt es schon ein Unternehmen!<br />';
    }
    // Kundennummer pr�fen
    if($_POST['kundennummer'] != "")
    {
        $datenfac->getByKuNr($_POST['kundennummer']);
        if($pruefdaten = $datenfac->getElement()) $_SESSION['err'] .= 'F�r diese Kundennummer gibt es schon ein Unternehmen!<br />';
    }
}

// wenn update Datensatz und Kundennummer oder Mail neu Dopplungen pr�fen
if($_GET['mode']=='update')
{   
   $datenfac->getById($_GET["id"]);
   $daten = $datenfac->getElement(); 
   
   // Mail oder Kundennummer da 
    if($_POST['kundennummer'] == "" && $_POST['email'] == "")
    {
        $_SESSION['err'] .= 'Eine eMail-Adresse oder eine Kundennummer sind n�tig!<br />';
    }
    // Mail pr�fen
    if($_POST['email'] != "")
    {
        if($daten->email != $_POST['email'])
        {
            $datenfac->getByMail($_POST['email']);
            if($pruefdaten = $datenfac->getElement()) $_SESSION['err'] .= 'F�r diese eMail-Adresse gibt es schon ein Unternehmen!<br />';
        }
    }
    // Kundennummer pr�fen
    if($_POST['kundennummer'] != "")
    {
        if($daten->kundennummer != $_POST['kundennummer'])
        {
            $datenfac->getByKuNr($_POST['kundennummer']);
            if($pruefdaten = $datenfac->getElement()) $_SESSION['err'] .= 'F�r diese Kundennummer gibt es schon ein Unternehmen!<br />';
        }
    }
}

// Bei Fehler zur�ck
if($_SESSION['err'])
{
    if($_GET['mode']=='update') header("Location:edit.php?mode=update&id=".$_GET['id']."&sv=1");
    elseif($_GET['mode']=='new') header("Location:edit.php?mode=new&sv=1");
    else header("Location:view.php?status=".$_GET['status']."&sv=1");
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Neuer Datensatz
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="new" && !$_SESSION['err'])
{
    if ($_FILES['bildneu'] != "")// Bild ausgew�hlt
    {                        
        $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);
    
        // �berpr�fung des Bildtyps
        if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
        {
            if ($filetyp[2]==2) $type = '.jpg';
            if ($filetyp[2]==1) $type = '.gif';
      		                
            // neues Bild hochladen
            $key=generateRandomKey(10);
       	    $pic = $datenfac->writePic($_FILES['bildneu']["tmp_name"],$key,$type);
            $bildname[]= array("url"=>$key.$type,
                     "bildkommentar"=>$_POST['bildkommentarneu']);
        }
            //Wenn falscher Bildtyp 
        else 
        {    
            $error= $error."[Bild -- falschen Typ]";
        }
    }
    
    // Wenn Premium, dann Premiumdatum
    $premiumdatum="";
    if ($_POST['status'] == "P" )$premiumdatum=time();
    
    // Beschreibung parsen
    $tParser = new TextParser($_POST['text']);
    $tParser->parse();
   
    // Array f�r Datenspeicherung
    $data[]="";                                                                 //Frei f�r ID
    $data[]=$_POST['name'];                                                     //Praxistyp
    $data[]=$_POST['email'];                                                    //eMail
    $data[]=$_POST['aktiv'];                                                    //aktiv
    $data[]=$_POST['vorname'];                                                  //Vorname
    $data[]=$_POST['famname'];                                                  //Nachname
    $data[]=$_POST['strasse'];                                                  //Strasse
    $data[]=$_POST['plz'];                                                      //PLZ
    $data[]=$_POST['ort'];                                                      //Ort
    $data[]=$_POST['land'];                                                     //Land
    $data[]=$_POST['tel'];                                                      //Telefon
    $data[]=$_POST['fax'];                                                      //Fax
    $data[]=$_POST['homepage'];                                                 //Homepage
    $data[]=$_POST['status'];                                                   //Neu / Free oder Premium
    $data[]=serialize($bildname);                                               //Hochgeladenes Bild
    $data[]=$tParser->getParsedText();                                          //Bechreibung
    $data[]=$_POST['zahlungsweise'];                                            //Zahlungsweise
    $data[]=$_POST['ktoinh'];                                                   //Kontoinhaber
    $data[]=$_POST['konto'];                                                    //Konto
    $data[]=$_POST['blz'];                                                      //BLZ
    $data[]=$_POST['bank'];                                                     //Bank
    $data[]=$_POST['kreditinh'];                                                //Kreditkarteninhaber
    $data[]=$_POST['kartentyp'];                                                //Kartentyp
    $data[]=$_POST['gueltig'];                                                  //Gueltigkeit
    $data[]=$_POST['kartennummer'];                                             //Kartennumer
    $data[]=$_POST['passwd'];                                                   //Clearpasswort
    $data[]=serialize($_POST['fachgebiete']);                                   //Fachgebiete
    $data[]=$_POST['titel'];                                                    //Titel
    $data[]=serialize($_POST['suchbegriff']);                                   //Suchbegrife
    $data[]=time();                                                             //Anlagedatum
    $data[]=$premiumdatum;                                                      //Premiumdatum
    $data[]=$_POST['bewertungsstatus'];                                         //Freigabe f�r Bewertung
    $data[]=$_POST['kundennummer'];                                             //Kundennummer
    $data[]="";                                                                 //Klaerung
    $data[]=serialize($_POST['schlagworte']);                                   //Schlagworte
    $data[]=$_POST['anrede'];                                                   //Anrede
    $datenfac->write($data); 
    
    // Array f�r Benutzerdatenbank
    if ($_POST['email'] != "") $login=$_POST['email'];
    else $login=$_POST['kundennummer'];
    
    $salt=generateRandomKey(8);
    $benutzerdata[]="";                                                         //id
    $benutzerdata[]=$_POST['anrede'];                                           //anrede
    $benutzerdata[]=$_POST['vorname'];                                          //vorname
    $benutzerdata[]=$_POST['famname'];                                          //nachname
    $benutzerdata[]=md5($_POST['passwd'].$salt);                                //pass
    $benutzerdata[]=$salt;                                                      //salt
    $benutzerdata[]=$_POST['aktiv'];                                            //aktiv
    $benutzerdata[]="unternehmen";                                              //typ
    $benutzerdata[]=$login;                                                     //email
    $benutzerdata[]="";                                                         //adresse
    $benutzerdata[]="";                                                         //plz
    $benutzerdata[]="";                                                         //ort
    $benutzerdata[]="";                                                         //land
    $benutzerdata[]="";                                                         //tel
    $benutzerdata[]="";                                                         //fax
    $benutzerdata[]="";                                                         //vorwahl
    $benutzerdata[]="";                                                         //homepage
    $benutzerdata[]="";                                                         //anmeldedatum
    $benutzerdata[]="";                                                         //aktivierungskey
    $benutzerdata[]="";                                                         //nickname
    $benutzerdata[]="";                                                         //geburtstag
    $benutzerdata[]="";                                                         //bild
    $benutzerdata[]="";                                                         //rechte
    $benutzerdata[]="";                                                         //freigabe
    $benutzerdata[]=$_POST['titel'];                                            //titel

    $benutzerfac= new Benutzer();
    $benutzerfac->write($benutzerdata);
    
    //Datenarray f�r Ordzeiten
    // firmid hohlen zum Abspeichern
    $datenfac=new Daten();
    if ($_POST['email']!="") $datenfac->getByMail($_POST['email']);
    else if($_POST['kundennummer']!="") $datenfac->getByKuNr($_POST['kundennummer']);
    
    if($daten=$datenfac->getElement())
    {
        //$daten = $datenfac->getElement();
        $firmenid = $daten->id;
        
        // neuer Datensatz bzw. neue Datens�tze schreiben
        for($i=1;$i<8;$i++)
        {
            if($_POST['day'.$i])
            {
                unset($odata);
                unset($ordzeitenfac1);
                $ordzeitenfac1 = new Ordzeiten(); 
                $odata[] = "";                       // id
                $odata[] = $firmenid;                // firmid
                $odata[] = $i;                       // daystart
                $odata[] = $i;                       // dayend
                $odata[] = $_POST['time1'.$i];       // timestart
                $odata[] = $_POST['time2'.$i];       // timeend
                $odata[] = $_POST['memo'.$i];        // timeend
                $ordzeitenfac1->write($odata);
            }
            if($_POST['day'.$i] && $_POST['time3'.$i] != '')
            {  
                unset($o2data);
                unset($ordzeitenfac2);
                $ordzeitenfac2 = new Ordzeiten(); 
                $o2data[] = "";                       // id
                $o2data[] = $firmenid;                // firmid
                $o2data[] = $i;                       // daystart
                $o2data[] = $i;                       // dayend
                $o2data[] = $_POST['time3'.$i];       // timestart
                $o2data[] = $_POST['time4'.$i];       // timeend
                $o2data[] = $_POST['memo'.$i];        // timeend
                $ordzeitenfac2->write($o2data);
            }
        }   // end for
    }   // end if($countfirma == 1)*/
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Datensatz �ndern
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="update" && !$_SESSION['err'])
{
    // datensatz einlesen
    $unternehmenfac = new Daten(1); 
    $unternehmenfac->getById($_GET["id"]);
    $daten = $unternehmenfac->getElement();
    
    // Wenn Bilder vorhanden sind, dann als Array einlesen
    if (is_array(unserialize($daten->bild))) $bildname=unserialize($daten->bild);
    else $bildname=array();
    
    // Bildkommentare einlesen;
    $bildkommentar=$_POST['bildkommentar'];
   
    // Schauen ob Bilder gel�scht werden sollen
    for ($x=5;$x>-1;$x--)
    {
        // eventuell altes Bild l�schen
        if($_POST['bildloeschen'.$x])
        {
            // Grafiktyp auslesen
            $typ=getimagesize(LOCALDIR."images/unternehmen/".$bildname[$x]['url']) ;
      
            // je nach typ Endung bestimmen   
            if ($typ[2]==1) $endung=".gif";
            if ($typ[2]==2) $endung=".jpg"; 
            
            // Bild und Thumb l�schen
       	    unlink(LOCALDIR."images/unternehmen/".$bildname[$x]['url']);
            unlink(LOCALDIR."images/unternehmen/".basename($bildname[$x]['url'],$endung)."_thumb".$endung);
            
            // Bildurl und Name l�schen
            unset($bildname[$x]);
            unset($bildkommentar[$x]);
            
            // Arrays neu aufbauen und durchnummerieren
            $bildname=array_values($bildname);     
            $bildkommentar=array_values($bildkommentar);  
        }  
    }
    
    // Bildkommentare aktualisieren
    for ($x=0;$x<sizeof($bildname);$x++)
    {
        $bildname[$x]['bildkommentar']=$bildkommentar[$x];
    }
    
    // neue Bilder hochladen     
    if ($_FILES['bildneu'] != "") // Bild ausgew�hlt
    {                        
        // �berpr�fung des Bildtyps
        $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);
 
        // �berpr�fung des Bildtyps
        if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
        {
            if ($filetyp[2]==2) $type = '.jpg';
            if ($filetyp[2]==1) $type = '.gif';
                   
            // neues Bild hochladen
            $key=generateRandomKey(10);
            $pic = $datenfac->writePic($_FILES['bildneu']["tmp_name"],$key,$type);
  	        $bildname[]= array("url"=>$key.$type,
                     "bildkommentar"=>$_POST['bildkommentarneu']);
        }

        //Wenn falscher Bildtyp 
        else 
        {    
           $error= $error."[Bild -- falschen Typ]";
        }
    }
    
    // Beschreibung parsen
    $tParser = new TextParser($_POST['text']);
    $tParser->parse();
    
    // Update der DB 
    $unternehmenfac->update("anrede='".$_POST['anrede']."', 
                            email='".$_POST['email']."', 
                            aktiv='".$_POST['aktiv']."',
                            titel='".$_POST['titel']."', 
                          vorname='".$_POST['vorname']."', 
                          famname='".$_POST['famname']."',  
                          strasse='".$_POST['strasse']."',
                              plz='".$_POST['plz']."', 
                              ort='".$_POST['ort']."',
                             land='".$_POST['land']."',
                              tel='".$_POST['tel']."',
                              fax='".$_POST['fax']."',
                         homepage='".$_POST['homepage']."',
                           status='".$_POST['status']."',
                 bewertungsstatus='".$_POST['bewertungsstatus']."',
                             bild='".serialize($bildname)."',
                     beschreibung='".$tParser->getParsedText()."',
                    zahlungsweise='".$_POST['zahlungsweise']."',
                     kontoinhaber='".$_POST['ktoinh']."',
                            konto='".$_POST['konto']."',
                              blz='".$_POST['blz']."',
                             bank='".$_POST['bank']."',
                    karteninhaber='".$_POST['kreditinh']."',
                        kartentyp='".$_POST['kartentyp']."',
                      gueltigkeit='".$_POST['gueltig']."',
                     kartennummer='".$_POST['kartennummer']."',
                      fachgebiete='".serialize($_POST['fachgebiete'])."',
                     suchbegriffe='".serialize($_POST['suchbegriff'])."',  
                     kundennummer='".$_POST['kundennummer']."',
                      schlagworte='".serialize($_POST['schlagwort'])."',                  
                       pass_clear='".$_POST['passwd']."'");
     
    // Wenn bestimmte Daten ge�ndert wurden, Update der UserTabelle
    if ($daten->email != $_POST['email'] || $daten->kundennummer != $_POST['kundennummer'] || $daten->pass_clear != $_POST['passwd'] || $daten->aktiv != $_POST['aktiv'])
    {
        // Abfrage ob eMail angegeben wurde        
        if ($_POST['email'] != "") $login=$_POST['email']; 
        else $login=$_POST['kundennummer'];
        
        // Abfrage was in der Logintabelle steht (email oder Kundennummer)
        if ($daten->email != "") $ref=$daten->email;
        else $ref=$daten->kundennummer;         
        
        // Update der Logintabelle
        $salt=generateRandomKey(8); // wof�r???
        $benutzerfac= new Benutzer();
        $benutzerfac->update("email='".$login."' , pass='".md5($_POST['passwd'].$salt)."' , salt='".$salt."' , aktiv='".$_POST['aktiv']."' , anrede='".$_POST['anrede']."' , titel='".$_POST['titel']."' WHERE email='".$ref."'");
    } 
    
    // alte Ordzeiten l�schen neue Datens�tze schreiben
    $ordzeitenfac = new Ordzeiten(); 
    $ordzeitenfac->getByFirmId($_GET["id"]);
   
    while($ordzeit = $ordzeitenfac->getElement())
    {
        $ordzeitenfac->deleteElement(); // alte Datens�tze l�schen
    }
    
    // neuen Datensatz bzw. neue Datens�tze schreiben
    for($i=1;$i<8;$i++)
    {
        if($_POST['day'.$i])
        {
            unset($odata);
            unset($ordzeitenfac1);
            $ordzeitenfac1 = new Ordzeiten(); 
            $odata[] = "";                       // id
            $odata[] = $_GET["id"];              // firmid
            $odata[] = $i;                       // daystart
            $odata[] = $i;                       // dayend
            $odata[] = $_POST['time1'.$i];       // timestart
            $odata[] = $_POST['time2'.$i];       // timeend
            $odata[] = $_POST['memo'.$i];        // timeend
            $ordzeitenfac1->write($odata);
        }
        if($_POST['day'.$i] && $_POST['time3'.$i] != '')
        {  
            unset($o2data);
            unset($ordzeitenfac2);
            $ordzeitenfac2 = new Ordzeiten(); 
            $o2data[] = "";                       // id
            $o2data[] = $_GET["id"];              // firmid
            $o2data[] = $i;                       // daystart
            $o2data[] = $i;                       // dayend
            $o2data[] = $_POST['time3'.$i];       // timestart
            $o2data[] = $_POST['time4'.$i];       // timeend
            $o2data[] = $_POST['memo'.$i];        // timeend
            $ordzeitenfac2->write($o2data);
        }
    }           
} 

//////////////////////////////////////////////////////////////////////////////////////////////////
// Free oder premium, neue Premiumanmeldungen sind A
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="changestatus")
{
    // Datensatz holen
    $datenfac->getById($_GET["id"]);
    $daten = $datenfac->getElement();
    
    // Status bestimmen und Datensatz upaten, angefragte freischalten d.h.
    // Stati "F" oder "A" auf "P" setzen, oder auf "F" zur�ckstufen
    if ($daten->status != "P") $datenfac->update("status='P', premiumdatum='".time()."' ");
    else $datenfac->update("status='F'"); 
              
} 

//////////////////////////////////////////////////////////////////////////////////////////////////
// Bewertungsfreigabe �ndern
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="changebewertung")
{
    // Datensatz holen
    $datenfac->getById($_GET["id"]);
    $daten = $datenfac->getElement();
    
    // Status bestimmen und Datensatz upaten
    if ($daten->bewertungsstatus == "A") $datenfac->update("bewertungsstatus='I'");  
    else $datenfac->update("bewertungsstatus='A'"); 
              
} 

//////////////////////////////////////////////////////////////////////////////////////////////////
// Unternehmen Sperren und freischalten
//////////////////////////////////////////////////////////////////////////////////////////////////
    
if ($_GET['mode']=="changeaktiv")
{
    // Datensatz holen
    $datenfac->getById($_GET["id"]);
    $daten = $datenfac->getElement();
    
    // Status bestimmen und Datensatz upaten
     // 2 = neu, 1= aktiv, 0= inaktiv
    if ($daten->aktiv == "0" || $daten->aktiv == "2" ) $newstatus="1"; 
    elseif($daten->aktiv == "1" ) $newstatus="0"; 
    $datenfac->update("aktiv='".$newstatus."'");
    
    // Status zus�tzlich in Logintabelle �ndern
    $benutzerfac=new Benutzer();
    $benutzerfac->update("aktiv='".$newstatus."' WHERE email='".$_GET[email]."'");           
}     
    
if(!$_SESSION['err']) header("Location:view.php?status=".$_GET['status']."&amp;kat=".$_GET['kat']."&amp;sv=1");
?>
