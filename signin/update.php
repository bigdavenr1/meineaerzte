<?
include("../inc/config.php");
include(INCLUDEDIR."std/email.class.php");


// Sessionupdate
//////////////////////////////////////////////////////////////////////////////////////////////////


unset($_SESSION["err"]);
unset($err);

if (isset($_SESSION))
{
    switch ($_GET['step'])
    {
        case 2:
            $_POST['nick']=trim($_POST["nick"]);
            $_POST['pass']=trim($_POST["pass"]);
            $_POST['repass']=trim($_POST["repass"]);
            if (!$_POST['nick'] || $_POST['nick']=="") $err .= "Sie haben das Nicknamefeld vergessen<br />";
            if (!$_POST["mail"]) $err .= "Sie haben das Emailfeld vergessen<br />";
            if (!$_POST["pass"] || $_POST['pass']=="" ) $err .= "Sie haben das Passwortfeld vergessen<br />";
            if (!$_POST["repass"] || $_POST['repass']=="" ) $err .= "Sie haben das Best�tigungsfeld vergessen<br />";   
            if (!$_POST["anrede"]) $err .= "Bitte eine Anrede ausw�hlen<br />";
            if (!$_POST["vorname"]) $err .= "Sie haben das Vornamefeld vergessen<br />";
            if (!$_POST["nachname"]) $err .= "Sie haben das Nachnamefeld vergessen<br />";
            if (!$_POST["strasse"]) $err .= "Sie haben das Strassenfeld vergessen<br />";
            if (!$_POST["plz"] && $_POST["plz"] != 0) $err .= "Sie haben das Plz-Feld vergessen<br />";
            if (!$_POST["ort"]) $err .= "Sie haben das Ortfeld vergessen<br />";
            if (!$_POST["land"]) $err .= "Sie haben das Landfeld vergessen<br />";
            
            if ($_POST["mail"] && strlen($_POST["mail"]) < 9) $err .= "Ihre Emailadresse ist zu kurz (min 9 Zeichen)<br />";
            if ($_POST["pass"] && strlen($_POST["pass"]) < 6) $err .= "Ihr Passwort ist zu kurz (min 6 Zeichen)<br />";
            if ($_POST["vorname"] && strlen($_POST["vorname"]) < 2) $err .= "Ihr Vorname ist zu kurz (min 2 Zeichen)<br />";
            if ($_POST["nachname"] && strlen($_POST["nachname"]) < 2) $err .= "Ihr Nachname ist zu kurz (min 2 Zeichen)<br />";
            if (($_POST["plz"] || $_POST["plz"] == 0) && strlen($_POST["plz"]) < 4) $err .= "Ihre Plz ist zu kurz (min 4 Zeichen)<br />";
            if ($_POST["ort"] && strlen($_POST["ort"]) < 3) $err .= "Ihr Wohnort ist zu kurz (min 3 Zeichen)<br />";
            if ($_POST["land"] && strlen($_POST["land"]) < 3) $err .= "Ihr Land ist zu kurz (min 3 Zeichen)<br />";           
            if ($_POST["repass"] && $_POST["pass"] != $_POST["repass"]) $err .= "Ihr Passwort stimmt nicht �berein<br />";
            if ($_POST["mail"] && !isMail($_POST["mail"])) $err .= "Ihre Emailadresse ist nicht g�ltig<br />";

            if ($_POST["nick"] && ereg("[^A-Za-z0-9�������._*-]", ereg_replace(" ","",$_POST["nick"]))) $err .= "Ihr Nickname enth�lt unerlaubte Zeichen<br>";
            if (!$err)
            {
                $benutzerfac = new Benutzer();
                $benutzerfac->getByMail($_POST["mail"]);
                if ($benutzerfac->getElementCount()) $err .= "Diese Emailadresse gibt es schon.<br />";
                
                $benutzerfac = new Benutzer();
                $benutzerfac->getByNick($_POST["nick"]);
                if ($benutzerfac->getElementCount()) $err .= "Diesen Nicknamen gibt es schon.<br />";
            }
            
            $_SESSION["reg"]["mail"] = addslashes(htmlentities(strip_tags($_POST["mail"])));
            $_SESSION["reg"]["titel"] = addslashes(htmlentities(strip_tags($_POST["titel"])));
            $_SESSION["reg"]["nick"] = addslashes(htmlentities(strip_tags($_POST["nick"])));
            $_SESSION["reg"]["pass"] = addslashes(htmlentities(strip_tags($_POST["pass"])));
            $_SESSION["reg"]["anrede"] = addslashes(htmlentities(strip_tags($_POST["anrede"])));
            $_SESSION["reg"]["vorname"] = addslashes(htmlentities(strip_tags($_POST["vorname"])));
            $_SESSION["reg"]["nachname"] = addslashes(htmlentities(strip_tags($_POST["nachname"])));
            $_SESSION["reg"]["plz"] = addslashes(htmlentities(strip_tags($_POST["plz"])));
            $_SESSION["reg"]["ort"] = addslashes(htmlentities(strip_tags($_POST["ort"])));
            $_SESSION["reg"]["land"] = addslashes(htmlentities(strip_tags($_POST["land"])));
            $_SESSION["reg"]["strasse"] = addslashes(htmlentities(strip_tags($_POST["strasse"])));
            $_SESSION["reg"]["homepage"] = addslashes(htmlentities(strip_tags($_POST["homepage"])));
            $_SESSION["reg"]["geburtstag_tag"] = $_POST["geburtstag_tag"];
            $_SESSION["reg"]["geburtstag_monat"] = $_POST["geburtstag_monat"];
            $_SESSION["reg"]["geburtstag_jahr"] = $_POST["geburtstag_jahr"];
           
            break;
          
            
        case 3:
          
            if (!$_POST["agb"]) $err .= "Sie haben vergessen, die AGB's zu akzeptieren.";
           
            if (!$err)
            {
                // Neuen Benutzer anlegen + Aktivierungsmail schicken
                //////////////////////////////////////////////////////////////////////////////////
           
           
                $benutzerfac = new Benutzer();
                $geb = $_SESSION["reg"]["geburtstag_jahr"]."-".$_SESSION["reg"]["geburtstag_monat"]."-".$_SESSION["reg"]["geburtstag_tag"];
                $salt = generateRandomKey(8); // salt als Variable erzeugen
                
                $data[] = "";                                       // id
                $data[] = $_SESSION["reg"]["anrede"];               // anrede
                $data[] = $_SESSION["reg"]["vorname"];              // vorname
                $data[] = $_SESSION["reg"]["nachname"];             // nachname
                $data[] = md5($_SESSION["reg"]["pass"].$salt);      // pass mit salt
                $data[] = $salt;                                    // salt
                $data[] = 0;                                        // aktiv
                $data[] = "benutzer";                               // typ
                $data[] = $_SESSION["reg"]["mail"];                 // mail
                $data[] = $_SESSION["reg"]["strasse"];              // adressse
                $data[] = $_SESSION["reg"]["plz"];                  // plz
                $data[] = $_SESSION["reg"]["ort"];                  // ort
                $data[] = $_SESSION["reg"]["land"];                 // land               
                $data[] = "";                   // tel
                $data[] = "";                  // fax
                $data[] = "";              // vorwahl
                $data[] = $_SESSION["reg"]["homepage"];             // homepage
                $data[] = time();                                   // anmeldedatum               
                $data[] = generateRandomKey(30);                    // aktivierungskey
                $data[] = $_SESSION["reg"]["nick"];                 // Nickname
                $data[] = $geb;                                     // geburtstag
                $data[] = "";                                       // Bild
                $data[] = "";                                       // Rechte
                $data[] = "";                                       // Freigabe
                $data[] = $_SESSION["reg"]["titel"];                // Titel
                
                $benutzerfac->write($data);                         //Daten schreiben	

                // login mit salted hash
                
                $benutzerfac->getByMail(htmlentities(strip_tags($_SESSION["reg"]["mail"])));
    			$testbenutzer = $benutzerfac->getElement();
    			if(!$testbenutzer)// kein Datensatz f�r Mail
    			{
    				$err .= "Fehler beim Profilanlegen. Bitte probieren Sie es sp�ter noch einmal.";
    			}
                $benutzerfac->getByLogin($_SESSION['reg']['mail'], md5($_SESSION['reg']['pass'].$testbenutzer->salt));
                   
                if ($benutzer = $benutzerfac->getElement())
                {
                    $msg = "Hallo ".$benutzer->anrede."  ".$benutzer->name.chr(10);
                    $msg .= chr(10);
                    $msg .= "Jetzt sind Sie nur noch einen Klick entfernt. Der folgende Link aktiviert einmalig Ihr Profil. Diese Mail ist automatisch generiert, bitte nicht antworten!".chr(10).chr(10);
                    $msg .= "Viel Spass!".chr(10).chr(10);
                    $msg .= "http://www.meineaerzte.at/signin/activate.php?key=".$benutzer->aktivierungskey.chr(10);
                                   
                    $emailfac = new Email(1);
                    $emailfac->setFrom($CONST_MAIL['from']);
                    $emailfac->setTo($benutzer->email);
                    $emailfac->setSubject("Account - Aktivierung - �rztebewertung");
                    $emailfac->setContent($msg);
                    $emailfac->sendMail();
                    
                    // Mail an Langer
                    unset($msg);
                    $msg = "Hallo Herr Langer".chr(10);
                    $msg .= chr(10);
                    $msg .= "Eine neuer User hat sich am ".date("d.m.Y",time()).", angemeldet:".chr(10).chr(10);
                    
                    $msg .= $benutzer->anrede." ".$benutzer->name." ".$benutzer->vorname;
                    $msg .= " hat sich angemeldet!".chr(10).chr(10);
                
                    $emailfac = new Email(1);
                    $emailfac->setFrom($CONST_MAIL['from']);
                    $emailfac->setTo($CONST_MAIL['an']);
                    $emailfac->setSubject("User Anmeldung");
                    $emailfac->setContent($msg);
                    //echo nl2br($msg);
                    $emailfac->sendMail();
                    // End Mail Langer
                    
                    // Newsletter pr�fen
                    if($_POST["newsletter"])
                    {
                        $newsletterfac = new Newsletter();
                        $newsletterfac->getByEmail($benutzer->email);
                        // in Tabelle vorhanden
                        if($newsletter = $newsletterfac->getElement()) 
                        {
                            // schon drin?
                            $err .= 'Mit Ihrer Mailadresse ist schon der Newsletter aboniert!<br />';
                        }
                        else
                        {   
                            // Email eintragen
                            if($_POST['newsletter'])
                            {
                                $newsletterdata[] = '';                         // Id
                                $newsletterdata[] = $benutzer->email;           // Email
                                $newsletterfac->write($newsletterdata);
                            }
                        } 
                     }// end Newsletter
                }
                else
                {
                    $err .= "Ihr Profil konnte leider nicht angelegt werden. Bitte probieren Sie es sp�ter noch einmal.";
                }
            }
            break;
    }
    
    //
    // Wenn Fehler auftaucht, "ein Schritt" zur�ck gehen
    // (auf dem Aktuellen bleiben)
    // 
    
    if ($err) 
    {
        $_GET["step"]--;
        $_SESSION["err"] = $err;
    }
    
    //header("Location: anmelden_user_".$_GET['step'].".htm?sv=1");
    header("Location: anmelden.php?step=".$_GET['step']."&sv=1");
}
else
{
    //
    // Wenn Session nicht gesetzt, gib Fehlermeldung aus
    //
    
    header("Location: ".WEBDIR."msg.php");
}
?>