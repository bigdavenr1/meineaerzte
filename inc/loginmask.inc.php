<?php include(INCLUDEDIR."zusatz.php"); ?>
                </div>
            </div>
            <div id="right">


<?php

// BEGIN BANNER WERBEMODUL
            
            echo '<div style="position:absolute;top:55px;right:20px;">'; 
                    echo '<i>Werbung</i><br/>';
            
            // aktive Banner 
            $anzeigebannerfac = new Anzeigebanner();
            $anzeigebannerfac->getHalfRand();                // Rand Suche auf aktiven Bannern
            
            $x=0;
            if($banner = $anzeigebannerfac->getElement())
            {
                $_SESSION['halfbannerid']=$banner->id;    
                $anzeigebannerfac->update(" `show`='".($banner->show+1)."'");// show in Banner erh�hen
                if ($banner->bild != '')
                {
                    // Grafiktyp auslesen
                    $typ=getimagesize(LOCALDIR."images/werbung/".$banner->bild) ;
                  
                    // je nach typ Endung bestimmen   
                    if ($typ[2]==1) $endung=".gif";
                    if ($typ[2]==2) $endung=".jpg"; 
                        
                    // Reinnamen ohne Endung
                    $name=basename($banner->bild,$endung);
                    
                    // Half-Banners Gr��e auslesen
                    $size=getimagesize(LOCALDIR."images/werbung/".$name."_half".$endung);
                        
                    // HTML Bild schreiben
                    echo $l->makeLink('<img src="'.WEBDIR.'images/werbung/'.$name.'_half'.$endung.'" width="234" height="60"  alt="" />',WEBDIR."bewertungen/showanzeigebanner.php?id=".$banner->id."&amp;sv=1","no","taget='_blank'");
                }

                else 
                {
                    // wenn nur ein Banner vorhanden alte Id l�schen
                    $_SESSION['halfbannerid']="";  
                    echo $l->makeLink('<img src="'.WEBDIR.'images/ihrewerbung_half.gif" width="234" height="60"  alt="" />',WEBDIR."bewertungen/kontakt.php");
                }
            }
            else 
            {
                // wenn nur ein Banner vorhanden alte Id l�schen
                $_SESSION['halfbannerid']="";  
                echo $l->makeLink('<img src="'.WEBDIR.'images/ihrewerbung_half.gif" width="234" height="60"  alt="" />',WEBDIR."bewertungen/kontakt.php");
            }
            echo '</div>';


//////////////////////////////////////////////////////////////////////////////////////////////////
// Loginmaske - Zeigt bei keiner Session['user'] den Login an, ansonsten Statistik
//////////////////////////////////////////////////////////////////////////////////////////////////

if (!$_SESSION["user"])
{?>
    <h1>
        Login
    </h1>
    <form action="<?php echo WEBDIR;?>bewertungen/login.php" method="post"  >
        <label for="loginname">eMail:</label><input type="text" id="loginname" name="name" value="eMail" />
        <label for="pass">Passwort:</label><input type="password" id="pass" name="pass" value="passwort" />
        <input type="submit" value="login" class="submit" /><br class="clr" />
    </form>
    <?php echo $l->makeLink("Passwort vergessen?",WEBDIR."bewertungen/pw_remind.php");?> / <?php echo $l->makeLink("Registrieren",WEBDIR."signin/anmelden.php");?>
    
<?php 
}
else
{
    echo '<div class="sidemenu">Hallo <b> '.$_SESSION['user']->anrede.' '.$_SESSION['user']->titel.' '.$_SESSION['user']->vorname.' '.$_SESSION['user']->name.'</b><br/></div>';
    if ($_SESSION['user']->typ=="admin")
    {?>
        <h1>Adminmenu</h1>
        <div class="sidemenu">
            <?php echo $l->makeLink("Fachgebiete verwalten",WEBDIR."admin/fachgebiete/view.php");?>
            <?php echo $l->makeLink("Unternehmen verwalten",WEBDIR."admin/unternehmen/view.php");?>
            <?php echo $l->makeLink("Unternehmensvorschl�ge verwalten",WEBDIR."admin/unternehmen/view.php?mode=new");?>
            <?php echo $l->makeLink("Premiumanmeldungen verwalten",WEBDIR."admin/unternehmen/view.php?mode=premium");?>
            <?php echo $l->makeLink("Bewertungsbogen verwalten",WEBDIR."admin/bewertungsbogen/view.php");?>
            <?php echo $l->makeLink("Zufriedenheitsbogen verwalten",WEBDIR."admin/zufriedenheitsbogen/view.php");?>
            <?php echo $l->makeLink("Benutzer verwalten",WEBDIR."admin/benutzer/view.php");?>
            <?php echo $l->makeLink("Benutzerrechte verwalten",WEBDIR."admin/rechte/view.php");?>
            <?php echo $l->makeLink("Forum verwalten",WEBDIR."bewertungen/forum.view.php");?>
            <?php echo $l->makeLink("Bewertungen verwalten",WEBDIR."admin/bewertungen/view.php");?>
            <?php echo $l->makeLink("Kommentare verwalten",WEBDIR."admin/kommentare/view.php");?>
            <?php echo $l->makeLink("Antworten verwalten",WEBDIR."admin/antworten/view.php");?>
            <?php echo $l->makeLink("Kl�rungsanfragen einsehen",WEBDIR."admin/klaerung/view.php");?>
            <?php echo $l->makeLink("Schlagworte verwalten",WEBDIR."admin/schlagworte/view.php");?>
            <?php echo $l->makeLink("Die meistgesuchten Begriffe",WEBDIR."admin/schlagworte/suchwort.view.php");?>
            <?php echo $l->makeLink("bestellte Newsletter",WEBDIR."admin/newsletter/view.php");?>
            <?php echo $l->makeLink("Erstbewertungen",WEBDIR."admin/erstbewertung/view.php");?>
            <?php echo $l->makeLink("Werbekunden",WEBDIR."admin/werbung/kunden.view.php");?>
            <?php echo $l->makeLink("Werbe Anzeigen",WEBDIR."admin/werbung/anzeige.view.php");?>
            <?php echo $l->makeLink("Werbe Banner",WEBDIR."admin/werbung/banner.view.php");?>
            <?php echo $l->makeLink("Feste Werbepl�tze",WEBDIR."admin/werbung/festeplaetze.view.php");?>

            <br/>
            <?php echo $l->makeLink("zur Adminstartseite",WEBDIR."admin/index.php");?>
            <?php echo $l->makeLink("Logout",WEBDIR."bewertungen/logout.php");?>
        </div>
     <?
    }
    if ($_SESSION['user']->typ=="benutzer")
    {
    ?>
        <h1>
            Usermenu
        </h1>
        <div class="sidemenu">
            <?php echo $l->makeLink("pers�nliche Daten �ndern",WEBDIR."user/change-data.php");?>
            <?php echo $l->makeLink("Profildaten �ndern",WEBDIR."user/change-online.php");?>
            <?php echo $l->makeLink("Forum aufrufen",WEBDIR."bewertungen/forum.view.php");?>
            <?php echo $l->makeLink("Eintrag f�r Bewertung suchen",WEBDIR."bewertungen/search.php?mod=searchplus&amp;time=new");?>
            <?php echo $l->makeLink("Bewertung zur�ckziehen",WEBDIR."user/bewertung-loeschen.php");?>
            <br/>
            <?php echo $l->makeLink("zur Userseite",WEBDIR."user/index.php");?>
            <?php echo $l->makeLink("Logout",WEBDIR."bewertungen/logout.php");?>
            
        <?php
        if ($_SESSION['rechte'])
        {
        ?>
            <br/><br/>
            <h1>
                Moderatormenu
            </h1>
        <?php
            if($_SESSION['rechte']['antworten']){echo $l->makeLink("Antworten verwalten",WEBDIR."admin/antworten/view.php");} 
            if($_SESSION['rechte']['benutzer']){echo $l->makeLink("Benutzer verwalten",WEBDIR."admin/benutzer/view.php");}
            if($_SESSION['rechte']['bewertungsbogen']){echo $l->makeLink("Bewertungsbogen verwalten",WEBDIR."admin/bewertungsbogen/view.php");}
            if($_SESSION['rechte']['fachgebiete']){echo $l->makeLink("Fachgebiete verwalten",WEBDIR."admin/fachgebiete/view.php");} 
            if($_SESSION['rechte']['klaerung']){echo $l->makeLink("Kl�rungsanfragen einsehen",WEBDIR."admin/klaerung/view.php");} 
            if($_SESSION['rechte']['kommentare']){echo $l->makeLink("Kommentare verwalten",WEBDIR."admin/kommentare/view.php");}
            if($_SESSION['rechte']['rechte']){echo $l->makeLink("Benutzerrechte verwalten",WEBDIR."admin/rechte/view.php");}
            if($_SESSION['rechte']['rechte']){echo $l->makeLink("Forum administrieren",WEBDIR."bewertungen/forum.view.php");}
            if($_SESSION['rechte']['schlagworte'])
            {
                echo $l->makeLink("Schlagworte verwalten",WEBDIR."admin/schlagworte/view.php");
                echo $l->makeLink("Die meistgesuchten Begriffe",WEBDIR."admin/schlagworte/suchwort.view.php");
            }
            if($_SESSION['rechte']['unternehmen'])
            {
                echo $l->makeLink("Unternehmen verwalten",WEBDIR."admin/unternehmen/view.php");
                echo $l->makeLink("Unternehmensvorschl�ge verwalten",WEBDIR."admin/unternehmen/view.php?mode=new");
            }
            if($_SESSION['rechte']['zufriedenheitsbogen']){echo $l->makeLink("Zufriedenheitsbogen verwalten",WEBDIR."admin/zufriedenheitsbogen/view.php");}
         }
         ?>
         </div>
    <?php 
    }

    if ($_SESSION['user']->typ=="unternehmen")
    {?>
        <h1>
            Mitgliedermenu
        </h1>
        <div class="sidemenu">
            <?php 
            if($_SESSION['ustatus']== "N")
            {
               echo $l->makeLink("AGB zustimmen",WEBDIR."unternehmen/index.php");
            }
            else
            {
                echo $l->makeLink("pers�nliche Daten bearbeiten",WEBDIR."unternehmen/change-data.php");
                echo $l->makeLink("Profilbilder bearbeiten",WEBDIR."unternehmen/change-pictures.php");
                echo $l->makeLink("Suchw�rter und Fachgebiete bearbeiten",WEBDIR."unternehmen/change-fachgebiete.php");
                echo $l->makeLink("Beschreibung �ndern",WEBDIR."unternehmen/change-beschreibung.php");
                echo $l->makeLink("�ffnungszeiten bearbeiten",WEBDIR."unternehmen/change-ordzeiten.php");
                echo $l->makeLink("Erhaltene Bewertungen",WEBDIR."unternehmen/bewertungen.show.php");
                echo $l->makeLink("Auf Bewertungskommentare antworten",WEBDIR."unternehmen/bewertungen.view.php");
                if($_SESSION['ustatus'] != "A" && $_SESSION['ustatus'] != "P") echo $l->makeLink("Premiumupdate",WEBDIR."unternehmen/premiumanmeldung.php");
            }
            ?>
            <br/>
            <?php //echo $l->makeLink("zur Unternehmensstartseite",WEBDIR."unternehmen/index.php");?>
            <?php echo $l->makeLink("Logout",WEBDIR."bewertungen/logout.php");?>
        </div>
        <?php
    }
    if ($_SESSION['user']->typ=="werbung")
    {?>
        <h1>
            Werbepartnermenu
        </h1>
        <div class="sidemenu">
            <?php 
             echo $l->makeLink("pers�nliche Daten bearbeiten",WEBDIR."werbekunden/data.edit.php");
             echo $l->makeLink("Anzeige und Banner verwalten",WEBDIR."werbekunden/view.php");
            ?>
            <br/>
            <?php echo $l->makeLink("zur Werbekundenstartseite",WEBDIR."werbekunden/index.php");?>
            <?php echo $l->makeLink("Logout",WEBDIR."bewertungen/logout.php");?>
        </div>
        <?php
    }
    
}
?>