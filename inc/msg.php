<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");
    
    $msg = $_GET['msg'];
    
    switch ($msg)
    {
        case "login":
            echo "<h1>Fehler</h1>Sie sind nicht eingeloggt. Falls Sie noch nicht angemeldet sind, k�nnen Sie das ".$l->makeLink("hier", WEBDIR."signin/anmelden.php")." machen."; 
            break;
            
        case "timeout":
            echo "<h1>Fehler</h1>Sie waren l�ngere Zeit inaktiv und wurden ausgeloggt.";
            break;
            
        case "logout":
            echo "<h1>Logout</h1>Sie haben sich ausgeloggt. Wir w�nschen Ihnen noch einen sch�nen Tag!";
            break;
            
        case "loggedout":
            echo "<h1>Fehler</h1>Sie sind bereits ausgeloggt!";
            break;
            
        case "korrectkey":
            echo "<h1>Aktivierung</h1>Sie haben Ihr Profil erfolgreich aktiviert. Jetzt k�nnen Sie sich jetzt einloggen und unser Portal im vollem Umfang nutzen!!";
            break;
            
        case "korrectkeyf":
            echo "Ihr Ordinationsprofil wurde erfolgreich aktiviert. Wir w�nschen Ihnen viel Erfolg beim schalten Ihrer Werbung!";
            break;
            
        case "wrongkey":
            echo "<h1>Fehler</h1>Dieser Schl�ssel ist nicht g�ltig oder wurde bereits benutzt!";
            break;
            
        case "wronglogin":
            echo "<h1>Fehler</h1>Ihr Benutzername oder Passwort ist falsch.";
            break;
            
        case "inactiv":
            echo "<h1>Fehler</h1>Ihr Profil ist inaktiv. Entweder wurden Sie gesperrt oder haben Ihre eMail-Adresse noch nicht best�tigt.";
            break;
            
        case "keychange":
            echo "<h1>Fehler</h1>Bei Ihnen steht noch eine Best�tigungsmail aus.";
            break;

        case "mailkorrectkey":
            echo "<h1>�nderung �bernommen</h1>Ihre Emailadresse wurde ge�ndert.";
            break;
            
        case "rechte":
            echo "<h1>Fehler</h1>Sie haben hier keinen Zugriff!";
            break;
            
        case "newpass":
            echo '<h1>Passwort zur�ckgesetzt</h1>Ihr Passwort wurde erfolgreich zur�ckgesetzt.t<br /><br />Sie haben eine >eMail erhalten in der Sie das neue Passwort finden<br/><br/>Das Passwort k�nnen Sie in Ihrem gesch�tzten bereich problemlos �ndern. <br /><br />'.$l->makeLink("zur�ck zur Startseite",WEBDIR."index.php");
            break;
            
        case "newmail":
            echo '<h1>�nderung erfolgreich</h1>Die Benutzerdaten wurden erfolgreich ge�ndert<br /><br />Da Sie die eMail-Adresse ver�ndert haben, wurden Sie automatisch ausgeloggt und Ihnen wurde eine eMail zur Verifizierung an die neu eingetragene eMail-Adresse versendet.<br/><br/> Ohne die Best�tigung der eMail-Adresse k�nnen Sie sich nicht einloggen und Kommentare zu Bewertungen verfassen.<br /><br />'.$l->makeLink("zur�ck zur Startseite",WEBDIR."index.php");
            break;
        
        case "bewertung_ok":
            echo "<h1>Bewertung erfolgreich</h1><br/>Sie haben diesen Eintrag erfolgreich bewertet!<br />";
            if($_GET['id']) echo $l->makeLink("Zur�ck zur Detailansicht", WEBDIR."bewertungen/showeintrag.php?id=".$_GET['id'] , "login");
            echo '<br />'.$l->makeLink("Zur erweiterten Suche", WEBDIR."bewertungen/search.php?mod=searchplus&amp;time=new", "login");
            break;
            
        case "zufriedenheit_ok":
            echo "<h1>Bewertung der Zuftriedenheit erfolgreich</h1><br/>Sie haben erfolgreich Ihre Zufriedenheit mit der Webseite bewertet!";
            break;
                   
        default:
            echo "<h1>Fehler</h1>Sorry, ein unbekannter Fehler ist aufgetreten.";
            break;
    }


include(INCLUDEDIR."footer.inc.php");
?>