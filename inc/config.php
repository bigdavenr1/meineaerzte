<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
ini_set('memory_limit', "64M");

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// C O N F I G  - aertztebewertungen
//              - Datum:  M�rz 2008
//              - Author: nb-cooperation
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

// D I R S
// WICHTIG: Framework braucht $root
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$root = "/";

define('WEBDIR', $root);
define('LOCALDIR', $_SERVER['DOCUMENT_ROOT'].$root);
define('INCLUDEDIR', $_SERVER['DOCUMENT_ROOT'].$root."inc/");

// D A T E N B A N K
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$sql['maxergebnisse']=1000;
$sql["host"] = "dd14200.kasserver.com";
$sql["db"]   = "d019badb";
$sql["user"] = "d019badb";
$sql["pass"] = "bolygo";

$sql["table_data"]                  = "unternehmen";
$sql["table_kat"]                   = "Kategorien";
$sql["table_menu"]                  = "menu";
$sql["table_benutzer"]              = "benutzer";
$sql["table_kommentar"]             = "Kommentar";
$sql["table_suchwolke"]             = "suchwolke";
$sql["table_fach"]                  = "fachgebiete";
$sql["table_bewertungspunkte"]      = "bewertungspunkte";
$sql["table_bewertung"]             = "bewertungen";
$sql["table_forum"]                 = "forum";
$sql["table_forummsg"]              = "forummsg";
$sql["table_datei"]                 = "datei";
$sql["table_suchworte"]             = "suchworte";              // class Suchworte
$sql["table_schlagworte"]           = "schlagworte";            // class Schlagworte
$sql["table_ordzeiten"]             = "ordzeiten";              // class Ordzeiten
$sql["table_zufriedenheitspunkte"]  = "zufriedenheitspunkte";   // class Zufriedenheitspunkt
$sql["table_zufriedenheit"]         = "zufriedenheit";          // class Zufriedenheit
$sql["table_katzufriedenheit"]      = "katzufriedenheit";       // class Zufriedenheitspunkt
$sql["table_klaerung"]              = "klaerung";               // class Zufriedenheitspunkt
$sql["table_newsletter"]            = "newsletter";             // class Newsletter
$sql["table_erstbewertung"]         = "erstbewertung";          // class Erstbewertung
$sql["table_anzeige"]               = "anzeige";                // class Anzeige / WEBEMODUL
$sql["table_anzeigeordzeiten"]      = "anzeigeordzeiten";       // class Anzeigeordzeiten / WEBEMODUL
$sql["table_anzeigeort"]            = "anzeigeort";             // class Anzeigeort / WEBEMODUL
$sql["table_anzeigefachgebiete"]    = "anzeigefachgebiete";     // class Anzeigefachgebiete / WEBEMODUL
$sql["table_anzeigerotation"]       = "anzeigerotation";        // class Anzeigerotation / WEBEMODUL
$sql["table_anzeigebanner"]         = "anzeigebanner";          // class Anzeigebanner / WEBEMODUL
$sql["table_bewertungtemp"]         = "bewertungentemp";        // class Bewertungtemp

// K O N S T A N T E N
////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Title - Meta-Keywords - Meta-Description
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$CONST_META[0]["url"] = "default";
$CONST_META[0]["title"] = "Meine Ärzte! Ihr Online-Ärzte-Portal - finden, informieren und bewerten. www.meineaerzte.at";
$CONST_META[0]["keywords"] = "Arzt, Ärzte, Zahnarzt, Frauenarzt, Bewertung, Meinung, Praxis, Ordination, Ordinationszeiten, Allgemeinmedizin, Hausarzt, Gesundheit";
$CONST_META[0]["description"] = "Meine Gesundheit - Meine Meinung - Mein Arzt! www.meineaerzte.at";



// Email
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$CONST_MAIL['from'] = "info@meineaerzte.at";
$CONST_MAIL['an']   = "info@meineaerzte.at";
$CONST_MAIL['cc']   = "";
$CONST_MAIL['bcc']  = "";

// Icons
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$icon_info = '<img src="'.WEBDIR.'images/icons/page_find.gif" alt="details" title="details" />';
$icon_refresh = '<img src="'.WEBDIR.'images/icons/action_refresh.gif" alt="Status ändern" title="Status ändern" />';
$icon_edit_small = '<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />';
$icon_delete_small = '<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />';
$icon_archiv = '<img src="'.WEBDIR.'images/icons/box.gif" alt="Archiv" title="Archiv" />';
$icon_archiv_small = '<img src="'.WEBDIR.'images/icons/box_small.gif" alt="Archiv" title="Archiv" />';
$icon_in_gelesen_true = '<img src="'.WEBDIR.'images/icons/email_in_open.gif" alt="Engang gelesen" title="Eingang gelesen" />';
$icon_in_gelesen_false = '<img src="'.WEBDIR.'images/icons/email_in.gif" alt="Eingang nicht gelesen" title="Eingang nicht gelesen" />';
$icon_out_gelesen_true = '<img src="'.WEBDIR.'images/icons/email_out_open.gif" alt="Ausgang gelesen" title="Ausgang gelesen" />';
$icon_out_gelesen_false = '<img src="'.WEBDIR.'images/icons/email_out.gif" alt="Ausgang nicht gelesen" title="Ausgang nicht gelesen" />';
$icon_in_sw = '<img src="'.WEBDIR.'images/icons/email_in_sw.gif" alt="zum Posteingang" title="zum Posteingang" />';
$icon_out_sw = '<img src="'.WEBDIR.'images/icons/email_out_sw.gif" alt="zum Postausgang" title="zum Postausgang" />';
$icon_archiv_sw = '<img src="'.WEBDIR.'images/icons/email_archiv_sw.gif" alt="zum Postarchiv" title="zum Postarchiv" />';
$icon_arrow_up = '<img src="'.WEBDIR.'images/icons/arrow-up-big.gif" alt="hoch" title="hoch" />';
$icon_arrow_down = '<img src="'.WEBDIR.'images/icons/arrow-down-big.gif" alt="runter" title="runter" />';
$icon_arrow_down_red = '<img src="'.WEBDIR.'images/icons/arrow-down-big_red.gif" alt="runter" title="runter" />';
$icon_arrow_up_small = '<img src="'.WEBDIR.'images/icons/arrow-up-small.gif" alt="hoch" title="hoch" />';
$icon_arrow_down_small = '<img src="'.WEBDIR.'images/icons/arrow-down-small.gif" alt="runter" title="runter" />';
$icon_arrow_down_small_red = '<img src="'.WEBDIR.'images/icons/arrow-down-small_red.gif" alt="runter" title="runter" />';
$icon_arrow_up_down = '<img src="'.WEBDIR.'images/icons/arrow_up_down_2.gif" alt="" title="" />';
$icon_arrow_right_small = '<img src="'.WEBDIR.'images/icons/arrow-right_small.gif" alt="" title="" />';
$icon_camera = '<img src="'.WEBDIR.'images/icons/camera.gif" alt="" title="" />';
$icon_camera_small = '<img src="'.WEBDIR.'images/icons/camera_small.gif" alt="" title="" />';
$icon_neu_small = '<img src="'.WEBDIR.'images/icons/note_new.gif" alt="" title="" />';
$iconcommu1 = '<img src="'.WEBDIR.'images/icons/page_user_dark.gif" alt="�bersicht" />';
$iconcommu8 = '<img src="'.WEBDIR.'images/icons/box.gif" alt="Forum" />';
$iconstd5 = '<img src="'.WEBDIR.'images/icons/box.gif" alt="Forum" />';
$icon_sort_asc = '<img src="'.WEBDIR.'images/icons/s_asc.gif" alt="sort_asc" />';
$icon_sort_desc = '<img src="'.WEBDIR.'images/icons/s_desc.gif" alt="sort_desc" />';
$pre = "&raquo;&nbsp;";

// S E S S I O N - I N I T I A L I S I E R U N G
// Muss vor den Klassen initialisiert werden
////////////////////////////////////////////////////////////////////////////////////////////////////////////

    session_start();

// S T A N D A R D - I N C L U D E S
////////////////////////////////////////////////////////////////////////////////////////////////////////////

include(INCLUDEDIR."std/func.inc.php");
include(INCLUDEDIR."std/link.class.php");
include(INCLUDEDIR."std/meta.class.php");
include(INCLUDEDIR."std/basicdb.class.php");
include(INCLUDEDIR."std/menuhandle.class.php");
include(INCLUDEDIR."std/benutzer.class.php");
include(INCLUDEDIR."std/textparser.class.php");
include(INCLUDEDIR."std/htmlarea.class.php");
include(INCLUDEDIR."std/daten.class.php");
include(INCLUDEDIR."std/datei.class.php");


// K L A S S E N - I N C L U D E S
////////////////////////////////////////////////////////////////////////////////////////////////////////////

include(INCLUDEDIR."class/kategorien.class.php");
include(INCLUDEDIR."class/menu.class.php");
include(INCLUDEDIR."class/blogkommentar.class.php");
include(INCLUDEDIR."class/suchwolke.class.php");
include(INCLUDEDIR."class/fachgebiete.class.php");
include(INCLUDEDIR."class/bewertungspunkte.class.php");
include(INCLUDEDIR."class/bewertung.class.php");
include(INCLUDEDIR."class/forum.class.php");
include(INCLUDEDIR."class/forummsg.class.php");
include(INCLUDEDIR."class/suchworte.class.php");        // f�r Tabelle suchworte
include(INCLUDEDIR."class/schlagworte.class.php");      // f�r Tabelle schlagworte
include(INCLUDEDIR."class/ordzeiten.class.php");        // f�r Tabelle ordzeiten
include(INCLUDEDIR."class/zufriedenheit.class.php");    // f�r Tabelle zufriedenheit
include(INCLUDEDIR."class/zufriedenheitspunkte.class.php");    // f�r katzufriedenheit und zufriedenheitspunkte
include(INCLUDEDIR."class/klaerung.class.php");    // f�r katzufriedenheit und zufriedenheitspunkte
include(INCLUDEDIR."class/newsletter.class.php");        // f�r Tabelle newsletter
include(INCLUDEDIR."class/erstbewertung.class.php");     // f�r Tabelle erstbewertung
include(INCLUDEDIR."class/anzeige.class.php");           // f�r Tabelle anzeige / WERBEMODUL
include(INCLUDEDIR."class/anzeigeordzeiten.class.php");  // f�r Tabelle anzeigeordzeiten / WERBEMODUL
include(INCLUDEDIR."class/anzeigeort.class.php");        // f�r Tabelle anzeigeort / WERBEMODUL
include(INCLUDEDIR."class/anzeigefachgebiete.class.php");// f�r Tabelle anzeigefachgebiete / WERBEMODUL
include(INCLUDEDIR."class/anzeigerotation.class.php");   // f�r Tabelle anzeigerotation / WERBEMODUL
include(INCLUDEDIR."class/anzeigebanner.class.php");     // f�r Tabelle anzeigebanner / WERBEMODUL
include(INCLUDEDIR."class/bewertungtemp.class.php");     // f�r Tabelle Tempbewertung


// I N I T I A L I S I E R T E   O B J E K T E
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$l = new sessionLink();

// U S E R - V E R W A L T U N G
// R E C H T E - V E R W A L T U N G
////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Rechte einlesen
$_SESSION['rechte']=unserialize($_SESSION['user']->rechte);

// USER
if (isInUrl("/user/"))
{
    if(!$_SESSION["user"]->typ == "benutzer" || !$_SESSION["user"]->typ == "admin")
    {
        header("Location: ".WEBDIR."inc/msg.php?msg=login");
    }
}

// Unternehmen
if (isInUrl("/unternehmen/") && !isInUrl("/admin/") && $_SESSION["user"]->typ != "unternehmen")
{
    header("Location: ".WEBDIR."inc/msg.php?msg=login");
}

// ADMIN + Eingeschr�nkte Rechte f�r User
if (isInUrl("/admin/") && $_SESSION["user"]->typ != "admin")
{
    if (isInUrl("/admin/antworten/") && !$_SESSION['rechte']['antworten']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/bewertungen/") && !$_SESSION['rechte']['bewertungen']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/benutzer/") && !$_SESSION['rechte']['benutzer']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/bewertungsbogen/") && !$_SESSION['rechte']['bewertungsbogen']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/fachgebiete/") && !$_SESSION['rechte']['fachgebiete']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/forum/") && !$_SESSION['rechte']['forum']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/klaerung/") && !$_SESSION['rechte']['klaerung']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/kommentare/") && !$_SESSION['rechte']['kommentare']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/rechte/") && !$_SESSION['rechte']['rechte']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/schlagworte/") && !$_SESSION['rechte']['schlagworte']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/unternehmen/") && !$_SESSION['rechte']['unternehmen']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/zufriedenheitsbogen/") && !$_SESSION['rechte']['zufriedenheitsbogen']) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
    if (isInUrl("/admin/index.php")) header("Location: ".WEBDIR."inc/msg.php?msg=rechte");
}


// Deutsches Datum
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$monate[1] = 'Januar';
$monate[2] = 'Februar';
$monate[3] = 'M�rz';
$monate[4] = 'April';
$monate[5] = 'Mai';
$monate[6] = 'Juni';
$monate[7] = 'Juli';
$monate[8] = 'August';
$monate[9] = 'September';
$monate[10] = 'Oktober';
$monate[11] = 'November';
$monate[12] = 'Dezember';

?>