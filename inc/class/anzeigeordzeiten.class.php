<?php
////////////////////////////////////////////////////////////////////////////////
// Anzeigeordzeiten f�r Tabelle anzeigeordzeiten
////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Anzeigeordzeiten"))
{
    class Anzeigeordzeiten extends Basicdb
    {
       // Konstruktor
        ////////////////////////////////////////////////////////////////////////
         function Anzeigeordzeiten($anzahl = 0,$affix = false)
        {
            global $sql;
            $this->table = $sql["table_anzeigeordzeiten"];
            $this->anzahl = $anzahl;
            $this->offsetaffix = $affix;
            parent::Basicdb();
        }

        // User Funktionen
        ////////////////////////////////////////////////////////////////////////
         function getByFirmId($firmid)
        {
            parent::createQuery("WHERE firmid='".mysql_real_escape_string($firmid)."'");
        }
        
         function getByAnzeigeId($anzeigeid)
        {
            parent::createQuery("WHERE anzeigeid='".mysql_real_escape_string($anzeigeid)."'");
        }
        
         function getByAnzeigeIdAndDay($anzeigeid,$daystart)
        {
            parent::createQuery("WHERE anzeigeid='".mysql_real_escape_string($anzeigeid)."' AND daystart='".mysql_real_escape_string($daystart)."' ORDER by id ASC" );
        }
        
         function getByAnzeigeIdAndDayVormittags($anzeigeid,$daystart)
        {
            parent::createQuery("WHERE anzeigeid='".mysql_real_escape_string($anzeigeid)."' AND daystart='".mysql_real_escape_string($daystart)."' AND timeend <='25'");
        }
         function getByAnzeigeIdAndDayNachmittags($firmid,$daystart)
        {
            parent::createQuery("WHERE anzeigeid='".mysql_real_escape_string($anzeigeid)."' AND daystart='".mysql_real_escape_string($daystart)."' AND timestart > '25'");
        }
        
        function sucheZeitraum($sd,$ed,$st,$et,$fid=false,$id=false)
        {
            if($fid && $id)
            { 
                $statement .= "firmid='".mysql_real_escape_string($fid)."' AND id!='".mysql_real_escape_string($id)."' AND ";
            }
            $statement .= " (( daystart <='".mysql_real_escape_string($sd)."' AND dayend >='".mysql_real_escape_string($sd)."') OR";
            $statement .= " ( daystart <='".mysql_real_escape_string($ed)."' AND dayend >='".mysql_real_escape_string($sd)."')) AND ";
            $statement .= " (( timestart <='".mysql_real_escape_string($st)."' AND timeend >='".mysql_real_escape_string($st)."') OR";
            $statement .= " ( timestart <='".mysql_real_escape_string($et)."' AND timeend >='".mysql_real_escape_string($st)."')) ";
            
            parent::createQuery("WHERE ".$statement);
        }
                
        function parseDay($day,$mod="short") // Tage ausgeben
        {
           switch($day){
              case 1:
                  if($mod == "short") return "Mo";
                  else return "Montag";
                  break;
              case 2:
                  if($mod == "short") return "Di";
                  else return "Dienstag";
                  break;
              case 3:
                  if($mod == "short") return "Mi";
                  else return "Mittwoch";
                  break;
              case 4:
                  if($mod == "short") return "Do";
                  else return "Donnerstag";
                  break;
              case 5:
                  if($mod == "short") return "Fr";
                  else return "Freitag";
                  break;
              case 6:
                  if($mod == "short") return "Sa";
                  else return "Samstag";
                  break;
              case 7:
                  if($mod == "short") return "So";
                  else return "Sonntag";
                  break;
              default:
                  return "Ung�ltiges Datum";
                  break;
              }
          }
          
        function parseTime($time,$mod="std") // Zeiten ausgeben
        {
           switch($time){
              case 1:
                  if($mod == "std") return "0:00";
                  else return "0.00";
                  break;
              case 2:
                  if($mod == "std") return "0:30";
                  else return "0.30";
                  break;
              case 3:
                  if($mod == "std") return "1:00";
                  else return "1.00";
                  break;
              case 4:
                  if($mod == "std") return "1:30";
                  else return "1.30";
                  break;
              case 5:
                  if($mod == "std") return "2:00";
                  else return "2.00";
                  break;
              case 6:
                  if($mod == "std") return "2:30";
                  else return "2.30";
                  break;
              case 7:
                  if($mod == "std") return "3:00";
                  else return "3.00";
                  break;
              case 8:
                  if($mod == "std") return "3:30";
                  else return "3.30";
                  break;
              case 9:
                  if($mod == "std") return "4:00";
                  else return "4.00";
                  break;
              case 10:
                  if($mod == "std") return "4:30";
                  else return "4.30";
                  break;
              case 11:
                  if($mod == "std") return "5:00";
                  else return "5.00";
                  break;
              case 12:
                  if($mod == "std") return "5:30";
                  else return "5.30";
                  break;
              case 13:
                  if($mod == "std") return "6:00";
                  else return "6.00";
                  break;
              case 14:
                  if($mod == "std") return "6:30";
                  else return "6.30";
                  break;
              case 15:
                  if($mod == "std") return "7:00";
                  else return "7.00";
                  break;
              case 16:
                  if($mod == "std") return "7:30";
                  else return "7.30";
                  break;
              case 17:
                  if($mod == "std") return "8:00";
                  else return "8.00";
                  break;
              case 18:
                  if($mod == "std") return "8:30";
                  else return "8.30";
                  break;
              case 19:
                  if($mod == "std") return "9:00";
                  else return "9.00";
                  break;
              case 20:
                  if($mod == "std") return "9:30";
                  else return "9.30";
                  break;
              case 21:
                  if($mod == "std") return "10:00";
                  else return "10.00";
                  break;
              case 22:
                  if($mod == "std") return "10:30";
                  else return "10.30";
                  break;
              case 23:
                  if($mod == "std") return "11:00";
                  else return "11.00";
                  break;
              case 24:
                  if($mod == "std") return "11:30";
                  else return "11.30";
                  break;
              case 25:
                  if($mod == "std") return "12:00";
                  else return "12.00";
                  break;
              case 26:
                  if($mod == "std") return "12:30";
                  else return "12.30";
                  break;
              case 27:
                  if($mod == "std") return "13:00";
                  else return "13.00";
                  break;
              case 28:
                  if($mod == "std") return "13:30";
                  else return "13.30";
                  break;
              case 29:
                  if($mod == "std") return "14:00";
                  else return "14.00";
                  break;
              case 30:
                  if($mod == "std") return "14:30";
                  else return "14.30";
                  break;
              case 31:
                  if($mod == "std") return "15:00";
                  else return "15.00";
                  break;
              case 32:
                  if($mod == "std") return "15:30";
                  else return "15.30";
                  break;
              case 33:
                  if($mod == "std") return "16:00";
                  else return "16.00";
                  break;
              case 34:
                  if($mod == "std") return "16:30";
                  else return "16.30";
                  break;
              case 35:
                  if($mod == "std") return "17:00";
                  else return "17.00";
                  break;
              case 36:
                  if($mod == "std") return "17:30";
                  else return "17.30";
                  break;
              case 37:
                  if($mod == "std") return "18:00";
                  else return "18.00";
                  break;
              case 38:
                  if($mod == "std") return "18:30";
                  else return "18.30";
                  break;
              case 39:
                  if($mod == "std") return "19:00";
                  else return "19.00";
                  break;
              case 40:
                  if($mod == "std") return "19:30";
                  else return "19.30";
                  break;
              case 41:
                  if($mod == "std") return "20:00";
                  else return "20.00";
                  break;
              case 42:
                  if($mod == "std") return "20:30";
                  else return "20.30";
                  break;
              case 43:
                  if($mod == "std") return "21:00";
                  else return "21.00";
                  break;
              case 44:
                  if($mod == "std") return "21:30";
                  else return "21.30";
                  break;
              case 45:
                  if($mod == "std") return "22:00";
                  else return "22.00";
                  break;
              case 46:
                  if($mod == "std") return "22:30";
                  else return "22.30";
                  break;
              case 47:
                  if($mod == "std") return "23:00";
                  else return "23.00";
                  break;
              case 48:
                  if($mod == "std") return "23:30";
                  else return "23.30";
                  break;
              case 49:
                  if($mod == "std") return "24:00";
                  else return "24.00";
                  break;
              default:
                  return "Ung�ltige Zeit";
                  break;
              }
          }
        // �berladen
        ////////////////////////////////////////////////////////////////////////
                
         function getById($id)
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
   }
}

?>