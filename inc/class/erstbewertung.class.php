<?php
////////////////////////////////////////////////////////////////////////////////
// UserKlasse - Klasse f�r Zugriff auf Tabelle erstbewertung 
//              Function ->getBewerteteUnternehmen auch
//              Zugriff auf Tabelle unternehmen und    LEFT JOIN bewertungen 
////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Erstbewertung"))
{
    class Erstbewertung extends Basicdb
    {

        // Konstruktor
        ////////////////////////////////////////////////////////////////////////

        function Erstbewertung($anzahl=false)
        {
            global $sql;
            $this->table = $sql["table_erstbewertung"];
            parent::Basicdb();    
            $this->anzahl = $anzahl;       
        }

        // User Funktionen
        ////////////////////////////////////////////////////////////////////////
        function getBewerteteUnternehmen()
        {
            $this->felder = " bewertungen.id AS bewertung, unternehmen.id AS unternehmen, unternehmen.famname AS unternehmenname, unternehmen.vorname AS unternehmenvorname";
            // Tabelle auf unternehmen
            $this->table = "unternehmen";
             
            $join = " RIGHT JOIN bewertungen ON (bewertungen.unternehmenid=unternehmen.id) ";
            $group = " GROUP BY unternehmen.id ";
            $statement = "";
            
            parent::createQuery($join." ".$statement." ".$group); // ." ORDER BY ".$order.", unternehmen.status DESC"
            
            // Tabelle und Felder zur�cksetzen
            $this->table = "erstbewertung";
            $this->felder = false;
        }
        
        function getUnternehmen()
        {
            $this->felder = " unternehmen.id AS unternehmen, unternehmen.famname AS unternehmenname, unternehmen.vorname AS unternehmenvorname, erstbewertung.id AS erstid, erstbewertung.status AS erststatus";
            // Tabelle auf unternehmen
            $this->table = "erstbewertung";
             
            $join = " LEFT JOIN unternehmen ON (erstbewertung.unternehmenid=unternehmen.id) ";
            $statement = "";
            
            parent::createQuery($join." ".$statement); 
            
            // Felder zur�cksetzen
            $this->felder = false;
        }
        
        function insertUnternehmen($uid)
        {
            $statement ="('".mysql_real_escape_string($uid)."', '1')";
            $query = "INSERT INTO `erstbewertung` (`unternehmenid`, `status`) VALUES ".$statement;
            $this->result = mysql_query($query);
        }
        
        function getByUnternehmenId($unternehmenid)
        {
            parent::createQuery("WHERE unternehmenid='".mysql_real_escape_string($unternehmenid)."'");
        }
        
        
        // �berladen
        ////////////////////////////////////////////////////////////////////////
        function getById($id)
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
 
    }
}

?>