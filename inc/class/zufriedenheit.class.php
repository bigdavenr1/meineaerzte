<?php
////////////////////////////////////////////////////////////////////////////////
// zufriedenheit.class.php UserKlasse - zum Zugriff auf Tabelle zufriedenheit
////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Zufriedenheit"))
{
    class Zufriedenheit extends Basicdb
    {
        // Konstruktor
        ////////////////////////////////////////////////////////////////////////
       
         function Zufriedenheit($anzahl=false)
        {
            global $sql;
            $this->table = $sql["table_zufriedenheit"];
            parent::Basicdb();
            $this->anzahl = $anzahl;
           
        }
        
        // User Funktionen
        ////////////////////////////////////////////////////////////////////////
        
        /* function getByUid($uid,$sort="")
        {
            parent::createQuery("WHERE unternehmenid='".mysql_real_escape_string($uid)."'");
        }*/
        function getPunkteWithKat()
        {
            $this->felder="* , katzufriedenheit.name AS katname ";
            $this->table="katzufriedenheit,zufriedenheitspunkte";
            parent::createQuery("WHERE zufriedenheitspunkte.kat = katzufriedenheit.id ORDER BY katzufriedenheit.id" );
            $this->felder=false;
            $this->table = $sql["table_zufriedenheit"];
        }
        
        function getByUserId($user)
        {
            parent::createQuery("WHERE userid='".mysql_real_escape_string($user)."' ");
        }
        
        function getNewComment()
        {
            parent::createOwnQuery("SELECT zufriedenheit.id, zufriedenheit.kommentar, zufriedenheit.userid, benutzer.email AS username, unternehmen.name AS unternehmen, unternehmen.famname AS name, unternehmen.vorname AS vorname FROM bewertungen LEFT JOIN benutzer ON (benutzer.id=bewertungen.userid) RIGHT JOIN unternehmen ON ( unternehmen.id = bewertungen.unternehmenid ) WHERE bewertungen.aktiv='0' AND bewertungen.kommentar!=''");
        }
        
              
    }
}
?>