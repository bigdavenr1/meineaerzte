<?php
////////////////////////////////////////////////////////////////////////////////
// UserKlasse - Klasse f�r Zugriff auf Tabelle klaerung und LEFT JOIN abfragen
////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Klaerung"))
{
    class Klaerung extends Basicdb
    {

        // Konstruktor
        ////////////////////////////////////////////////////////////////////////

        function Klaerung($anzahl=false)
        {
            global $sql;
            $this->table = $sql["table_klaerung"];
            parent::Basicdb();    
            $this->anzahl = $anzahl;       
        }

        // User Funktionen
        ////////////////////////////////////////////////////////////////////////
        function getKlaerung($uid=false,$fid=false)
        {
            if ($uid) $where="WHERE benutzer.id='".mysql_real_escape_string($uid);
            if ($fid) $where="WHERE unternehmen.id='".mysql_real_escape_string($fid);
            $this->felder=" klaerung.id, bewertungen.id AS bewertung, bewertungen.datum AS datum, benutzer.id AS benutzer, benutzer.nickname AS benutzername, benutzer.email AS benutzermail, unternehmen.id AS unternehmen, unternehmen.famname AS unternehmenname, unternehmen.vorname AS unternehmenvorname, unternehmen.email AS unternehmenmail";
            parent::createQuery("LEFT JOIN bewertungen ON (bewertungen.id=klaerung.bid) LEFT JOIN benutzer ON (benutzer.id=bewertungen.userid) LEFT JOIN unternehmen ON (unternehmen.id=bewertungen.unternehmenid) ".$where." ORDER BY datum DES");
            //parent::createOwnQuery("SELECT klaerung.id, bewertungen.id AS bewertung, bewertungen.datum AS datum, benutzer.id AS benutzer, benutzer.nickname AS benutzername, benutzer.email AS benutzermail, unternehmen.id AS unternehmen, unternehmen.famname AS unternehmenname, unternehmen.vorname AS unternehmenvorname, unternehmen.email AS unternehmenmail FROM ".$this->table." LEFT JOIN bewertungen ON (bewertungen.id=klaerung.bid) LEFT JOIN benutzer ON (benutzer.id=bewertungen.userid) LEFT JOIN unternehmen ON (unternehmen.id=bewertungen.unternehmenid) ".$where." ORDER BY datum DESC");
        }
        
        // sortierte Suche ORDER-String wird �bergeben
        function getKlaerungSort($sort)
        {
            $this->felder=" klaerung.id, bewertungen.id AS bewertung, bewertungen.datum AS datum, benutzer.id AS benutzer, benutzer.nickname AS benutzername, benutzer.email AS benutzermail, unternehmen.id AS unternehmen, unternehmen.famname AS unternehmenname, unternehmen.vorname AS unternehmenvorname, unternehmen.email AS unternehmenmail";
            parent::createQuery("LEFT JOIN bewertungen ON (bewertungen.id=klaerung.bid) LEFT JOIN benutzer ON (benutzer.id=bewertungen.userid) LEFT JOIN unternehmen ON (unternehmen.id=bewertungen.unternehmenid) ".$sort);
            //parent::createOwnQuery("SELECT klaerung.id, bewertungen.id AS bewertung, bewertungen.datum AS datum, benutzer.id AS benutzer, benutzer.nickname AS benutzername, benutzer.email AS benutzermail, unternehmen.id AS unternehmen, unternehmen.famname AS unternehmenname, unternehmen.vorname AS unternehmenvorname, unternehmen.email AS unternehmenmail FROM ".$this->table." LEFT JOIN bewertungen ON (bewertungen.id=klaerung.bid) LEFT JOIN benutzer ON (benutzer.id=bewertungen.userid) LEFT JOIN unternehmen ON (unternehmen.id=bewertungen.unternehmenid) ".$sort);
        }
        
        // �berladen
        ////////////////////////////////////////////////////////////////////////
        function getById($id)
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
 
    }
}

?>