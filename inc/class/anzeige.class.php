<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// Anzeige f�r Tabelle anzeige
//////////////////////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Anzeige"))
{
    class Anzeige extends Basicdb
    {
       // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
         function Anzeige($anzahl = 0,$affix = false)
        {
            global $sql;
            $this->table = $sql["table_anzeige"];
            $this->anzahl = $anzahl;
            $this->offsetaffix = $affix;
            parent::Basicdb();
        }

        // �berladen
        ////////////////////////////////////////////////////////////////////////
        function getAll($statement = "ORDER by key DESC")
        {
            parent::createQuery($statement);
        }
        
        function getById($id)
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
        
        // User Funktionen
        ////////////////////////////////////////////////////////////////////////
        function getAktivByKey($key)
        {
            parent::createQuery("WHERE `status`='1' AND `key`='".mysql_real_escape_string($key)."' ORDER by RAND() Limit 0,10");
        }
        
        function getAktiv()
        {
            parent::createQuery("WHERE `status`='1' ORDER by RAND() Limit 0,10");
        }
        
        function getAllAktiv()
        {
            parent::createQuery("WHERE `status`='1' ");
        }
        
        function searchAktivAnzeige($str)
        {
            $str2=explode(" ",mysql_real_escape_string($str));
                       
            for ($x=0; $x<sizeof($str2);$x++)
            { 
                $statement.=" AND (suchwort LIKE '%".$str2[$x]."%')";
            }
            parent::createQuery(" WHERE status='1' ".$statement." ORDER by RAND() Limit 0,10");
        }
        
        function searchAktivAnzeigeByKey($str,$key)
        {
            $str2=explode(" ",mysql_real_escape_string($str));
                       
            for ($x=0; $x<sizeof($str2);$x++)
            { 
                $statement.=" AND (suchwort LIKE '%".$str2[$x]."%')";
            }
            parent::createQuery(" WHERE status='1' AND `key`='".mysql_real_escape_string($key)."' ".$statement." ORDER by RAND() Limit 0,10");
        }
        
        function getByWerbeid($werbeid)
        {
            parent::createQuery("WHERE werbeid='".mysql_real_escape_string($werbeid)."'");
        }
        
        function getByFachidAndPlz($fachid ,$plz)
        {
            $this->felder="anzeige.id AS id";
            parent::createQuery("LEFT JOIN anzeigeort ON (anzeigeort.fachid='".mysql_real_escape_string($fachid)."' AND anzeigeort.plz='".mysql_real_escape_string($plz)."') WHERE anzeige.id= anzeigeort.platz1 OR anzeige.id= anzeigeort.platz2 OR anzeige.id= anzeigeort.platz3");
        }
        
        function getByFachidAndOrt($fachid ,$ort)
        {
            $this->felder="anzeige.id AS id";
            parent::createQuery("LEFT JOIN anzeigeort ON (anzeigeort.fachid='".mysql_real_escape_string($fachid)."' AND anzeigeort.ort='".mysql_real_escape_string($ort)."') WHERE anzeige.id= anzeigeort.platz1 OR anzeige.id= anzeigeort.platz2 OR anzeige.id= anzeigeort.platz3");
        }
        
        
        function getRandByFachidAndPlz($fachid,$plz)
        {
            $this->felder="*, anzeige.id AS id, anzeige.key AS `key`";
            parent::createQuery("LEFT JOIN anzeigefachgebiete ON (anzeigefachgebiete.fachid='".mysql_real_escape_string($fachid)."' AND anzeigefachgebiete.plz='".mysql_real_escape_string($plz)."') WHERE anzeigefachgebiete.anzeigeid=anzeige.id ORDER BY RAND() LIMIT 0,10 ");
        }
        
        
        function getLastByWerbeid($werbeid)
        {
            parent::createQuery("WHERE werbeid='".mysql_real_escape_string($werbeid)."' ORDER by `id` DESC" );
        }
        
        public function writePic($pic,$key,$type)// erstellt Thumbnailbild
        {
            // Gr��e auslesen (size[0]=breite, size[1]=h�he)
            $size = getimagesize($pic);

            if ($size[0] > $size[1])
            {
                $thumbBreite = 125;
                $thumbHoehe = (125*$size[1])/$size[0];
                $thumbBreite2 = 360;
                $thumbHoehe2 = (360*$size[1])/$size[0];
            }
            
            else
            {
                $thumbHoehe = 125;
                $thumbBreite= (125*$size[0])/$size[1];
                $thumbHoehe2 = 360;
                $thumbBreite2 = (360*$size[0])/$size[1];
            }
            
            if($type == '.jpg') $altesBild = ImageCreateFromJPEG($pic);
            if($type == '.gif') $altesBild = ImageCreateFromGIF($pic);
                       
            $neuesBild = imageCreateTrueColor($thumbBreite, $thumbHoehe);
            $neuesBild2 = imageCreateTrueColor($thumbBreite2, $thumbHoehe2);
        
            imageCopyResampled($neuesBild, $altesBild, 0, 0, 0, 0, $thumbBreite, $thumbHoehe, $size[0], $size[1]);
            imageCopyResampled($neuesBild2, $altesBild, 0, 0, 0, 0, $thumbBreite2, $thumbHoehe2, $size[0], $size[1]);
            
            if($type == '.jpg')
            { 
                ImageJPEG($neuesBild,  LOCALDIR."images/werbung/".$key."_thumb".$type, 85);
                ImageJPEG($neuesBild2, LOCALDIR."images/werbung/".$key.$type, 85);
            }
            if($type == '.gif')
            {             
               ImageGIF($neuesBild,  LOCALDIR."images/werbung/".$key."_thumb".$type);
               ImageGIF($neuesBild2, LOCALDIR."images/werbung/".$key.$type);
            }
            
            chmod( LOCALDIR."images/werbung/".$key."_thumb".$type, 0777);
            chmod( LOCALDIR."images/werbung/".$key.$type, 0777);
        }
   }
}

?>