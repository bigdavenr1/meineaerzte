<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// UserKlasse - Klasse um alle Arten von Benutzern zu verwalten (auch Admins)
//            - WICHTIG: Session muss vorher gestartet sein
//////////////////////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Bewertungspunkt"))
{
    class Bewertungspunkt extends Basicdb
    {
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
       
         function Bewertungspunkt()
        {
            global $sql;
            $this->table = $sql["table_bewertungspunkte"];
            parent::Basicdb();
        }
        
        // User Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////

        // �berladen
         function getFach()
        {
            parent::createQuery("ORDER BY name");
        }
        
         function getById($id,$sort="")
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
        
        function getBewertungWithKat()
        {
            $this->felder="*, katbewertung.name AS katname ";
            $this->table=" katbewertung, bewertungspunkte ";
            parent::createQuery("WHERE bewertungspunkte.kat = katbewertung.id ORDER BY katbewertung.id DESC");
            $this->table=$sql["table_bewertungspunkte"];
            //parent::createOwnQuery("SELECT * , katbewertung.name AS katname FROM katbewertung,bewertungspunkte WHERE bewertungspunkte.kat = katbewertung.id ORDER BY katbewertung.id DESC" );
        }
        
        function getBewertungWithKatById($id)
        {
            $this->felder=" katbewertung.name AS katname ";
            $this->table=" katbewertung, bewertungspunkte ";
            parent::createQuery("WHERE bewertungspunkte.kat = katbewertung.id AND bewertungspunkte.id=$id ORDER BY katbewertung.id DESC");
            $this->table=$sql["table_bewertungspunkte"];
            //parent::createOwnQuery("SELECT * , katbewertung.name AS katname FROM katbewertung,bewertungspunkte WHERE bewertungspunkte.kat = katbewertung.id AND bewertungspunkte.id=$id ORDER BY katbewertung.id DESC" );
        }
        
        function writeKat($data)
        {
            $this->table = "katbewertung";
            parent::Basicdb();
            parent::write($data);
        }
        
        function getKat()
        {
            $this->table = "katbewertung";
            parent::Basicdb();
            parent::getAll();
        }
        
        function getKatById($id)
        {
            $this->table = "katbewertung";
            parent::Basicdb();
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
    }
}
?>