<?php
////////////////////////////////////////////////////////////////////////////////
// Anzeigeort - Klasse f�r Zugriff auf Tabelle anzeigeort 
////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Anzeigeort"))
{
    class Anzeigeort extends Basicdb
    {

        // Konstruktor
        ////////////////////////////////////////////////////////////////////////

        function Anzeigeort($anzahl=false)
        {
            global $sql;
            $this->table = $sql["table_anzeigeort"];
            parent::Basicdb();    
            $this->anzahl = $anzahl;       
        }

        // User Funktionen
        ////////////////////////////////////////////////////////////////////////
                
        function insertPlatz1($ort,$anzeigeid)
        {
            $statement ="('".mysql_real_escape_string($ort)."', ".mysql_real_escape_string($anzeigeid)."')";
            $query = "INSERT INTO `anzeigeplz` (`ort`, `platz1`) VALUES ".$statement;
            $this->result = mysql_query($query);
        }
        
        function getByOrt($ort)
        {
            parent::createQuery("WHERE ort='".mysql_real_escape_string($ort)."'");
        }
        
        function getByFachidAndOrt($fachid ,$ort)
        {
            parent::createQuery("WHERE `fachid`='".mysql_real_escape_string($fachid)."' AND `ort`='".mysql_real_escape_string($ort)."'");
        }
        
        function getByFachidAndPlz($fachid ,$plz)
        {
            parent::createQuery("WHERE `fachid`='".mysql_real_escape_string($fachid)."' AND `plz`='".mysql_real_escape_string($plz)."'");
        }
        
        
        
        function getByAnzeigeIdPlatz1($anzeigeid)
        {
            parent::createQuery("WHERE platz1='".mysql_real_escape_string($anzeigeid)."'");
        }
        
        function getByAnzeigeIdPlatz2($anzeigeid)
        {
            parent::createQuery("WHERE platz2='".mysql_real_escape_string($anzeigeid)."'");
        }
        
        function getByAnzeigeIdPlatz3($anzeigeid)
        {
            parent::createQuery("WHERE platz3='".mysql_real_escape_string($anzeigeid)."'");
        }
        
        // �berladen
        ////////////////////////////////////////////////////////////////////////
        function getById($id)
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
 
    }
}

?>