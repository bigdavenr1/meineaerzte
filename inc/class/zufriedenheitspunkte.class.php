<?php
////////////////////////////////////////////////////////////////////////////////
// zufriedenheitspunkte.class.php - Zugriff auf Tabelle Zufriedenheitspunkte
////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Zufriedenheitspunkt"))
{
    class Zufriedenheitspunkt extends Basicdb
    {
        // Konstruktor
        ////////////////////////////////////////////////////////////////////////
       
         function Zufriedenheitspunkt()
        {
            global $sql;
            $this->table = $sql["table_zufriedenheitspunkte"];
            parent::Basicdb();
        }
        
        // User Funktionen
        ////////////////////////////////////////////////////////////////////////

        // �berladen
         function getFach()
        {
            parent::createQuery("ORDER BY name");
        }
        
         function getById($id,$sort="")
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
        
        function getBewertungWithKat()
        {
            $this->felder="* , katzufriedenheit.name AS katname ";
            $this->table="katzufriedenheit,zufriedenheitspunkte";
            parent::createQuery("WHERE zufriedenheitspunkte.kat = katzufriedenheit.id ORDER BY katzufriedenheit.id" );
            $this->felder=false;
            $this->table = $sql["table_zufriedenheitspunkte"];
        }

        
        function writeKat($data)
        {
            $this->table = "katzufriedenheit";
            parent::Basicdb();
            parent::write($data);
        }
        
        function getKat()
        {
            $this->table = "katzufriedenheit";
            parent::Basicdb();
            parent::getAll();
        }
        
        function getKatById($id)
        {
            $this->table = "katzufriedenheit";
            parent::Basicdb();
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
    }
}
?>