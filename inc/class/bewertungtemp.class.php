<?php
////////////////////////////////////////////////////////////////////////////////
// UserKlasse - Klasse um Tabelle bewertungtemp zu verwalten 
////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Bewertungtemp"))
{
    class Bewertungtemp extends Basicdb
    {
        // Konstruktor
        ////////////////////////////////////////////////////////////////////////
       
         function Bewertungtemp($anzahl=false)
        {
            global $sql;
            $this->table = $sql["table_bewertungtemp"];
            parent::Basicdb();
            $this->anzahl = $anzahl;
           
        }
       
       
       // �berladen
       /////////////////////////////////////////////////////////////////////////
         function getByUid($uid,$sort="")
        {
            parent::createQuery("WHERE unternehmenid='".mysql_real_escape_string($uid)."'");
        }
        
        // User Funktionen
        ////////////////////////////////////////////////////////////////////////
        function getByFirmidAndKeys($firmid,$key1,$key2)
        {
            parent::createQuery("WHERE `unternehmenid`='".mysql_real_escape_string($firmid)."' AND `kommentar`= '".mysql_real_escape_string($key1)."' AND `antwort`= '".mysql_real_escape_string($key2)."'");
        }
        
        function getByUidKat($uid)
        {
            $this->felder=" bewertungentemp.bewertung AS bewertung, unternehmen.id AS untid, unternehmen.aktiv AS uaktiv";
            parent::createQuery("LEFT JOIN unternehmen ON ( unternehmen.id = bewertungentemp.unternehmenid ) WHERE unternehmenid='".mysql_real_escape_string($uid)."'");
        }
        
        function getByUidAndKom($uid)
        {
            parent::createQuery("WHERE unternehmenid='".mysql_real_escape_string($uid)."' AND kommentar != ''");
        }
        
        function getByUserAndUid($user,$uid)
        {
            parent::createQuery("WHERE userid='".mysql_real_escape_string($user)."' AND unternehmenid='".mysql_real_escape_string($uid)."'");
        }
        
        function getByUser($user)
        {
            $this->felder=" bewertungentemp.datum AS datum, bewertungentemp.unternehmenid AS unternehmenid, bewertungentemp.bewertung AS bewertung, bewertungentemp.id, bewertungentemp.aktiv, bewertungentemp.kommentar, bewertungentemp.userid, unternehmen.name AS unternehmen, unternehmen.famname AS name, unternehmen.vorname AS vorname ";
            parent::createQuery(" LEFT JOIN unternehmen ON ( unternehmen.id = bewertungentemp.unternehmenid ) WHERE bewertungentemp.userid='".$user."'");
        }
        
        function getByUserAndFirmAndDate($user,$firm,$date)
        {
            parent::createQuery("WHERE userid='".mysql_real_escape_string($user)."' AND unternehmenid='".mysql_real_escape_string($firm)."' AND datum='".$date."'");
        }
        
              
    }
}
?>