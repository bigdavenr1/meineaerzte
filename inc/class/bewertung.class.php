<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// UserKlasse - Klasse um alle Arten von Benutzern zu verwalten (auch Admins)
//            - WICHTIG: Session muss vorher gestartet sein
//////////////////////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Bewertung"))
{
    class Bewertung extends Basicdb
    {
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
       
         function Bewertung($anzahl=false)
        {
            global $sql;
            $this->table = $sql["table_bewertung"];
            parent::Basicdb();
            $this->anzahl = $anzahl;
           
        }
        
        // User Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        // �berladen
        
         function getByUid($uid,$sort="")
        {
            parent::createQuery("WHERE unternehmenid='".mysql_real_escape_string($uid)."'");
        }
        
        function getByUidKat($uid)
        {
            $this->felder=" bewertungen.bewertung AS bewertung, unternehmen.id AS untid, unternehmen.aktiv AS uaktiv";
            parent::createQuery("LEFT JOIN unternehmen ON ( unternehmen.id = bewertungen.unternehmenid ) WHERE unternehmenid='".mysql_real_escape_string($uid)."'");
        }
        
        function getByUidAndKom($uid)
        {
            parent::createQuery("WHERE unternehmenid='".mysql_real_escape_string($uid)."' AND kommentar != ''");
        }
        
        function getByUserAndUid($user,$uid)
        {
            parent::createQuery("WHERE userid='".mysql_real_escape_string($user)."' AND unternehmenid='".mysql_real_escape_string($uid)."' ORDER by datum DESC");
        }
        
        function getByUser($user)
        {
            $this->felder=" bewertungen.datum AS datum, bewertungen.unternehmenid AS unternehmenid, bewertungen.bewertung AS bewertung, bewertungen.id, bewertungen.aktiv, bewertungen.kommentar, bewertungen.userid, unternehmen.name AS unternehmen, unternehmen.famname AS name, unternehmen.vorname AS vorname ";
            parent::createQuery(" LEFT JOIN unternehmen ON ( unternehmen.id = bewertungen.unternehmenid ) WHERE bewertungen.userid='".$user."'");
        }
        
        function getNewComment($sort)
        {
            $this->felder=" bewertungen.antwort AS antwort,bewertungen.id, bewertungen.kommentar, bewertungen.userid, benutzer.email AS username, bewertungen.unternehmenid AS unternehmenid, unternehmen.name AS unternehmen, unternehmen.famname AS name, unternehmen.vorname AS vorname ";
            parent::createQuery("LEFT JOIN benutzer ON (benutzer.id=bewertungen.userid) RIGHT JOIN unternehmen ON ( unternehmen.id = bewertungen.unternehmenid ) WHERE bewertungen.aktiv='0' AND bewertungen.kommentar!='' ".$sort);
        }
        
        function getNewCommentSort($aktiv="0",$sort)
        {
            $this->felder=" bewertungen.antwort AS antwort,bewertungen.id, bewertungen.kommentar, bewertungen.userid, benutzer.email AS username, bewertungen.unternehmenid AS unternehmenid, unternehmen.name AS unternehmen, unternehmen.famname AS name, unternehmen.vorname AS vorname ";
            parent::createQuery("LEFT JOIN benutzer ON (benutzer.id=bewertungen.userid) RIGHT JOIN unternehmen ON ( unternehmen.id = bewertungen.unternehmenid ) WHERE bewertungen.aktiv='".$aktiv."' AND bewertungen.kommentar!=''".$sort);
        }
        
        function getAllBewertung()
        {
            $this->felder .= "bewertungen.antwort AS antwort, bewertungen.aktiv AS status, benutzer.email AS username, bewertungen.datum AS datum, bewertungen.unternehmenid AS unternehmenid, bewertungen.bewertung AS bewertung, bewertungen.id, bewertungen.kommentar, bewertungen.userid, unternehmen.name AS unternehmen, unternehmen.famname AS name, unternehmen.vorname AS vorname ";
            parent::createQuery("LEFT JOIN benutzer ON (benutzer.id=bewertungen.userid) LEFT JOIN unternehmen ON ( unternehmen.id = bewertungen.unternehmenid )");
        }
        
        function getAllBewertungSort($sort)
        {
            $this->felder .= "bewertungen.antwort AS antwort, bewertungen.aktiv AS status, benutzer.name AS username, benutzer.vorname AS uservorname, benutzer.id AS benutzerid, bewertungen.datum AS datum, bewertungen.unternehmenid AS unternehmenid, bewertungen.bewertung AS bewertung, bewertungen.id, bewertungen.kommentar, bewertungen.userid, unternehmen.name AS unternehmen, unternehmen.famname AS name, unternehmen.vorname AS vorname ";
            parent::createQuery("LEFT JOIN benutzer ON (benutzer.id=bewertungen.userid) LEFT JOIN unternehmen ON ( unternehmen.id = bewertungen.unternehmenid )".$sort);
        }
        
        function getByUserAndFirmAndDate($user,$firm,$date)
        {
            parent::createQuery("WHERE userid='".mysql_real_escape_string($user)."' AND unternehmenid='".mysql_real_escape_string($firm)."' AND datum='".$date."'");
        }
        
              
    }
}
?>