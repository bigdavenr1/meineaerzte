<?php



//////////////////////////////////////////////////////////////////////////////////////////////////////
// ForumMessage-Klasse - Erbt von Basicdb
//                     - Verwaltet die Foreneinträge
//////////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("ForumMsg"))
{
    class ForumMsg extends Basicdb
    {
    
    
    
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        public function __construct($anzahl = 0, $affix = "")
        {
            global $sql;
            $this->table = $sql["table_forummsg"];
            $this->anzahl = $anzahl;
            $this->offsetaffix = $affix;
            parent::__construct();
        }
        
        
        
        // ForumMsg Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        // nach neuesten sortiert
        public function getOrderByForumId($id)
        {
            parent::createQuery("WHERE forum_id='".$id."' ORDER BY datum DESC");
        }
        
        public function getByUser($id)
        {
            parent::createQuery("WHERE user_id=".$id);
        }
        
        public function getByForumId($id)
        {
            parent::createQuery("WHERE forum_id=".$id);
        }
        
        public function getByForumIdAndSuperId($id, $super)
        {
            parent::createQuery("WHERE forum_id=".$id." AND super_id='".$super."' ORDER BY datum");
        }
        
        public function getByForumIdAndSuperIdLast($id, $super)
        {
            parent::createQuery("WHERE forum_id=".$id." AND super_id='".$super."' ORDER BY datum DESC");
        }
        
        public function getTopicByForumIdAndSuperId($id, $super)
        {
            parent::createQuery("WHERE forum_id=".$id." AND super_id='".$super."' AND titel<>'' ORDER BY datum");
        }
        
        public function getOneTopicByForumIdAndSuperId($id, $super)
        {
            parent::createQuery("WHERE forum_id=".$id." AND super_id='".$super."' AND titel='' ORDER BY datum");
        }
        
        public function getTopicByForumId($id)
        {
            parent::createQuery("WHERE forum_id=".$id." AND titel<>'' ORDER BY last_datum DESC");
        }
        
        public function getBySuperId($super)
        {
            parent::createQuery("WHERE super_id='".$super."'");
        }
        
        // Beitrag mit Titel des Topics hohlen, d.h. den ältesten
        public function getBySuperIdFirst($super)   
        {
            parent::createQuery("WHERE super_id='".$super."' ORDER BY datum ");
        }
        
    }
}
?>