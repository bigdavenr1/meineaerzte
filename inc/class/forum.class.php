<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// Forum-Klasse - Erbt von Basicdb
//              - Verwaltet die Foren
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("Forum"))
{
    class Forum extends Basicdb
    {
    
    
    
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        public function __construct($anzahl = 0, $affix = "")
        {
            global $sql;
            $this->table = $sql["table_forum"];
            $this->anzahl = $anzahl;
            $this->offsetaffix = $affix;
            parent::__construct();
        }
        
        
        
        // Forum Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////
        // SL
        public function getByKat()
        {
            parent::createQuery("ORDER BY kategorie");
        }
        // END SL
        
        
        public function getByTyp($typ)
        {
            parent::createQuery("WHERE typ=".$typ." ORDER BY sort");
        }
        
        
        public function getByKategorie($kat)
        {
            parent::createQuery("WHERE kategorie='".$kat."' AND typ=0 ORDER BY sort");
        }
        
        
        public function getKategorien()
        {
            parent::createOwnQuery("SELECT DISTINCT kategorie FROM ".$this->table);
        }
    }
}
?>