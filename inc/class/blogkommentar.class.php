<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// BlogKommentar-Klasse - Erbt von Basicdb
//                      - Zeigt die Kommentare zu den Blogeinträgen an
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("Kommentar"))
{
    class Kommentar extends Basicdb
    {
    
    
    
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
         function Kommentar($anzahl = 0)
        {
            global $sql;
            $this->table = $sql["table_kommentar"];
            $this->anzahl = $anzahl;
            parent::Basicdb();
        }
        
        
        
        // BlogKommentar Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
         function getByRefId($id)
        {
            parent::createQuery("WHERE RefID=".$id." ORDER BY datum DESC");
        }
        
         function getById($id)
        {
            parent::createQuery("WHERE id=".$id);
        }
        
         function getNew()
        {
            parent::createQuery("WHERE Status='N'");
        }
    }
}
?>