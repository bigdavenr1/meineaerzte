<?php
////////////////////////////////////////////////////////////////////////////////
// Anzeigerotation - Klasse f�r Zugriff auf Tabelle anzeigerotation 
////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Anzeigerotation"))
{
    class Anzeigerotation extends Basicdb
    {
        // Variablen werden von Function rotieren geschrieben
        var $searchrotation;
        var $bannerrotation;

        // Konstruktor
        ////////////////////////////////////////////////////////////////////////

        function Anzeigerotation($anzahl=false)
        {
            global $sql;
            $this->table = $sql["table_anzeigerotation"];
            parent::Basicdb();    
            $this->anzahl = $anzahl;       
        }

        // User Funktionen
        ////////////////////////////////////////////////////////////////////////
                
        function insertPlatz1($plz,$werbeid)
        {
            $statement ="('".mysql_real_escape_string($plz)."', ".mysql_real_escape_string($werbeid)."')";
            $query = "INSERT INTO `anzeigeplz` (`plz`, `platz1`) VALUES ".$statement;
            $this->result = mysql_query($query);
        }
        
        function getSearch()
        {
            parent::createQuery("WHERE kontex='search'");
        }
        
        function getBanner()
        {
            parent::createQuery("WHERE kontex='banner'");
        }
        
        function rotieren($kontex='search')
        {
            if($kontex == 'search')
            {
                $this->getSearch();
            }
            elseif($kontex == 'banner')
            {
                $this->getBanner();
            }
            
            $datensatz = $this->liste[0];
            
            $value = $datensatz->value;
            
            switch($value){
                case 0:
                    $newvalue = 1;
                    break;
                case 1:
                    $newvalue = 2;
                    break;
                case 2:
                    $newvalue = 0;
                    break;  
                default:
                    $newvalue = 0;
                    break;                  
            }
            
            if($kontex == 'search')
            {
                $this->searchrotation = $newvalue;
            }
            elseif($kontex == 'banner')
            {
                $this->bannerrotation = $newvalue;
            }
            
            $statement = " `value`='".$newvalue."'";
            
            parent::update($statement);
            
            return $datensatz->last_id;
        }
        
        // �berladen
        ////////////////////////////////////////////////////////////////////////
        function getById($id)
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
 
    }
}

?>