<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// Class Anzeigebanner f�r Tabelle anzeigebanner
//////////////////////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Anzeigebanner"))
{
    class Anzeigebanner extends Basicdb
    {
       // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
         function Anzeigebanner($anzahl = 0,$affix = false)
        {
            global $sql;
            $this->table = $sql["table_anzeigebanner"];
            $this->anzahl = $anzahl;
            $this->offsetaffix = $affix;
            parent::Basicdb();
        }

        // �berladen
        ////////////////////////////////////////////////////////////////////////
        function getAll($statement = "ORDER by id DESC")
        {
            parent::createQuery($statement);
        }

        function getAktiv()
        {
            parent::createQuery("WHERE `status`='1' ORDER by RAND() Limit 0,10");
        }

        function getHalfRand()
        {
            parent::createQuery("WHERE `status`='1' AND bannerkey='2' AND id != '".$_SESSION['halfbannerid']."' ORDER by RAND() Limit 0,1");
        }

        function getFullRand()
        {
            parent::createQuery("WHERE `status`='1' AND bannerkey='1' AND id != '".$_SESSION['fullbannerid']."' ORDER by RAND() Limit 0,1");
        }

        function getAllAktiv()
        {
            parent::createQuery("WHERE `status`='1' ");
        }

        function getById($id)
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }

        // User Funktionen
        ////////////////////////////////////////////////////////////////////////

        function getByWerbeid($werbeid)
        {
            parent::createQuery("WHERE werbeid='".mysql_real_escape_string($werbeid)."'");
        }

        public function writePic($pic,$key,$type)// erstellt Banner und Half-Bannerbild
        {
            // Gr��e auslesen (size[0]=breite, size[1]=h�he)
            $size = getimagesize($pic);

            $thumbBreite = 234;
            $thumbBreite2 = 468;

            $thumbHoehe = 60;
            $thumbHoehe2 = 60;

            if($type == '.jpg') $altesBild = ImageCreateFromJPEG($pic);
            if($type == '.gif') $altesBild = ImageCreateFromGIF($pic);

            $neuesBild = imageCreateTrueColor($thumbBreite, $thumbHoehe);
            $neuesBild2 = imageCreateTrueColor($thumbBreite2, $thumbHoehe2);

            imageCopyResampled($neuesBild, $altesBild, 0, 0, 0, 0, $thumbBreite, $thumbHoehe, $size[0], $size[1]);
            imageCopyResampled($neuesBild2, $altesBild, 0, 0, 0, 0, $thumbBreite2, $thumbHoehe2, $size[0], $size[1]);

            if($type == '.jpg')
            {
                ImageJPEG($neuesBild,  LOCALDIR."images/werbung/".$key."_half".$type, 85);
                ImageJPEG($neuesBild2, LOCALDIR."images/werbung/".$key.$type, 85);
            }
            if($type == '.gif')
            {
               ImageGIF($neuesBild,  LOCALDIR."images/werbung/".$key."_half".$type);
               ImageGIF($neuesBild2, LOCALDIR."images/werbung/".$key.$type);
            }

            chmod( LOCALDIR."images/werbung/".$key."_half".$type, 0777);
            chmod( LOCALDIR."images/werbung/".$key.$type, 0777);
        }
   }
}

?>