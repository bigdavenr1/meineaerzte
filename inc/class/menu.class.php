<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// Menu-Klasse - Erbt von Basicdb
//             - Zeigt die Submen�s an
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("Menu"))
{
    class Menu extends Basicdb
    {
    
    
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
         function Menu($anzahl = 0)
        {
            global $sql;
            $this->table = $sql["table_menu"];
            $this->anzahl = $anzahl;
            parent::__construct();
        }
        
        
        // Menu Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
         function getMainKat()
        {
            parent::createQuery("WHERE name=kategorie");
        }
        
         function getSubKat()
        {
            parent::createQuery("WHERE name<>kategorie");
        }
        
         function getSubKatByKat($kat)
        {
            parent::createQuery("WHERE kategorie='".$kat."' AND name<>kategorie");
        }
    }
}
?>