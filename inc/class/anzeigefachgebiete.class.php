<?php
////////////////////////////////////////////////////////////////////////////////
// Anzeigefachgebiete - Klasse f�r Zugriff auf Tabelle anzeigefachgebiete 
////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Anzeigefachgebiete"))
{
    class Anzeigefachgebiete extends Basicdb
    {

        // Konstruktor
        ////////////////////////////////////////////////////////////////////////

        function Anzeigefachgebiete($anzahl=false)
        {
            global $sql;
            $this->table = $sql["table_anzeigefachgebiete"];
            parent::Basicdb();    
            $this->anzahl = $anzahl;       
        }

        // User Funktionen
        ////////////////////////////////////////////////////////////////////////
        
        function insertTupel($anzeigeid, $nr, $fachid, $plz )
        {
            $statement ="('".mysql_real_escape_string($anzeigeid)."', '".mysql_real_escape_string($nr)."', '".mysql_real_escape_string($fachid)."', '".mysql_real_escape_string($plz)."')";
            $query = "INSERT INTO `anzeigefachgebiete` (`anzeigeid`, `nr` , `fachid` , `plz`) VALUES ".$statement;
            $this->result = mysql_query($query);
        }
                
        function getByPlz($plz)
        {
            parent::createQuery("WHERE plz='".mysql_real_escape_string($plz)."'");
        }
        
        function getByFachid($fachid)
        {
            parent::createQuery("WHERE fachid='".mysql_real_escape_string($fachid)."'");
        }
        
        function getByFachidAndPlz($fachid,$plz)
        {
            parent::createQuery("WHERE `fachid`='".mysql_real_escape_string($fachid)."' AND plz='".mysql_real_escape_string($plz)."'");
        }
        
        function getRandByPlz($plz)
        {
            parent::createQuery("WHERE `plz`='".mysql_real_escape_string($plz)."' ORDER BY RAND() LIMIT 0,10 ");
        }
        
        function getRandByFachid($fachid)
        {
            parent::createQuery("WHERE `fachid`='".mysql_real_escape_string($fachid)."' ORDER BY RAND() LIMIT 0,10 ");
        }
        
        /*function getRandByFachidAndPlz($fachid,$plz)
        {
            //SELECT * FROM `anzeigefachgebiete` WHERE fachid='20' LIMIT 0,3  ORDER BY RAND()
            parent::createQuery("WHERE fachid='".mysql_real_escape_string($fachid)."' AND plz='".mysql_real_escape_string($plz)."%' ORDER BY RAND() LIMIT 0,10 ");
        }*/
        
        function getByAnzeigeId($anzeigeid)
        {
            parent::createQuery("WHERE anzeigeid='".mysql_real_escape_string($anzeigeid)."'");
        }
        
        function getByAnzeigeIdAndNr($anzeigeid,$nr)
        {
            parent::createQuery("WHERE anzeigeid='".mysql_real_escape_string($anzeigeid)."' AND nr='".mysql_real_escape_string($nr)."'");
        }
        
        // �berladen
        ////////////////////////////////////////////////////////////////////////
        function getById($id)
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
 
    }
}

?>