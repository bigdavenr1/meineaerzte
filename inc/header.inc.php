<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
    <head>
    
    <?php
    // Globals aus Config
    $metas = new Meta();
    ?>

        <title><?php echo $metas->getTitle();?></title>
        <meta name="description" content="<?php echo $metas->getDescription();?>" />
        <meta name="keywords" content="<?php echo $metas->getKeywords();?>" />
        <meta name="author" content="" />
        <meta name="copyright" content="" />
        <meta name="language" content="deutsch, de, at, ch, german" />
        <meta name="robots" content="index,follow" />
        <meta name="revisit-after" content="5 days" />
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link href="<?php echo WEBDIR;?>style/common.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo WEBDIR;?>style/template.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo WEBDIR;?>style/menu.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo WEBDIR;?>style/content.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo WEBDIR;?>style/form.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo WEBDIR;?>style/htmlarea.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo WEBDIR;?>style/lightbox.css" type="text/css" media="screen" />
            <?php if ($_GET['admincode']) echo admin(); ?>
        <?php if (ereg("MSIE", $_SERVER['HTTP_USER_AGENT']) && !ereg("Opera", $_SERVER['HTTP_USER_AGENT']) && !ereg("MSIE 7", $_SERVER['HTTP_USER_AGENT'])) {?>
        <link href="<?php echo WEBDIR;?>style/ie.css" type="text/css" rel="stylesheet" />
        <?php }?>
        <?//link rel="shortcut icon" href="<?php echo WEBDIR;images/icons/fav.ico" /?>
        
    </head>
    <body>
        <div id="container">
            <div id="left"> 
   
                        
                <div id="top">
                    <h1>www.meineaerzte.at</h1>
                    <br class="clr"/>
                    <div id="menu1">
                        <?php 
                        $menu = new MenuHandle();
                        echo $menu->createLink("Forum",WEBDIR."bewertungen/forum.view.php");?>
                    </div>
                    <div id="menu">
                        <?php include(INCLUDEDIR."menu.inc.php");?>
                    </div>
                </div>
                <div id="content">

