<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// htmlarea.class.php - MiniWord
//                    - ! Ben�tigt addon.js !
//                    - ! Ben�tigt htmlarea.css !
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("HtmlArea"))
{
    class HtmlArea
    {
        var $areaname;
        
        
        
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
         function HtmlArea($elementname)
        {
            $this->areaname = $elementname;
            global $scriptincluded;
            
            if (!$scriptincluded)
            {
                //echo '<script src="'.WEBDIR.'script/htmlarea.js" language="javascript" type="text/javascript"></script>';
                $scriptincluded = true;
            }
        
        }
    
        
        
        // HtmlArea Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
    
    
        
         function getHtmlButtons()
        {
            $html = '<div class="htmlarea">';
        
            // Button "bold"
            $befehl = "[b][/b]";
            $html .= '<input type="button" value="B" class="small" style="font-weight:bold;" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\');" />';
            
            // Button "italic"
            $befehl = "[i][/i]";
            $html .= '<input type="button" value="I" class="small" style="font-style:italic;" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\');" />';
            
            // Button "underline"
            $befehl = "[u][/u]";
            $html .= '<input type="button" value="U" class="small" style="text-decoration:underline;" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\');" />';
            
            // Button "link"
            //$befehl = "[a url=www.mylink.de][/a]";
            //$html .= '<input type="button" value="Link" class="small" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\');" />';
            
            // Button "bild"
            //$befehl = "[img url=www.mypic.de/pic.jpg+titel=Name]";
            //$html .= '<input type="button" value="Bild" class="small" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\');">';
            
            // Button "smilies"
            //$html .= '<input type="button" value="Smilies" class="medium" onclick="setElementPosOnElement(\'smiliemenu\', this, 0, 18); swapShowElementById(\'smiliemenu\');">';
            //$html .= '<div id="smiliemenu" class="smiliemenu">';
            //$html .= $this->getSmilies();
            //$html .= '</div>';
            
            /*/ Button "quote"
            $befehl = "[quote msg=nr]";
            $html .= '<input type="button" value="Quote" class="medium" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\');">';
            */
            
            
            $html .= '</div>';
            
            
            return $html;
        }
    
        
         function getSmilies()
        {
            global $root;
        
            $befehl = ':-)';
            $html  = '<img src="'.$root.'images/smilies/emoticon_smile.png" alt="smilie" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\'); swapShowElementById(\'smiliemenu\');">';
            
            $befehl = ':-D';
            $html .= '<img src="'.$root.'images/smilies/emoticon_grin.png" alt="smilie" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\'); swapShowElementById(\'smiliemenu\');">';
            
            $befehl = ':-P';
            $html .= '<img src="'.$root.'images/smilies/emoticon_tongue.png" alt="smilie" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\'); swapShowElementById(\'smiliemenu\');">';
            
            $befehl = ':-(';
            $html .= '<img src="'.$root.'images/smilies/emoticon_unhappy.png" alt="smilie" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\'); swapShowElementById(\'smiliemenu\');">';
            
            $befehl = ';-)';
            $html .= '<img src="'.$root.'images/smilies/emoticon_wink.png" alt="smilie" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\'); swapShowElementById(\'smiliemenu\');">';
            
            $befehl = ':-O';
            $html .= '<img src="'.$root.'images/smilies/emoticon_surprised.png" alt="smilie" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\'); swapShowElementById(\'smiliemenu\');">';
            
            $befehl = ':-?';
            $html .= '<img src="'.$root.'images/smilies/emoticon_waii.png" alt="smilie" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\'); swapShowElementById(\'smiliemenu\');">';
            
            $befehl = 'X-D';
            $html .= '<img src="'.$root.'images/smilies/emoticon_evilgrin.png" alt="smilie" onclick="addTextToElement(\''.$befehl.'\', \''.$this->areaname.'\'); swapShowElementById(\'smiliemenu\');">';
        
        
            return $html;
        }
    }
}
?>
