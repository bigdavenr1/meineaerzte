<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// UserKlasse - Klasse um alle Arten von Benutzern zu verwalten (auch Admins)
//            - WICHTIG: Session muss vorher gestartet sein
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("Benutzer"))
{
    class Benutzer extends Basicdb
    {
     
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
         function Benutzer($anzahl = 0)
        {
            global $sql;
            $this->table = $sql["table_benutzer"];
            $this->anzahl = $anzahl;
            parent::Basicdb();
        }
        
        
        
        // User Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        // �berladen
         function getBenutzer($sort=false)
        {   
            if($sort)
            {
                parent::createQuery("WHERE typ='benutzer'".$sort,"all");
            }
            else  parent::createQuery("WHERE typ='benutzer'","all");
        }
        
        // Beginn Werbemodul
         function getWerbekunden($sort=false)
        {   
            if($sort)
            {
                parent::createQuery("WHERE typ='werbung'".$sort,"all");
            }
            else  parent::createQuery("WHERE typ='werbung'","all");
        }
        
        function getWerbekundenById($id)
        {
            parent::createQuery(" WHERE typ='werbung' AND id='".mysql_real_escape_string($id)."'");
        }
        
        function getWerbekundenByKuNr($nr)
        {
            parent::createQuery(" WHERE typ='werbung' AND email='".mysql_real_escape_string($nr)."'");
        }
        
        // End Werbemodul
        
         function getById($id)
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
        
         function getBenutzerById($id)
        {
            parent::createQuery(" WHERE typ='benutzer' AND id='".mysql_real_escape_string($id)."'");
        }
        
         function getByIdBasic($id)
        {
            parent::createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }
        
         function getByLogin($name, $pass)
        {
            parent::createQuery("WHERE email='".mysql_real_escape_string($name)."' AND pass='".mysql_real_escape_string($pass)."'");
        }
        
         function getByNick($name)
        {
            parent::createQuery("WHERE nickname='".mysql_real_escape_string($name)."'");
        }
        
         function getByMail($mail)
        {
            parent::createQuery("WHERE email='".mysql_real_escape_string($mail)."'");
        }
        
         function getByGeschlecht($art)
        {
            parent::createQuery("WHERE geschlecht='".mysql_real_escape_string($art)."'");
        }
        
         function getByKey($key)
        {
            parent::createQuery("WHERE aktivierungskey='".mysql_real_escape_string($key)."'");
        }
        
         function insertIntoSession($benutzer)
        {
            unset($_SESSION["user"]);
            $_SESSION["user"] = $benutzer;
        }
        
         function checkRight($typ)
        {
            return $this->typ == $typ;
        }
        
         function calculateAge($datum)
        {
            $benutzerjahr = explode("-", $datum); 
            $jahre = date("Y") - $benutzerjahr[0];
            if ($benutzerjahr[1] > date("m")) $jahre--;
            if ($benutzerjahr[1] == date("m") && $benutzerjahr[2] > date("d")) $jahre--;
            return $jahre;
        }
        
         function search($str)
        {
            parent::createQuery("WHERE aktiv=1 AND tmp<>'gewinnspiel' AND nickname LIKE '%".mysql_real_escape_string($str)."%'");
        }
        
        
        public function writePic($pic,$key,$type)// erstellt Thumbnailbild
        {
            // Gr��e auslesen (size[0]=breite, size[1]=h�he)
            $size = getimagesize($pic);

            if ($size[0] > $size[1])
            {
                $thumbBreite = 125;
                $thumbHoehe = (125*$size[1])/$size[0];
                $thumbBreite2 = 360;
                $thumbHoehe2 = (360*$size[1])/$size[0];
            }
            
            else
            {
                $thumbHoehe = 125;
                $thumbBreite= (125*$size[0])/$size[1];
                $thumbHoehe2 = 360;
                $thumbBreite2 = (360*$size[0])/$size[1];
            }
            
            if($type == '.jpg') $altesBild = ImageCreateFromJPEG($pic);
            if($type == '.gif') $altesBild = ImageCreateFromGIF($pic);
                       
            $neuesBild = imageCreateTrueColor($thumbBreite, $thumbHoehe);
            $neuesBild2 = imageCreateTrueColor($thumbBreite2, $thumbHoehe2);
        
            imageCopyResampled($neuesBild, $altesBild, 0, 0, 0, 0, $thumbBreite, $thumbHoehe, $size[0], $size[1]);
            imageCopyResampled($neuesBild2, $altesBild, 0, 0, 0, 0, $thumbBreite2, $thumbHoehe2, $size[0], $size[1]);
            
            if($type == '.jpg')
            { 
                ImageJPEG($neuesBild,  LOCALDIR."images/user/".$key."_thumb".$type, 85);
                ImageJPEG($neuesBild2,  LOCALDIR."images/user/".$key.$type, 85);
            }
            if($type == '.gif')
            {             
               ImageGIF($neuesBild,  LOCALDIR."images/user/".$key."_thumb".$type);
               ImageGIF($neuesBild2,  LOCALDIR."images/user/".$key.$type);
            }
            
            chmod( LOCALDIR."images/user/".$key."_thumb".$type, 0777);
            chmod( LOCALDIR."images/user/".$key.$type, 0777);
        }
        
    }
}
?>