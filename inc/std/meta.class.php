<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// Meta-Klasse - Verwaltet die Suchmaschinenbegriffe
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("Meta"))
{
    class Meta
    {
        var $keywords;
        var $description;
        var $title;
        var $mode;
        
        
        
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        function Meta($keywords = "", $description = "", $title = "")
        {
            if (!$keywords && !$description && !$title)
            {
                global $CONST_META;
                $metacopie = $CONST_META;
                $found = false;
                

                while ($obj = array_shift($metacopie))    
                {
                    if (ereg($obj["url"], $_SERVER["PHP_SELF"]))
                    {
                        $obj["title"] ? $this->title = $obj["title"] : $this->title = $CONST_META[0]["title"];
                        $obj["keywords"] ? $this->keywords = $obj["keywords"].($obj["mode"] != "new" ? ','.$CONST_META[0]["keywords"] : '') : $this->keywords = $CONST_META[0]["keywords"];
                        $obj["description"] ? $this->description = $obj["description"] : $this->description = $CONST_META[0]["description"];
                        
                        $found = true;
                    }
                }
                
                
                if (!$found)
                {
                    $this->title = $CONST_META[0]["title"];
                    $this->keywords = $CONST_META[0]["keywords"];
                    $this->description = $CONST_META[0]["description"];
                }
            }
            else
            {
                $this->keywords = $keywords;
                $this->description = $description;
                $this->title = $title;
            }
        }
        
        
        
        // Meta Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        function getKeywords()
        {
            // RANDOM
            $words = explode(",", $this->keywords);
            shuffle($words);
            $result = implode(",", $words);
            
            return $result;
        }
        
        
        function getDescription()
        {
            return $this->description;
        }
        
        
        function getTitle()
        {
            return $this->title;
        }
    }
}