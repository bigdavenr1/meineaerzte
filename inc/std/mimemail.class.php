<?php

/**
 * ------------------------------------------------------------------------------
 * MIMEMailxPHP4_V2.inc - source code of the free MIME Mail class for PHP4
 *
 * Creation:     2005-01-25  Rainer Feike / feike.biz
 * Last Update:  2006-12-02  Rainer Feike
 * Version:      2.1.0
 * 
 * Copyright (c) 2001-2006 Rainer Feike, rainer(at)feike(dot)biz
 *
 * Das Programm ist freie Software unter GPL.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * ------------------------------------------------------------------------------
 *
 */
class MIMEMailxPHP4_V2 {
	
	// these are all private variables you dont need to know
	var $attachments;
	var $subject;
	var $sender;
	var $replyTo;
	var $textContent;
	var $htmlContent;
	var $recievers;
	var $BCC_recievers;
	var $CC_recievers;
	var $lastErrNum;
	var $errors;
	var $eol;
	
	// the functions are all public
	function MIMEMailxPHP4()
	{
	 	$this->attachments = Array();
	 	$this->subject = "";
	 	$this->sender = "";
		$this->replyTo = "";
	 	$this->textContent = Array();
	 	$this->htmlContent = Array();
	 	$this->recievers = "";
	 	$this->BCC_recievers = "";
	 	$this->CC_recievers = "";		
	 	$this->lastErrNum = 0;
	 	$this->eol = "\n";
	 	// define errorcodes and errortexts here
		$this->errors = Array(
								0 => "Erfolg",
								1 => "Datei nicht gefunden",
								2 => "fopen() nicht erfolgreich",
								3 => "fread() nicht erfolgreich"
								);
	}
	
	
	/**
	 * Das Zeilenende setzen. Default ist "\n". Einige MTAs haben hier allerdings Probleme, dann kann "\r\n" helfen.
	 * 
	 * @param  $eol das Zeilenendezeichen, Default ist "\n".
	 * @return 0 im Erfolgsfall, Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 *
	 */
	function setEOL($eol)
	{
		$this->eol = $eol;
		return 0;
	}
	
	/**
	 * Im Fehlerfall setzen Funktionen einen Fehlercode. Diese Funktion kann den zuletzt
	 * gesetzten Fehlercode zur�ck geben.
	 * 
	 * @return 0 oder den zuletzt gesetzten Fehlercode, ein Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 *
	 */
	function getLastErrorCode()
	{
		return $this->lastErrNum;
	}
	
	/**
	 * Gibt die textuelle Beschreibung des zuletzt aufgetretenen Fehlers zur�ck.
	 * 
	 * @return die textuelle Beschreibung des zuletzt aufgetretenen Fehlers, ein Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 *
	 */
	function getLastErrorString()
	{
		return $this->errors[$this->lastErrNum];
	}

	/**
	 * Den plaintext Teil der Mail setzen. Dieser Text wird von Mail Clients angezeigt,
	 * die eine HTML Anzeige nicht beherrschen. Falls kein HTML Text angegeben wird, wird
	 * ebenfalls dieser Text in der Mail zu sehen sein. Kein Default.
	 * Falls Sie HTML Content setzen, und den Text Content leer lassen, dann
	 * wird der HTML Content in Text umgewandelt und als TextContent gesetzt 
	 * (sonst k�nnen Text-only Clients die Mail nicht lesen).
	 * 
	 * @param $text der zu setzende Text.
	 * @param $charset das charset des Textes, Default ist "ISO-8859-1".
	 * @param $encoding des Texts, Default ist "8bit".
	 * @return 0 im Erfolgsfall, Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 * 
	 */
	function setTextContent($text, $charset = "ISO-8859-1", $encoding = "8bit")
	{
		$this->textContent[0] = $text;
		$this->textContent[1] = $charset;
		$this->textContent[2] = $encoding;
		return 0;
	}
	
	/**
	 * Den HTML Teil der Mail setzen. Dieser Text wird von Mail Clients angezeigt,
	 * die eine HTML Anzeige beherrschen. Kein Default.
	 * Auf ein als Attachment angegebenes Bild (inline-image) kann man sich beziehen �ber den
	 * Syntax <img src="cid:ContentID">, dabei ist ContentID ein Parameter in der
	 * Funktion addBinaryFileAsAttachment und addBinaryStringAsAttachment.
	 * Falls Sie HTML Content setzen, und den Text Content leer lassen, dann
	 * wird der HTML Content in Text umgewandelt und als TextContent gesetzt 
	 * (sonst k�nnen Text-only Clients die Mail nicht lesen).
	 * 
	 * @param $html der zu setzende HTML-Code.
	 * @param $charset das charset des HTML, Default ist "ISO-8859-1".
	 * @param $encoding des HTML, Default ist "8bit".
	 * @return 0 im Erfolgsfall, Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 * 
	 */
	function setHTMLContent($html, $charset = "ISO-8859-1", $encoding = "8bit")
	{
		$this->htmlContent[0] = $html;
		$this->htmlContent[1] = $charset;
		$this->htmlContent[2] = $encoding;
		if (empty($this->textContent[0]))
			$this->setTextContent(strip_tags($html), $charset, $encoding);
		
		return 0;
	}
		
	/**
	 * Eine Bin�rdatei als Attachment anh�ngen. Falls die Datei als Inline-Image genutzt
	 * werden soll, geben Sie ContentID an, sonst lassen Sie CID leer. Unter der
	 * angegebenen ContentID kann das Bild dann �ber <img src="cid:contentID"> als
	 * Inline-Image in der Mail benutzt werden. Intern wird fopen genutzt, es k�nnen
	 * also abh�ngig von der Laufzeitkonfiguration auch URLS genutzt werden.
	 * 
	 * @param $file die einzulesende Datei in relativer oder absoluter Notation.
	 * @param $type der MIME-Typ der Datei, z.B. "image/jpg", "application/pdf" etc.
	 * @param $name der Name unter dem die Datei als attachment genannt wird.
	 * @param $ContentID die ContentID der Datei falls sie als inline-image genutzt wird. Default ist empty.
	 * @return 0 im Erfolgsfall, sonst eine Fehlerzahl aus der Fehlertabelle
	 *
	 */
	function addBinaryFileAsAttachment($file, $type, $name, $ContentID = "")
	{
		$this->lastErrNum = 0;
		if (!file_exists($file)) {
			$this->lastErrNum = 1;
			return $this->lastErrNum;
		}
		$handle = fopen($file,'rb');
		if (!$handle) {
			$this->lastErrNum = 2;
			return $this->lastErrNum;
		}
		$file_content = fread($handle,filesize($file));
		@fclose($handle);
		if (!$file_content) {
			$this->lastErrNum = 3;
			return $this->lastErrNum;
		}
		$attachment_string = chunk_split(base64_encode($file_content));
		$this->attachments[] = Array($attachment_string, $type, $name, $ContentID);
	}
	
	/**
	 * Einen Bin�rstring als Attachment anh�ngen. Falls die Daten als Inline-Image genutzt
	 * werden sollen, geben Sie ContentID an, sonst lassen Sie CID leer. Unter der
	 * angegebenen ContentID kann das Bild dann �ber <img src="cid:contentID"> als
	 * Inline-Image in der Mail benutzt werden. 
	 * 
	 * @param $string die anzuh�ngenden Bin�rdaten.
	 * @param $type der MIME-Typ der Datei, z.B. "image/jpg", "application/pdf" etc.
	 * @param $name der Name unter dem die Datei als attachment genannt wird.
	 * @param $ContentID die ContentID der Datei falls sie als inline-image genutzt wird. Default ist empty.
	 * @return 0 im Erfolgsfall, Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 *
	 */
	function addBinaryStringAsAttachment($string, $type, $name, $ContentID = "")
	{
		$this->attachments[] = Array(chunk_split(base64_encode($string)), $type, $name, $ContentID);
		return 0;
	}
	
	/**
	 * Das Thema, den Betreff der Mail setzen, Default ist ""
	 * 
	 * @param  $subject das Thema (Subject) der Mail.
	 * @return 0 im Erfolgsfall, Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 *
	 */
	function setSubject($subject)
	{
		$this->subject = $subject;
		return 0;
	}
	
	/**
	 * Die Headerkomponente REPLY-TO setzen. Default ist "".
	 * 
	 * @param  $repl sie zu setzende Headerkomponente REPLY-TO.
	 * @return 0 im Erfolgsfall, Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 *
	 */
	function setReplyTo($repl)
	{
		$this->replyTo = $repl;
		return 0;
	}
	
	/**
	 * Die Empf�nger der Mail setzen. Es kann ein einzelner String oder ein Array von Strings mit
	 * mehreren Empf�nger �bergeben werden.
	 * 
	 * @param $rcvs zu setzende Empf�nger als String oder Array von Strings, die maximale L�nge der Empf�ngerliste variiert zwischen MTAs.
	 * @return 0 im Erfolgsfall, Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 *
	 */
	function setRecievers($rcvs)
	{
		$this->recievers = "";
		if (is_array($rcvs)) {
			foreach ($rcvs as $rcv)
				$this->recievers .= ",$rcv";
			$this->recievers = substr($this->recievers, 1);
		} else {
			$this->recievers = $rcvs;
		}
		return 0;
	}
	
	/**
	 * Die CC-Empf�nger der Mail setzen. Es kann ein einzelner String oder ein Array von Strings mit
	 * mehreren CC-Empf�ngern �bergeben werden.
	 * 
	 * @param $rcvs zu setzende CC-Empf�nger als String oder Array von Strings, die maximale L�nge der Empf�ngerliste variiert zwischen MTAs.
	 * @return 0 im Erfolgsfall, Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 *
	 */
	function setCCRecievers($rcvs)
	{
		$this->CC_recievers = "";
		if (is_array($rcvs)) {
			foreach ($rcvs as $rcv)
				$this->CC_recievers .= ",$rcv";
			$this->CC_recievers = substr($this->CC_recievers, 1);
		} else {
			$this->CC_recievers = $rcvs;
		}
		return 0;
	}
	
	/**
	 * Die BCC-Empf�nger (versteckte Empf�nger) der Mail setzen. Es kann ein einzelner String oder ein Array von Strings mit
	 * mehreren BCC-Empf�ngern �bergeben werden.
	 * 
	 * @param $rcvs zu setzende BCC-Empf�nger als String oder Array von Strings, die maximale L�nge der BCC-Empf�ngerliste variiert zwischen MTAs.
	 * @return 0 im Erfolgsfall, Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 *
	 */
	function setBCCRecievers($rcvs)
	{
		$this->BCC_recievers = "";
		if (is_array($rcvs)) {
			foreach ($rcvs as $rcv)
				$this->BCC_recievers .= ",$rcv";
			$this->BCC_recievers = substr($this->BCC_recievers, 1);
		} else {
			$this->BCC_recievers = $rcvs;
		}
		return 0;
	}
	
	/**
	 * Den Absender der EMail setzen. Default ist "", Bsp. "Jonny <johnny@example.nix>".
	 * 
	 * @param  $from den zu setzenden Absender.
	 * @return 0 im Erfolgsfall, Fehlerfall nicht m�glich, Fehlercode wird nicht ver�ndert.
	 *
	 */
	function setFromHeader($from)
	{
		$this->sender = $from;
		return 0;
	}
	
	/**
	 * Die Mail �ber PHP mail commando senden. Falls dump = true gesetzt wird, wird
	 * die mail nicht versandt, sondern der generierte Mailtext als string
	 * zur�ck gegeben. Wir verwenden hier absichtlich das PHP mail. Falls Sie direkt �ber
	 * SMTP senden m�chten, k�nnen Sie diese Klasse nicht verwenden.
	 * 
	 * @param  $dump falls true, wird die Mail nicht versandt, sondern ein String mit der Mail zur�ck gegeben (Testzwecke).
	 * @return 0 im Erfolgsfall, sonst eine Fehlerzahl aus der Fehlertabelle, bzw. der Mailstring falls dump==true gilt.
	 *
	 */
	function sendMail($dump = false)
	{
		$mime_boundary_alternative = "ALT+".md5(time());
		$mime_boundary_mixed = "MIXD+".md5(time());
		$eol = $this->eol;
		// if there are attachments, we need the mp-mixed mode
		
		// test the full program
		// build the header
		$header = "";
  		$header .= "From: {$this->sender}".$eol;
  		if (!empty($this->CC_recievers))
  			$header .= "CC: {$this->CC_recievers}".$eol;
  		if (!empty($this->BCC_recievers))
  			$header .= "BCC: {$this->BCC_recievers}".$eol;
  		if (!empty($this->replyTo))
  			$header .= "Reply-To: {$this->replyTo}".$eol;
  		if (!empty($this->replyTo))
  			$header .= "Return-Path: {$this->replyTo}".$eol;    
		$header .= "X-Mailer: MIMEMailxPHP4 V1".$eol; 
	  	$header .= "Mime-Version: 1.0".$eol;
		$header .= "Content-Type: multipart/mixed; boundary=\"$mime_boundary_mixed\"".$eol;
		// build the mailbody
		$mailbody = "";
		/*
    $mailbody .= "If you can read this, please consider to upgrade your ".$eol;
		$mailbody .= "mail client program".$eol;
		$mailbody .= $eol;
		$mailbody .= "--$mime_boundary_mixed".$eol;
		$mailbody .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary_alternative\"".$eol;
		$mailbody .= $eol;
		$mailbody .= "--$mime_boundary_alternative".$eol;
		$mailbody .= "Content-Type: text/plain; charset={$this->textContent[1]}".$eol;
		$mailbody .= "Content-Transfer-Encoding: {$this->textContent[2]}".$eol;
		$mailbody .= $eol;
		*/$mailbody .= $this->textContent[0];
		$mailbody .= $eol;
		$mailbody .= $eol;
		if (!empty($this->htmlContent[0])) {
			$mailbody .= "--$mime_boundary_alternative".$eol;
			$mailbody .= "Content-Type: text/html; charset={$this->htmlContent[1]}".$eol;
			$mailbody .= "Content-Transfer-Encoding: {$this->htmlContent[2]}".$eol;
			$mailbody .= $eol;
			$mailbody .= $this->htmlContent[0];
			$mailbody .= $eol;
			$mailbody .= $eol;
		}
		/*$mailbody .= "--{$mime_boundary_alternative}--".$eol;
		$mailbody .= $eol;
		$mailbody .= "--$mime_boundary_mixed";
		*/
    // now add the attachments
    if ($this->attachments)
		foreach ($this->attachments as $attachment) {
			$dispo = empty($attachment[3]) ? "attachment" : "inline";
			$mailbody .= $eol;
			$mailbody .= "Content-Disposition: $dispo; filename={$attachment[2]}".$eol;
			$mailbody .= "Content-Type: {$attachment[1]}; name={$attachment[2]}".$eol;
			if (!empty($attachment[3])) {
				$mailbody .= "Content-ID: {$attachment[3]}".$eol;
			}
			$mailbody .= "Content-Transfer-Encoding: base64".$eol;
			$mailbody .= $eol;
			$mailbody .= $attachment[0];
			$mailbody .= $eol;
			$mailbody .= "--$mime_boundary_mixed";
		}
		//$mailbody .= "--".$eol;
		
		// finally send it out with mail
		if ($dump == false) {
			mail($this->recievers, $this->subject, $mailbody, $header);
		} else { // or dump it
			return $header."$eol".$mailbody;
		}
	}
	
	
}
?>
