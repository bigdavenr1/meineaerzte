<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// sessionLink-Klasse - Dient zur Aufrechterhaltung der Session
//                    - Kontrolliert session_start() und ob Cookies erlaubt sind
//                    - WICHTIG: session_start() muss VOR dieser Klasse ausgeführt werden,
//                      falls eine session benutzt wird
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("sessionLink"))
{
    class sessionLink
    {
        var $sesslink;
        var $sid;
        
        
        
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        function sessionLink()
        {
            $this->sesslink = false;
            $this->sid = strip_tags(SID);
    
            if (isset($_SESSION) && !$_COOKIE[session_name()])
            {
                $this->sesslink = true;
            }
        }
        
        
        
        // sessionLink Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        function makeLink($name, $href, $class = false, $target = false, $extra = false, $ankor = false)
        {
            if ($this->sesslink)
            {
                if (ereg("[?]", $href)) $href .= '&amp;'.$this->sid;
                else $href .= '?'.$this->sid;
            }
            
            if (ereg("[?]", $href)) $href .= '&amp;sv=1';
            else $href .= '?sv=1';
            if ($ankor) $href .='#'.$ankor;
        
            $html .= '<a href="'.$href.'"';
            if ($target) $html .= ' target="'.$target.'"';
            if ($class) $html .= ' class="'.$class.'"';
            if ($extra) $html .= ' '.$extra;
            $html .= '>'.$name.'</a>';
        
            return $html; 
        }

        function makeFormLink($href)
        {
            if ($this->sesslink)
            {
                if (ereg("[?]", $href)) $href .= '&amp;'.$this->sid;
                else $href .= '?'.$this->sid;  
            }
            
            if (ereg("[?]", $href)) $href .= '&amp;sv=1';
            else $href .= '?sv=1';
            
            return $href;
        }
        
        function makeUrl($href)
        {
            if ($this->sesslink)
            {
                if (ereg("[?]", $href)) $href .= '&'.$this->sid;
                else $href .= '?'.$this->sid;  
            }
            
            if (ereg("[?]", $href)) $href .= '&sv=1';
            else $href .= '?sv=1';
            
            return $href;
        }
    }
}

?>