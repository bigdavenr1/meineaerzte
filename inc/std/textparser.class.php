<?php




/////////////////////////////////////////////////////////////////////////////////////////////////////
// Text Parser - Ersetzt "eigene Befehle" in HTML-Tags
/////////////////////////////////////////////////////////////////////////////////////////////////////




if (!class_exists("TextParser"))
{
    class TextParser
    {
        var $text;
    
    
        
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
         function TextParser($text)
        {
            $this->text = $text;
        }
        
        
        
        // TextParser Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
    
        
     
         function extractBetweenDelimeters($inputstr, $delimeterLeft, $delimeterRight) 
        {
           $posLeft = stripos($inputstr, $delimeterLeft) + strlen($delimeterLeft);
           $posRight = stripos($inputstr, $delimeterRight, $posLeft+1);
           return substr($inputstr, $posLeft, $posRight-$posLeft);
        }
        
        
        function extractBetweenOffsetAndDelimiter($inputstr, $pos, $delimiter)
        {
            $resstr = "";
            for ($rpos = $pos; $inputstr[$rpos] != "]" && $inputstr[$rpos] != ""; $rpos++)
            {
                $resstr .= $inputstr[$rpos];
            }
            
            return $resstr;
        }
        
        
         function parse()
        {
            global $root;
            global $CONST_IMG_CHAT_MAX;
                
        
            $replaces = Array 
            (
            
            
                // Vorgabe Smilies
                //////////////////////////////////////////////////////////////////////////////////
                
                
                
                Array ( ':-)', '<img src="'.$root.'images/smilies/emoticon_smile.png" alt="smilie" />' ),
                Array ( ':-D', '<img src="'.$root.'images/smilies/emoticon_grin.png" alt="smilie" />' ),
                Array ( ':-P', '<img src="'.$root.'images/smilies/emoticon_tongue.png" alt="smilie" />' ),
                Array ( ':-(', '<img src="'.$root.'images/smilies/emoticon_unhappy.png" alt="smilie" />' ),
                Array ( ';-)', '<img src="'.$root.'images/smilies/emoticon_wink.png" alt="smilie" />' ),
                Array ( ':-O', '<img src="'.$root.'images/smilies/emoticon_surprised.png" alt="smilie" />' ),
                Array ( ':-?', '<img src="'.$root.'images/smilies/emoticon_waii.png" alt="smilie" />' ),
                Array ( 'X-D', '<img src="'.$root.'images/smilies/emoticon_evilgrin.png" alt="smilie" />' ),
                Array ( '8-)', '<img src="'.$root.'images/smilies/smoke.gif" alt="smilie" />' ),
                
                    
                    
                // Vorgabe Textformatierung
                //////////////////////////////////////////////////////////////////////////////////
                    
                    
                    
                Array ( '[b]',   '<b>' ),
                Array ( '[/b]',  '</b>' ),
                Array ( '[i]',   '<i>' ),
                Array ( '[/i]',  '</i>' ),
                Array ( '[u]',   '<u>' ),
                Array ( '[/u]',  '</u>' ),
                Array ( '[/a]',  '</a>' ),
                
                
                
                // htmlspecialchars
                //////////////////////////////////////////////////////////////////////////////////
                    
                    
                    
                Array ( '&',   '&amp;' )
                
            );
            
            
            
            // Befehle / Parameter parsen
            //////////////////////////////////////////////////////////////////////////////////////
            
            
            
            for ($ipos = 0; $ipos < strlen($this->text); $ipos++)
            {
                $matchstr .= $this->text[$ipos];
                
                
                //
                // Link - [A url=www.link.de]text[/A]
                //
                
                
                if (ereg("\[a ", $matchstr))
                {
                    $matchstr = "";
                    $paramstr = $this->extractBetweenOffsetAndDelimiter($this->text, $ipos-2, "]");     
                    $param = explode(" ", trim($paramstr));
                    
                    $url = substr($param[1], (strlen($param[1]) - 4) * -1);
                    
                    if (!ereg("http://", $url)) $url = "http://".$url;
                    
                    $htmlstr = '<a href="'.$url.'" target="blank">';
                    
                    $this->text = str_replace($paramstr."]", $htmlstr, $this->text);
                }
                
                
                //
                // Quote - [quote msg=53]
                //
                
                
                if (ereg("\[quote ", $matchstr))
                {
                    
                    $paramstr = $this->extractBetweenOffsetAndDelimiter($this->text, $ipos, "]");     
                    trim($paramstr);
                    
                    $wert = explode("=", $paramstr);
                    if (!is_numeric($wert[1])) $wert[1] = false;
                    
                    $forummsgfac = new ForumMsg();
                    $forummsgfac->getById($wert[1]);
                    if ($msg = $forummsgfac->getElement())
                    {
                        $qbenutzerfac = new Benutzer();
                        $qbenutzerfac->getById($msg->user_id);
                        $qbenutzer = $qbenutzerfac->getElement();
                    
                        $ql = new sessionLink();
                    
                        $htmlstr = '<br/><span style="font-style:italic;font-weight:bold;font-size:10px;">'.$ql->makeLink($qbenutzer->nickname, WEBDIR."bewertungen/userprofil.php?id=".$qbenutzer->id, ($qbenutzer->geschlecht=="m"?"mcolor":"wcolor")).' schrieb:</span><br/><span class="quote"><br/>'.$msg->text.'</span>';
                    }
                    else
                    {
                        $htmlstr = '<span class="quote">Diesen Beitrag gibt es nicht...</span>';
                    }
                    
                    $this->text = str_replace("[quote".$paramstr."]", $htmlstr, $this->text);
                    
                    $matchstr = "";
                }
                
                
                //
                // Bild - [IMG url=www.link.de+titel=text]
                //
                
                
                if (ereg("\[img ", $matchstr))
                {
                    $matchstr = "";
                    $paramstr = $this->extractBetweenOffsetAndDelimiter($this->text, $ipos, "]");     
                    $param = explode("+", trim($paramstr));
                    
                    $url = explode("=", $param[0]);
                    if (!ereg("http://", $url[1])) $url[1] = "http://".$url[1];
                    
                    $titel = explode("=", $param[1]);
                    
                    
                    //
                    // Bildgr��e �berpr�fen
                    // filesize geht nicht, wegen no-cache
                    //
                    
                    
                    /*echo "{".$url[1]."}";
                    
                    if (strlen($url[1]) > 20) {}
                        $size = getimagesize($url[1]);
                        //$img = ImageCreateFromGIF($url[1]);
                    
                    
                    unset($err);
                    if (!$size[0] && !$size[1]) $err = '*Das ist kein Bild*';
                    
                    */
                    $htmlstr = '<img src="'.$url[1].'" alt="'.$titel[1].'" title="'.$titel[1].'">';
                    $this->text = str_replace("[img".$paramstr."]", $htmlstr, $this->text);  
                         
                }
                
                
                
                
                //
                // Bild aus eigener Galerie - [IMG nr=2]
                //
                
                
                /*
                if (ereg("\[img ", $matchstr))
                {
                    $matchstr = "";
                    $paramstr = $this->extractBetweenOffsetAndDelimiter($this->text, $ipos, "]");     
                    $param = explode(" ", trim($paramstr));
                    
                    
                    $fotofac = new Foto();
                    $fotofac->getByUser($benutzer->id);
                    
                    
                    //
                    // Bildid �berpr�fen
                    //
                    
                    
                   
                    unset($err);
                    if (!$size[0] && !$size[1]) $err = '*Das ist kein Bild*';
                    
                    if (!$err)
                    {
                        $htmlstr = '<img src="'.$url[1].'" width="'.$thumbBreite.'" height="'.$thumbHoehe.'" alt="'.$titel[1].'" title="'.$titel[1].'">';
                    }
                    else
                    {
                        $htmlstr = $err;
                    }
                    
                    $this->text = str_replace($paramstr."]", $htmlstr, $this->text);        
                }
                */
                
                
                
            }
            
            
            
            // Replacesarray anwenden
            //////////////////////////////////////////////////////////////////////////////////////
            
            
            
            for($i = 0; $i < count($replaces); $i++)
            {
                $this->text = str_replace($replaces[$i][0], $replaces[$i][1], $this->text);
                if (ereg("></a",$this->text))
                {
                    $this->text = str_replace("></a", ">".$url."</a", $this->text);
                }
            }
        
            
        }
        
        
         function getParsedText()
        {
            return $this->text;
        }
    }
}
?>