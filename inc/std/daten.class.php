<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// UserKlasse - Klasse um alle Arten von Benutzern zu verwalten (auch Admins)
//            - WICHTIG: Session muss vorher gestartet sein
//////////////////////////////////////////////////////////////////////////////////////////////////

if (!class_exists("Daten"))
{
    class Daten extends Basicdb
    {
    
        // Anzeigebegrenzer
        var $limit;
        var $limitanzahl;
        var $limitseiten;
        var $limitoffset;
        var $limitoffsetaffix;
        
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
                
         function Daten($anzahl = 0)
        {
            global $sql;
            $this->table = $sql["table_data"];
            $this->anzahl = $anzahl;
            
            $_GET["limitoffset".$this->limitoffsetaffix] ? $this->limitoffset = $_GET["limitoffset".$this->limitoffsetaffix] : $this->limitoffset = 0;

            parent::Basicdb();
        }
      
        
        // User Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        // �berladen
         function getByAutor($autor)
        {
            parent::createQuery("WHERE Autor='".$autor."' ORDER BY Datum DESC");
        }
        
        function setLimit($limit)
        {
            $this->limit=$limit;
        }
        
        function getLimit()
        {
            return $this->limit;
        }
        
        function getLimitLink($version = "std", $para = "")
        {
            $navilink = new sessionLink();
            $html = "";
            
            if($this->limit > 0)  
            {
                $this->limitanzahl = parent::getElementCount();
                $this->limitseiten =  round($this->limitanzahl/$this->limit);
            }
            
            if ($version == "std")
            {
                if ($this->limitoffset > 0) 
                {
                    $vorindex = ($this->limitoffset-1) * $this->limit;
                    $html .= $navilink->makeLink("&laquo;&laquo;&laquo;&nbsp;", $_SERVER['SCRIPT_URI']."?limitoffset".$this->limitoffsetaffix."=".($this->limitoffset-1)."&amp;index=".$vorindex.$para);
                }
                else $html .= '<span class="browsersmooth">&laquo;&laquo;&laquo;&nbsp;</span>';
                
                if ($this->limitseiten<=5)
                {
                    for ($i = 0; $i < $this->limitseiten; $i++)
                    {
                        $linknr = "";
                        $i==$this->limitoffset?$linknr .= "<b><big>":"";
                        $linknr .= "[".(1+$i)."]";
                        $index = 0 + ($i * $this->limit);
                        $i==$this->limitoffset?$linknr .= "</big></b>":"";
                    
                        $html .= $navilink->makeLink($linknr, $_SERVER['SCRIPT_URI']."?limitoffset".$this->limitoffsetaffix."=".$i."&amp;index=".$index.$para);
                    }
                }
                
                else
                {
                    for ($i = 0; $i < $this->limitseiten; $i++)
                    {   
                        if ($i==0 || $i == ($this->limitseiten-1))
                        { 
                            $linknr = "";
                            $i==$this->limitoffset?$linknr .= "<b><big>":"";
                            $linknr .= "[".(1+$i)."]";
                            $index = 0 + ($i * $this->limit);
                            $i==$this->limitoffset?$linknr .= "</big></b>":"";
                        
                            $html .= $navilink->makeLink($linknr, $_SERVER['SCRIPT_URI']."?limitoffset".$this->limitoffsetaffix."=".$i."&amp;index=".$index.$para);
                        }
                        
                        if ((($i+1) == $this->limitoffset && $i !=0) || ($i == $this->limitoffset  && ($i !=0 && $i != ($this->limitseiten-1)) )|| (($i-1) == $this->limitoffset && $i != ($this->limitseiten-1)))
                        { 
                         
                            $linknr = "";
                            if (($i+1) == $this->limitoffset && $i !=0 && $i-1!=0) $linknr.=" ... ";
                            $i==$this->limitoffset?$linknr .= "<b><big>":"";
                            $linknr .= "[".(1+$i)."]";
                            $index = 0 + ($i * $this->limit);
                            $i==$this->limitoffset?$linknr .= "</big></b>":"";
                            if (($i-1) == $this->limitoffset && $i !=0 && $i+1!=0) $linknr.=" ... ";
                        
                            $html .= $navilink->makeLink($linknr, $_SERVER['SCRIPT_URI']."?limitoffset".$this->limitoffsetaffix."=".$i."&amp;index=".$index.$para);
                        }
                    }
                }
                $maxindex = $this->limitanzahl-1;
                if ($this->limitoffset < $this->limitseiten - 1) 
                {
                    $nachindex = ($this->limitoffset+1) * $this->limit;
                    $html .= $navilink->makeLink("&nbsp;&raquo;&raquo;&raquo;", $_SERVER['SCRIPT_URI']."?limitoffset".$this->limitoffsetaffix."=".($this->limitoffset+1)."&amp;index=".$nachindex.$para);
                }
                else $html .= '<span class="browsersmooth">&nbsp;&raquo;&raquo;&raquo;</span>';
            }
                       
            return $html;
        }  
				
	    function getByAutorAndAktiv($autor)
        {
            parent::createQuery("WHERE Autor='".$autor."' AND status != 'N' ORDER BY Datum DESC");
        }
        
         function getByFree($free)
        {
            parent::createQuery("WHERE Free='".$free."' ORDER BY Datum DESC");
        }
        
         function getDaten()
        {
            parent::createQuery(" ORDER BY Datum DESC");
        }
        
        function getNew()
        {
            parent::createQuery("WHERE aktiv ='2' ");
        }
        
        function getNewSort($sort)
        {
            parent::createQuery("WHERE aktiv !='1' AND aktiv !='0' ".$sort);
        }
        
        function getNewPremium()
        {
            parent::createQuery("WHERE status ='A' ");
        }
        
        function getNewPremiumSort($sort)
        {
            parent::createQuery("WHERE status ='A' ".$sort);
        }
        
         function getByKat($kat)
        {
            parent::createQuery("WHERE Kategorie='".$kat."' ORDER BY Datum DESC");
        }
        
          function getByDate($date,$date1)
        {
            parent::createQuery("WHERE Datum > '".$date."' AND Datum < '".$date1."' AND status != 'N' ORDER BY Datum DESC");
        }
        
         function getByKatAndStatus($status,$kat)
        {
            parent::createQuery("WHERE Kategorie='".$kat."' AND Status='".$status."' ORDER BY Datum DESC");
        }
        
         function getById($id,$sort="")
        {
            parent::createQuery("WHERE id=".$id);
        }
        
         function getByStatus($status,$sort="")
        {
            parent::createQuery("WHERE Status='".$status."' ORDER by '".$sort."'");
        }
        
        
         function getByIdBasic($id,$sort="")
        {
            parent::createQuery("WHERE id=".$id."'");
        }
        
         function getByLogin($name, $pass, $sort="")
        {
            parent::createQuery("WHERE nickname='".$name."' AND pass='".$pass."' AND aktiv=1");
        }
        
         function getByNick($name, $sort="")
        {
            parent::createQuery("WHERE nickname='".$name."' AND aktiv=1");
        }
        
         function getByMail($mail, $sort="")
        {
            parent::createQuery("WHERE email='".$mail."'");
        }
        
         function getByKuNr($mail, $sort="")
        {
            parent::createQuery("WHERE kundennummer='".$mail."'");
        }
        
         function getByTyp($typ,$sort="")
        {
            parent::createQuery("WHERE typ='".$typ."' ORDER by '".$sort."'");
        }
        
         function getByGeschlecht($art,$sort="")
        {
            parent::createQuery("WHERE geschlecht='".$art."' AND aktiv=1 ");
        }
        
         function getByKey($key,$sort="")
        {
            parent::createQuery("WHERE aktivierungskey='".$key."'");
        }
        
         function insertIntoSession($benutzer)
        {
            unset($_SESSION["user"]);
            $_SESSION["user"] = $benutzer;
        }
        
         function checkRight($typ)
        {
            return $this->typ == $typ;
        }
        
         function calculateAge($datum)
        {
            $benutzerjahr = explode("-", $datum); 
            $jahre = date("Y") - $benutzerjahr[0];
            if ($benutzerjahr[1] > date("m")) $jahre--;
            if ($benutzerjahr[1] == date("m") && $benutzerjahr[2] > date("d")) $jahre--;
            return $jahre;
        }
        
        public function search($str=false,$plz=false,$ort=false,$land=false,$fach=false,$sd=false,$ed=false,$st=false,$et=false,$name=false)
        {
            $statement="";
            $join ="";
            $select ="";
            
            if ($str)
            {
                if($fach!="") 
                {   
                    $join .= " LEFT JOIN fachgebiete ON (fachgebiete.id='".$fach."')";
                    $select .= ", fachgebiete.id AS fachid , fachgebiete.name ";
                }
                
                $str2=explode(" ",mysql_real_escape_string($str));
                       
                for ($x=0; $x<sizeof($str2);$x++)
                { 
                    if($fach!="") 
                    {   
                        $statement.=" AND (suchbegriffe LIKE '%".$str2[$x]."%' OR Ort LIKE '%".$str2[$x]."%' OR plz LIKE '%".$str2[$x]."%' OR unternehmen.name LIKE '%".$str2[$x]."%' OR famname LIKE '%".$str2[$x]."%' OR beschreibung LIKE '%".$str2[$x]."%' OR schlagworte LIKE '%".$str2[$x]."%' OR fachgebiete.name LIKE '%".$str2[$x]."%' )";
                    }
                    else $statement.=" AND (suchbegriffe LIKE '%".$str2[$x]."%' OR Ort LIKE '%".$str2[$x]."%' OR plz LIKE '%".$str2[$x]."%' OR unternehmen.name LIKE '%".$str2[$x]."%' OR famname LIKE '%".$str2[$x]."%' OR beschreibung LIKE '%".$str2[$x]."%' OR schlagworte LIKE '%".$str2[$x]."%' )";
                }
            }
            if ($plz!="") $statement.=" AND plz LIKE '".mysql_real_escape_string($plz)."%'"; 
            if ($name!="") $statement.=" AND famname LIKE '".mysql_real_escape_string($name)."%'";
            if ($ort!="") $statement.=" AND ort LIKE '%".mysql_real_escape_string($ort)."%'";
            if ($land!="") $statement.=" AND land='".mysql_real_escape_string($land)."'";
            if ($fach!="") 
            {   
                $statement.=" AND fachgebiete LIKE '%".mysql_real_escape_string($fach)."%'";  
            } 
            if ($st!="") 
            {
                //$join = " INNER LEFT JOIN ordzeiten ON (firmid=".$this->table.".id AND (( daystart <='".mysql_real_escape_string($sd)."' AND dayend >='".mysql_real_escape_string($sd)."') OR ( daystart <='".mysql_real_escape_string($ed)."' AND dayend >='".mysql_real_escape_string($sd)."')) AND (( timestart <='".mysql_real_escape_string($st)."' AND timeend >='".mysql_real_escape_string($st)."') OR ( timestart <='".mysql_real_escape_string($et)."' AND timeend >='".mysql_real_escape_string($st)."')))";
                $join .= " LEFT JOIN ordzeiten ON (ordzeiten.firmid=".$this->table.".id AND (( ordzeiten.daystart <='".mysql_real_escape_string($sd)."' AND ordzeiten.dayend >='".mysql_real_escape_string($sd)."') OR ( ordzeiten.daystart <='".mysql_real_escape_string($ed)."' AND ordzeiten.dayend >='".mysql_real_escape_string($sd)."')) AND (( ordzeiten.timestart <'".mysql_real_escape_string($st)."' AND ordzeiten.timeend >'".mysql_real_escape_string($st)."') OR ( ordzeiten.timestart <'".mysql_real_escape_string($et)."' AND ordzeiten.timeend >'".mysql_real_escape_string($st)."')))";
                $select .= ", ordzeiten.id AS offen"; 
                //$group = "GROUP BY unternehmen.id";
                //$order = " ordzeiten.id DESC ";
            }
            $this->felder=" *, unternehmen.id AS uid ".$select;
            parent::createQuery($join." WHERE aktiv='1' ".$statement." ".$group); // ." ORDER BY ".$order.", unternehmen.status DESC"
            //$string = $join." WHERE aktiv='1' ".$statement;//." ".$group
            //return $string;
        }
        
        public function writePic($pic,$key,$type)// erstellt Thumbnailbild
        {
            // Gr��e auslesen (size[0]=breite, size[1]=h�he)
            $size = getimagesize($pic);

            if ($size[0] > $size[1])
            {
                $thumbBreite = 125;
                $thumbHoehe = (125*$size[1])/$size[0];
                $thumbBreite2 = 360;
                $thumbHoehe2 = (360*$size[1])/$size[0];
            }
            
            else
            {
                $thumbHoehe = 125;
                $thumbBreite= (125*$size[0])/$size[1];
                $thumbHoehe2 = 360;
                $thumbBreite2 = (360*$size[0])/$size[1];
            }
            
            if($type == '.jpg') $altesBild = ImageCreateFromJPEG($pic);
            if($type == '.gif') $altesBild = ImageCreateFromGIF($pic);
                       
            $neuesBild = imageCreateTrueColor($thumbBreite, $thumbHoehe);
            $neuesBild2 = imageCreateTrueColor($thumbBreite2, $thumbHoehe2);
        
            imageCopyResampled($neuesBild, $altesBild, 0, 0, 0, 0, $thumbBreite, $thumbHoehe, $size[0], $size[1]);
            imageCopyResampled($neuesBild2, $altesBild, 0, 0, 0, 0, $thumbBreite2, $thumbHoehe2, $size[0], $size[1]);
            
            if($type == '.jpg')
            { 
                ImageJPEG($neuesBild,  LOCALDIR."images/unternehmen/".$key."_thumb".$type, 85);
                ImageJPEG($neuesBild2,  LOCALDIR."images/unternehmen/".$key.$type, 85);
            }
            if($type == '.gif')
            {             
               ImageGIF($neuesBild,  LOCALDIR."images/unternehmen/".$key."_thumb".$type, 85);
               ImageGIF($neuesBild2,  LOCALDIR."images/unternehmen/".$key.$type, 85);
            }
            
            chmod( LOCALDIR."images/unternehmen/".$key."_thumb".$type, 0777);
            chmod( LOCALDIR."images/unternehmen/".$key.$type, 0777);
        }
    }
}
?>