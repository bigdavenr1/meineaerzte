<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// MenuHandle Klasse - Erstellt Menulinks
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("MenuHandle"))
{
    class MenuHandle extends sessionLink
    {
        var $name;
        var $url;
        var $dir;
        var $class;
        var $linkobj;
        
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
    
        
         function MenuHandle()
        {
            $this->linkobj = new sessionLink();
        }
        
        
        // MenuHandle Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
         function createLink($name, $href, $dir = "", $id = "", $target = "")
        {
            $this->name = $name;
            $this->href = $href;
            $this->dir = $dir;
                    
            if ($this->dir == "")
            {
                isInUrl($this->href) ? $this->class = "menu_on".$id : $this->class = "menu".$id;
            }
            else
            {   
                isInUrl($this->dir) ? $this->class="menu_on".$id : $this->class="menu".$id;
            } 
        
            echo $this->linkobj->makeLink($name, $href, $this->class, $target);
        }
    }
}
?>
