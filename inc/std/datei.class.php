<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// Datei / Bilder-Klasse - Klasse zum Schreiben und �ffnen von Dateien und Bildern
//                       - Speichert Dateien in eine DB oder auf die Platte
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("Datei"))
{
    class Datei extends Basicdb
    {
        var $mode;
        var $data = Array();
        
    
    
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
    
        
        
        public function __construct($mode = "db", $anzahl = 0)
        {
            global $sql;
            $this->table = $sql["table_datei"];
            $this->mode = $mode;
            $this->anzahl = $anzahl;
            parent::__construct();
        }
    
        
        
        // Datei Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        public function getByGroup($group)
        {
            parent::createQuery("WHERE ref_group='".$group."'");
        }
        
        
        public function getByRefAndGroup($id, $group)
        {
            if (!$id) $id = 0;
            parent::createQuery("WHERE ref_id=".$id." AND ref_group='".$group."' ORDER BY id, sort");
        }
        
        
        public function getByLatestRefAndGroup($id, $group, $limit = 1)
        {
            if (!$id) $id = 0;
            parent::createQuery("WHERE ref_id=".$id." AND ref_group='".$group."' ORDER BY id DESC LIMIT ".$limit);
        }
        
        
        public function getByRefAndGroupExcepting($id, $group, $except)
        {
            parent::createQuery("WHERE ref_id=".$id." AND ref_group='".$group."' AND ".$except." ORDER BY id, sort");
        }
        
        
        public function getDatei($datei = false)
        {        
            if ($datei) $tmpdata = $datei;
            else $tmpdata = parent::getLastElement();
            
        
            if ($this->mode == "db")
            {
                header("Pragma: public");
                header("Cache-Control: no-store, no-cache");
                header("Content-Type: ".$tmpdata->mime);
                header("Content-Disposition: inline; filename=".$tmpdata->name);
                header("Content-Length: ".$tmpdata->size);
                
                return $tmpdata->content;
            }
            
            if ($this->mode == "file")
            {
                global $root;
                return $root.$tmpdata->path.$tmpdata->name;
            }
        }
        
        
        public function write($id, $ref_id, $ref_group, $sort, $fn, $text = "", $path = "")
        {
            // Umlaute umwandeln
            $filename = convertUmlaut(md5($id.$ref_id.$text.time())."_".$_FILES[$fn]["name"]);
            // Leerzeichen durch Unterstriche ersetzen
            $filename = ereg_replace(" ", "_", $filename);
            
            
            $this->data[] = $id;
            $this->data[] = $ref_id;
            $this->data[] = $ref_group;
            $this->data[] = $sort;
            $this->data[] = $filename;
            $this->data[] = $_FILES[$fn]["type"];
            $this->data[] = $_FILES[$fn]["size"];
            $this->data[] = "";
            $this->data[] = $path;
            $this->data[] = $text;
            
            if ($this->mode == "db")
            {
                $this->data[7] = addslashes(fread(fopen($_FILES[$fn]["tmp_name"], "rb"), $_FILES[$fn]["size"]));
                parent::write($this->data);
            }
            
            if ($this->mode == "file")
            {
                global $root;
                if (move_uploaded_file($_FILES[$fn]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].$root.$path.$filename))
                {
                
                    //
                    // L�schen der alten Datei mit der selben ID
                    //
                    
                    if ($id)
                    {
                        $tmpdata = parent::getById($id);
                        $tmpdata = parent::getElement();
                        unlink($_SERVER['DOCUMENT_ROOT'].$root.$tmpdata->path.$tmpdata->name);
                    }
                    
                    parent::write($this->data);
                    
                    //
                    // Rechte setzen
                    //
                    
                    chmod($_SERVER['DOCUMENT_ROOT'].$root.$path.$filename, 0777);
                }
                else
                {
                    echo print_r($this->data);
                    echo "<br><br>Sorry, die Datei konnte nicht hochgeladen werden.";
                }
            }
        }
        
        
        public function deleteElement()
        {
            if ($this->mode == "file")
            {
                $tmpdata = parent::getLastElement();
                global $root;
                unlink($_SERVER['DOCUMENT_ROOT'].$root.$tmpdata->path.$tmpdata->name);
            }
            
            parent::deleteElement();
        }
        
        
        
        // Extra Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
    
        
        
        public function createTable()
        {
            $query = 'CREATE TABLE `datei` ('
                   . ' `id` INT NOT NULL AUTO_INCREMENT, '
                   . ' `ref_id` INT NOT NULL, '
                   . ' `ref_group` VARCHAR(50) NOT NULL, '
                   . ' `sort` INT NOT NULL, '
                   . ' `name` VARCHAR(50) NOT NULL, '
                   . ' `mime` VARCHAR(4) NOT NULL, '
                   . ' `size` INT NOT NULL, '
                   . ' `content` MEDIUMBLOB NOT NULL, '
                   . ' `path` VARCHAR(255) NOT NULL, '
                   . ' `text` MEDIUMTEXT NOT NULL, '
                   . ' `access` INT NOT NULL,'
                   . ' PRIMARY KEY (`id`)'
                   . ' )'
                   . ' TYPE = myisam';
              
            mysql_query($query) or die(mysql_error());
        }
    }
}
?>