<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// Email-Klasse - Verschickt Mails an mehrere Empf�nger (TO, CC, BCC)
//              - HTML-Content
//////////////////////////////////////////////////////////////////////////////////////////////////



if (!class_exists("Email"))
{
    class Email
    {
        var $from;
        var $to;
        var $cc = Array();
        var $bcc = Array();
        var $replayto;
        var $subject;
        var $content;
        var $typ;
        
        
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
    
    
         function Email($typ = false)
        {
            if ($typ == "html") 
            {
                $this->typ = "text/html";
            }
            else 
            {
                $this->typ = "text/plain";
            }
        }
        
        
        // Email Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
    
        
         function setFrom($from)
        {
            $this->from = $from;
        }
        
         function setTo($to)
        {
            $this->to = $to;
        }
        
         function setCC($cc)
        {
            $this->cc = $cc;
        }
        
         function setBCC($bcc)
        {
            $this->bcc = $bcc;
        }
        
         function setReplayto($replayto)
        {
            $this->replayto = $replayto;
        }
        
         function setSubject($subject)
        {
            $this->subject = $subject;
        }
        
         function setContent($content)
        {
            $this->content = $content;
        }
        
         function sendMail()
        {	    
                foreach ($this->cc as $cc)
                {
                    $ccs .= ",".$cc;
    		    }
                $ccs = substr($ccs, 1);
        
                foreach ($this->bcc as $bcc)
                {
                    $bccs .= ",".$bcc;
    		    }
                $bccs = substr($bccs, 1);
        
        
                // Header zusammenbauen
    		    $header = "From: ".$this->from."\n";
                
                if ($this->cc) 
                    $header .= "CC: ".$ccs."\n"; 
                if ($this->bcc) 
                    $header .= "BCC: ".$bccs."\n";
                if ($this->replayto) 
                    $header .= "Reply-To: ".$this->replayto."\n";
                
                $header .= "X-Mailer: PHP/".phpversion()."\n";
    		    $header .= "X-Sender-IP: ".$_SERVER['REMOTE_ADDR']."\n";
                $header .= "MIME-Version: 1.0\n";
    		    $header .= "Content-Type: ".$this->typ."; charset=iso-8859-1\n";
                
                mail($this->to, $this->subject, $this->content, $header);
        }
    }
}
?>
