<?php

//V 1.0 - 25.Juni.2008

//////////////////////////////////////////////////////////////////////////////////////////////////
// Basicdb-Klasse - Basisklasse aller Tabellenklassen
//                - Navigator zur limitierten Darstellung integriert
//////////////////////////////////////////////////////////////////////////////////////////////////

        
        

if (!class_exists("Basicdb"))
{
    class Basicdb
    {
        // Tabelle und Query
        var $table;
        var $result;
        var $id;
        var $field = Array();
        var $felder = false;    // Variable f�r JOINS
        var $maxergebnisse;
        
        // Listing
        var $liste = Array();
        var $pointer;
        
        // Navigator
        var $maxanzahl;
        var $anzahl;
        var $offset;
        var $offsetaffix;
        var $seiten;
        
        // Extra
        var $db_performence;
        
        
        
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
         function Basicdb()
        {
            
            global $root;
            global $sql;
            require_once($_SERVER['DOCUMENT_ROOT'].$root."/inc/std/dbconnect.inc.php");
            $this->maxergebnisse = $sql["maxergebnisse"];
            $_GET["offset".$this->offsetaffix] ? $this->offset = $_GET["offset".$this->offsetaffix] : $this->offset = 0;
        
            // DB-Perfomence-Statistik
            
            global $db_performence;
            $this->db_performence = $db_performence;
        }
        
        
        
        // Tabellenmethoden
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
         function createOwnQuery($query)
        {
                        
            $this->result = mysql_query($query) or die($query."<br>".mysql_error());
            $querycount = "SELECT count(*) AS max FROM ".$this->table." ".$query;
            $tmpres = mysql_fetch_object(mysql_query($querycount)) or die($query."<br>".mysql_error());
            $this->makeList($tmpres->max);
            
            if ($this->anzahl) 
            {
                $this->maxanzahl = $this->getElementCount();
                //$tmpres = mysql_fetch_object(mysql_query($querycount)) or die($query."<br>".mysql_error());
                //$this->maxanzahl = $tmpres->max;
                $this->maxanzahl ? $this->seiten = ceil($this->maxanzahl / $this->anzahl) : $this->seiten = 0;
            
                $limit = " LIMIT ".$this->getLimitStart().",".$this->anzahl;
                $query .= $limit;
                $this->result = mysql_query($query) or die($query."<br>".mysql_error());
                $this->makeList($tmpres->max);
            }
        }
        
        
         function createQuery($statement,$outputunlimited=false)
        {
            // Query generieren
            if ($this->felder) $query = "SELECT ".$this->felder." FROM ".$this->table." ".$statement;
            else $query = "SELECT * FROM ".$this->table." ".$statement;
            
            $querycount = "SELECT count(*) AS max FROM ".$this->table." ".$statement;
            $tmpres = mysql_fetch_object(mysql_query($querycount)) or die($query."<br>".mysql_error());

            // Navigatorlimit hinzuf�gen
            if ($this->anzahl) 
            {
                $outputunlimited="all";
                $this->maxanzahl = $tmpres->max;
                $this->maxanzahl ? $this->seiten = ceil($this->maxanzahl / $this->anzahl) : $this->seiten = 0;
            
                $limit = " LIMIT ".$this->getLimitStart().",".$this->anzahl;
                $query .= $limit;
            }
            
            
            // Performence-Ausgabe starten
            if ($this->db_performence) 
            {   
                $time = microtime(true);
                
                echo "<div style='position:relative; font-weight:bold; font-size:12px; background-color:#ffffff'>";
            }
            
            
            // Kompletten Query ausf�hren
            $this->result = mysql_query($query) or die($query."<br>".mysql_error());
            
            
            
            if ($this->db_performence)
            {
                // Infos
                $time = substr((microtime(true) - $time) / 1000, 0, 4);
                global $dbquerytime; 
                $dbquerytime += $time;  
                
                global $dbsize;
                $tabledata = mysql_fetch_array($this->result);
                $this->result = mysql_query($query);
                for($i = 0; $i < count($tabledata); $i++)
                {
                    $size += $tabledata[$i]["Data_length"] + $tabledata[$i]["Index_length"];
                }
                $dbsize += $size;
                
                global $dbquerys;
                $dbquerys++;
                
                echo "<span style='color:red;'>".$dbquerys." [".$time." ms] [".$size." Byte] </span>".$query;
                echo "</div><br><br>";
            }    
            
            if (!$outputunlimited)
            {
                // Interne Ergbnisliste erzeugen
                $this->makeList($tmpres->max);
            }
            
            else $this->makeList();
        }
        
        
         function createArray()
        {
            $query = "SELECT * FROM ".$this->table;
            $res = mysql_query($query);
            
            for ($i = 0; $i < mysql_num_fields($res); $i++)
            {
                $this->field[] = mysql_field_name($res, $i);
            }
        }
        
        
         function write($data, $out = true)
        {       
            unset($this->field);
            $this->createArray();
        
            for ($i = 0; $i < count($this->field); $i++)
            {
                if (!($data[0] && $this->field[$i] == "id"))
                    $statement .= " `" . $this->field[$i] . "`='" . $data[$i] . "',";
            }
            
            $statement = substr($statement, 0, -1);
            
            if (!$data[0]) $query = "INSERT INTO ".$this->table." SET".$statement;
            else $query = "UPDATE ".$this->table." SET".$statement." WHERE ID=".$data[0];
            
            if ($out) $this->result = mysql_query($query) or die($query."<br>".mysql_error());
            else {echo $query."<br>"; exit();}
        }
        
        
         function update($statement)
        {
            if ($this->id)
            {
                $query = "UPDATE ".$this->table." SET ".$statement." WHERE id=".$this->id;
                $this->result = mysql_query($query) or die($query."<br>".mysql_error());
            }
            
            else
            {
                $query = "UPDATE ".$this->table." SET ".$statement;
                $this->result = mysql_query($query) or die($query."<br>".mysql_error());
            }
        }
        
        
         function deleteElement($statement=false, $all=false)
        {
            if ($this->id)
            {
            $query = "DELETE FROM ".$this->table." WHERE id=".$this->id;
            $this->result = mysql_query($query) or die($query."<br>".mysql_error());
            }
            else if ($all)
            {
                $query = "DELETE FROM ".$this->table.$statement;
                $this->result = mysql_query($query) or die($query."<br>".mysql_error());
            }
        }
        
        
        
         function getAll($statement = "",$outputunlimited = false)
        {
            if (!$outputunlimited ) $this->createQuery($statement);
            else $this->createQuery($statement,$outputunlimited);
        }
        
        
         function getById($id)
        {
            if (!$id) $id = 0;
            $this->createQuery("WHERE id='".mysql_real_escape_string($id)."'");
        }  
            
              
         function getLatest($limit = 1)
        {
            $this->createQuery("ORDER BY id DESC LIMIT ".$limit);
        }
        
        
         function getLatestBy($query = "", $limit = 1)
        {
            $this->createQuery($query." ORDER BY id DESC LIMIT ".$limit);
        }
        
        
        
        // Listmethoden
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        function makeList($maximum=false)
        {
            unset($this->liste);
            if ($maximum)
            {
                if ($maximum<$this->maxergebnisse)
                {
                    while ($datensatz = mysql_fetch_object($this->result))
                    {
                        $this->liste[] = $datensatz;
                    }
            
                    $this->pointer = 0;
                    $this->id = $this->liste[0]->id;
                }
                else 
                {
                    $this->liste[]="Fehler";
                    $_SESSION['err']="Ihre Suche ergab mehr als 1000 Suchergebnisse. Bitte verfeinern Sie Ihre Suche!<br/>";
                }
            }
            
            else
            {
                while ($datensatz = mysql_fetch_object($this->result))
                {
                    $this->liste[] = $datensatz;
                }
            
                $this->pointer = 0;
                $this->id = $this->liste[0]->id;
            }
        }
            
            
         function getElement($index = false)
        {
            if ($index === false)
            {
                $datensatz = $this->liste[$this->pointer];
                $this->id = $this->liste[$this->pointer]->id;
                $this->pointer++;
            }
            else
            {
                $datensatz = $this->liste[$index];
                $this->id = $this->liste[$index]->id;
            }
                
            return $datensatz;
        }
        
        
         function getLastElement()
        {
            $datensatz = $this->liste[($this->pointer-1 >= 0 ? $this->pointer-1 : 0)];
            $this->id = $this->liste[($this->pointer-1 >= 0 ? $this->pointer-1 : 0)]->id;
            return $datensatz;
        }
        
        
         function getElementCount()
        {
            return count($this->liste);
        }
        
        
         function searchElement($key)
        {
            return array_search($key, $this->liste);
        }
        
        
         function getAllElements()
        {
            return $this->liste;
        }
        
    
        
        // Navigatormethoden
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
         function getLimitStart()
        {
            return $this->anzahl * $this->offset;
        }
        
        
         function getSeitenAnzahl()
        {
            return $this->seiten;
        }
        
        
         function getHtmlNavi($version = "std", $para = "")
        {
            $navilink = new sessionLink();
            $html = "";
            
            if ($version == "std")
            {
                if ($this->offset > 0) $html .= $navilink->makeLink("&laquo;&laquo;&laquo;&nbsp;", $_SERVER['SCRIPT_URI']."?offset".$this->offsetaffix."=".($this->offset-1).$para);
                else $html .= '<span class="browsersmooth">&laquo;&laquo;&laquo;&nbsp;</span>';
                
                if ($this->seiten<=5)
                {
                    for ($i = 0; $i < $this->seiten; $i++)
                    {
                        $linknr = "";
                        $i==$this->offset?$linknr .= "<b><big>":"";
                        $linknr .= "[".(1+$i)."]";
                        $i==$this->offset?$linknr .= "</big></b>":"";
                    
                        $html .= $navilink->makeLink($linknr, $_SERVER['SCRIPT_URI']."?offset".$this->offsetaffix."=".$i.$para);
                    }
                }
                
                else
                {
                    for ($i = 0; $i < $this->seiten; $i++)
                    {   
                        if ($i==0 || $i == ($this->seiten-1))
                        { 
                            $linknr = "";
                            $i==$this->offset?$linknr .= "<b><big>":"";
                            $linknr .= "[".(1+$i)."]";
                            $i==$this->offset?$linknr .= "</big></b>":"";
                        
                            $html .= $navilink->makeLink($linknr, $_SERVER['SCRIPT_URI']."?offset".$this->offsetaffix."=".$i.$para);
                        }
                        
                        if ((($i+1) == $this->offset && $i !=0) || ($i == $this->offset  && ($i !=0 && $i != ($this->seiten-1)) )|| (($i-1) == $this->offset && $i != ($this->seiten-1)))
                        { 
                         
                            $linknr = "";
                            if (($i+1) == $this->offset && $i !=0 && $i-1!=0) $linknr.=" ... ";
                            $i==$this->offset?$linknr .= "<b><big>":"";
                            $linknr .= "[".(1+$i)."]";
                            $i==$this->offset?$linknr .= "</big></b>":"";
                            if (($i-1) == $this->offset && $i !=0 && $i+1!=0) $linknr.=" ... ";
                        
                            $html .= $navilink->makeLink($linknr, $_SERVER['SCRIPT_URI']."?offset".$this->offsetaffix."=".$i.$para);
                        }
                    }
                }
                
                if ($this->offset < $this->seiten - 1) $html .= $navilink->makeLink("&nbsp;&raquo;&raquo;&raquo;", $_SERVER['SCRIPT_URI']."?offset".$this->offsetaffix."=".($this->offset+1).$para);
                else $html .= '<span class="browsersmooth">&nbsp;&raquo;&raquo;&raquo;</span>';
            }
        
            if ($version == "ext")
            {
                for ($i = 0; $i < $this->seiten; $i++)
                {
                    $html .= '<a href="'.$_SERVER['PHP_SELF'].'?offset'.$this->offsetaffix.'='.$i.'" class="navilink">[';
                    $html .= (1+$i*$this->anzahl).'-';
                    ($i+1)*$this->anzahl > $this->maxanzahl ? $html .= $this->maxanzahl : $html .= ($i+1)*$this->anzahl;
                    $html .= ']</a>&nbsp;';
                }
            }
            
            if ($version == "ext2")
            {
            }
            
            if ($version == "ext3")
            {
            }
            
            return $html;
        }  
    }
}
?>