

function getSelected(obj) 
{
    for (i = 0; i < obj.length; ++i)
    {
        if (obj.options[i].selected == true) return obj.options[i].value;
    }
    
    return false;
}


function addTextToElement(text, id)
{
    var element = document.getElementById(id);
    insertAtCursor(element, text);
    
    //element.value += text;
    //element.focus();
}


function swapShowElementById(id)
{
    var element = document.getElementById(id);
    
    element.style.display == "block" ? element.style.display = "none" : element.style.display = "block";
}


function setElementPosOnElement(objid, superobj, offsetx, offsety)
{
    document.getElementById(objid).style.left = parseInt(superobj.offsetLeft) + offsetx + "px";
    document.getElementById(objid).style.top  = parseInt(superobj.offsetTop) + offsety + "px";
}


function insertAtCursor(myField, myValue) 
{
    // Internet Explorer
    if (document.selection) 
    {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = myValue;
    }
    
    // Mozilla / Netscape
    else if (myField.selectionStart || myField.selectionStart == '0') 
    {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
                      + myValue
                      + myField.value.substring(endPos, myField.value.length);
    } 
    else 
    {
        myField.value += myValue;
    }
}    