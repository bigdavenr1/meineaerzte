/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Script:  addon.js (c)
//  Author:  Jens Franke
//  Date:    April 07
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// E R W E I T E R U N G S F U N K T I O N E N   F � R   J A V A S C R I P T
/////////////////////////////////////////////////////////////////////////////////////////////////////////////



Object.prototype.contains = function(obj) 
{
    for (i = 0; i < this.length; i++) 
    {
        if (this[i] === obj) return i;
    }

    return false;
};



function openPopup(purl, ptitle, pwidth, pheight)
{
    x = screen.availWidth / 2 - pwidth / 2;
    y = screen.availHeight / 2 - pheight / 2;
    
    var popupWindow = window.open(purl, ptitle, "width="+pwidth+", height="+pheight+", left="+x+", top="+y+", screenX="+x+", screenY="+y+", toolbar=0, scrollbars=0, location=0, statusbar=0, menubar=0, resizable=0");
}


function openPopupWindow(wurl, wtitle, wwidth, wheight, wparam)
{
    if (!wparam) wparam = "toolbar=1, scrollbars=1, location=1, statusbar=1, menubar=1, resizable=1";

    x = screen.availWidth / 2 - wwidth / 2;
    y = screen.availHeight / 2 - wheight / 2;
    
    var popupWindow = window.open(wurl, wtitle, "width="+wwidth+", height="+wheight+", left="+x+", top="+y+", screenX="+x+", screenY="+y+", "+wparam);
}


function getMouseXY(e)
{
    if(window.ActiveXObject)
    {
        mouseX = window.event.x + document.body.scrollLeft;
        mouseY = window.event.y + document.body.scrollTop;
    }
    else
    {
        mouseX = e.pageX;
        mouseY = e.pageY;
    }
}


function createXmlHttpRequestObject()
{
    var xmlHttp;

    try
    {
        xmlHttp = new XMLHttpRequest();
        
    }
    catch(e)
    {
        var XmlHttpVersions = new Array("MSXML2.XMLHTTP.6.0",
                                        "MSXML2.XMLHTTP.5.0",
                                        "MSXML2.XMLHTTP.4.0",
                                        "MSXML2.XMLHTTP.3.0",
                                        "MSXML2.XMLHTTP",
                                        "Microsoft.XMLHTTP");
                                    
        for (var i = 0; i < XmlHttpVersions.length && !xmlHttp; i++)
        {
            try
            {
                xmlHttp = new ActiveXObject(XmlHttpVersions[i]);
            }
            catch (e) 
            {
                //alert(e + XmlHttpVersions[i]);
            }
        }
    }
    
    if (!xmlHttp) alert("Sorry, das XmlHttpRequestObject konnte nicht angelegt werden.");
    else return xmlHttp;
}


function LTrim(value) 
{
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");
}


function RTrim(value) 
{
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
}


function trim(value)
{
	return LTrim(RTrim(value));
}


function getSelected(obj) 
{
    for (i = 0; i < obj.length; ++i)
    {
        if (obj.options[i].selected == true) return obj.options[i].value;
    }
    
    return false;
}


function addTextToElement(text, id)
{
    var element = document.getElementById(id);
    insertAtCursor(element, text);
    element.focus();
}


function swapShowElementById(id)
{
    var element = document.getElementById(id);
    element.style.position = "absolute";
    element.style.display == "block" ? element.style.display = "none" : element.style.display = "block";
    
}


function ShowElementById(id)
{
    var element = document.getElementById(id);
    
    element.style.display = "block";
}


function HideElementById(id)
{
    var element = document.getElementById(id);
    
    element.style.display = "none";
}


function getX(id) {
    var e = document.getElementById ? document.getElementById(id) : document.all ? document.all[id] : document.layers[id];
    //var e = jahiaGetObject(id);
    x = getPageOffsetLeft(e);
    // adjust position for IE
    if (browser.isIE) {
        x += e.offsetParent.clientLeft;
    }
    return x;
}

function setElementPosOnElement(objid, superobj, offsetx, offsety)
{
    // navigator.appName
    //alert("->" + getX(superobj.id));
    //alert("->" + superobj.offsetTop);
    document.getElementById(objid).style.left = parseInt(superobj.offsetLeft) + offsetx + "px";
    document.getElementById(objid).style.top  = parseInt(superobj.offsetTop) + offsety + "px";
}

function setElementPosOnElementFriend(objid, superobj, offsetx, offsety)
{
    // navigator.appName
    //alert("->" + getX(superobj.id));
    //alert("->" + superobj.offsetTop);
    if (navigator.appName == "Netscape")
    {
        document.getElementById(objid).style.left = parseInt(superobj.offsetLeft) + offsetx + "px";
        document.getElementById(objid).style.top  = parseInt(superobj.offsetTop) + offsety + "px";
    }
    else
    {
        document.getElementById(objid).style.left=0 +"px";
        document.getElementById(objid).style.top=-60 +"px";
        //document.getElementById(objid).style.position="absolute";
        //alert (document.getElementById(objid).style.top);
        //alert (document.getElementById(objid).style.left);
        //alert (document.getElementById(objid).style.position);
    }
}


function insertAtCursor(myField, myValue) 
{
    // Internet Explorer
    if (document.selection) 
    {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = myValue;
    }
    
    // Mozilla / Netscape
    else if (myField.selectionStart || myField.selectionStart == '0') 
    {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
                      + myValue
                      + myField.value.substring(endPos, myField.value.length);
    } 
    else 
    {
        myField.value += myValue;
    }
} 