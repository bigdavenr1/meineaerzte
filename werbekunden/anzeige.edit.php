<?php
////////////////////////////////////////////////////////////////////////////////
// werbekunden/anzeige.edit.php zeigt das Formular zur �nderung von Werbeanzeigen
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");
?>
<script src="<?php echo WEBDIR;?>script/addon.js" language="javascript" type="text/javascript"></script>
<?php

$kundenfac = new Benutzer();
$kundenfac->getWerbekundenById($_SESSION['user']->id);

if($kunde = $kundenfac->getElement())
{
    ?>
    <h1>Anzeige <?php  if($_GET['mode'] == 'update') echo 'bearbeiten'; else echo 'erstellen'; ?></h1>
    <?php if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}?>
    <br />
    
    <form action="<?php echo $l->makeFormLink(WEBDIR.'werbekunden/anzeige.update.php?mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>
                Anzeige Daten
            </legend>
            <?php
            $anzeigefac = new Anzeige();
            $anzeigefac->getById($_GET['id']);
            $anzeige = $anzeigefac->getElement();
            //if (is_array(unserialize($anzeige->fachgebiete))) $fachgebiete=unserialize($anzeige->fachgebiete);
            //else $fachgebiete=array();
            if (is_array(unserialize($anzeige->suchwort))) $suchworte=unserialize($anzeige->suchwort);
            else $suchworte=array();
            if (is_array(unserialize($anzeige->bild))) $bilder=unserialize($anzeige->bild);
            else $bilder=array();
            ?>
            <input type="hidden" name="werbeid" value="<?php echo $_SESSION['user']->id; ?>" />
            <label for="anzeigefirmname">
                Anzeige Firmenname
            </label>
            <input id="anzeigefirmname" name="anzeigefirmname" type="text" value="<?php echo $anzeige->firmname;?>"/>
             <br class="clr" /><br />
            <label for="anzeigetitel">
                Anzeige Titel
            </label>
            <input id="anzeigetitel" name="anzeigetitel" type="text" value="<?php echo $anzeige->titel;?>"/>
            <br class="clr" /><br />
            <label for="name">
                Anzeige Nachname
            </label>
            <input type="text" name="name" id="name" value="<?php echo $anzeige->name;?>" />
            <br class="clr" />
            <label for="vorname">
                Anzeige Vorname
            </label>
            <input type="text" id="vorname" name="vorname" value="<?php echo $anzeige->vorname;?>" />
            <br class="clr" /><br/>
            <label for="strasse">
                Anzeige Strasse
            </label>
            <input type="text" id="strasse" name="strasse" value="<?php echo $anzeige->strasse;?>" />
            <br class="clr" />
            <label for="plz">
                Anzeige PLZ / Ort
            </label>
            <input type="text" id="plz" name="plz" value="<?php echo $anzeige->plz;?>" class="plz"/>
            <input type="text" id="ort" name="ort" value="<?php echo $anzeige->ort;?>" class="ort" alt="ort"/>
            <br class="clr" />
            <label for="land">
                Anzeige Land
            </label>
            <input type="text" id="land" name="land" value="<?php echo $anzeige->land;?>" />
            <br class="clr" /><br/>
            <label for="tel">
                Anzeige Telefon
            </label>
            <input type="text" id="tel" name="tel" value="<?php echo $anzeige->tel;?>" />
            <br class="clr" />
            <label for="fax">
                Anzeige Fax
            </label>
            <input type="text" id="fax" name="fax" value="<?php echo $anzeige->fax;?>" />
            <br class="clr" /><br/>
             <label for="homepage">
                Anzeige Homepage
            </label>
            <input type="text" id="homepage" name="homepage" value="<?php echo $anzeige->homepage;?>" />
            <label for="area">
                Anzeige Text
            </label>
            <?php $htmlarea = new HtmlArea("area");
            echo $htmlarea->getHtmlButtons();?>
            <textarea name="text" cols="0" rows="0" id="area"><?php echo $anzeige->text;?></textarea>
       </fieldset>
            <br class="clr" /><br />
       <fieldset>
            <legend>
                Anzeige Profilbilder
            </legend>
            <?php
            if(sizeof($bilder)==0) echo 'Kein Anzeige Profilbild hochgeladen';
            
            else 
            {
                for ($x=0; $x<sizeof($bilder);$x++)
                {
                    // Grafiktyp auslesen
                    $typ=getimagesize(LOCALDIR."images/werbung/".$bilder[$x]['url']) ;
          
                    // je nach typ Endung bestimmen   
                    if ($typ[2]==1) $endung=".gif";
                    if ($typ[2]==2) $endung=".jpg"; 
                
                    // Reinnamen ohne Endung
                    $name=basename($bilder[$x]['url'],$endung);
                
                    // Gr��e des Thumbnails auslesen
                    $size=getimagesize(LOCALDIR."images/werbung/".$name."_thumb".$endung);
          
                    // HTML Bild schreiben 
                    ?>
                    <div <?php if ($x%2) echo 'class="td1"';?>>
                    <img src="<?php echo WEBDIR;?>images/werbung/<?php echo $name;?>_thumb<?php echo $endung;?>" height="<?php echo $size[1]; ?>" width="<?php echo $size[0]; ?>" alt="Bild von <?php echo $kunde->name." ".$kunde->vorname?>"/>
                    <input type="checkbox" name="bildloeschen<?php echo $x;?>" class="selectbox"> Bild l�schen  <br class="clr"/>
                    <label>Bildkommentar</label>
                    <input type="text" name="bildkommentar[<?php echo $x;?>]" id="bildkommentar" value="<?php echo $bilder[$x]['bildkommentar'];?>"/>
                    </div> 
            <?php
                }
            }
            ?>
            <br class="clr"/><br/><hr/><br/>
            
            <?php if (sizeof($bilder)<5) { ?>
            <label for="bildneu">
                weiteres Bild hinzuf�gen:
            </label>
            <input id="bildneu" name="bildneu" type="file" /><br class="clr" />
            <label for="bildkommentarneu">
                Bildkommentar
            </label>
            <input id="bildkommentarneu" name="bildkommentarneu" type="text" />
            <?php } ?>
       </fieldset>
            <br class="clr" /><br />
       <fieldset>
            <legend >
                Anzeige Fachgebiet
            </legend>
            <input type="text" id="anzeigefachgebiete" name="anzeigefachgebiete" value="<?php echo $anzeige->fachgebiete;?>" />

            <?php
                /*$kategoriefac1 = new Fach();
                $kategoriefac1->getAll();
                while ($kategorien = $kategoriefac1->getElement())
                {
                    $checked="";
                    if (in_array($kategorien->id,$fachgebiete)) $checked='checked="checked"';
                    echo '<div style="width:190px;float:left"><input type="checkbox" class="selectbox" '.$checked.' name="anzeigefachgebiete['.$kategorien->id.']" value="'.$kategorien->id.'"/>'.$kategorien->name.'</div>';
                }*/
            ?>
            <br class="clr"/>
       </fieldset>
            <br class="clr" /><br />
       <fieldset>
            <legend>
                Anzeige Suchbegriffe
            </legend>
            <?php
                for($x=0;$x<6;$x++)
                {
                    echo '<input type="text" name="suchworte[]" value="'.$suchworte[$x].'"/>';
                }
            ?>
            <br class="clr"/>
        </fieldset>
            <br class="clr" /><br />
        <fieldset>
           <?php 
           $ordzeitenfac = new Anzeigeordzeiten();
           $ordzeitenfac->getByAnzeigeId($_GET['id']);
            ?>
            <legend>
               Anzeige - �ffnungszeitenformular
            </legend>
            <table ><tr><th>Tage</th><th colspan="2">Vormittags</th><th colspan="2">Nachmittags</th><th>Ordination/Vereinbarung</th></tr>
            <?php
            for($i=1;$i<8;$i++)
            {
            ?>
             <tr><td <?php if ($i%2) echo 'class="td1"'; ?> >
                <label for="<?php echo 'day'.$i ?>" ><?php echo $ordzeitenfac->parseDay($i,"long"); ?></label>
                <?php 
                    $ordzeitenfac->getByAnzeigeIdAndDay($_GET['id'],$i); // Datens�tz von Firma und Tag hohlen ORDER by id ASC
                    $counttimes = $ordzeitenfac->getElementCount(); // Anzahl der Datens�tze festhalten
                    $ordzeit = $ordzeitenfac->getElement();
                ?>
                <input type="checkbox" id="<?php echo 'day'.$i; ?>" name="<?php echo 'day'.$i; ?>" value="<?php echo $ordzeitenfac->parseDay($i,"long"); ?>" class="selectbox"  <?php if($ordzeit) echo "checked='checked'"?>/></td>
               <td <?php if ($i%2) echo 'class="td1"'; ?> >
                  von<select id="<?php echo 'time1'.$i ?>" name="<?php echo 'time1'.$i ?>" style="width:75px">
                  <option value =''>---</option>
                  <?php  
                      for($j=1;$j<50;$j++)
                      {
                             
                          unset($teststring); 
                          if($ordzeit->timestart == $j) $teststring = "selected='selected'"; 
                          else $teststring = "";  
                          echo "<option value='".$j."'".$teststring." >".$ordzeitenfac->parseTime($j)."</option>";
                      }  
                   ?>
                  </select>
                </td>
                <td <?php if ($i%2) echo 'class="td1"'; ?> >
                    bis<select id="<?php echo 'time2'.$i ?>" name="<?php echo 'time2'.$i ?>" style="width:75px">
                    <option value =''>---</option>  
                      <?php  
                          for($k=1;$k<50;$k++)
                          {   
                              unset($teststring); 
                              if($ordzeit->timeend == $k) $teststring = "selected='selected'"; 
                              else $teststring = "";  
                              echo "<option value='".$k."'".$teststring." >".$ordzeitenfac->parseTime($k)."</option>";
                          }  
                       ?>
                      </select>
                 </td>
                 <td <?php if ($i%2) echo 'class="td1"'; ?> >
                    von<select id="<?php echo 'time3'.$i ?>" name="<?php echo 'time3'.$i ?>" style="width:75px">
                    <option value =''>---</option>
                    <?php  
                        if($counttimes > 1) // zweiten datensatz holen
                            {
                                $ordzeit = $ordzeitenfac->getElement();
                            }
                        for($m=1;$m<50;$m++)
                        {
                            unset($teststring);
                            if($counttimes > 1)
                            {
                                if($ordzeit->timestart == $m) $teststring = "selected='selected'"; 
                                else $teststring = "";
                            }
                            echo "<option value='".$m."'".$teststring." >".$ordzeitenfac->parseTime($m)."</option>";
                        }  
                     ?>
                  </select>
                 </td>
                 <td <?php if ($i%2) echo 'class="td1"'; ?> >
                    bis<select id="<?php echo 'time4'.$i ?>" name="<?php echo 'time4'.$i ?>" style="width:75px">
                    <option value =''>---</option>
                    <?php  
                        for($n=1;$n<50;$n++)
                        {   
                            unset($teststring);
                            if($counttimes > 1)
                            {
                                if($ordzeit->timeend == $n) $teststring = "selected='selected'"; 
                                else $teststring = "";
                            }    
                            echo "<option value='".$n."'".$teststring." >".$ordzeitenfac->parseTime($n)."</option>";
                        }  
                     ?>
                    </select>
                 </td>
                 <td <?php if ($i%2) echo 'class="td1"'; ?>>
                    <input type="text" id="<?php echo 'memo'.$i; ?>" name="<?php echo 'memo'.$i; ?>" value="<?php echo $ordzeit->memo; ?>" />
                 </td>
              </tr>
              <?php
              }// end Forschleife f�r Tage
           ?>  
           </table>
         </fieldset>
         <br class="clr"/><br/>
        <?php echo $l->makeLink("Zur�ck zur �bersicht", "./view.php", "backlink"); ?><input type="submit" value="Daten speichern" />
        <br class="clr"/><br/>
 </form>
    <?php
}
else
{?>
    <h1>Fehler</h1><br/> Sie sind nicht als Werbekunde eingeloggt, bzw. Ihre Daten wurden nicht gefunden.
<?php   
}

    include(INCLUDEDIR."footer.inc.php");
 // end else zu if($stopp == "ENDE")
?>