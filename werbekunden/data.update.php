<?php
////////////////////////////////////////////////////////////////////////////////
// werbekunden/data.update.php regelt das update der benutzer Tabelle
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");

unset($_SESSION['err']);
//echo "<pre>"; print_r($_POST); print_r($_GET); echo "</pre>";

// Daten pr�fen


$kundenfac = new Benutzer();
$kundenfac->getWerbekundenById($_SESSION['user']->id);
if($kunde = $kundenfac->getElement())
{
     if($kunde->aktiv != '1') $_SESSION['err'] .= "Kein aktiver Werbepartner!<br />"; 
}
else $_SESSION['err'] .= "Kein Werbepartner!<br />"; 

// Fehlerpr�fung OHNE Email-Dopplungspr�fung
if (!$_POST["name"]) $_SESSION['err'] .= "Sie haben das Feld 'Name' vergessen<br />";
if (!$_POST["vorname"]) $_SESSION['err'] .= "Sie haben das Feld 'Vorname' vergessen<br />";
if (!$_POST["strasse"]) $_SESSION['err'] .= "Sie haben das Feld 'Strasse' vergessen<br />";
if (!$_POST["plz"]) $_SESSION['err'] .= "Sie haben das Feld 'PLZ' vergessen<br />";
if (!$_POST["ort"]) $_SESSION['err'] .= "Sie haben das Feld 'Ort' vergessen<br />";
if (!$_POST["land"]) $_SESSION['err'] .= "Sie haben das Feld 'Land' vergessen<br />";
if (!$_POST["tel"]) $_SESSION['err'] .= "Sie haben das Feld 'Telefon' vergessen<br />";   

if ($_POST["passwd"])
{   
    if($_POST["passwd"] != $_POST["repass"])
    {
        $_SESSION['err'] .= "Ihr neues Passwort stimmt nicht mit der Passwortwiederholung �berein<br />";
    }
}


if(!$_SESSION['err'])
{
    // Update der Logintabelle
    $kundenfac->update("`pass`='".md5($_POST['passwd'].$salt)."' , 
                        `salt`='".$salt."' , 
                        `anrede`='".$_POST['anrede']."' , 
                        `titel`='".$_POST['titel']."',
                        `name`='".$_POST['name']."',
                        `vorname`='".$_POST['vorname']."',
                        `adresse`='".$_POST['strasse']."', 
                        `plz`='".$_POST['plz']."',
                        `ort`='".$_POST['ort']."',
                        `land`='".$_POST['land']."',
                        `tel`='".$_POST['tel']."',
                        `fax`='".$_POST['fax']."',
                        `homepage`='".$_POST['homepage']."',
                        `nickname`='".$_POST['passwd']."'");
    
    $_SESSION['msg'] .= "Pers�hliche Daten wurden gespeichert!<br />";
    header("Location:./view.php?sv=1");                   
}
else
{
    header("Location:./data.edit.php?sv=1");
}

?>