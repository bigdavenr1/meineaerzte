<?php
////////////////////////////////////////////////////////////////////////////////
// werbekunden/banner.update.php regelt das update der anzeigebanner Tabelle
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."std/email.class.php");

//echo "<pre>"; print_r($_POST); print_r($_GET); echo "</pre>";

// Daten pr�fen
unset($_SESSION['err']);

if (!$_GET['mode']) $_SESSION['err'] .= "Aufruffehler!<br />";

$kundenfac = new Benutzer();
$kundenfac->getWerbekundenById($_SESSION['user']->id);
if($kunde = $kundenfac->getElement())
{
     if($kunde->aktiv != '1') $_SESSION['err'] .= "Kein aktiver Werbepartner!<br />"; 
}
else $_SESSION['err'] .= "Kein Werbepartner!<br />"; 

// Bild pr�fen

if ($_FILES['bildneu'] != "" && $_FILES['bildneu']['name'] != "" )// Bild ausgew�hlt
{                        
    $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);

    // �berpr�fung des Bildtyps
    if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
    {
       // hier nicht hochladen --- erst sp�ter schreiben
    }
    //Wenn falscher Bildtyp 
    else 
    {    
        $_SESSION['err'] .= "[Bild -- hat falschen Typ]<br/ >";
    }
}

// neuer Datensatz muss Bild ausw�hlen und immer ist Homepage n�tig
if($_FILES['bildneu']['name'] == "" && $_GET['mode']=="new") $_SESSION['err'] .= "Kein Bild ausgew�hlt!<br/ >";
if($_POST['homepage'] == "") $_SESSION['err'] .= "Verlinkte Webseite ist n�tig!<br/ >";


// Bei Fehler zur�ck
if($_SESSION['err'])
{
    if($_GET['mode']=='update') header("Location:./banner.edit.php?mode=update&id=".$_GET['id']."&sv=1");
    elseif($_GET['mode']=='new') header("Location:./banner.edit.php?mode=new&sv=1");
    else header("Location:./view.php?sv=1");
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Neuer Datensatz
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="new" && !$_SESSION['err'])
{
    // neuer Datensatz in Tabelle anzeigebanner
    
    // neues Anzeigeprofil schreiben
    $anzeigebannerfac = new Anzeigebanner();
        
    if ($_FILES['bildneu']['name'] != "")// Bild ausgew�hlt
    {                        
        $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);
    
        // �berpr�fung des Bildtyps
        if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
        {
            if ($filetyp[2]==2) $type = '.jpg';
            if ($filetyp[2]==1) $type = '.gif';
      		                
            // neues Bild hochladen
            $key=generateRandomKey(10);
       	    $pic = $anzeigebannerfac->writePic($_FILES['bildneu']["tmp_name"],$key,$type);
            $bildname = $key.$type;
        }
    }
    else $bildname = '';
        
    // bannerarray
    $bannerdata[] = "";                                                    // id
    $bannerdata[] = $_SESSION['user']->id;                                 // werbeid
    $bannerdata[] = 0;                                                     // status 0 = inaktiv
    $bannerdata[] = $bildname;                                             // bild 
    $bannerdata[] = $_POST['homepage'];                                    // homepage
    $bannerdata[] = "";                                                    // show
    $bannerdata[] = "";                                                    // klick
    $bannerdata[] = 2;                                                     // bannerkey    2 = 33%  
    $anzeigebannerfac->write($bannerdata);
    
    $_SESSION['msg'] .= 'Werbebanner erfolgreich geschrieben!<br />';
}// if ($_GET['mode']=="new" && !$_SESSION['err'])

//////////////////////////////////////////////////////////////////////////////////////////////////
// Datens�tze �ndern
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET['mode']=="update" && !$_SESSION['err'])
{
    // datensatz einlesen
    // Update des Anzeigebanner 
    $anzeigebannerfac = new Anzeigebanner();
    $anzeigebannerfac->getById($_GET["id"]);
    $daten = $anzeigebannerfac->getElement();
    
    // eventuell altes Bild l�schen
    if($_FILES['bildneu']['name'] != '' && $daten->bild !='')
    {
        // Grafiktyp auslesen
        $typ=getimagesize(LOCALDIR."images/werbung/".$daten->bild) ;
  
        // je nach typ Endung bestimmen   
        if ($typ[2]==1) $endung=".gif";
        if ($typ[2]==2) $endung=".jpg"; 
        
        // Bild und Thumb l�schen
   	    unlink(LOCALDIR."images/werbung/".$daten->bild);
        unlink(LOCALDIR."images/werbung/".basename($daten->bild,$endung)."_half".$endung);
    }  
    
    // neues Bild hochladen     
    if ($_FILES['bildneu']['name'] != "") // Bild ausgew�hlt
    {                        
        // �berpr�fung des Bildtyps
        $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);
 
        // �berpr�fung des Bildtyps
        if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
        {
            if ($filetyp[2]==2) $type = '.jpg';
            if ($filetyp[2]==1) $type = '.gif';
                   
            // neues Bild hochladen
            $key=generateRandomKey(10);
            $pic = $anzeigebannerfac->writePic($_FILES['bildneu']["tmp_name"],$key,$type);
  	        $bildname = $key.$type;
            
            // Datensatz updaten
            $anzeigebannerfac->update(" `homepage`='".$_POST['homepage']."', 
                                        `bild`='".$bildname."'"); 
        }
    }
    else $anzeigebannerfac->update(" `homepage`='".$_POST['homepage']."'"); // Datensatz updaten ohne Bild
    
    // eMail an Langer versenden
    $msg = "Hallo Herr Langer".chr(10);
    $msg .= chr(10);
    $msg .= "Die Banner mit ID:".$daten->id.chr(10);
    $msg .= " des Werbekunden mit der ID: ".$_SESSION['user']->id." wurde am ".date("d.m.Y",time())." ge�ndert!".chr(10).chr(10);
    
    $emailfac = new Email(1);
    $emailfac->setFrom($CONST_MAIL['from']);
    $emailfac->setTo($CONST_MAIL['an']);
    $emailfac->setSubject("Banner ge�ndert");
    $emailfac->setContent($msg);
    //echo nl2br($msg);
    $emailfac->sendMail();
    // Ende Email an Langer
         
    $_SESSION['msg'] .= 'Werbebanner erfolgreich ge�ndert!<br />';
} // end if ($_GET['mode']=="update" && !$_SESSION['err'])

    
if(!$_SESSION['err']) header("Location:./view.php?sv=1");
?>
