<?php
////////////////////////////////////////////////////////////////////////////////
// werbekunden/banner.edit.php zeigt das Formular zur �nderung von Werbebannern
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

unset($stopp);

$kundenfac = new Benutzer();
$kundenfac->getWerbekundenById($_SESSION['user']->id);

if(!$kunde = $kundenfac->getElement())
{
    $_SESSION['err'] = '<br/>Der aufgerufene Werbepartner existiert nicht!<br />';
    $stopp = "ENDE";
}

if($stopp != 'ENDE')
{
    ?>
    <h1>Banner <?php  if($_GET['mode'] == 'update') echo 'bearbeiten'; else echo 'erstellen'; ?></h1>
    <?php if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}?>
    <br />
    
    <form action="<?php echo $l->makeFormLink(WEBDIR.'werbekunden/banner.update.php?mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>
                Banner Daten
            </legend>
            <?php
            $anzeigebannerfac = new Anzeigebanner();
            $anzeigebannerfac->getById($_GET['id']);
            $anzeigebanner = $anzeigebannerfac->getElement();
            ?>
            <label for="homepage">
                Ziel-Webadresse (nach http://)
            </label>
            <input type="text" id="homepage" name="homepage" value="<?php echo $anzeigebanner->homepage;?>" />
            <br /><br />
            <?php
            if($anzeigebanner->bild=='')
            {
                echo 'Kein Bannerbild hochgeladen';
            }
            else 
            {
                // Grafiktyp auslesen
                $typ=getimagesize(LOCALDIR."images/werbung/".$anzeigebanner->bild) ;
      
                // je nach typ Endung bestimmen   
                if ($typ[2]==1) $endung=".gif";
                if ($typ[2]==2) $endung=".jpg"; 
            
                // Reinnamen ohne Endung
                $name=basename($anzeigebanner->bild,$endung);
            
                // Gr��e des Thumbnails auslesen
                $size=getimagesize(LOCALDIR."images/werbung/".$name."_half".$endung);
      
                // HTML Bild schreiben 
                ?>
                <div <?php if ($x%2) echo 'class="td1"';?>>
                <img src="<?php echo WEBDIR;?>images/werbung/<?php echo $name;?>_half<?php echo $endung;?>" height="<?php echo $size[1]; ?>" width="<?php echo $size[0]; ?>" alt="Bild von <?php echo $_SESSION['user']->name." ".$_SESSION['user']->vorname?>"/>
                </div> 
            <?php
            }
            ?>
            <br class="clr"/><br/><hr/><br/>
            
            <label for="bildneu">
                Neues Bild raussuchen:(altes Bild wird gel�scht)
            </label>
            <input id="bildneu" name="bildneu" type="file" /><br class="clr" />
         </fieldset>
         <br class="clr"/><br/>
        <?php echo $l->makeLink("Zur�ck zur �bersicht", "./view.php", "backlink"); ?><input type="submit" value="Daten speichern" />
        <br class="clr"/><br/>
 </form>
    <?php
    include(INCLUDEDIR."footer.inc.php");
} // end else zu if($stopp == "ENDE")
?>