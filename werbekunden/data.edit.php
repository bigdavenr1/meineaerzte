<?php
////////////////////////////////////////////////////////////////////////////////
// werbekunden/data/edit.php zeigt das Formular zur �nderung von Werbekunden an
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

unset($stopp);


$kundenfac = new Benutzer();
$kundenfac->getWerbekundenById($_SESSION['user']->id);

    
if(!$kunde = $kundenfac->getElement())
{
    $_SESSION['err'] = '<br/>Der aufgerufene Werbepartner existiert nicht!<br />';
    $stopp = "ENDE";
}


if($stopp == "ENDE")
{
    echo '<h1>Fehler</h1>';
    if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}
    echo $l->makeLink("Zur�ck zur �bersicht", "./view.php", "backlink");
    include(INCLUDEDIR."footer.inc.php");
}
else    // weitermachen
{
    ?>
    <h1>Werbekundenseintrag bearbeiten</h1>
    <?php if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}?>
    <br />
    
    <form action="<?php echo $l->makeFormLink(WEBDIR.'werbekunden/data.update.php?mode='.$_GET['mode'].'&amp;id='.$_GET['id']);?>" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>
                Werbekundendaten
            </legend>
            <label for="titel">
                Titel
            </label>
            <input type="text" name="titel" id="titel" value="<?php echo $kunde->titel;?>" />
            <br class="clr" />
            <label for="anrede">
                Anrede
            </label>
            <select name="anrede" id="anrede">
                <option value="Herr" <?php if($kunde->anrede == "Herr")echo 'selected='.'"selected"'; ?>>Herr</option>
                <option value="Frau" <?php if($kunde->anrede == "Frau")echo 'selected='.'"selected"'; ?>>Frau</option>
            </select>
            <br class="clr" />
            
            <label for="name">
                Nachname
            </label>
            <input type="text" name="name" id="name" value="<?php echo $kunde->name;?>" />
            <br class="clr" />
            <label for="vorname">
                Vorname
            </label>
            <input type="text" id="vorname" name="vorname" value="<?php echo $kunde->vorname;?>" />
            <br class="clr" /><br/>
            <label for="strasse">
                Strasse
            </label>
            <input type="text" id="strasse" name="strasse" value="<?php echo $kunde->strasse;?>" />
            <br class="clr" />
            <label for="plz">
                PLZ / Ort
            </label>
            <input type="text" id="plz" name="plz" value="<?php echo $kunde->plz;?>" class="plz"/>
            <input type="text" id="ort" name="ort" value="<?php echo $kunde->ort;?>" class="ort" alt="ort"/>
            <br class="clr" />
            <label for="land">
                Land
            </label>
            <input type="text" id="land" name="land" value="<?php echo $kunde->land;?>" />
            <br class="clr" /><br/>
            <label for="tel">
                Telefon
            </label>
            <input type="text" id="tel" name="tel" value="<?php echo $kunde->tel;?>" />
            <br class="clr" />
            <label for="fax">
                Fax
            </label>
            <input type="text" id="fax" name="fax" value="<?php echo $kunde->fax;?>" />
            <br class="clr" /><br/>
             <label for="homepage">
                Homepage
            </label>
            <input type="text" id="homepage" name="homepage" value="<?php echo $kunde->homepage;?>" />
            <br class="clr" /><br/>
             <label for="passwd">
                Passwort
            </label>
            <input type="password" id="passwd" name="passwd" value="" />
            <br class="clr" /><br/>
             <label for="repass">
                Passwort wiederholen
            </label>
            <input type="password" id="repass" name="repass" value="" />
            <br class="clr" /><br/>
        </fieldset>
        <br class="clr" /><br/>
        <br class="clr" /><br />
         <br class="clr"/><br/>
        <?php echo $l->makeLink("Zur�ck zur �bersicht", "./view.php", "backlink"); ?><input type="submit" value="Daten speichern" />
        <br class="clr"/><br/>
 </form>
    <?php
    include(INCLUDEDIR."footer.inc.php");
} // end else zu if($stopp == "ENDE")
?>

