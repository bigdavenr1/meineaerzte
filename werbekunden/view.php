<?php
////////////////////////////////////////////////////////////////////////////////
// werbekunden/view.php - Zeigt das Tupel aus Tabelle anzeigen an
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

if($_SESSION['user']->typ == 'werbung')
{
    if($_SESSION['user']->aktiv != 1)
    {
        echo 'Ihr Werbeprofil ist nicht aktiv geschaltet!<br />';
        break;
    }
    $anzeigefac = new Anzeige();
    $anzeigefac->getByWerbeid($_SESSION['user']->id);

?>
    <h1>Anzeigen und Banner Statistik</h1>
    <?php if ($_SESSION['msg']){ echo '<span class="msg">'.$_SESSION['msg'].'</span><br/>'; unset($_SESSION['msg']);}?>

    <br class="clr" /><br />
    
    <h2>Ihre Anzeigen</h2>
    <table style="width:100%;">
        <tr>
            <th >
                Anzeige-Id
            </th>
            <th>
                Anzeige-Status
            </th>
            <th >
                Gezeigt
            </th>
            <th>
                Geklickt
            </th>
            <th >
                Links
            </th>
        </tr>    
<?php 
    $x=0;
    while($anzeige = $anzeigefac->getElement())
    {
    $x++;
    ?>
        <tr >
           <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
               <?php echo $anzeige->id;?>
           </td>
           <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
               <?php if ($anzeige->status=="0") echo "<i>inaktiv</i>"; elseif ($anzeige->status=='1') echo '<b style="color:green">aktiv</b>'; else echo "<b>unbekannt</b>" ;?>
           </td>
           <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
                <?php  echo $anzeige->show ;?>
           </td>
           <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
                <?php  echo $anzeige->klick ;?>
           </td>
           <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
               <?php echo $l->makeLink($icon_edit_small,WEBDIR."werbekunden/anzeige.edit.php?mode=update&amp;id=".$anzeige->id);?>
           </td>
       </tr>
    <?php 
    }
    ?>
    </table>
    
    <br />
    <?php
    if($x==0)
    {
        echo 'Noch keine Anzeige angelegt!<br />';
    }
    echo $l->makeLink($icon_info." <b>[ Anzeige erstellen ]</b>",WEBDIR."werbekunden/anzeige.edit.php?mode=new");

    ?>
    <br />
    <h2>Ihre Werbebanner</h2>
    <br/>
    <table style="width:100%;">
        <tr>
            <th >
                Banner-Id
            </th>
            <th>
                Banner-Status
            </th>
            <th >
                Gezeigt
            </th>
            <th>
                Geklickt
            </th>
            <th >
                Links
            </th>
        </tr>   
    <?php
    $anzeigebannerfac = new Anzeigebanner();
    $anzeigebannerfac->getByWerbeid($_SESSION['user']->id);
    
    $x=0;
    while($anzeigebanner = $anzeigebannerfac->getElement())
    {
    $x++;
    ?>
        <tr >
           <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
               <?php echo $anzeigebanner->id;?>
           </td>
           <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
               <?php if ($anzeigebanner->status=="0") echo "<i>inaktiv</i>"; elseif ($anzeigebanner->status=='1') echo '<b style="color:green">aktiv</b>'; else echo "<b>unbekannt</b>" ;?>
           </td>
           <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
                <?php  echo $anzeigebanner->show ;?>
           </td>
           <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
                <?php  echo $anzeigebanner->klick ;?>
           </td>
           <td <?php if ($x%2) echo 'class="td1"'; ?> style="text-align:center">
               <?php echo $l->makeLink($icon_edit_small,WEBDIR."werbekunden/banner.edit.php?mode=update&amp;id=".$anzeigebanner->id);?>
           </td>
       </tr>
    <?php 
    }
    ?>
    </table>
    
    <?php    
    if($x==0)
    {
        echo 'Noch kein Banner angelegt!<br />';
    }
        echo $l->makeLink($icon_info." <b>[ Banner erstellen ]</b>",WEBDIR."werbekunden/banner.edit.php?mode=new");

    ?>
    <br />
    
<?php
}// end if($_SESSION['user']->typ == 'werbung'
else
{
    echo 'Nicht als Werbekunde eingeloggt!<br/>';
}
include(INCLUDEDIR."footer.inc.php");
?>