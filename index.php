<?php



//////////////////////////////////////////////////////////////////////////////////////////////////
// Startseite - Anzeige von News und neuesten Nutzern
//////////////////////////////////////////////////////////////////////////////////////////////////

include("inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<p align="center">Sehr geehrter <b>www.meineaerzte.at</b> Besucher,<br/>wir freuen uns, das Sie den Weg zu uns gefunden haben.<br/><br/>
Dieses Portal besch�ftigt sich mit drei Schwerpunkten:
</p>
<ul style="text-align:center">
    <li>
        <b>Suchen</b> - eine umfangreiche Suchfunktion (�sterreichweit nach Fachbereichen, PLZ, Ordinationszeiten, etc.)
    </li>
    <li>

        <b>Informieren</b> - ein Forum, f�r etwaige Diskussionen, Fragen und Antworten
    </li>
    <li>

        <b>Bewerten</b> - eine detailierte Bewertungsfunktion - Patienten geben Ihre Erfahrungen weiter.
    </li>
</ul>
<p align="center">
<b>Dies alles ist f�r Patienten vollkommen kostenfrei!</b><br/><br/>
Mehr �ber die Idee hinter diesem Portal finden Sie unter <?php echo $l->makeLink("&Auml;rztecheck",WEBDIR."bewertungen/aerztecheck.php"); ?>
<br/><br/>
Unter allen Besuchern, die sich bis ende August 2008 bei uns registrieren und aktiv an unserem Portal teilnehmen, verlosen wir attraktive Preise, die uns mit freundlicher Unterst�tzung bereit gestellt wurden!<br/>
<br/></p>
<div align="center">
<table border="0" width="100%" id="table1" bgcolor="#E6F9FD">
	<tr>
		<td width="50%" valign="top">
		<p align="center">1 Wohlf�hlgutschein f�r 2 Personen<br/>
		2 N�chte
		im wundersch�nen Bad Gastein<br/>
&nbsp;</p>
		<p align="center">Mit freundlicher Unterst�zung bereitgestellt vom<br/>
&nbsp;<?php echo $l->makeLink("Hotel Elisabethpark",WEBDIR."bewertungen/partner1.php");?>
		<br/><br/>
<br/>
		<img border="0" src="images/partner1logo.jpg" width="98" height="70" alt="Hotel Elisabethpark" /></p></td>
		<td width="50%" valign="top">
		<p align="center">3 Gutscheine f�r 2 Personen<br/>
		Ein Abenteuer im Hochseilgarten inkl. �bernachtung im Naturpark 
		S�lkt�ler</p>
		<p align="center">Mit freundlicher Unterst�zung bereitgestellt von<br/>
		&nbsp;<?php echo $l->makeLink("together",WEBDIR."bewertungen/partner2.php");?>
		<br/>
		<br/>
		<img border="0" src="images/partner2logo.jpg" width="130" height="100" alt="together" /><br/></p>
&nbsp;</td>
	</tr>
	<tr>
		<td align="center"><?php echo $l->makeLink("mehr Informationen",WEBDIR."bewertungen/partner1.php");?></td>
		<td align="center"><?php echo $l->makeLink("mehr Informationen",WEBDIR."bewertungen/partner2.php");?></td>
	</tr>
</table>
</div>

<?php
include(INCLUDEDIR."footer.inc.php");
?>