<?php
// bewertungen/schowanzeige.php - zeigt Anzeigedetails
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// Wenn eine ID �bergeben wurde
if ($_GET['id'] && is_numeric($_GET['id']))
{
    // Datensatz holen
    $anzeigefac = new Anzeige();
    $anzeigefac->getById(htmlentities(trim($_GET['id']))); 
    $anzeige= $anzeigefac->getElement();
        
    // Wenn Datensatz vorhanden und aktiv
    if($anzeige->status == "1")
    {   
        // Statistik aktualisieren
        $anzeigefac->update(" `klick`='".($anzeige->klick+1)."'");
    ?>
        <script type="text/javascript" src="<?php echo WEBDIR;?>script/prototype.js"></script>
        <script type="text/javascript" src="<?php echo WEBDIR;?>script/scriptaculous.js?load=effects,builder"></script>
        <script type="text/javascript" src="<?php echo WEBDIR;?>script/lightbox.js"></script>
        <h1>Werbung  Detailansicht - "<?php echo $anzeige->firmname.' '.$anzeige->titel.' '.$anzeige->name.', '.$anzeige->vorname ;?>"</h1>
        <div class="headline"><div class="right"><?php echo $l->makeLink("Werbeprofilfehler melden",WEBDIR."bewertungen/kontakt.php?typ=promo");?></div></div>
        <br />
        <div class="td1" style="padding:5px;border:1px dotted #000;">
            <div style="float:left;margin-right:10px;">
                <?php 
                // Bildinfos aus DB auslesen
                if (is_array(unserialize($anzeige->bild)))$bild=unserialize($anzeige->bild);
                else $bild=array();
    
                // pr�fen ob Bild vorhanden
                if (sizeof($bild)!=0)
                {
                    // Grafiktyp auslesen
                    $typ=getimagesize(LOCALDIR."images/werbung/".$bild[0]['url']) ;
                  
                    // je nach typ Endung bestimmen   
                    if ($typ[2]==1) $endung=".gif";
                    if ($typ[2]==2) $endung=".jpg"; 
                        
                    // Reinnamen ohne Endung
                    $name=basename($bild[0]['url'],$endung);
                        
                    // Gr��e des Thumbnails auslesen
                    $size=getimagesize(LOCALDIR."images/werbung/".$name."_thumb".$endung);
                        
                    // HTML Bild schreiben
                    echo $l->makeLink('<img src="'.WEBDIR.'images/werbung/'.$name.'_thumb'.$endung.'" width="'.$size[0].'" height="'.$size[1].'" style="border:1px dotted #000;float:left;margin-right:5px;" alt="" />',WEBDIR."images/werbung/".$name.$endung,"","_blank",'rel="lightbox" title="'.$bild[0]['bildkommentar'].'"');
                }
                // Wenn kein Bild vorhanden, dann no-image-Bild hinein
                else echo '<img src="'.WEBDIR.'images/nopicture.gif" width="125" height="125" alt="" style="border:1px dotted #000;float:left;margin-right:5px;" />';
                echo '<br class="clr"/>';
                
                
                // Wenn Gr��e des Bild-Arrays !=0
                if (sizeof($bild)!=0)
                {
                    // Bilder (Thumbnails) hochz�hlen
                    for ($x=1;$x<=(sizeof($bild)-1);$x++)
                    {
                        // Umbruch nach 2 Thumnails
                        if ($x==3)echo '<br class="clr"/>';
                        
                        // Grafiktyp auslesen
                        $typ=getimagesize(LOCALDIR."images/werbung/".$bild[$x]['url']) ;
                  
                        // je nach typ Endung bestimmen   
                        if ($typ[2]==1) $endung=".gif";
                        if ($typ[2]==2) $endung=".jpg"; 
                        
                        // Reinnamen ohne Endung
                        $name=basename($bild[$x]['url'],$endung);
                        
                        // Gr��e des Thumbnails auslesen
                        $size=getimagesize(LOCALDIR."images/werbung/".$name."_thumb".$endung);
                        
                        // HTML Bild schreiben
                        echo '<div style="width:50px;height:50px;border:1px dotted #000;float:left;margin:6px;">';
                        echo $l->makeLink('<img src="'.WEBDIR.'images/werbung/'.$name.'_thumb'.$endung.'" width="'.number_format(($size[0]/2.5),0,'','').'" height="'.number_format(($size[1]/2.5),0,'','').'" style="margin-top:'.number_format(((50-($size[1]/2.5))/2),0,'','').'px;margin-left:'.number_format(((50-($size[0]/2.5))/2),0,'','').'px" alt="'.$bild[$x]['bildkommentar'].'" title="'.$bild[$x]['bildkommentar'].'" />',WEBDIR."images/werbung/".$name.$endung,"","_blank",'rel="lightbox" title="'.$bild[$x]['bildkommentar'].'"');
                        echo '</div>';
                    }
                }
                // Einf�gen des Werbestempels?
                //echo '<img src="'.WEBDIR.'images/ungeprueft.gif" width="100" height="100" alt="geprueft (3K)" style="margin-top:-25px;"/>';
                ?>  
                
            </div>
               <div style="float:right;border-left:1px dotted #000;border-bottom:1px dotted #000;padding-left:5px;min-height:125px;width:200px;">
               <h1 style="margin-top:0px;">Werbung </h1>
                <?php
                // Wenn nicht zum Bewerten Freigegeben
                echo 'Dieser Eintrag ist Werbung !<br/><br/>';
                ?>
               
                </div>
         
                <div>
                    <b><?php echo $anzeige->firmname;?><br/><?php echo $anzeige->titel;?><br/><?php echo $anzeige->name;?>, <?php echo $anzeige->vorname;?>  </b><br/><br/>
                    <div style="float:left;width:160px;">
                        <?php echo $anzeige->strasse;?><br/>
                        <?php echo $anzeige->plz.' '.$anzeige->ort;?><br/><br/>
                        <?php echo $anzeige->land;?><br/>
                    </div>
                    <div style="width:160px;float:left;">
                        Tel: <?php echo $anzeige->tel;?><br/>
                        <?php if ($anzeige->fax!='') echo 'Fax: '.$anzeige->fax.'<br/>';?>
                        <br/>
                        <?php echo $anzeige->homepage;?>
                    </div><br class="clr" />
                    <div style="width:230px;margin-right:70px;float:left;// float:auto;">
                    <div class="headline">Fachgebiet</div>
                        <br class="clr" />
                      <?php 
                      // Fachgebieteausgeben
                      if ($anzeige->fachgebiete != '')
                      {
                        echo $anzeige->fachgebiete;
                      }
                       ?>
                </div>       
                <br class="clr" />
                <hr/><br/>
                <div><div class="headline">Werbung Ordinationszeiten</div>
                <table>
                
                <?php
                $t=0;
                for ($i=1;$i<8;$i++)
                {
                    $ordzeitenfac = new Anzeigeordzeiten();
                    $ordzeitenfac->getByAnzeigeIdAndDay($_GET['id'],$i);
                    $counttimes = $ordzeitenfac->getElementCount();
                    if ($ordzeit = $ordzeitenfac->getElement())
                    {
                        $t++;
                        echo "<tr>";
                        echo "<td>".$ordzeitenfac->parseDay($i,"long")."</td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timestart)."</td><td> - </td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timeend)."</td>".chr(10);
                        if ($counttimes>1)
                        {
                            $ordzeit = $ordzeitenfac->getElement();
                            echo "<td> und </td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timestart)."</td><td> - </td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timeend)."</td>";
                        }  
                        else echo '<td colspan="4"></td>';
                        echo "<td> - ".$ordzeit->memo."</tr>";
                        echo "</tr>";
                    }
                }
                if ($t==0) echo "<tr><td>Keine Werbung Ordinationszeiten hinterlegt</td></tr>";   
               ?>  
                </table>
                </div>
                
                <br/>
                
                <br class="clr"/>
                <?php 
                // Wenn Beschreibung da
                if ($anzeige->text!="") 
                {
                ?>
                <br/>
                <hr/>
                <br/>
                <u><b>Werbung Beschreibung</b></u><br/><br/>    
                <?php
                    // Beschreibung ausgeben 
                    echo nl2br($anzeige->text); 
                } 
                ?>
                </div>
                <br class="clr" />
                <br/>
               </div>
            
    <?php
    }
    // Wenn kein Datensatz gefunden wurde
    else 
    {
        echo "<h1>Werbung  nicht gefunden</h1>Die von Ihnen aufgerufene Werbung  existiert nicht<br /><br />";
    }
}

// Wenn keine ID �bertragen wurde
else
{
    echo "<h1>Werbung nicht gefunden</h1>Die von Ihnen aufgerufene Werbung existiert nicht<br /><br />";
}

include(INCLUDEDIR."footer.inc.php");
?>