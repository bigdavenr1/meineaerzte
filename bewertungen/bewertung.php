<?php
include("../inc/config.php");
include(INCLUDEDIR."std/email.class.php");
unset ($_SESSION['err']);


/*echo "<pre>"; print_r($_POST); print_r($_GET); print_r($_SESSION['user']);echo "</pre>";*/ 

// Wenn keine ID �bergeben wurde
if (!$_GET['id'])
{
    include(INCLUDEDIR."header.inc.php");
    echo '<h1>Bewertung eines Eintrages</h1><br/> Es ist ein Fehler aufgetreten. Bitte versuchen Sie es noch einmal';
}
else
{
    // Object initialisieren
    $datenfac = new Daten();
    $datenfac->getById($_GET['id']);
    
    // Wenn Datensatz gefunden
    if ($daten = $datenfac->getElement())
    {
        // wenn abgesendet wurde
        if ($_POST['Bewertungabsenden'] && $daten->bewertungsstatus=="A" )
        {
           // Bewertungsmerkmale holen
           $bewertungspunktfac=new Bewertungspunkt();
           $bewertungspunktfac->getBewertungWithKat();
           $katname=array();
    
           // Bewertungsmerkmale mit POST-Inhalt abgleichen
           while($bewertungspunkt= $bewertungspunktfac->getElement())
           {
               // Wenn Felder vergessen wurde
               if (!$_SESSION['bewertungspunkte'][$bewertungspunkt->id] ) $_SESSION['err']= "Sie m�ssen eine vollst�ndige Bewertung abgeben!<br/>";
               $bewertungspunkte[$bewertungspunkt->id]=$_SESSION['bewertungspunkte'][$bewertungspunkt->id];
           }
           
           // Wenn kein Fehler aufgetreten ist und das Formular abgesendet wurde
           if (!$_SESSION['err'])
           {
               if($_SESSION['bewertung']['kommentar']) $_POST['kommentar'] = $_SESSION['bewertung']['kommentar'];
               // Schauen ob registrierter oder freier User
               if ($_SESSION['user'])
               {
                   $user = $_SESSION['user']->id;
                   //else $user=$_SERVER['REMOTE_ADDR'];
                   $zeit=time();
                   
                   // Neue Bewertung dem Array hinzuf�gen
                   $bewertung[]="";                                 // id
                   $bewertung[]=$_SESSION['user']->id;              // userid
                   $bewertung[]=$_GET['id'];                        // unternehemensid
                   $bewertung[]=serialize($bewertungspunkte);       // bewertung
                   $bewertung[]=$_POST['kommentar'];                // kommentar
                   $bewertung[]="1";                                // aktiv
                   $bewertung[]=$zeit;                              // datum
                   
                    // Bewertung speichern
                    $bewertungsfac= new Bewertung();
                    $bewertungsfac->getByUserAndUid($user,$_GET['id']);
                    if (!$bewertungsvar = $bewertungsfac->getElement())
                    {   
                        // noch nicht vorhanden Bewertung reinschreiben
                        $bewertungsfac->write($bewertung);
                        
                        // Tabelle Erstbewertung aktualisieren
                        $erstbewertungfac = new Erstbewertung();
                        $erstbewertungfac->getByUnternehmenId($_GET['id']);
                        if($erstbewertung = $erstbewertungfac->getElement())
                        {
                            // Datensatz vorhanden, eventuell Status �ndern
                            if($erstbewertung->status == 2)
                            {
                                $erstbewertungfac->update("`status`='3'");
                            }
                        }
                        else
                        {
                            // Datensatz reinschreiben
                            $erstbewertungfac->insertUnternehmen($_GET['id']);
                        }
                        
                        // eMail an Langer versenden
                        $msg = "Hallo Herr Langer".chr(10);
                        $msg .= chr(10);
                        $msg .= "Eine neue Bewertung wurde am ".date("d.m.Y",time())." abgegeben:".chr(10).chr(10);
                        
                        $msg .= "Es wurde  ".$daten->famname." mit FirmId ".$daten->id." bewertet.";
                        
                        $emailfac = new Email(1);
                        $emailfac->setFrom($CONST_MAIL['from']);
                        $emailfac->setTo($CONST_MAIL['an']);
                        $emailfac->setSubject("Neue Bewertung");
                        $emailfac->setContent($msg);
                        //echo nl2br($msg);
                        $emailfac->sendMail();
                        // Ende Email an Langer
                        
                        if ($_POST['klaerung'])
                        {
                            $newbewertungsfac =new Bewertung();
                            $newbewertungsfac->getByUserAndFirmAndDate($user,$_GET['id'],$zeit);
                            if ($newbewertung= $newbewertungsfac->getElement())
                            {
                                $klaerungsfac = new Klaerung();
                                $data[]="";
                                $data[]=$newbewertung->id;
                                $klaerungsfac->write($data);
                                
                                $msg = "Hallo ".$daten->titel."  ".$daten->vorname." ".$daten->famname.chr(10);
                                    $msg .= chr(10);
                                    $msg .= "Ein Benutzer hat eine Bewertung �ber Sie abgegeben und m�chte Sie nun bitte mit ihm Kontakt aufzunehmen, zwecks Kl�rung dieser Bewertung.".chr(10).chr(10);
                                    $msg .= "Der User ist unter der eMail-Adresse: ".$_SESSION['user']->email." erreichbar!".chr(10).chr(10);
                                    $msg .= 'Die Bewertung k�nnen Sie sich unter http://www.meineaerzte.at/bewertungen/showeintrag.php?id='.$daten->id.'&amp;bid='.$newbewertung->id.' anschauen.'.chr(10).chr(10).'Dies ist eine automatisch generierte eMail. Bitte antworten Sie nicht auf Diese eMail!'.chr(10).chr(10).'Ihr meineaerzte.at-Team';
                                                                               
                                    $emailfac = new Email(1);
                                    $emailfac->setFrom("klaerung@meineaerzte.at");
                                    $emailfac->setTo($daten->email);
                                    $emailfac->setSubject("Kl�rungsanfrage zu einer Bewertung bei meineaerzte.at");
                                    $emailfac->setContent($msg);
                                    $emailfac->sendMail();
                            }
                        }// end if ($_POST['klaerung'])
                        header ("LOCATION:".WEBDIR."inc/msg.php?msg=bewertung_ok&id=".$_GET['id']);
                        // SESSION Variablen l�schen
                        unset($_SESSION['bewertung']);
                        unset($_SESSION['bewertungspunkte']);
                    } // end if (!$bewertungsvar = $bewertungsfac->getElement())
                    else 
                    {
                        if(is_numeric($bewertungsvar->userid) && (time()-$bewertungsvar->datum)<604800)
                        {
                            $_SESSION['err'].="Sie haben diesen Eintrag bereits bewertet. F�r registrierte User ist eine Bewertung des gleichen Eintrages nur nach 7 Tagen Wartefrist erneut gestattet.<br/>";
                        }  
                        else if(is_numeric($bewertungsvar->userid) && (time()-$bewertungsvar->datum)>604800)
                        {
                            $bewertungsfac->write($bewertung);
                            
                            // Tabelle Erstbewertung aktualisieren
                            $erstbewertungfac = new Erstbewertung();
                            $erstbewertungfac->getByUnternehmenId($_GET['id']);
                            if($erstbewertung = $erstbewertungfac->getElement())
                            {
                                // Datensatz vorhanden, eventuell Status �ndern
                                if($erstbewertung->status == 2)
                                {
                                    $erstbewertungfac->update("`status`='3'");
                                }
                            }
                            else
                            {
                                // Datensatz reinschreiben
                                $erstbewertungfac->insertUnternehmen($_GET['id']);
                            }
                            
                            if ($_POST['klaerung'])
                            {
                                $newbewertungsfac =new Bewertung();
                                $newbewertungsfac->getByUserAndFirmAndDate($user,$_GET['id'],$zeit);
                                if ($newbewertung= $newbewertungsfac->getElement())
                                {
                                    $klaerungsfac = new Klaerung();
                                    $data[]="";
                                    $data[]=$newbewertung->id;
                                    $klaerungsfac->write($data);
                                    
                                    $msg = "Hallo ".$daten->titel."  ".$daten->vorname." ".$daten->famname.chr(10);
                                    $msg .= chr(10);
                                    $msg .= "Ein Benutzer hat eine Bewertung �ber Sie abgegeben und m�chte Sie nun bitte mit ihm Kontakt aufzunehmen, zwecks Kl�rung dieser Bewertung.".chr(10).chr(10);
                                    $msg .= "Der User ist unter der eMail-Adresse: ".$_SESSION['user']->email." erreichbar!".chr(10).chr(10);
                                    $msg .= 'Die Bewertung k�nnen Sie sich unter http://host-k.nb-cooperation.de/web/bewertungen/showeintrag.php?id='.$daten->id.'&amp;bid='.$newbewertung->id.' anschauen.'.chr(10).chr(10).'Dies ist eine automatisch generierte eMail. Bitte antworten Sie nicht auf Diese eMail!'.chr(10).chr(10).'Ihr meineaerzte.at-Team';
                                                                                                           
                                    $emailfac = new Email(1);
                                    $emailfac->setFrom("klaerung@meineaerzte.at");
                                    $emailfac->setTo($daten->email);
                                    $emailfac->setSubject("Kl�rungsanfrage zu einer Bewertung bei meineaerzte.at");
                                    $emailfac->setContent($msg);
                                    $emailfac->sendMail();
                                    
                                }
                            }// end  if ($_POST['klaerung'])
                            // SESSION Variablen l�schen
                            unset($_SESSION['bewertung']);
                            unset($_SESSION['bewertungspunkte']);
                            header ("LOCATION:".WEBDIR."inc/msg.php?msg=bewertung_ok&id=".$_GET['id']);
                        }// end else if(is_numeric($bewertungsvar->userid)...
                        else
                        {
                            $_SESSION['err'].="Sie haben diesen Eintrag bereits bewertet. Mehrfachbewertungen sind nicht m�glich!<br/>";
                        }
                     }// end else
                 } // end if ($_SESSION['user'])
                 else   // freier User
                 {
                   $user = $_SESSION['bewertung']['email'];
                   $key1 = generateRandomKey(8);               // f�r Aktivierungslink
                   $key2 = generateRandomKey(8);               // f�r Aktivierungslink
                   $zeit=time();
                   
                   // Neue Bewertung dem Array hinzuf�gen
                   $bewertungtemp[]="";                                 // id
                   $bewertungtemp[]=$_SESSION['bewertung']['email'];    // userid
                   $bewertungtemp[]=$_GET['id'];                        // unternehemensid
                   $bewertungtemp[]=serialize($bewertungspunkte);       // bewertung
                   $bewertungtemp[]=$key1;                              // kommentar hier aktivierungsschl�ssel1
                   $bewertungtemp[]="1";                                // aktiv
                   $bewertungtemp[]=$zeit;                              // datum
                   $bewertungtemp[]=$key2;                              // antwort hier aktivierungsschl�ssel2
                   
                    // Bewertung suchen
                    $bewertungsfac= new Bewertung();
                    $bewertungsfac->getByUserAndUid($user,$_GET['id']);
                    if (!$bewertungsvar = $bewertungsfac->getElement())
                    {   
                        // noch nicht vorhanden, in Bewertungtemp suchen
                        $bewertungstempfac= new Bewertungtemp();
                        $bewertungstempfac->getByUserAndUid($user,$_GET['id']);
                        // nicht vorhanden reinschreiben
                        if (!$bewertungstempvar = $bewertungstempfac->getElement())
                        {
                            $bewertungstempfac->write($bewertungtemp);
                            
                            // Aktivierungs eMail an User versenden
                            $msg = "Hallo Bewerter von meineaerzte.at ".chr(10);
                            $msg .= chr(10);
                            $msg .= "Zum aktivieren Ihrer Bewertung vom ".date("d.m.Y",time())." klicken sie folgenden Link:".chr(10).chr(10);
                            $msg .= "http://www.meineaerzte.at/bewertungen/bewertung.aktiv.php?firmid=".$daten->id."&kommentar=".$key1."&antwort=".$key2.chr(10);
                            //$msg .= "http://host-k.nb-cooperation.de/web/bewertungen/bewertung.aktiv.php?firmid=".$daten->id."&kommentar=".$key1."&antwort=".$key2.chr(10);

                            $msg .= "Vielen Dank f�r Ihre Bewertung!";
                            
                            $emailfac = new Email(1);
                            $emailfac->setFrom($CONST_MAIL['from']);
                            $emailfac->setTo($_SESSION['bewertung']['email']);
                            $emailfac->setSubject("Abgegebene Bewertung");
                            $emailfac->setContent($msg);
                            //echo nl2br($msg);
                            $emailfac->sendMail();
                            // Ende Email an User
                        
                            // SESSION Variablen l�schen
                            unset($_SESSION['bewertung']);
                            unset($_SESSION['bewertungspunkte']);
                            header ("LOCATION:".WEBDIR."bewertungen/bewertung.send.php");
                        }// end if (!$bewertungstempvar = $bewertungstempfac->getElement())
                        else
                        {
                            $_SESSION['err'].="Sie haben diesen Eintrag bereits bewertet. Mehrfachbewertungen sind f�r nichtregistrierte User nicht m�glich!<br/>";
                        }
                    } // end if (!$bewertungsvar = $bewertungsfac->getElement())
                    else
                    {
                        $_SESSION['err'].="Sie haben diesen Eintrag bereits bewertet. Mehrfachbewertungen sind f�r nichtregistrierte User nicht m�glich!<br/>";
                    }
                 } // end else nichtregistrierter User
              } // end  if (!$_SESSION['err'])
            }// end  if ($_POST['Bewertungabsenden']
            
            // wenn Vorschau abgesendet wurde
            if ($_POST['Bewertungshow'] && $daten->bewertungsstatus=="A" )
            {
               // vorl�ufiger Kommentar sichern
               if($_POST['kommentar']) $_SESSION['bewertung']['kommentar']= $_POST['kommentar'];

               // Bewertungsmerkmale holen
               $bewertungspunktfac=new Bewertungspunkt();
               $bewertungspunktfac->getBewertungWithKat();
               $katname=array();
        
               // Bewertungsmerkmale mit POST-Inhalt abgleichen
               while($bewertungspunkt= $bewertungspunktfac->getElement())
               {
                   // Wenn Felder vergessen wurde
                   if (!$_POST[$bewertungspunkt->id] ) $_SESSION['err'] = "Sie m�ssen eine vollst�ndige Bewertung abgeben!<br/>";
                   else
                   {
                        $bewertungspunkte[$bewertungspunkt->id]=$_POST[$bewertungspunkt->id];
                        $_SESSION['bewertungspunkte'][$bewertungspunkt->id]=$_POST[$bewertungspunkt->id];
                   }
               }
               
               // eventuell vorhandene Email-Adresse pr�fen und sichern
               if (!$_SESSION['user'])
               {
                   if($_POST['email']=='')
                   {
                        $_SESSION['err'] .= "Sie m�ssen eine Emailadresse eintragen!<br/>";
                   }
                   
                   if($_POST['email'] != '')
                   {
                       $_SESSION['bewertung']['email'] = $_POST['email'];
                       $test = isMail($_POST['email']);
                       if(!$test) $_SESSION['err'] .= "Emailadresse nicht korrekt!<br/>";
                   }
               }
               
               // Wenn kein Fehler aufgetreten ist und Vorschau abgesendet wurde
               if (!$_SESSION['err'])
               {
                   // Schauen ob registrierter oder freier User
                   if ($_SESSION['user']) $user=$_SESSION['user']->id;
                   else $user= $_SESSION['bewertung']['email'];
                   //else $user=$_SERVER['REMOTE_ADDR'];
                   $zeit=time();
                   
                   $bewertungsfac= new Bewertung();
                   $bewertungsfac->getByUserAndUid($user,$_GET['id']);
                   if (!$bewertungsvar = $bewertungsfac->getElement())
                   { 
                      // neue Bewertung
                       $_SESSION['bewertung']['status']= "neu";
                   }
                   else
                   {  
                      // alte Bewertung
                      if(is_numeric($bewertungsvar->userid) && (time()-$bewertungsvar->datum)<604800)
                      {
                          $_SESSION['err'].="Sie haben diesen Eintrag bereits bewertet. F�r registrierte User ist eine Bewertung des gleichen Eintrages nur nach 7 Tagen Wartefrist erneut gestattet.<br/>";
                      }  
                      elseif(is_numeric($bewertungsvar->userid) && (time()-$bewertungsvar->datum)>604800)
                      {
                          // alles klar, daten in SESSION Variable
                          $_SESSION['bewertung']['status']= "alte �berschreiben";
                      }
                      else
                      {
                          $_SESSION['err'].="Sie haben diesen Eintrag bereits bewertet. Mehrfachbewertungen sind nicht m�glich!<br/>"; 
                      }
                   }
                }//end if (!$_SESSION['err'])
            }// end  elseif ($_POST['Bewertungshow']...*/
        
        // Wenn Fehler oder noch nicht abgesendet wurde
        include(INCLUDEDIR."header.inc.php");
        ?>
        <h1>Bewertung des Eintrages: "<?php echo $daten->name.' '.$daten->titel.''.$daten->vorname.' '.$daten->famname; ?>"</h1><br/>
        <?php
           if ($_SESSION['user']->typ=="unternehmen") echo '<span class="err">Als eingetragener Arzt d�rfen Sie keine Bewertungen anderer �rzte vornehmen!</span><br/><br/><br/>';
           if ($_SESSION['user']->typ=="werbung") echo '<span class="err">Als Werbepartner d�rfen Sie keine Bewertungen anderer �rzte vornehmen!</span><br/><br/><br/>';
        ?>
        <div class="td1" style="padding:5px;border:1px dotted #000;">
       <div style="float:left;margin-right:10px;">
            <?php 
            // Bildinfos aus DB auslesen
            if (is_array(unserialize($daten->bild)))$bild=unserialize($daten->bild);
            else $bild=array();
                           
            // pr�fen ob Bild vorhanden
            if (sizeof($bild)!=0)
            {
                // Grafiktyp auslesen
                $typ=getimagesize(LOCALDIR."images/unternehmen/".$bild[0]['url']) ;
              
                // je nach typ Endung bestimmen   
                if ($typ[2]==1) $endung=".gif";
                if ($typ[2]==2) $endung=".jpg"; 
                    
                // Reinnamen ohne Endung
                $name=basename($bild[0]['url'],$endung);
                    
                // Gr��e des Thumbnails auslesen
                $size=getimagesize(LOCALDIR."images/unternehmen/".$name."_thumb".$endung);
                    
                // HTML Bild schreiben
                echo $l->makeLink('<img src="'.WEBDIR.'images/unternehmen/'.$name.'_thumb'.$endung.'" width="'.$size[0].'" height="'.$size[1].'" style="border:1px dotted #000;float:left;margin-right:5px;" alt="" />',"#");
            }
                
            // Wenn kein Bild vorhanden, dann no-image-Bild hinein
            else echo '<img src="'.WEBDIR.'images/nopicture.gif" width="125" height="125" alt="" style="border:1px dotted #000;margin-right:5px;" />';
            
           echo '<br class="clr"/>';
            if ($daten->status=="F") echo '<img src="'.WEBDIR.'images/geprueft.gif" width="100" height="100" alt="geprueft (3K)" style="margin-top:-25px;"/>';
                else if ($daten->status=="P") echo '<img src="'.WEBDIR.'images/premium.gif" width="100" height="100" alt="geprueft (3K)" style="margin-top:-25px;"/>';
                else echo '<img src="'.WEBDIR.'images/ungeprueft.gif" width="100" height="100" alt="geprueft (3K)" style="margin-top:-25px;"/>';
                echo '<br class="clr"/><br/>';
            ?></div>
            <div style="height:125px;float:left;//float:auto;">
                <b><?php echo $daten->name ?></b><br/>
                <b><?php echo $daten->titel.' '.$daten->vorname.' '.$daten->famname ?></b>
                <br/><br/>
                <?php echo $daten->strasse;?><br/>
                <?php echo $daten->plz.' '.$daten->ort; ?><br/>
                <?php echo $daten->land ?>
                
            </div>
            <br class="clr"/>
           
        </div>
        <br class="clr"/>
        <br/>
        <?php if ($_SESSION['err']) echo '<span class="err">'.$_SESSION['err'].'</span><br/>';?>
        <?php 
            if($_SESSION['bewertungspunkte'] && $_POST['Bewertungshow'] && !$_SESSION['err'])
            {
                echo "Nachfolgend Ihre vorl�ufige Bewertung, Sie k�nnen diese senden oder �ndern!";
            }
            else
            {
            ?>
            Nehmen Sie sich bitte ausreichend Zeit f�r diese Bewertung!<br/>
            Klicken Sie zum bewerten einfach auf die Punktzahl, die Sie den einzelnen Bereichen geben m�chten.<br/>
            Versuchen Sie bitte m�glichst objektiv und fair zu sein!<br/><br/>
            <?php
            }
            ?>
        <?php if ($_SESSION['user']->typ != "benutzer" && $_SESSION['user']->typ != "admin" && $_SESSION['user']->typ!="unternehmen" && $_SESSION['user']->typ!="werbung") 
            {
                echo 'F�r eine ausf�hrlichere Bewertung m�ssen Sie sich als User '.$l->makeLink("registrieren",WEBDIR."signin/anmelden.php");
                echo '<br />F�r eine einfache Bewertung m�ssen Sie eine Email-Adresse angeben, die Bewertung wird erst nach dem Klicken auf den an diese ';
                echo ' Adresse gesannten Link freigeschaltet! Dies dient der Mi�brauchsvorbeugung! <br />';
            }
        
        echo '<br/>';
        // Wenn Bewertungsstatus nicht aktiv    
        if ($daten->bewertungsstatus!="A")
        {
            echo "<br/> Dieser Datensatz wurde nicht f�r eine Bewertung freigegeben. ";
        }
        // Wenn Bewertungsstatus aktiv
        else
        {?>
            <br/>
            <form action="bewertung.php?id=<?php echo $_GET['id']?>" method="post">
                <fieldset>
                <?php 
                if($_SESSION['bewertungspunkte'] && $_POST['Bewertungshow'] && !$_SESSION['err'])
                {
                ?>
                <legend>Vorl�ufige Bewertung</legend>
                    <?php 
                    // Wenn Benutzer nicht eingeloggt oder kein Unternehmen bzw. Werbepartner ist, Anzeige Email
                    if ($_SESSION['user']->typ != "benutzer" && $_SESSION['user']->typ != "admin" && $_SESSION['user']->typ!="unternehmen" && $_SESSION['user']->typ != "werbung" ) 
                    {
                        echo 'Ihre Mailadresse: '.$_SESSION['bewertung']['email'].'<br />';
                    }
                }
                else
                {
                ?>
                <legend>Bewertung abgeben</legend>
                    <?php 
                    // Wenn Benutzer nicht eingeloggt oder kein Unternehmen bzw. Werbepartner ist, Email-Inputfeld
                    if ($_SESSION['user']->typ != "benutzer" && $_SESSION['user']->typ != "admin" && $_SESSION['user']->typ!="unternehmen" && $_SESSION['user']->typ != "werbung") 
                    {
                    ?>
                        <label>E-Mail</label><input type="text" name="email" value="<?php if($_SESSION['bewertung']['email']) echo $_SESSION['bewertung']['email'] ?>" />
                    <?php   
                    }
                }
                
                $k=1;
                $bewertungspunktnewfac=new Bewertungspunkt();
                $bewertungspunktnewfac->getBewertungWithKat();
                //$datenfac->createOwnQuery("SELECT * , katbewertung.name AS katname FROM  katbewertung,bewertungspunkte WHERE bewertungspunkte.kat = katbewertung.id ORDER BY katbewertung.id" );
                $katname=array();
                while($bewertung= $bewertungspunktnewfac->getElement())
                {
                    $k++;
                    if (!in_array($bewertung->katname,$katname))
                    {
                        echo '<br/><h1>'.$bewertung->katname.'</h1><div style="margin-left:200px;">schlecht | mittel | sehr gut</div>'.CHR(10);
                        $katname[]=$bewertung->katname;
                        $k=1;
                    }
                    if ($k%2) $class='class="td1"';
                    else $class='class="td"';
                    
                 
                    echo '<div style="width:200px;float:left;padding-top:5px;padding-left:5px;" '.$class.'>'.$bewertung->name.'</div><div style="width:200px;float:left;padding-top:5px;padding-left:5px;" '.$class.'>';
                    
                    // Ausgabe der Sterne f�r vorl�ufige Bewertung
                    if($_SESSION['bewertungspunkte'] && $_POST['Bewertungshow'] && !$_SESSION['err'])
                    {
                        for ($r=1;$r<=5;$r++)
                        {
                            if ($r<=$_SESSION['bewertungspunkte'][$bewertung->id])
                            {
                                echo '<img src="'.WEBDIR.'images/icons/star.gif" alt="Stern-bewertet-'.$r.'"/>';
                            }
                    
                            else
                            {
                               echo '<img src="'.WEBDIR.'images/icons/star_grey.gif" alt="Stern-unbewertet-'.$r.'"/>';
                             
                            }
                        }
                    }
                    else
                    {
                      for($d=1;$d<=5;$d++)
                      {
                          if($_SESSION['bewertungspunkte'][$bewertung->id]== $d) $check = "checked='checked'"; else $check = "";
                          echo '<input type="radio" name="'.$bewertung->id.'" value="'.$d.'" style="" class="star" '.$check.' />'.CHR(10);
                      }
                    }
                    echo '</div><br class="clr"/>';
                }?>   
                <br/>
                </fieldset>
                <br/>
                <?php 
                if ($_SESSION['user']->typ == "benutzer") 
                {
                ?>
                <fieldset>
                    <?php if($_SESSION['bewertungspunkte'] && $_POST['Bewertungshow'] && !$_SESSION['err'])
                    {
                    ?>
                    <legend>vorl�ufiger Bewertungskommentar</legend>
                    <?php
                    echo $_SESSION['bewertung']['kommentar'];
                    }
                    else
                    {
                    ?>
                    <legend>Bewertungskommentar eingeben</legend>
                    <br/>
                    Schreiben Sie hier Ihre positiven, aber auch Ihre negativen Eindr�cke m�glichst fair und sachlich.<br/>Da Sie hier ausschlie�lich Ihre subjektive Meinung abgeben, verwenden Sie bitte Formulierungen wie "Meiner Meinung nach..." und "Ich finde..."<br/><br/>
                    
                    <label>
                     Kommentar
                    </label>
                    <textarea name="kommentar" rows="10" cols="10"><?php if($_SESSION['bewertung']['kommentar']) echo $_SESSION['bewertung']['kommentar']; ?></textarea>
                    <br/><br/>
                    Beachten Sie bitte, das wir Rufsch�digung, Beleidigungen und falsche Tatsachenbehauptungen
                    nicht tolerieren k�nnen und diese nicht freischalten werden.<br/>
                    Bedenken Sie - nur konstruktive Kritik kann helfen, den Service zu verbessern!
                    <?php
                    }
                    ?>
                </fieldset>
                <?php 
                    if ($_SESSION['user'])
                    { 
                    ?>
                      <br/>
                      <?php 
                          if($daten->klaerung == "A")
                          {
                          ?>
                            <fieldset>
                                <legend>Kl�rung veranlassen</legend>
                                Dieser Arzt ist an der Kl�rung eines eventuell aufgetauchten Problemes interessiert.<br/><br/>
                                Sie k�nnen gleichzeitig mit der Bewertung Ihre Kontaktdaten an den Arzt senden um eine Kl�rung zu veranlassen. Es wird bei Aktivierung dieser Funktion automatisch eine eMail an den betreffenden Arzt gesendet mit der Bitte um Kl�rung und Ihrer Emailadresse!<br/><br/>
                                <input type="checkbox" name="klaerung" class="selectbox"/> Ja, ich m�chte eine Kl�rung veranlassen. 
                            </fieldset>
                          <?php
                          }// end if($daten->klaerung == "A")
                    } // end if ($_SESSION['user'])
                    ?>
                <br/>
                <?php 
                } // end if ($_SESSION['user']->typ == "benutzer")
                
                    if($_POST['Bewertungshow'] && !$_SESSION['err'])
                    {
                    ?>
                        <input type="submit" class="submit" name="Bewertungabsenden" value="Bewertung absenden" /><br/>
                        <input type="submit" class="submit" name="Bewertungchange" value="Bewertung �ndern" /><br/><br/><br/>
                    <?php
                    }
                    else
                    {
                        if($_SESSION['user']->typ!="unternehmen" && $_SESSION['user']->typ!="werbung")
                        {
                    ?>
                        <input type="submit" class="submit" name="Bewertungshow" value="Bewertungs Vorschau" /><br/><br/><br/>
                    <?php
                        }
                    }
                    ?>
            </form>
            <?php   
        }// end else if ($daten->bewertungsstatus!="A")
    } // end if ($daten = $datenfac->getElement())
    // Wenn Datensatz nicht gefunden
    else echo "<h1>Bewertung eines Eintrages</h1><br/>Dieser Datensatz wurde nicht in unserer Datenbank gefunden";
}
include(INCLUDEDIR."footer.inc.php");
?>