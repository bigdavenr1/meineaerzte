<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

if(!$_GET['firmid'])
{
    ?>
    <h1>Bewertung wurde versendet</h1>
    
    Die Bewertung wurde tempor�r gespeichert, aber noch nicht freigegeben.
    Sie erhalten umgehend eine Best�tigungsmail mit einem Link, der die Bewertung 
    freigibt. Wenn Sie den Arzt (in fr�hestens 7 Tagen) erneut bewerten wollen, m�ssen Sie 
    registrierter User sein. Bitte melden Sie sich dazu unverbindlich und kostenlos
    <?php 
    echo $l->makeLink(" HIER ",WEBDIR."signin/anmelden.php"); 
    ?>an.
<?php 
}
else
{
    ?>
    <h1>Die Bewertung wurde aktiviert</h1>
    <?php
    echo $l->makeLink("Zur�ck zur Detailansicht des bewerteten Unternehmens", WEBDIR."bewertungen/showeintrag.php?id=".$_GET['firmid'] , "login");
    echo '<br />'.$l->makeLink("Zur erweiterten Suche", WEBDIR."bewertungen/search.php?mod=searchplus&amp;anz=new", "login");
}    
?>

<?php
include(INCLUDEDIR."footer.inc.php");
?>