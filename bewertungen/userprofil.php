<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// Wenn eine ID �bergeben wurde
if ($_GET['id'])
{
    // Datensatz holen
    $datenfac = new Benutzer();
    $datenfac->getById($_GET['id']); 
    $daten= $datenfac->getElement();
    
    $freigabe=array();
    if (is_array(unserialize($daten->freigabe))) $freigabe=unserialize($daten->freigabe);
    
    // Wenn Datensatz vorhanden und aktiv und nicht admin
    if($daten->aktiv == "1" && $daten->typ!="admin" )
    {?>
        <script type="text/javascript" src="<?php echo WEBDIR;?>script/prototype.js"></script>
        <script type="text/javascript" src="<?php echo WEBDIR;?>script/scriptaculous.js?load=effects,builder"></script>
        <script type="text/javascript" src="<?php echo WEBDIR;?>script/lightbox.js"></script>
        <h1>Detailansicht - "<?php echo $daten->nickname;?>"</h1><div class="headline"><div class="right"><?php echo $l->makeLink("Profilfehler melden",WEBDIR."bewertungen/kontakt.php?typ=profil");?></div></div>
        <br />
        <div class="td1" style="padding:5px;border:1px dotted #000;">
            <div style="float:left;margin-right:10px;">
                <?php 
                // pr�fen ob Bild vorhanden
                if ($daten->bild!="")
                {
                    // Grafiktyp auslesen
                    $typ=getimagesize(LOCALDIR."images/user/".$daten->bild) ;
                  
                    // je nach typ Endung bestimmen   
                    if ($typ[2]==1) $endung=".gif";
                    if ($typ[2]==2) $endung=".jpg"; 
                        
                    // Reinnamen ohne Endung
                    $name=basename($daten->bild,$endung);
                        
                    // Gr��e des Thumbnails auslesen
                    $size=getimagesize(LOCALDIR."images/user/".$name."_thumb".$endung);
                        
                    // HTML Bild schreiben
                    echo $l->makeLink('<img src="'.WEBDIR.'images/user/'.$name.'_thumb'.$endung.'" width="'.$size[0].'" height="'.$size[1].'" style="border:1px dotted #000;float:left;margin-right:5px;" alt="" />',WEBDIR."images/user/".$name.$endung,"","_blank",'rel="lightbox"');
                }
                // Wenn kein Bild vorhanden, dann no-image-Bild hinein
                else echo '<img src="'.WEBDIR.'images/nopicture.gif" width="125" height="125" alt="" style="border:1px dotted #000;margin-right:5px;" />';
                echo '<br class="clr"/>';
                ?>  
            </div>
            <div>
                <b class="<?php echo $daten->anrede; ?>"><?php echo $daten->nickname;?></b><br/><br/>
                PLZ/Ort: <?php if ($freigabe['plz']) echo $daten->plz; else echo substr($daten->plz,0,2)."...";?> <?php if ($freigabe['ort']) echo $daten->ort; else echo "(nicht freigegeben)"; ?><br/>
                Land: <?php echo $daten->land;?><br/><br/>
                Alter: <?php if ($freigabe['geburtstag']) { if($daten->geburtstag!="") {echo $datenfac->calculateAge($daten->geburtstag);} else {echo "(nicht angegeben)";}} else {echo "(nicht freigegeben)";}?>
                <br/><br/>
                eMail: <?php if ($freigabe['email']) echo $daten->email; else echo '(nicht freigegeben)';?> <br/>
                Homepage: <?php if ($freigabe['homepage']) echo $daten->homepage; else echo '(nicht freigegeben)';?> <br/>
                <br class="clr"/>
            </div>
            <div ><br />
                <?php 
                // Bewertungsdaten holen
                $bewertungfac=new Bewertung();
                $bewertungfac->getByUser($daten->id);
                $z=0;
                while ($bewertung = $bewertungfac->getElement())
                {
                    // Bewertungsarray()
                    $bewertungen=array();
                    
                    // Bewertung auslesen 
                    $bewertungen[]=unserialize($bewertung->bewertung);
                    
                    // Gr��e des Bewertungsarrays bestimmen         
                    $size_bewertung=sizeof($bewertungen);
                    
                    $punktebewertung=0;
                             
                    //Bewertungspunkte addieren 
                    for ($x=0;$x<$size_bewertung;$x++)
                    {
                        $punktebewertung=$punktebewertung+(array_sum($bewertungen[$x])/sizeof($bewertungen[$x]));
                    }
                               
                    // Quersumme bilden
                    $gesamtbewertung=$punktebewertung/$size_bewertung;
                    
                    $summenarray[$z]['summe']= $gesamtbewertung;
                    $z++;
                }// end while
                // Wenn KEINE Bewertung vorhanden ist
                if ($z==0)
                {
                    echo 'Dieser User hat noch keine Eintr�ge bewertet!<br/><br/>';
                }
                // Wenn mindestens eine Bewertung vorhanden ist
                else
                {
                    // summenarray auswerten
                    $counts = count($summenarray);
                    if($counts >1)
                    {
                        $countp = 0;
                        foreach($summenarray As $daten)
                        {
                            $countp += $daten['summe'];
                        }
                        $countx = $countp/$counts;
                        
                    }
                    else
                    {
                        $counts = $z;
                        $countp = $gesamtbewertung;
                        $countx = $countp/$counts;
                    }
                    echo 'Abgegebene Bewertungen: '.$counts.'<br />';
                    //echo 'Durchnittliche Bewertung: '.round($countx,0).'<br />';
                    // Ausgabe der Bewertung
                    
                    for ($r=1;$r<=5;$r++)
                    {
                        if ($r<=round($countx,0))
                        {?>
                            <img src="<?php echo WEBDIR;?>images/icons/star.gif" alt="Stern-bewertet-<?php echo $r;?>"/>
                        <?php 
                        }
                        else
                        {?>
                            <img src="<?php echo WEBDIR;?>images/icons/star_grey.gif" alt="Stern-unbewertet-<?php echo $r;?>"/>
                        <?php
                        }
                    }
                 }// end else f�r if($z==0)
                 ?>
            </div>
             <br class="clr"/>
        </div>
    <?php
    }        
    // Wenn admin oder inaktiv
    else 
    {
        echo '<h1>Das Profil des von Ihnen aufgerufenen User ist nicht freigeschaltet!</h1><br /><br />';
    }
}
// Wenn keine ID �bertragen wurde
else
{
    echo "Kein User gew�hlt!";
}
    
include(INCLUDEDIR."footer.inc.php"); ?>