<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>


<h1>AGB</h1>
<b>�1. G�ltigkeit</b><br /><br />
Sie �bergeben oder nutzen Artikel auf AUTORnet. Eingesandte Artikel werden an AUTORnet mit allen Rechten �bergeben. Die von Ihnen erstellten Artikel oder eingestellte e-books k�nnen sie zum Verkauf anbieten oder nur f�r Ihren Artikel frei mit Ihrem Link einsenden.  Von uns vorgefertigte Texte k�nnen von Ihnen und zu Ihren Seiten, gegen Entgelt gelinkt werden. Texte die von uns Vorgefertigt sind,  k�nnen zu dem angegebenen Preis gekauft werden. B�cher als ebook k�nnen ebenfalls �ber das Kaufsystem abgewickelt werden. Es gelten automatisch unsere allgemeinen Gesch�ftsbedingungen. Abweichende Bedingungen von Seiten des Webseiten-Nutzers, haben keinerlei G�ltigkeit, oder bed�rfen der Schriftform mit gegenseitiger schriftlicher Zustimmung.
<br /><br />
<b>�2. Ablehnung</b>
<br /><br />
Bei den Artikeln die Sie zur Ver�ffentlichung an AUTORnet senden behalten wir uns grunds�tzlich vor,  Artikel abzulehnen, die nicht den Nutzungsbedingungen entsprechen. Es besteht kein rechtlicher Anspruch auf diesen Dienst und keinen Anspruch auf die Ver�ffentlichung von Artikeln, egal welche Gr�nde von unserer Seite her der Ablehnung zugrunde liegen.
<br /><br />
<b>�3. Artikell�schung</b>
<br /><br />
Trotz eindeutiger Bedingungen halten sich manche Autoren nicht an die geforderte Exklusivit�t eines Artikels. Die Administration beh�lt sich vor, Artikel die nach der Ver�ffentlichung auf dieser Webseite im Nachhinein mehrfach in gleicher Form (v�llig gleiche oder �berwiegend gleiche Text-Duplikate) verbreitet werden, kommentarlos aus dem Artikelbestand wieder zu entfernen, zumindest wenn Nachteile f�r das Gesamtprojekt zu bef�rchten sind.
<br /><br />
<b>�4. Verweise und Links</b>
<br /><br /> 
Wir erkl�ren hiermit ausdr�cklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die aktuelle und zuk�nftige Gestaltung, die Inhalte oder die Urheberschaft der verlinkten/verkn�pften Seiten haben wir keinen Einfluss. Deshalb distanzieren wir uns hiermit ausdr�cklich von allen Inhalten aller verlinkten Seiten, die nach der Linksetzung ver�ndert wurden. F�r illegale, fehlerhafte oder unvollst�ndige Inhalte und insbesondere f�r Sch�den, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Webseite, auf welche verwiesen wurde.
<br /><br />
<b>�5. Urheber- und Kennzeichenrecht</b>
<br /><br /> 
Wir sind bestrebt, f�r die Darstellung dieser Webseite die Urheberrechte der verwendeten Bilder und Texte zu beachten, oder selbst erstellte, gekaufte oder lizenzfreie Bilder und Texte zu nutzen. Alle innerhalb dieses Angebotes genannten und ggf. durch Dritte gesch�tzten Marken- und Warenzeichen unterliegen uneingeschr�nkt den Bestimmungen des jeweils g�ltigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigent�mer. Die Rechte auf durch uns ver�ffentlichte und Selbsterstellten Bilder und Texte, bleiben alleinig bei uns. Eine Vervielf�ltigung oder Verwendung dieser Bilder, Texte und Informationen in anderen elektronischen oder gedruckten Publikationen, ist ohne unsere ausdr�ckliche Zustimmung nicht gestattet. <br /><br />
F�r die eingereichten und ver�ffentlichten Artikel und Bilder der Autoren, �bernehmen die Autoren selbst die Verantwortung. Einen Versto� gegen Urheberrechte Dritter durch die Autoren zu �berpr�fen und zu ermitteln, ist von unserer Seite her nur sehr bedingt m�glich. Eine Ma�nahme hierzu ist die halbautomatische �berpr�fung der eingereichten Texte auf Duplikate im Internet zum Zeitpunkt der Einreichung. Eine Duplikatspr�fung auf gedruckten oder anderen digitalen oder analogen Medien ist nicht m�glich. Wenn aus dieser �berpr�fung heraus Duplikate gefunden werden, und diese nicht dem Artikel-Autor als Urheber zugeordnet werden k�nnen, erfolgt keine Ver�ffentlichung des Artikels. 
<br /><br />
<b>�6. Haftungsausschluss</b>
<br /><br />
<b><i>Datensicherung</i></b>
<br /><br />
Die Inhalte dieser Webseite werden in regelm��igen Abst�nden mittels entsprechender Datensicherungstechniken gesichert. So eine Datensicherung erfolgt in der Regel "nach" einer Abarbeitungs-Phase der eingereichten Artikel-Entw�rfe, sprich wenn die Artikel-Entw�rfe in die ver�ffentlichte Form �bergehen. Die administrative Abarbeitung der Artikel-Entw�rfe erfolgt nicht t�glich, sondern unregelm��ig im Verlauf von bis zu mehreren Tagen oder gar Wochen. <br /><br />
Zu beachten ist, dass vor allem die Artikel der Autoren in der "Entwurfsphase" oder "Einreichungsphase" auf keinen Fall rekonstruiert werden k�nnen, wenn es hier aus technischen Gr�nden zu einem Datenverlust kommen sollte, da hier noch keine Datensicherung durch uns stattfinden konnte. Jeder Autor der einen Artikel einreicht ist dazu verpflichtet, von seinen eigenen Texten unabh�ngig von den Speicherungsm�glichkeiten dieser Webseite, selbstst�ndig eine externe Datensicherung anzulegen (egal in welcher Form). Wir �bernehmen keinerlei Haftung daf�r, wenn ein gespeicherter Artikel durch: Technikausfall, h�here Gewalt, Script-Fehler, Hackerangriff, oder administrative Fehler, verloren gehen.<br /><br />
Eine Haftung f�r Datenverluste und verloren gegangene Texte wird auch dann nicht �bernommen, wenn ein Artikel bereits ver�ffentlicht und einer routinem��igen Datensicherung unterzogen wurde. Denn auch diese Datensicherungen k�nnen technische Defekte erleiden, was eine Wiederherstellung ebenso unm�glich macht.
<br /><br />
<b>�7. Datenspeicherung</b>
<br /><br />
Der Schutz Ihrer personenbezogenen Daten bei der Erhebung, Verarbeitung und Nutzung anl�sslich Ihres Besuchs auf unserer Homepage ist uns ein wichtiges Anliegen. Ihre Daten werden im Rahmen der gesetzlichen Vorschriften gesch�tzt. 
<br /><br />
<b>�8. Anwendbares Recht</b>
<br /><br />
F�r alle Rechtsbeziehungen gilt das Recht der Bundesrepublik Deutschland.
<br /><br />
<b>�9. Salvatorische Klausel</b>
<br /><br />
Sollte eine dieser Bestimmungen unwirksam sein, so ber�hrt dies die Wirksamkeit der �brigen Bestimmungen nicht. Die unwirksame Bestimmung wird durch eine wirksame ersetzt, die dem verfolgten Zweck am n�chsten kommt.

<?php include(INCLUDEDIR."footer.inc.php");?>