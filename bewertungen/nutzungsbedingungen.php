<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>


<h1>Nutzungsbedingungen</h1>
 
Artikelverzeichnis Webzeitung Artikelkatalog e-books<br /><br />
Es ist zu beachten, dass f&uuml;r eine Artikelerfassung aus technischen Gr&uuml;nden nur &uuml;ber eine zwingend notwendige Registrierung  notwendig ist. Die Einreichung von Texten per eMail ist <b>NICHT</b> m&ouml;glich.
<br /><br />
<b>Artikel-Aufnahmenbedingungen</b>
<ol>
    <li>Ihr Text auf AUTORnet. Dieser darf nicht gegen Schutz-Rechte Dritter versto&szlig;en (Marken, Urheberrecht)</li> 
    <li>Der Artikel muss aus mindestens 450 W&ouml;rtern bestehen </li>
     <li>Keine pornografischen, Gewaltverherrlichenden oder illegalen Inhalte! </li>
<li>Der Artikel darf <b>max. einen ausgehenden Link</b> auf Ihre Webseite enthalten</li> 
<li>Garantie auf Exklusivit&auml;t des Artikels (keine Duplikate auf anderen Webseiten - wird &uuml;berpr&uuml;ft!)</li> 
<li>Nur deutschsprachige Texte (abgesehen von Fachbegriffen usw.) </li>
<li>Keinen Schreibstil in der ICH- Form anwenden (nur allgemein gehaltene Artikel)</li> 
<li>Keine reine Verkaufsangebote oder Artikel mit &uuml;berwiegendem Werbe-Charakter </li> 
<li>Ein Artikel wird kein zweites Mal aufgenommen (auch nicht doppelt in mehreren Kategorien) </li> 
<li>Keine enthaltene Links auf Webseiten, die mit dem Artikelinhalt/Thema nichts zu tun haben </li> 
<li>F&uuml;r den Inhalt der Artikel ist alleinig der Autor verantwortlich </li> 
<li>Es darf 1  Bild enthalten. (Bilder m&uuml;ssen per eMail zugesandt werden) </li> 
<li>Im Artikel enthaltene Links, die l&auml;ngere Zeit "ins Leere" f&uuml;hren, d&uuml;rfen administrativ gel&ouml;scht werden, ohne das hierzu der komplette Artikel entfernt wird </li>  
<li>Links zu anderen Artikelverzeichnissen, Wikipedia, Domains ohne Google-Indizierung, PageRank-Diensten, egal ob Startseite oder Unterseiten, werden nicht akzeptiert.</li> 
</ol> 
<b>Vorbehalt</b>
<ol>
<li>Artikel ohne Wert f&uuml;r Leser, mit unzureichendem Schreibstil, oder mit grammatikalischen Fehlern, werden abgelehnt.</li> 
<li>Es d&uuml;rfen von der Administration in jedem Artikel Zwischen&uuml;berschriften, Google-Anzeigen, und bis maximal 2 "interne" Verlinkungen auf Unterseiten innerhalb des Projektes AUTORnet platziert werden, sowie entdeckte Schreib- und Textbaufehler korrigiert und Absatzformatierungen vorgenommen werden</li>  
<li>Ein eingereichter Artikel wird AUTORnet "dauerhaft" zur Verf&uuml;gung gestellt. </li> 
<li>Eingereichte Bilder oder Grafiken die im Artikel erscheinen sollen, d&uuml;rfen von der Adminstration in der Gr&ouml;sse und in der Qualit&auml;t angepasst werden</li>  
<li>Eine "nachtr&auml;gliche" Artikel-Verschiebung in eine andere oder neu erstellte Kategorie (die thematisch geeigneter ist) muss akzeptiert werden</li> 
</ol> 
<b>Hinweise</b><br /><br/>
Ein Artikel kann nach Abgabe nur noch von AUTORnet ge&auml;ndert oder korrigiert werden
<br /><br />
<b>Ablehnung/L&ouml;schung</b>
<br /><br/>
Im Falle einer Ablehnung, wird der eingereichte Artikel endg&uuml;ltig aus der Datenbank gel&ouml;scht. Nach einer L&ouml;schung ist keine Rekonstruktion des Artikels mehr m&ouml;glich. Ein Autor ist f&uuml;r die Sicherung eines Duplikats des Artikels und Textinhalts selbst verantwortlich. Bei einer Ablehnung oder L&ouml;schung erfolgt keine weitere Benachrichtigung.

<?php include(INCLUDEDIR."footer.inc.php");?>