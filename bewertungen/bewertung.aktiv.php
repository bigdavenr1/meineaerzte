<?php
////////////////////////////////////////////////////////////////////////////////
// bewertung.aktiv.php - aktiviert die Bewertung , d.h. schreibt Tupel in 
//                      Tabelle bewertungen und l�scht Tupel in bewertungentemp
//                      und Mail an Langer
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."std/email.class.php");

if($_GET['kommentar']&& $_GET['antwort']&& $_GET['firmid'])
{
   $bewertungstempfac= new Bewertungtemp();
   $bewertungstempfac->getByFirmidAndKeys($_GET['firmid'],$_GET['kommentar'],$_GET['antwort']);
   if($bewertungstemp = $bewertungstempfac->getElement())
   {
      unset($bewertung);
      
      // Neue Bewertung dem Array hinzuf�gen
      $bewertung[]="";                                 // id
      $bewertung[]=$bewertungstemp->userid;            // userid
      $bewertung[]=$bewertungstemp->unternehmenid;     // unternehemenid
      $bewertung[]=$bewertungstemp->bewertung;         // bewertung
      $bewertung[]="";                                 // kommentar
      $bewertung[]="1";                                // aktiv
      $bewertung[]=$bewertungstemp->datum;             // datum
      $bewertung[]="";                                 // antwort
      
      // Bewertung speichern
      $bewertungsfac= new Bewertung();
      $bewertungsfac->write($bewertung);
      
       // Tabelle Erstbewertung aktualisieren
      $erstbewertungfac = new Erstbewertung();
      $erstbewertungfac->getByUnternehmenId($bewertungstemp->unternehmenid);
      if($erstbewertung = $erstbewertungfac->getElement())
      {
          // Datensatz vorhanden, eventuell Status �ndern
          if($erstbewertung->status == 2)
          {
              $erstbewertungfac->update("`status`='3'");
          }
      }
      else
      {
          // Datensatz reinschreiben
          $erstbewertungfac->insertUnternehmen($bewertungstemp->unternehmenid);
      }
      
      // Firmdaten f�r Mail hohlen
      $datenfac = new Daten();
      $datenfac->getById($bewertungstemp->unternehmenid);
      $daten = $datenfac->getElement();
      
      // Datensatz in Tabelle bewertungentemp l�schen
      $bewertungstempfac->deleteElement();
      
      // Mail an Langer
      $msg = "Hallo Herr Langer".chr(10);
      $msg .= chr(10);
      $msg .= "Eine neue Bewertung wurde am ".date("d.m.Y",time())." aktiviert:".chr(10).chr(10);
      
      $msg .= "Es wurde  ".$daten->famname." mit FirmId ".$daten->id." bewertet.";
      
      $emailfac = new Email(1);
      $emailfac->setFrom($CONST_MAIL['from']);
      $emailfac->setTo($CONST_MAIL['an']); 
      $emailfac->setSubject("Bewertung aktiviert");
      $emailfac->setContent($msg);
      //echo nl2br($msg);
      $emailfac->sendMail();
      // end Mail an Langer
       
      // weiterleiten zur Bewertungsseite bewertung.send.php
      header("Location: ".WEBDIR."bewertungen/bewertung.send.php?firmid=".$_GET['firmid']."&sv=1");
   }
   else 
   {
        include(INCLUDEDIR."header.inc.php");
        echo "Schl�ssel ung�ltig!<br />";
        include(INCLUDEDIR."footer.inc.php");
   }
}
else 
{
    include(INCLUDEDIR."header.inc.php");
    echo "Fehler! Schl�ssel nicht �bergeben oder unvollst�ndig!<br />";
    include(INCLUDEDIR."footer.inc.php");
}
?>