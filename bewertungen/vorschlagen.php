<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// unternehmen/edit.php zeigt das Formular zur �nderung von Unternehmen an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

?>

<h1>Arzt vorschlagen</h1>
<br />
Ihr Arzt ist noch nicht bei uns gelistet? Kein Problem. F�llen Sie das folgende Formular aus. Damit schlagen Sie uns Ihren Arzt zum Eintrag in unsere Datenbank vor. 
<br/><br/>
<?php if ($_SESSION['err']) echo '<span class="err">'.$_SESSION['err'].'</span><br/>';?>
<form action="<?php echo $l->makeFormLink(WEBDIR.'bewertungen/vorschlagen-send.php');?>" method="post" >
    <fieldset>
        <legend>
            allgemeine Daten
        </legend>
        <label for="anrede">
            Anrede
        </label>
         <select name="anrede" id="anrede">
                <option value="Herr" <?php if($daten->anrede == "Herr")echo 'selected='.'"selected"'; ?>>Herr</option>
                <option value="Frau" <?php if($daten->anrede == "Frau")echo 'selected='.'"selected"'; ?>>Frau</option>
          </select>
        <br class="clr" />
        <label for="titel">
            Titel
        </label>
        <input type="text" name="titel" id="titel" value="<?php echo $daten->titel;?>" />
        <br class="clr" />
        <label for="famname">
            Nachname
        </label>
        <input type="text" name="famname" id="famname" value="<?php echo $daten->famname;?>" />
        <br class="clr" />
        <label for="vorname">
            Vorname
        </label>
        <input type="text" id="vorname" name="vorname" value="<?php echo $daten->vorname;?>" />
        <br class="clr" /><br/>
        <label for="strasse">
            Strasse
        </label>
        <input type="text" id="strasse" name="strasse" value="<?php echo $daten->strasse;?>" />
        <br class="clr" />
        <label for="plz">
            PLZ / Ort
        </label>
        <input type="text" id="plz" name="plz" value="<?php echo $daten->plz;?>" class="plz"/>
        <input type="text" id="ort" name="ort" value="<?php echo $daten->ort;?>" class="ort" alt="ort"/>
        <br class="clr" />
        <label for="land">
            Land
        </label>
        <input type="text" id="land" name="land" value="<?php echo $daten->land;?>" />
        <br class="clr" /><br/>
        <label for="tel">
            Telefon
        </label>
        <input type="text" id="tel" name="tel" value="<?php echo $daten->tel;?>" />
        <br class="clr" />
        <label for="fax">
            Fax
        </label>
        <input type="text" id="fax" name="fax" value="<?php echo $daten->fax;?>" />
        <br class="clr" /><br/>
        <label for="email">
            eMail
        </label>
        <input type="text" id="email" name="email" value="<?php echo $daten->email;?>" />
        <br class="clr" />
        <label for="homepage">
            Homepage
        </label>
        <input type="text" id="homepage" name="homepage" value="<?php echo $daten->homepage;?>" />
    </fieldset>
    <br class="clr"/>
    <br/>
    <input type="submit" value="Vorschlag absenden" class="submit" />
</form>
<?php
include(INCLUDEDIR."footer.inc.php");
?>

