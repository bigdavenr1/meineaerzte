<?php
////////////////////////////////////////////////////////////////////////////////
// bewertungen/search.php - Suche von �rzten
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

unset($_SESSION['err']);


if($_GET['anz'] == 'new')
{
    //Sessionvariable l�schen bei Neuer Suche
    unset($_SESSION['werbung']);        // WERBEMODUL 
    unset($_SESSION['rotation']);       // WERBEMODUL 
}

if (!$_GET['suchwort'] && $_GET['mod']!='searchplus') 
{
    $_SESSION['err'] .= "kein Suchwort �bergeben!<br />";
?>
    <h1>Suche fehlgeschlagen</h1><br/>
    Sie haben vergessen ein Suchwort einzugeben.<br /><br />Bitte versuchen Sie es erneut!    
<?php
}

// Daten pr�fen
if ($_GET['mod']== "searchplus" && $_GET['absenden'] && ($_GET['suchwort']=='' && $_GET['plz']=='' && $_GET['ort']=='' && $_GET['fachgebiet']=='' && $_GET['name']=='' && $_GET['land']=='' && $_GET['bundesland']=='')) 
{
    $_SESSION['err'].= "Sie m�ssen eins der folgenden Felder ausf�llen!<br />";
}
    
if($_GET['daystart'])
{
    if($_GET['timeend']<=$_GET['timestart']) $_SESSION['err'] .= "Die Endzeit muss nach der Startzeit liegen!<br />";
}

if ($_GET['mod']== "searchplus" && ($_GET['anz'] || $_SESSION['err'] || !$_GET['absenden']))
{
?>
    <h1>erweiterte Sucheinstellungen</h1>  <br/>
    <?php if ($_SESSION['err'] /*&& !$_GET['time']*/) echo '<span class="err">'.$_SESSION['err'].'</span><br/><br/>';?>
    <form action="<?php echo $_SERVER['PHP_SELF']?>" method="get">
        <fieldset>
            <legend> erweiterte Suche</legend>
            <input type="hidden" name="mod" value="searchplus"/>
            <label for="suchwort_erw">
                Suchbegriff
            </label>
            <input type="text" name="suchwort" id="suchwort_erw" value="<?php if($_GET['suchwort']) echo $_GET['suchwort']; ?>" /><br class="clr"/>
            <label for="name">
                Name
            </label>
            <input type="text" name="name" id="name" value="<?php if($_GET['name']) echo $_GET['name']; ?>" /><br class="clr"/>
            <?php
            $ordzeitenfac = new Ordzeiten();
            $schlagwortefac=new Schlagworte();
            $schlagwortefac->getAll();
            $count = $schlagwortefac->getElementCount();
            if($count > 0)// Schlagwortauswertung
            {
                ?>
                <label for="schlagwort">Schlagworte</label>
                <select name="schlagwort" id="schlagwort">
                    <option value="">Bitte w�hlen</option>
                    <?php 
                    while ($schlagwort=$schlagwortefac->getElement())
                    {
                        unset($selected); 
                        if($_GET['schlagwort'] == $schlagwort->schlagwort) $selected = 'selected="selected"'; 
                        else $teststring = "";
                        echo '<option value="'.$schlagwort->schlagwort.'" '.$selected.'>'.$schlagwort->schlagwort.'</option>';
                    }     
                    ?>
                </select><br class="clr"/>
                <?php
            }
            ?>
            <br/>
            <label for="plz">
                PLZ / Ort
            </label>
            <input type="text" id="plz" name="plz" value="<?php if($_GET['plz']) echo $_GET['plz']; ?>" class="plz"/>
            <input type="text" id="ort" name="ort" value="<?php if($_GET['ort']) echo $_GET['ort']; ?>" class="ort" alt="ort"/>
            <br class="clr" />
            <label >
                Bundesland
            </label>
            <select name="bundesland">
                <option value="">Bitte w�hlen</option>
                <option value="0" <?php if($_GET['bundesland']== "0") echo "selected='selected'"; ?>>Burgenland</option>
                <option value="1" <?php if($_GET['bundesland']== "1") echo "selected='selected'"; ?>>Wien</option>
                <option value="3" <?php if($_GET['bundesland']== "3") echo "selected='selected'"; ?>>Nieder�sterreich</option>
                <option value="4" <?php if($_GET['bundesland']== "4") echo "selected='selected'"; ?>>Ober�sterreich</option>
                <option value="5" <?php if($_GET['bundesland']== "5") echo "selected='selected'"; ?>>Salzburg</option>
                <option value="6" <?php if($_GET['bundesland']== "6") echo "selected='selected'"; ?>>Tirol</option>
                <option value="7" <?php if($_GET['bundesland']== "7") echo "selected='selected'"; ?>>Voralberg</option>
                <option value="8" <?php if($_GET['bundesland']== "8") echo "selected='selected'"; ?>>Steiermark</option>
                <option value="9" <?php if($_GET['bundesland']== "9") echo "selected='selected'"; ?>>K�rnten</option>
            </select>
            <br class="clr" /><br/>
            <label for="land">
                Land
            </label>
            <input type="text" id="land" name="land" value="<?php if($_GET['land']) echo $_GET['land']; ?>" />
            <br class="clr" /><br/>
            <label>
                Fachgebiet
            </label>
            <select name="fachgebiet">
                <option value="">Bitte w�hlen</option>
                <?php 
                $fachgebietfac=new Fach();
                $fachgebietfac->getAll();
                while ($fachgebiet=$fachgebietfac->getElement())
                {  
                    $string = ""; 
                    if($_GET['fachgebiet']== $fachgebiet->id) $string = "selected='selected'";
                    echo '<option value="'.$fachgebiet->id.'"'.$string.'>'.$fachgebiet->name.'</option>';
                }     
                ?>
            </select>
            <br class="clr"/><br/>
            <label for="daystart">
                �ffnungszeit 
            </label>
            <select id="daystart" name="daystart" style="width:auto;">
                <option value="">Bitte w�hlen</option> 
                <?php  
                for($i=1;$i<8;$i++)
                { 
                    unset($teststring); 
                    if($_GET['daystart'] == $i) $teststring = "selected='selected'"; 
                    else $teststring = "";
                    echo "<option value='".$i."'".$teststring." >".$ordzeitenfac->parseDay($i,"long")."</option>";
                }   
                ?>
            </select>
            <select id="timestart" name="timestart" style="width:auto">
            <?php  
                for($i=1;$i<49;$i++)
                { 
                    unset($teststring); 
                    if($_GET['timestart'] == $i) $teststring = "selected='selected'"; 
                    else $teststring = "";
                    echo "<option value='".$i."'".$teststring." >".$ordzeitenfac->parseTime($i)."</option>";
                }  
             ?>
            </select>
            <select id="timeend" name="timeend" style="width:auto;">
            <?php  
                for($i=1;$i<49;$i++)
                {
                    unset($teststring); 
                    if($_GET['timeend'] == $i) $teststring = "selected='selected'"; 
                    else $teststring = "";   
                    echo "<option value='".$i."'".$teststring." >".$ordzeitenfac->parseTime($i)."</option>";
                }  
             ?>
            </select>
            <br class="clr" /><br/>
        </fieldset>
        <br/><br/>
        <input type="submit" class="submit" value="Suche Starten" name="absenden"/>
    </form>
<?php
}   // end if ($_GET['mod']== "searchplus" && ($_GET['time'] || $_SESSION['err']))

if(!$_SESSION['err'] && $_GET['absenden'])
{
    if(isset($_GET['suchwort']) && trim($_GET['suchwort']) != "")
    {
        // Suchwort eintragen bzw. Klicks erh�hen
        $suchwortefac = new Suchworte();
        $suchwortefac->getSuchworteByName(htmlentities($_GET['suchwort']));
        if ($suchwort = $suchwortefac->getElement())
        {
            $suchwortefac->update("klicks='".($suchwort->klicks+1)."'");
        }
        else
        {
            $sdata[]="";
            $sdata[]=htmlentities($_GET['suchwort']);
            $sdata[]="1";
            $suchwortefac->write($sdata);
        } 
    }
    if ($_GET['mod'] != "searchplus" )
    {?> 
        <h1>Suchergebnisse - "<?php echo htmlentities(strip_tags($_GET['suchwort']));?>"</h1><br/>
    <?php  
    } 
    else 
    {?>
        <h1>Suchergebnisse - der erweiterten Suche</h1><br/>
    <?php
    }

      //Daten holen
      $datenfac = new Daten();
      $datenfac->setLimit(10);
      // Schlagwort den Suchw�rtern hinzuf�gen
      if(isset($_GET['schlagwort']))
      {
          if($_GET['suchwort']) $_GET['suchwort'].= " ";
          $_GET['suchwort'] .= $_GET['schlagwort'];
      }
      // Fachgebietid hohlen
      if($_GET['fachgebiet']=='' && $_GET['suchwort']!='')
      {
         $fachfac = new Fach();
         $fachfac->search($_GET['suchwort']);
         if($fach = $fachfac->getElement())
         {
            $_GET['fachgebiet'] = $fach->fachid; // erster Treffer in Fachgebiet
         }
      }
      
      // Wenn Bundesland gew�hlt
      if($_GET['bundesland'] != '') $_GET['plz'] = $_GET['bundesland'];

      echo $testausgabe = $datenfac->search($_GET['suchwort'],$_GET['plz'],$_GET['ort'],$_GET['land'],$_GET['fachgebiet'],$_GET['daystart'],$_GET['daystart'],$_GET['timestart'],$_GET['timeend'],$_GET['name']);

      $x=0;
      $y=0;     // Z�hlvariable f�r Suchergebnisse und Index Ergebnisarray
      
      while($daten = $datenfac->getElement())
      {
          $datenarray[$y]['daten']= $daten;
          
          // Bewertungen hohlen und Speichern
          $bewertungen=array();
          $x++;
          $bewertungfac=new Bewertung();
          $bewertungfac->getByUid($daten->uid);
          $z=0;
          $gesamtbewertung = 0; // zur�cksetzen der Variablen
          while ($bewertung = $bewertungfac->getElement())
          {
              $z++;
              $bewertungen[]=unserialize($bewertung->bewertung);
          } 
          $size_bewertung=sizeof($bewertungen);
          
          if ($daten->bewertungsstatus=="A")
          {
             if($size_bewertung > 0)
             {
                $punktebewertung=0;
                             
                //Bewertungspunkte addieren 
                for ($x=0;$x<$size_bewertung;$x++)
                {
                    $punktebewertung=$punktebewertung+(array_sum($bewertungen[$x])/sizeof($bewertungen[$x]));
                }
               
                // Quersumme bilden
                $gesamtbewertung=$punktebewertung/$size_bewertung;
             }
          }
          
          // Daten speichern
          $datenarray[$y]['offen']= $daten->offen;
          $datenarray[$y]['status']= $daten->status;
          $datenarray[$y]['size_bewertung'] = $size_bewertung;
          $datenarray[$y]['gesamtbewertung'] = $gesamtbewertung;
          
          $y++;
      }
      
      // Sortieren der Ergebnisse, wenn Ergebnisse
      if($y > 0)
      {
          foreach ($datenarray as $key => $row) {       // aus Spalten Arrays machen
            $offen[$key]  = $row['offen'];              // �ffnungszeit-Treffer
            $status[$key] = $row['status'];             // Status P / N 
            $gesamtbew[$key] = $row['gesamtbewertung']; // Bewertungspunkte
          }
    
          $success = array_multisort($offen, SORT_NUMERIC, SORT_DESC ,$status, SORT_DESC, $gesamtbew, SORT_NUMERIC, SORT_DESC, $datenarray );
          
          // Ausgabe
          $p=0;     // Z�hlvariable f�r Ausgabegestaltung auch bei Werbemodul verwendet
          
          if($_GET['index']) $index = $_GET['index'];
          else $index = 0;
          $limit = $datenfac->getLimit();
          $max = $index+$limit;
      
          // Forschleife und If-Abfrage statt While-Schleife f�r Begrenzung
          for($index;$index < $max;$index++)
          {
            if(is_array($daten = $datenarray[$index]))
            { 
                //echo "Daten : ".$daten['daten']->name."<br />";
                if ($_GET['mod']=="searchplus" && !$wechsel && $daten['daten']->offen=="")
                {
                    if($_GET['daystart'])echo "keine weiteren Ergebnisse f�r Ihre gew�nschten �ffnungszeiten gefunden. <br/><br/>Wir konnten f�r Sie aber folgende �rzte mit anderen �ffnungszeiten in unserer Datenbank finden.<hr><h1>Suchergebnisse mit anderen �ffnungszeiten</h1><br/> ";
                    $wechsel="on";
                }
                $p++;
                
                // BEGINN Werbemodul ##########################################
                if($p==6 ) // nach 5 Suchausgaben
                {
                    // Variablen setzen: neues Array Session['werbung'], neues Array Session['rotation'], Maximale Anzahl der Anzeigen in der Rotation, Initialisierung Anzeigenobjekt
                    $anzeigefac = new Anzeige();
                    if (!$_SESSION['werbung']) $_SESSION['werbung']=array();
                    if (!$_SESSION['rotation']) $_SESSION['rotation']=array();
                    if (!$_SESSION['rotation'][1]) $_SESSION['rotation'][1]=array();
                    if (!$_SESSION['rotation'][2]) $_SESSION['rotation'][2]=array();
                    $rotations_limiter=20;
                    
                    //Wenn die Suche neu ausgef�hrt wurde
                    if(!$_GET['limitoffset']) 
                    {         
                        $_GET['limitoffset']=0;            
                        // Bei Ortsuche und Fachgebiet
                        if( $_GET['ort'] && $_GET['ort'] != '' && $_GET['fachgebiet'] && $_GET['fachgebiet'] != '')
                        {
                            // Anzeigen raussuchen
                            $anzeigefac->getByFachidAndOrt($_GET['fachgebiet'] ,$_GET['ort']);
                            while($anzeige = $anzeigefac->getElement())
                            { 
                                // �berpr�fen, ob die Anzeige schon gefunden wurde
                                if (!in_array($anzeige->id,$_SESSION['werbung']))
                                {
                                    // AnzeigenID in Array schreiben und nachfolgende Suche abbrechen 
                                    $_SESSION['werbung'][] = $anzeige->id;
                                }
                            }
                        }
                        
                        // Bei Plz und Fachgebiet
                        if($_GET['plz'] && $_GET['plz'] != '' && $_GET['fachgebiet'] && $_GET['fachgebiet'] != '')
                        {
                            if (sizeof($_SESSION['werbung']<3))
                            {
                                // Anzeigen raussuchen
                                $anzeigefac->getByFachidAndPlz($_GET['fachgebiet'] ,$_GET['plz']);
                                while($anzeige = $anzeigefac->getElement())
                                {
                                     // �berpr�fen, ob die Anzeige schon gefunden wurde
                                    if (!in_array($anzeige->id,$_SESSION['werbung']))
                                    {   
                                        // AnzeigenID in Array schreiben
                                        $_SESSION['werbung'][] = $anzeige->id;
                                    }
                                }
                            }
                            // zuf�llige Datens�tze mit FachId und Plz holen und in Rotation schreiben
                            $anzeigefac->getRandByFachidAndPlz($_GET['fachgebiet'],$_GET['plz']);
                            while($anzeige = $anzeigefac->getElement())
                            {
                                // pr�fen ob Anzeige bereits gefunden wurde 
                                if (!in_array($anzeige->id,$_SESSION['werbung']) && !in_array($anzeige->id,$_SESSION['rotation'][1]) && !in_array($anzeige->id,$_SESSION['rotation'][2]))
                                { 
                                    $_SESSION['rotation'][$anzeige->key][]=$anzeige->id;
                                }
                            }
                        }
                    }

                    // Wenn Rotation noch nicht gef�llt und Suchwort angegeben wurde
                    if($_GET['suchwort'] && $_GET['suchwort'] != '' && (sizeof($_SESSION['rotation'][1])+sizeof($_SESSION['rotation'][2]))<$rotations_limiter)
                    { 
                        $anzeigefac->searchAktivAnzeige($_GET['suchwort']);
                        while($anzeige = $anzeigefac->getElement())
                        {
                            // Ergebnisse mit alten Treffern in $p=4,$p=7;$p=10 vergleichen
                            if(!in_array($anzeige->id,$_SESSION['werbung']) && !in_array($anzeige->id,$_SESSION['rotation'][1]) && !in_array($anzeige->id,$_SESSION['rotation'][2]))
                            {
                                $_SESSION['rotation'][$anzeige->key][]=$anzeige->id;
                            }
                        }
                    }
                    
                    echo '<i>------------------------------------------------------------------ ! Werbung  ! ---------------------------------------------------------------- </i><br/><br />';
                    
                    // allgemein nach Rotation und Gewichtung
                    //$anzeigerotfac = new Anzeigerotation();
                    //$last_id = $anzeigerotfac->rotieren(); 
                    if  (!$_SESSION['rotkey']) $_SESSION['rotkey']=0;
                    if ($_SESSION['rotkey']>2) $_SESSION['rotkey']=0;
                    $_SESSION['rotkey']++; 
                     
                    // key hohlen durch rotieren gesetzt
                    if($_SESSION['rotkey']==1) $key = 1;
                    else $key = 2;
                    
                    // Schauen ob eine feste Anzeige auf dem Platz vorhanden ist
                    if($_SESSION['werbung'][$_GET['limitoffset']])
                    {
               
                        // Anzeigedaten holen
                        $anzeigefac->getById($_SESSION['werbung'][$_GET['limitoffset']]);
                    }   
                    
                    // Wenn keine feste Werbung dann Rotation einsetzen // Nat�rlich nur wenn gef�llt
                    else if (!$_SESSION['werbung'][$_GET['limitoffset']] && (sizeof($_SESSION['rotation'][1])!=0 || sizeof($_SESSION['rotation'][2])!=0 ))
                    {
                        echo $last_id;
                        // Array durchw�rfeln
                        array_rand ($_SESSION['rotation'][$key]);
                        
                        if (sizeof($_SESSION['rotation'][$key])==0)
                        {
                            // neue LastID
                            $_SESSION['last_id']="END";
                        }
                        
                        //Wenn mehr als 1 Eintrag in der KeyGruppe Eintrag vorhanden ist und der erste neu gew�rfelt gefundene gleich der LastId ist den 2. Wert nehmen
                        if ($_SESSION['rotation'][$key][0] == $_SESSION['last_id'] && sizeof($_SESSION['rotation'][$key])!=1) 
                        {
                            // neue LastID
                            $_SESSION['last_id']=$_SESSION['rotation'][$key][1];
                        }
                        
                        //Wenn mehr als 1 Eintrag in der KeyGruppe Eintrag vorhanden ist und der erste neu gew�rfelt gefundene gleich der LastId dann auf "Hier k�nnte Ihre..." setzen
                        elseif (($_SESSION['rotation'][$key][0] == $_SESSION['last_id'] && sizeof($_SESSION['rotation'][$key])==1) || sizeof($_SESSION['rotation'][$key])==0) 
                        {
                            // neue LastID
                            $_SESSION['last_id']="END";
                        }
                        
                        // ansonsten den ersten Wert des Arrays nehmen
                        else 
                        {
                           $_SESSION['last_id']=$_SESSION['rotation'][$key][0]; 
                        }
                        // Anzeige holen
                        if ($_SESSION['last_id']!="END") $anzeigefac->getById($_SESSION['last_id']);
                        else $anzeigevorhanden="NO";
                   }
                   // Wenn keine Daten in der Rotation sind
                   else {$anzeigevorhanden="NO";}
                    
                   // Wenn eine Anzeige vorhanden ist
                   if ($anzeigevorhanden!="NO")
                   {
                        // Anzeige auslesen
                        $anzeigefac->getById($_SESSION['last_id']);
                        $anzeige=$anzeigefac->getElement();
                        $anzeigefac->update(" `show`='".($anzeige->show+1)."'");// show in Anzeige erh�hen
                        // Ausgabe der Anzeige
                        if ($p%2) $z=1; else $z=2;
                        {
                        ?>
                        
                        
                            <div class="td1" style="padding:5px;border:1px dotted #000;height:150px;">
                                  <div style="float:left;margin-right:10px;height:150px;position:relative;">
                                <?php 
                            
                                // Bildinfos aus DB auslesen
                                if (is_array(unserialize($anzeige->bild)))$bild=unserialize($anzeige->bild);
                                else $bild=array();
                                       
                                // pr�fen ob Bild vorhanden
                                if (sizeof($bild)!=0)
                                {
                                    // Grafiktyp auslesen
                                    $typ=getimagesize(LOCALDIR."images/werbung/".$bild[0]['url']) ;
                          
                                    // je nach typ Endung bestimmen   
                                    if ($typ[2]==1) $endung=".gif";
                                    if ($typ[2]==2) $endung=".jpg"; 
                                
                                    // Reinnamen ohne Endung
                                    $name=basename($bild[0]['url'],$endung);
                                
                                    // Gr��e des Thumbnails auslesen
                                    $size=getimagesize(LOCALDIR."images/werbung/".$name."_thumb".$endung);
                                
                                    // HTML Bild schreiben
                                    echo '<img src="'.WEBDIR.'images/werbung/'.$name.'_thumb'.$endung.'" width="'.$size[0].'" height="'.$size[1].'" style="border:1px dotted #000;float:left;margin-right:5px;" alt="" />';
                                }
                                // Wenn kein Bild vorhanden, dann no-image-Bild hinein
                                else echo '<img src="'.WEBDIR.'images/nopicture.gif" width="125" height="125" alt="" style="border:1px dotted #000;margin-right:5px;" />';
                                echo '<br class="clr"/><br/>';
                                 echo $l->makeLink('<b>[ Details ansehen ]</b>',WEBDIR.'bewertungen/showanzeige.php?id='.$anzeige->id,"","",'style="position:absolute;bottom:4px;left:10px;"');
                                ?>
                                </div>
                                <div style="float:right;border-left:1px dotted #000;border-bottom:1px dotted #000;padding-left:5px;min-height:125px;width:200px;">
                                <?php
                                    //Werbestempel?
                                    //echo '<img src="'.WEBDIR.'images/ungeprueft.gif" width="75" height="75" alt="geprueft (3K)" style="float:right"/>';
                                ?>
                              <h1 style="margin-top:0px;">Werbung</h1>
                                <?php
                                // Anzeigetext
                                echo 'Dieser Eintrag ist eine Werbeeinblendung !<br/><br/>';
                                ?>
                               
                                </div>
        
                        <div style="margin-left:142px;">
                                     
                                    <b><?php echo $anzeige->titel.' '.$anzeige->vorname.' '.$anzeige->name;?></b><br/><br/>
                                    <?php // <b>echo $daten['daten']->name;</b>?>
                                    <?php echo $anzeige->strasse; ?><br/>
                                    <?php echo $anzeige->plz.' '.$anzeige->ort; ?><br/>
                                    <?php echo $anzeige->land ?>
                                    <br/><br/><div class="headline" style="width:220px;">Firmenbezeichnung / Bet�tigungdfeld:</div>
                                    <?php echo $anzeige->fachgebiete;
                                    // Fachgebiete-Array einlesen
                                    /*if (is_array(unserialize($anzeige->fachgebiete))) $fachgebiete=unserialize($anzeige->fachgebiete);
                                    else $fachgebiete=array();
                                    $kategoriefac = new Fach();
                                    $kategoriefac->getAll();
                                    while ($kategorien = $kategoriefac->getElement())
                                    {
                                        $checked="";
                                        if (in_array($kategorien->id,$fachgebiete))
                                        {
                                          echo $kategorien->name.'<br />';
                                        }
                                    }*/
                                     ?>
                                   </div>
                                
                            </div>
                            <br class="clr" />
                        <?php    
                        }// end Ausgabe
                    }// end if($gefundenWerbeId != '')
                    else // Keine passenden Anzeigen vorhanden
                    {
                       echo "<br /><b>Hier k�nnte Ihre Werbung stehen!</b><br /><br/>";
                    } 
                    echo '<i>&nbsp;------------------------------------------------------------ ! Ende Werbung  ! -------------------------------------------------------------- </i><br /><br/>';

                }// end if($p==4 || $p==7 || $p==10)
                // END WERBEMODUL ##############################################
                
                if ($p%2) $z=1; else $z=2;
                // Ausgabe
                {?>
                    <div class="td1" style="padding:5px;border:1px dotted #000;height:150px;">
                          <div style="float:left;margin-right:10px;height:150px;position:relative;">
                        <?php 
                    
                        // Bildinfos aus DB auslesen
                        if (is_array(unserialize($daten['daten']->bild)))$bild=unserialize($daten['daten']->bild);
                        else $bild=array();
                               
                        // pr�fen ob Bild vorhanden
                        if (sizeof($bild)!=0)
                        {
                            // Grafiktyp auslesen
                            $typ=getimagesize(LOCALDIR."images/unternehmen/".$bild[0]['url']) ;
                  
                            // je nach typ Endung bestimmen   
                            if ($typ[2]==1) $endung=".gif";
                            if ($typ[2]==2) $endung=".jpg"; 
                        
                            // Reinnamen ohne Endung
                            $name=basename($bild[0]['url'],$endung);
                        
                            // Gr��e des Thumbnails auslesen
                            $size=getimagesize(LOCALDIR."images/unternehmen/".$name."_thumb".$endung);
                        
                            // HTML Bild schreiben
                            echo '<img src="'.WEBDIR.'images/unternehmen/'.$name.'_thumb'.$endung.'" width="'.$size[0].'" height="'.$size[1].'" style="border:1px dotted #000;float:left;margin-right:5px;" alt="" />';
                        }
                        // Wenn kein Bild vorhanden, dann no-image-Bild hinein
                        else echo '<img src="'.WEBDIR.'images/nopicture.gif" width="125" height="125" alt="" style="border:1px dotted #000;margin-right:5px;" />';
                        echo '<br class="clr"/><br/>';
                         echo $l->makeLink('<b>[ Details ansehen ]</b>',WEBDIR.'bewertungen/showeintrag.php?id='.$daten['daten']->uid,"","",'style="position:absolute;bottom:4px;left:10px;"');
                        ?>
                        </div>
                        <div style="float:right;border-left:1px dotted #000;border-bottom:1px dotted #000;padding-left:5px;min-height:125px;width:200px;">
                        <?php
                        if ($daten['status']=="F") echo '<img src="'.WEBDIR.'images/geprueft.gif" width="75" height="75" alt="geprueft (3K)" style="float:right"/>';
                        else if ($daten['status']=="P") echo '<img src="'.WEBDIR.'images/premium.gif" width="75" height="75" alt="geprueft (3K)" style="float:right"/>';
                        else echo '<img src="'.WEBDIR.'images/ungeprueft.gif" width="75" height="75" alt="geprueft (3K)" style="float:right"/>';
                        ?>
                      <h1 style="margin-top:0px;">Bewertungen</h1>
                        <?php
                        // Wenn zum Bewerten Freigegeben
                        if ($daten['daten']->bewertungsstatus=="A")
                        {
                            // Wenn KEINE Bewertung vorhanden ist
                            if ($daten['size_bewertung']==0)
                            {
                               echo 'Noch keine Bewertungen zu diesem Eintrag vorhanden<br/><br/>';
                            }
                            
                            // Wenn mindestens eine Bewertung vorhanden ist
                            else
                            {
                                // Ausgabe der Bewertung
                                for ($r=1;$r<=5;$r++)
                                {
                                    if ($r<=round($daten['gesamtbewertung'],0))
                                    {?>
                                        <img src="<?php echo WEBDIR;?>images/icons/star.gif" alt="Stern-bewertet-<?php echo $r;?>" />
                                        <?php 
                                    }
                                   
                                    else
                                    {?>
                                        <img src="<?php echo WEBDIR;?>images/icons/star_grey.gif" alt="Stern-unbewertet-<?php echo $r;?>" />
                                        <?php
                                    }
                                }
                                echo '<br/><span class="small">'.$daten['size_bewertung'].' Bewertungen abgegeben.</span><br/>';
                                echo "<br/>".$l->makeLink('<b>[ Bewertungsdetails ansehen ]</b>',WEBDIR.'bewertungen/showeintrag.php?id='.$daten['daten']->uid.'#bewertungen');
                            }
                            // Bewertungslink
                            echo "<br/>".$l->makeLink('<b>[ jetzt bewerten ]</b>',WEBDIR."bewertungen/bewertung.php?id=".$daten['daten']->uid);
                        }
                        // Wenn nicht zum Bewerten Freigegeben
                        else echo 'Dieser Eintrag ist noch nicht zur Bewertung freigegeben!<br/><br/> Bitte versuchen Sie es sp�ter nocheinmal!';
                        ?>
                       
                        </div>

                <div style="margin-left:142px;">
                             
                            <b><?php echo $daten['daten']->titel.' '.$daten['daten']->vorname.' '.$daten['daten']->famname;?></b><br/><br/>
                            <?php // <b>echo $daten['daten']->name;</b>?>
                            <?php echo $daten['daten']->strasse; ?><br/>
                            <?php echo $daten['daten']->plz.' '.$daten['daten']->ort; ?><br/>
                            <?php echo $daten['daten']->land ?>
                            <br/><br/><div class="headline" style="width:220px;">Fachbereich:</div>
                            <?php 
                            // Fachgebiete-Array einlesen
                            if (is_array(unserialize($daten['daten']->fachgebiete))) $fachgebiete=unserialize($daten['daten']->fachgebiete);
                            else $fachgebiete=array();
                            $kategoriefac = new Fach();
                            $kategoriefac->getAll();
                            while ($kategorien = $kategoriefac->getElement())
                            {
                                $checked="";
                                if (in_array($kategorien->id,$fachgebiete))
                                {
                                  echo $kategorien->name.'<br />';
                                }
                            }
                             ?>
                           </div>
                        
                    </div>
                    <br class="clr" /><br/>
                <?php    
                }// Ausgabe
            } // end foreach 
         }// end for
      }// end if($y>0)
    if ($y==0) 
    {
        if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span>';unset ($_SESSION['err']); }
        else echo "Leider keine Artikel zu diesen Suchbegriffen gefunden!<br />";
            
        // Suchdaten dem Link �bergeben
        echo "<br/>".$l->makeLink("Neue erweiterte Suche",WEBDIR."bewertungen/search.php?mod=searchplus&amp;msg=aendern&amp;suchwort=".$_GET['suchwort']."&amp;name=".$_GET['name']."&amp;schlagwort=".$_GET['schlagwort']."&amp;plz=".$_GET['plz']."&amp;ort=".$_GET['ort']."&amp;land=".$_GET['land']."&amp;fachgebiet=".$_GET['fachgebiet']."&amp;daystart=".$_GET['daystart']."&amp;timestart=".$_GET['timestart']."&amp;timeend=".$_GET['timeend']."&amp;bundesland=".$_GET['bundesland']);
    } 
    else
    {
        unset($_SESSION['suche']);
        echo 'Suchergebnisse: '.$y.'<br />';
        $seitentest = $y / $datenfac->getLimit();
        if($seitentest  > 1)
        {
            ?>
            <br class="clr" /><br />
            <div class="contentboxsmall" style="text-align:left;">
                Seiten: <?php  
                if($_GET['mod'] != 'searchplus') echo $datenfac->getLimitLink("std","&amp;absenden=absende&amp;suchwort=".$_GET['suchwort']);
                else echo $datenfac->getLimitLink("std", "&amp;mod=searchplus&amp;absenden=absende&amp;suchwort=".$_GET['suchwort']."&amp;name=".$_GET['name']."&amp;schlagwort=".$_GET['schlagwort']."&amp;plz=".$_GET['plz']."&amp;ort=".$_GET['ort']."&amp;land=".$_GET['land']."&amp;fachgebiet=".$_GET['fachgebiet']."&amp;daystart=".$_GET['daystart']."&amp;timestart=".$_GET['timestart']."&amp;timeend=".$_GET['timeend']."&amp;bundesland=".$_GET['bundesland']);
                ?>
            </div>  
        <?php
        }
    }
} // end if(!$_SESSION['err'])
//echo "<pre>"; print_r($_SESSION['werbung']); echo "</pre>";
//echo "<pre>"; print_r($_SESSION['rotation']); echo "</pre>";
include(INCLUDEDIR."footer.inc.php");
?>