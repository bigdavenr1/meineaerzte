<?PHP
include ("../inc/config.php");
include(INCLUDEDIR."header.inc.php");?> 
<h1>&Auml;rztecheck</h1>
Aufgrund vieler positiver, sowie auch negativer Erfahrungen in unserem n�heren Bekanntenkreis haben wir den Entschluss gefasst, 
ein �rzteportal mit ausgepr�gter Suchfunktion, sowie einem durchdachten Bewertungssystem zu erstellen.
Ein Diskussionsforum soll das Angebot f�r Patienten noch abrunden.<br/><br/>
Daraus entstand <b>www.meineaerzte.at</b>
<br><?php echo $l->makeLink("Spezial Thanks",WEBDIR."bewertungen/thanks.php");?> an alle, die uns dabei tatkr�ftig unterst�tzt haben!
<br/><br/>
Viele Patienten - vielleicht auch Sie? - haben einfach Angst vor einem Arztbesuch und vernachl�ssigen diesen daher oft viele Jahre. 
Kann man sich vorab Informieren, und Bewertungen wie etwa "einf�hlsam und engagiert", zu einem gew�hlten Arzt lesen, 
�berwindet man unserer Meinung nach eher seine �ngste und Hemmungen vorm ersten Arztbesuch.<br/><br/>
Ist der Arztbesuch regelm��ig, k�nnen auch viele Krankheitsbilder schon im Fr�hstadium erkannt werden. Dies erspart nicht nur sp�tere Beschwerden und Schmerzen f�r Sie, sondern auch Kosten f�r unser Gesundheitssystem, welches wir wiederum alle gemeinsam finanzieren.
Auch wenn nach einem Umzug in eine neue Stadt ein netter Hausarzt oder ein einf�hlsamer Gyn�kologe gesucht wird, soll dieses Portal weiter helfen.
<br/><br/>
Das Portal soll dem Arzt erm�glichen, ein Profil (Kontaktdaten, Fotos, Beschreibung der Praxis, Ordinationszeiten und vieles mehr) anzulegen und zu pflegen, 
um sich so seinen Patienten bestm�glich pr�sentieren zu k�nnen und um zielsicher gefunden zu werden. 
Au�erdem soll dieses Portal den Patienten erm�glichen, sich auszutauschen, �rzte zu bewerten, sowie zielsicher zu suchen und auch finden zu k�nnen. 
Auch soll es Patienten ein wenig die Angst und die eventuell vorhandenen Vorurteile nehmen, sodass der Besuch beim Arzt wieder leichter f�llt.<br/><br/>
Da uns dieses Projekt auch pers�nlich sehr am Herzen liegt, w�rden wir uns sehr �ber Ihr Lob, Ihre Anregungen und auch �ber Ihre Kritik freuen.
Benutzen Sie bitte hierzu unser <?php echo $l->makeLink("Kontaktformular",WEBDIR."bewertungen/kontakt.php")?> oder unseren <?php echo $l->makeLink("Fragebogen",WEBDIR."bewertungen/zufriedenheitsformular.php")?>!

<?php
include(INCLUDEDIR."footer.inc.php");
?>