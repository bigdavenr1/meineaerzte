<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");
?>

<h1>Bewerten Sie auch uns!</h1><br/> 
Um unseren Service und unser Angebot weiter f&uuml;r Sie auszubauen und verbessern zu k&ouml;nnen, bitten wir Sie, hier auch unsere Webseite zu bewerten. Egal ob Anregungen, Kritik oder vielleicht sogar ein kleines Lob - scheuen Sie sich nicht, uns Ihre Gedanken mitzuteilen.<br/><br/>
Ihr Name und Ihre hier abgegebene Bewertung werden nicht ver&ouml;ffentlicht!<br/><br/>

<?php
    if ($_SESSION['err']) {echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);}
?>
<p>Klicken Sie zum bewerten einfach auf die Punktzahl, die Sie den einzelnen Bereichen geben m&ouml;chten. </p><br/>
  <form action="zufriedenheit-send.php" method="post">
      <?php 
      if(!$_SESSION['user'])
      {
      ?>
        <fieldset>
        <legend>Bewerter</legend>
        <label for="name">Ihr Name</label><input typ="text" id="name" name="name"/>
        </fieldset>
      <?php
      }
      ?>
      <fieldset>
      <legend>Bewertung abgeben</legend>
      <?php
      $k=1;
      $zufriedenheitfac = new Zufriedenheit();
      $zufriedenheitfac->getPunkteWithKat();
      $katname=array();
      while($bewertung= $zufriedenheitfac->getElement())
      {
          $k++;
          if (!in_array($bewertung->katname,$katname))
          {
              echo '<br/><h1>'.$bewertung->katname.'</h1><div style="margin-left:200px;">schlecht | mittel | sehr gut</div>'.CHR(10);
              $katname[]=$bewertung->katname;
              $k=1;
          }
          if ($k%2) $class='class="td1"';
          else $class='class="td"';
          
       
          echo '<div style="width:200px;float:left;padding-top:5px;padding-left:5px;" '.$class.'>'.$bewertung->name.'</div><div style="width:200px;float:left;padding-top:5px;padding-left:5px;" '.$class.'>';
          for($d=1;$d<=5;$d++)
          {
              echo '<input type="radio" name="'.$bewertung->id.'" value="'.$d.'" style="" class="star"/>'.CHR(10);
          }
          echo '</div><br class="clr"/>';
      }?>   
      <br/>
      </fieldset>
      <br/>
      
      
      <fieldset>
          <legend>Bewertungskommentar eingeben</legend>
          <label>
           Kommentar
      </label>
          <textarea name="kommentar" rows="10" cols="10"></textarea>
      </fieldset>
      <br/>
      
      <input type="submit" class="submit" name="Zufriedenheitabsenden" value="Zufriedenheit absenden" /><br/><br/><br/>
  </form>
<?php   

include(INCLUDEDIR."footer.inc.php");
?>