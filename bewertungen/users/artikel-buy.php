<?php
include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

$datenfac = new Daten();
$datenfac->getById($_GET['id']);
$daten = $datenfac->getElement();

?>
<h1>einen Artikel kaufen</h1>
Sie m�chten den folgenden Artikel kaufen:
<br /><br />

<table width="100%">
    <tr>
        <th width="480">
            Titel
        </th>
        <th>
            Preis
        </th>
    </tr>
    <tr>
        <td class="td1">
            <?php echo $daten->Titel; ?>
        </td>
        <td class="td1">
            <?php echo number_format($daten->Preis,2,",",".");?> &euro;
        </td>
    </tr>
</table>
<br />
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_xclick" />
<input type="hidden" name="business" value="info@raeucherzeit.de" />
<?php //input type="hidden" name="business" value="seller_1196068090_biz@nb-cooperation.de" />?>
<input type="hidden" name="item_name" value="Artikel freischalten: - <?php echo $daten->Titel;?> -" />
<input type="hidden" name="amount" value="<?php echo number_format($daten->Preis,"2",".",",");?>" />
<input type="hidden" name="notify_url" value="http://host-b.nb-cooperation.de/web/artikelverzeichnis/user/verb-ok.php" />
<input type="hidden" name="return" value="http://host-b.nb-cooperation.de/web/artikelverzeichnis/user/artikel-ok.php" />
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="lc" value="DE">
<input type="hidden" name="item_number" value="<?php echo $daten->id;?>" />
<input type="hidden" name="bn" value="PP-BuyNowBF" />
<input type="hidden" name="custom" value="<?php echo $_SESSION['user']->id ?>" />
<input type="image" src="https://www.paypal.com/de_DE/i/btn/x-click-but23.gif" border="0" name="submit" alt="Zahlen Sie mit PayPal - schnell, kostenlos und sicher!" style="width:88px;height:21px;border:0">
<img alt="" border="0" src="https://www.paypal.com/de_DE/i/btn/x-click-but23.gif" width="1" height="1" style="display:none;">
</form>


<?php include(INCLUDEDIR."footer.inc.php");?>