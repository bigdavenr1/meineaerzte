<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");

    $benutzerfac = new Benutzer(1);
    $benutzerfac->getById($_SESSION['user']->id);
    $benutzer = $benutzerfac->getElement();
?>

<h1>Benutzer-Details</h1>
<br /><?php
if ($_SESSION["err"])
{
    echo '<div class="err">'.$_SESSION["err"].'</div><br />';
    unset($_SESSION["err"]);
}?>
<form action="<?php echo $l->makeFormLink(WEBDIR.'artikelverzeichnis/user/change-data-update.php?id='.$_SESSION['user']->id);?>" method="post">
    <label >
        Nickname:
    </label>
    <?php echo $benutzer->nickname; ?>
    <br class="clr" />
    <label >
       Titel:
    </label>
    <?php echo $benutzer->titel; ?>
    <br class="clr" />
    
     <label >
        Name:
    </label>
    <?php echo $benutzer->vorname; ?> <?php echo $benutzer->nachname; ?>
    <br class="clr" />
    
    <label >
        Geburtsdatum:
    </label>
    <?php echo $benutzer->geburtstag; ?>
    <br class="clr" />
    
    <label>
        Strasse:
    </label>
    <input type="text" name="strasse" value="<?php echo $benutzer->strasse; ?>" />
    <br class="clr" />
    
    <label >
        PLZ / Ort:
    </label>
    <input type="text" name="plz" value="<?php echo $benutzer->plz; ?>" style="width:40px;" /><input type="text" name="ort" value="<?php echo $benutzer->ort; ?>" style="width:127px;" />
    <br class="clr" />
    <?php /* ?>
    <label>
        Tel:
    </label>
        <input type="text" name="telefon" value="<?php echo $benutzer->telefon; ?>" />
        
        <br class="clr" />*/?>
    <label >
        eMail:
    </label>
        <input type="text" name="email" value="<?php echo $benutzer->mail; ?>" /><br class="clr" />
        <label>&nbsp;</label>
        <i>Neue eMail-Adressen m�ssen erneut verifiziert werden!</i>
    <br class="clr" /><br />
    
<br />
 <input type="submit" value="Daten speichern" class="submit" />
</form><br /><br />

<?php 
include(INCLUDEDIR."footer.inc.php");
?>

