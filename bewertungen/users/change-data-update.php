<?php


//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////


include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
include(INCLUDEDIR."std/email.class.php");

    if (!$_POST["strasse"]) $err .= "Du hast das Feld 'Strasse' vergessen<br>";
    if (!$_POST["plz"]) $err .= "Du hast das Feld 'PLZ' vergessen<br>";
    if (!$_POST["ort"]) $err .= "Du hast das Feld 'Ort' vergessen<br>";
    if (!$_POST["telefon"]) $err .= "Du hast das Feld 'Telefon' vergessen<br>";   
    if (!$_POST["email"]) $err .= "Du hast das Feld 'eMail' vergessen<br>";

    if (!$err)
    {



    $benutzerfac = new Benutzer();
    $benutzerfac->getById($_SESSION['user']->id);
    $benutzer = $benutzerfac->getElement();

    if ($_POST['email']==$benutzer->mail)
    {
        $benutzerfac->update("strasse='".$_POST['strasse']."', titel='".$_POST['titel']."', plz='".$_POST['plz']."', ort='".$_POST['ort']."', telefon='".$_POST['telefon']."'");
        
        echo '<h1>�nderung erfolgreich</h1>Die Benutzerdaten wurden erfolgreich ge�ndert<br /><br />'.$l->makeLink("zur�ck zur �berischt",WEBDIR."artikelverzeichnis/user/change-data.php");
    }  
    
    else
    {
        $key=generateRandomKey(30);
        $benutzerfac->update("aktiv='0',aktivierungskey='".$key."', strasse='".$_POST['strasse']."', plz='".$_POST['plz']."', ort='".$_POST['ort']."', telefon='".$_POST['telefon']."', mail='".$_POST['email']."'");
        
        $msg = "Hallo ".$benutzer->nickname.chr(10);
        $msg .= chr(10);
        $msg .= "Jetzt bist Du nur noch einen Klick davon entfernt, deine neue eMail-Adresse zu best�tigen. Der folgende Link reaktiviert einmalig dein Profil. Diese Mail ist automatisch generiert, Antworten w�r also nicht sinnvoll.".chr(10).chr(10);
        $msg .= "Viel Spass!".chr(10).chr(10);
        $msg .= "http://www.autornet.de/artikelverzeichnis/activate.php?key=".$key.chr(10);
                                  
        $emailfac = new Email();
        $emailfac->setFrom($CONST_MAIL['from']);
        $emailfac->setTo($_POST['email']);
        $emailfac->setSubject("Account - Best�tigung der ge�nderten eMail-Adresse - autornet.de");
        $emailfac->setContent($msg);
        $emailfac->sendMail();
        
        session_destroy();
        
        
        echo '<h1>�nderung erfolgreich</h1>Die Benutzerdaten wurden erfolgreich ge�ndert<br /><br />Da du die eMail-Adresse ver�ndert hast, wurdest du ausgeloggt und hast eine eMail zur Verifizierung an deine neue eMail-Adresse erhalten. Ohne die Best�tigung der eMail-Adresse kannst du dich nicht einloggen und Artikel oder Kommentare verfassen.<br /><br />'.$l->makeLink("zur�ck zur �berischt",WEBDIR."index.php");
    
    
    }
           

    }
    
    if ($err) 
    {
        
        $_SESSION["err"] = $err; 
        header("Location: ./change-data.php?sv=1");
    }
    
   



 include(INCLUDEDIR."footer.inc.php");
?>