
<head>
<style>
<!--
p
	{margin-right:0cm;
	margin-left:0cm;
	font-size:4.0pt;
	font-family:"Verdana","sans-serif";
	color:black;
	}
h2
	{margin-right:0cm;
	margin-left:0cm;
	font-size:18.0pt;
	font-family:"Times New Roman","serif";
	color:windowtext;
	font-weight:bold}
 p.MsoNormal
	{mso-style-parent:"";
	margin-bottom:.0001pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	color:windowtext;
	margin-left:0cm; margin-right:0cm; margin-top:0cm}
-->
</style>
</head>

<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// Anmeldeformular - Schritt 0 = Einleitung
//                 - Schritt 1 = Standarddaten
//                 - Schritt 2 = Interessenprofil
//                 - Schritt 3 = AGB
//                 - Schritt 4 = Benutzer anlegen -> Registration erfolgreich
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

if(isset($_GET['id'])) $_SESSION['reg']['hotelid']= $_GET['id'];
// Errors
//////////////////////////////////////////////////////////////////////////////////////////////////

if ($_GET["step"] >= 2 && $_GET["step"] <= 4 && !$_SESSION["reg"])
{
    $_GET["step"] = 0;
}

if ($_SESSION["err"] && $_GET["step"] >= 1)
{
    echo '<div class="err">'.$_SESSION["err"].'</div>';
    unset($_SESSION["err"]);
}
// Formulare
//////////////////////////////////////////////////////////////////////////////////////////////////
 
    switch ($_GET["step"]) 
    {
        case 1:?>
            <h1>Anmelden - Schritt 1</h1>
            
            <br />
            
            <form action="<?php echo $l->makeFormLink('update.php?step=2'); ?>" method="post" class="left" style="margin-right:50px;">
            <fieldset>
            <legend>Bitte geben Sie zuerst Ihre Stammdaten ein.</legend> 
                <input type="hidden" name="hotelid" value="<?php echo $_SESSION['reg']['hotelid']; ?>" />
                <label>Email</label><input type="text" name="mail" value="<?php echo $_SESSION['reg']['mail']; ?>" maxlength="50" /><br class="clr" />
                <label>Nickname</label><input type="text" name="nick" value="<?php echo $_SESSION['reg']['nick']; ?>" maxlength="50" /><br class="clr" />  
                <label>Passwort</label><input type="password" name="pass" value="<?php echo $_SESSION['reg']['pass']; ?>" maxlength="25" /><br class="clr" />
                <label>P. best�tigen</label><input type="password" name="repass" maxlength="25" /><br class="clr" />
                <br>
			<br>
			Die nachfolgenden Daten werden <b>nicht ver�ffentlicht!</b><br>
                <br/>
                 <label>Geburtsdatum</label>
                <select name="geburtstag_tag" class="datum_tag" style="width:60px;">
                    <?php for ($i = 1; $i <= 31; $i++) echo "<option value='".$i."'".($i == $_SESSION["reg"]["geburtstag_tag"] ? " selected='selected'" : "").">".$i."</option>"; ?>
                </select>&nbsp;
                <select name="geburtstag_monat" class="datum_monat" style="width:60px;">
                    <?php for ($i = 1; $i <= 12; $i++) echo "<option value='".$i."'".($i == $_SESSION["reg"]["geburtstag_monat"] ? " selected='selected'" : "").">".$i."</option>"; ?>
                </select>&nbsp;
                <select name="geburtstag_jahr" class="datum_jahr" style="width:75px;">
                    <?php for ($i = (date("Y")-16); $i >= (date("Y")-66); $i--) echo "<option value='".$i."'".($i == $_SESSION["reg"]["geburtstag_jahr"] ? " selected='selected'" : "").">".$i."</option>"; ?>
                </select>
                <br/>
                <br />   

                <label>Anrede</label><select name="anrede" id="anrede">
                    
                    <option value="Herr" <?php if($_SESSION['reg']['anrede']== "Herr")echo 'selected='.'"selected"'; ?>>Herr</option>
                    <option value="Frau" <?php if($_SESSION['reg']['anrede']== "Frau")echo 'selected='.'"selected"'; ?>>Frau</option></select> 
			<br class="clr"/>
                <label>Titel</label><input type="text" name="titel" value="<?php echo $_SESSION['reg']['titel']; ?>"  /> 
			<br class="clr" />
                <label>Vorname</label><input type="text" name="vorname" value="<?php echo $_SESSION['reg']['vorname']; ?>"  /><br class="clr" />
                <label>Nachname</label><input type="text" name="nachname" value="<?php echo $_SESSION['reg']['nachname']; ?>"  /> 
			<br class="clr" />
                <label>Strasse</label><input type="text" name="strasse" value="<?php echo $_SESSION['reg']['strasse']; ?>"  /> 
			<br class="clr" />
                <label>PLZ Ort</label><input type="text" class="plz" name="plz" value="<?php echo $_SESSION['reg']['plz']; ?>"  /> <input type="text" class="ort" name="ort" value="<?php echo $_SESSION['reg']['ort']; ?>"  /> 
			<br class="clr" /><br class="clr" />
                
                <label>Land</label><input type="text" name="land" value="<?php echo $_SESSION['reg']['land']; ?>"  /> 
			<br class="clr" /><br />
                <label>Homepage ohne "http://"</label><input type="text" name="homepage" value="<?php echo $_SESSION['reg']['homepage']; ?>" /><br class="clr" />
                <br /></fieldset>
                <?php echo $l->makeLink("Zur�ck", $_SERVER['PHP_SELF'], "backlink"); ?><input type="submit" value="Weiter" class="submit" />
            
            </form>
            
            
            <br class="clr" />
            
            <?php break;        
        
        case 2:?>
            <h1>Anmelden - Schritt 2</h1>
            Nat�rlich haben Sie als User bei uns auch Rechte und Pflichten.<br>
Bitte lesen Sie sich unsere AGB's durch und best�tigen Sie diese, wenn Sie damit 
einverstanden sind.<br>
Wir m�chten Sie nochmals darauf aufmerksam machen, das Ihre Registration und die 
Nutzung unseres Portales selbstverst�ndlich kostenlos und unverbindlich ist!
            <br /><br /><br />
            
            <div class="textarea" style="width:495px;height:250px;overflow:auto;">
            <p align="center" style="text-align: center"><b>
			<span lang="DE-AT" style="font-size:14.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			Allgemeine Gesch�ftsbedingungen f�r die Nutzung der Plattform 
			www.meineaerzte.at</span></b></p>
			<h2>
			<span lang="DE-AT" style="font-size:14.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			&nbsp;</span></h2>
			<h2>
			<span lang="DE-AT" style="font-size:14.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			1. Gegenstand</span></h2>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">1.1</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			www.meineaerzte.at stellt dem Nutzer eine Plattform und technischen 
			Applikationen bereit, die eine Pr�sentation und/oder einen 
			Meinungsaustausch der Nutzer untereinander erm�glichen. 
			www.meineaerzte.at beteiligt sich inhaltlich nicht an der 
			Kommunikation der Nutzer untereinander.</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">1.2</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			www.meineaerzte.at stellt die vom Nutzer bereitgestellten Daten 
			und/oder Informationen den anderen Nutzern nur zur Verf�gung, soweit 
			diese Daten und/oder Informationen nicht gegen gesetzliche 
			Vorschriften oder diese AGB versto�en. </span></p>
			<p style="text-align:justify"><b>
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			1.3 </span></b>
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			www.meineaerzte.at ist berechtigt, rechtswidrige Inhalte ohne 
			Vorank�ndigung zu entfernen.</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">1.3</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Der Nutzer erkennt an, dass eine hundertprozentige Verf�gbarkeit der 
			Website technisch nicht zu realisieren ist. www.meineaerzte.at 
			bem�ht sich, die Leistungen m�glichst konstant verf�gbar zu halten. 
			Insbesondere Wartungs-, Sicherheits- oder Kapazit�tsbelange sowie 
			Ereignisse, die nicht im Machtbereich von www.meineaerzte.at stehen 
			(wie z. B. St�rungen von �ffentlichen Kommunikationsnetzen, 
			Stromausf�lle etc.), k�nnen zu kurzzeitigen St�rungen oder zur 
			vor�bergehenden Einstellung der Dienste f�hren.</span></p>
			<h2>
			<span lang="DE-AT" style="font-size:14.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			2. Registrierung, Zusicherungen bei der Registrierung</span></h2>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2.1</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Der Nutzer hat sich vor Inanspruchnahme der Dienste der Plattform 
			www.meineaerzte.at unter seiner Benutzerkennung einzuloggen. </span>
			</p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2.2</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Der Nutzer sichert zu, dass alle von ihm bei der Registrierung 
			angegebenen Daten wahr und vollst�ndig sind. Der Nutzer ist 
			verpflichtet, �nderungen seiner Nutzerdaten unverz�glich anzuzeigen.
			</span></p>
			<p><strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			2.3</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Der Nutzer ist verpflichtet, sein Passwort geheim zu halten. </span>
			</p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2.4</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Durch das erstmalige Einloggen gibt der Nutzer ein Angebot zum 
			Abschluss des Vertrages �ber die Nutzung der Dienste der Plattform 
			www.meineaerzte.at ab. www.meineaerzte.at nimmt dieses Angebot durch 
			Freischaltung des Nutzers an. Durch diese Annahme kommt der Vertrag 
			zwischen dem Nutzer und www.meineaerzte.at zustande.</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">2.5</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Jeder Nutzer darf sich nur einmal registrieren und nur ein 
			Nutzerprofil anlegen. </span></p>
			<h2>
			<span lang="DE-AT" style="font-size:14.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			3. Pflichten des Nutzers</span></h2>
			<p><strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			3.1</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Der Nutzer ist verpflichtet,</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">3.1.1</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			ausschlie�lich wahre und nicht irref�hrende Angaben in seinem Profil 
			zu machen, insbesondere auch unter Einhaltung der ihn allenfalls 
			treffenden standesrechtlichen Verpflichtungen;</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">3.1.2</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			nur solche Fotos seiner Person zu verwenden, die den Nutzer klar und 
			deutlich erkennen lassen. Der Nutzer stellt sicher, dass die 
			�ffentliche Wiedergabe der von ihm �bermittelten Fotos erlaubt ist;</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">3.1.3</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			bei der Nutzung der Inhalte und Dienste sowie in der Kommunikation 
			mit anderen Nutzern ausschlie�lich wahre und nicht irref�hrende, 
			keine beleidigenden oder verleumderischen Inhalte zu verwenden und 
			auch sonst alle Rechte Dritter zu beachten, insbesondere auch unter 
			Einhaltung der den Nutzer treffenden standesrechtlichen 
			Verpflichtungen. </span></p>
			<h2>
			<span lang="DE-AT" style="font-size:14.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			4. �nderungen von Leistungen</span></h2>
			<p style="text-align:justify">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">www.meineaerzte.at beh�lt 
			sich vor, die angebotenen Leistungen zu �ndern oder abweichende 
			Dienste anzubieten, au�er dies ist f�r den Nutzer nicht zumutbar.</span></p>
			<h2>
			<span lang="DE-AT" style="font-size:14.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			5. Beendigung des Vertrags</span></h2>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">5.1</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Der Nutzer und www.meineaerzte.at k�nnen die unentgeltliche 
			Mitgliedschaft jederzeit ohne Angabe von Gr�nden k�ndigen. Die 
			K�ndigung kann mit dem Kontaktformular oder schriftlich vorgenommen 
			werden. Bei der K�ndigung sind die relevanten Daten des Nutzers 
			anzugeben. </span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">5.2</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Der Nutzer und www.meineaerzte.at k�nnen die Premium-Mitgliedschaft 
			ohne Angabe von Gr�nden mit einer Frist von vierzehn Tagen vor 
			Ablauf der Vertragslaufzeit k�ndigen. Die K�ndigung kann mit dem 
			Kontaktformular oder schriftlich erfolgen. Bei der K�ndigung sind 
			die relevanten Daten des Nutzers anzugeben. Nach der K�ndigung der 
			Premium-Mitgliedschaft durch den Nutzer bleibt dem Nutzer die 
			unentgeltliche Mitgliedschaft bis zu ihrer Beendigung erhalten.
			</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">5.3</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Die Bestimmungen lassen das Recht beider Parteien, aus wichtigem 
			Grund zu k�ndigen, unber�hrt. Ein wichtiger Grund f�r 
			www.meineaerzte.at liegt insbesondere dann vor, wenn die Fortsetzung 
			des Vertragsverh�ltnisses bis zum Ablauf der gesetzlichen 
			K�ndigungsfrist f�r www.meineaerzte.at unter Ber�cksichtigung aller 
			Umst�nde des Einzelfalls und unter Abw�gung der Interessen von 
			www.meineaerzte.at und des Nutzers nicht zumutbar ist. Wichtige 
			Gr�nde sind insbesondere die folgenden Ereignisse:</span></p>
			<p style="text-indent: -18.0pt; margin-left: 54.0pt">
			<span lang="DE-AT" style="font-size: 12.0pt; font-family: Symbol; color: windowtext">
			�<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span>
			</span>
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Nichteinhaltung 
			gesetzlicher Vorschriften durch den Nutzer,</span></p>
			<p style="text-indent: -18.0pt; margin-left: 54.0pt">
			<span lang="DE-AT" style="font-size: 12.0pt; font-family: Symbol; color: windowtext">
			�<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span>
			</span>
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Versto� des Nutzers gegen 
			seine vertraglichen Pflichten,</span></p>
			<p style="text-indent: -18.0pt; margin-left: 54.0pt">
			<span lang="DE-AT" style="font-size: 12.0pt; font-family: Symbol; color: windowtext">
			�<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp; </span>
			</span>
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">der Ruf der angebotenen 
			Leistungen und Dienste wird durch die Pr�senz des Nutzers erheblich 
			beeintr�chtigt; </span></p>
			<p style="text-align:justify"><b>
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			5.4 </span></b>
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			Bei Vorliegen eines wichtigen Grundes kann www.meineaerzte.at 
			unabh�ngig von einer K�ndigung auch unverz�glich die Sperrung des 
			Zugangs oder die L�schung einzelner oder aller Beitr�ge des 
			betreffenden Nutzers veranlassen.</span></p>
			<h2>
			<span lang="DE-AT" style="font-size:14.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			6. Verantwortlichkeit f�r Inhalte, Daten und/oder Informationen der 
			Nutzer</span></h2>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">6.1</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			www.meineaerzte.at �bernimmt keine Verantwortung f�r die von den 
			Nutzern bereitgestellten Inhalte, Daten und/oder Informationen.</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">6.2</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Soweit der Nutzer eine gesetzes- oder vertragswidrige Benutzung der 
			Plattform unter www.meineaerzte.at bemerkt, kann er diese mit dem 
			Kontaktformular, schriftlich oder telefonisch melden.</span></p>
			<h2>
			<span lang="DE-AT" style="font-size:14.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			7. Haftung </span></h2>
			<p style="text-align:justify"><b>
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			7.1 </span></b>
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			Schadensersatzanspr�che � gleich aus welchem Rechtsgrund � gegen 
			www.meineaerzte.at (einschl. deren Erf�llungsgehilfen), die leichte 
			Fahrl�ssigkeit voraussetzen, bestehen nur, wenn eine wesentliche 
			Vertragspflicht verletzt worden ist. Schadensersatzanspr�che sind in 
			diesem Fall der H�he nach auf den typischen vorhersehbaren Schaden 
			beschr�nkt. </span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">7.2</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Der Nutzer h�lt www.meineaerzte.at schad- und klaglos, soweit andere 
			Nutzer oder Dritte Anspr�che gegen www.meineaerzte.at wegen einer 
			Verletzung ihrer Rechte durch die vom Nutzer auf der Plattform unter 
			www.meineaerzte.at eingestellten Inhalte geltend machen.</span></p>
			<h2>
			<span lang="DE-AT" style="font-size:14.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">8. Rechte an Inhalten</span></h2>
			<p style="text-align:justify"><b>
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			8.1 </span></b>
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			Der Nutzer r�umt www.meineaerzte.at mit dem Einstellen eines Profil 
			und/oder eines Beitrags ein unbeschr�nktes, unwiderrufliches und 
			�bertragbares Nutzungsrecht ein, welches www.meineaerzte.at zu 
			jeglicher Art der Verwertung, insbesondere zur dauerhaften 
			Vorhaltung sowie zur Vermarktung der Plattform unter 
			www.meineaerzte.at berechtigt. </span></p>
			<p style="text-align:justify"><b>
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			8.2 </span></b>
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			www.meineaerzte.at hat damit das Nutzungsrecht an allen Beitr�gen. 
			Eine Vervielf�ltigung oder Verwendung der Beitr�ge oder deren 
			Inhalte in anderen elektronischen oder gedruckten Publikationen ist 
			ohne ausdr�ckliche schriftliche Einwilligung von www.meineaerzte.at 
			nicht gestattet. Das Kopieren, Herunterladen, Verbreiten und 
			Vertreiben sowie Speichern von Inhalten der www.meineaerzte.at bzw. 
			Dritter ist, mit Ausnahme der technisch notwendigen 
			Zwischenspeicherung ohne ausdr�ckliche Einwilligung nicht gestattet.</span></p>
			<p class="MsoNormal">
			<span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">&nbsp;</span></p>
			<p class="MsoNormal">
			<span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">&nbsp;</span></p>
			<h2>
			<span lang="DE-AT" style="font-size:14.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			9. Schlussbestimmungen </span></h2>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">9.1</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Der Vertrag und seine �nderungen bed�rfen der Schriftform. 
			Nebenabreden bestehen nicht.</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">9.2</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			www.meineaerzte.at beh�lt sich vor, diese AGB jederzeit ohne Nennung 
			von Gr�nden zu �ndern, es sei denn, das ist f�r den Nutzer nicht 
			zumutbar. www.meineaerzte.at wird den Nutzer �ber �nderungen der AGB 
			rechtzeitig benachrichtigen. Widerspricht der Nutzer der Geltung der 
			neuen AGB nicht innerhalb von zwei Wochen nach der Benachrichtigung, 
			gelten die ge�nderten AGB als vom Nutzer angenommen. 
			www.meineaerzte.at wird den Nutzer in der Benachrichtigung auf sein 
			Widerspruchsrecht und die Bedeutung der Widerspruchsfrist hinweisen.
			</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">9.3</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Soweit nichts anderes vereinbart ist, kann der Nutzer alle 
			Erkl�rungen an www.meineaerzte.at per E-Mail mit dem Kontaktformular 
			abgeben oder diese per Brief an www.meineaerzte.at �bermitteln. 
			www.meineaerzte.at kann Erkl�rungen gegen�ber dem Nutzer per E-Mail 
			oder per Fax oder Brief an die Adressen �bermitteln, die der Nutzer 
			als aktuelle Kontaktdaten in seinem Nutzerkonto angegeben hat.</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">9.4</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Sollten einzelne Regelungen dieser AGB unwirksam sein oder werden, 
			wird dadurch die Wirksamkeit der �brigen Regelungen nicht ber�hrt. 
			Die Vertragspartner verpflichten sich, eine unwirksame Regelung 
			durch eine solche wirksame Regelung zu ersetzen, die in ihrem 
			Regelungsgehalt dem wirtschaftlich gewollten Sinn und Zweck der 
			unwirksamen Regelung m�glichst nahe kommt. </span></p>
			<p><strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
			9.5</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Erf�llungsort ist der Sitz von www.meineaerzte.at. </span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">9.6</span></strong><span lang="DE-AT" style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> 
			Gerichtsstand ist das sachlich zust�ndige Gericht am Sitz von 
			www.meineaerzte.at.</span></p>
			<p style="text-align:justify">
			<strong style="font-family: 'Times New Roman',serif">
			<span lang="DE-AT" style="font-size:12.0pt;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">9.7</span><span lang="DE-AT" style="font-size: 12.0pt; font-family: Arial,sans-serif; font-weight: normal"> 
			Es gilt ausschlie�lich �sterreichisches Recht.</span></strong></p>
			<p>&nbsp;</div>
            <br /><br />
            
            <form action="<?php echo $l->makeFormLink('update.php?step=3'); ?>" method="post">    
                <input type="checkbox" name="agb" class="selectbox" style="width:25px;"><label style="width:400px;">Ich habe die AGB gelesen und bin damit einverstanden.</label><br class="clr" />
                
                <br />
                <?php echo $l->makeLink("Zur�ck",$_SERVER['PHP_SELF']."?step=".($_GET['step']-1), "backlink"); ?>
                <input type="submit" value="anmelden" class="submit">
            </form>
            
            <?php break;           
            
        case 3:?>
            <h1>Hallo <?php echo $_SESSION["reg"]["anrede"].'   '.$_SESSION["reg"]["nachname"];?></h1>
            Ihnen wurde eine Mail an <?php echo $_SESSION["reg"]["mail"];?> gesendet, damit Sie Ihre Anmeldung aktivieren k�nnen.<br /><br/>
            Klicken Sie dazu einfach auf den gesendeten Link.
            
            Mail nicht angekommen? Bitte �berpr�fen Sie auch Ihren Spamordner.
            <?php 
            unset($_SESSION["reg"]);
            break;
         
        
        default:?>
            <h1>Anmelden</h1>
            <br />
            Um unser Portal im vollem Umfang nutzen zu k�nnen, melden Sie sich 
bitte hier <b><br>
kostenlos und unverbindlich</b> an.&nbsp;<br>
Als&nbsp;registrierter User&nbsp;haben Sie die M�glichkeit, in unserem&nbsp;Forum zu schreiben, 
auf Wunsch ein Profil anzulegen&nbsp;und pers�nlich formulierte Bewertungskommentare 
abzugeben.<br/><br/>
            Falls sie schon angemeldet sind, k�nnen Sie sich im Loginformular (siehe rechts) einloggen.
            <br /><br />
            <form action="<?php echo $l->makeFormLink('update.php?step=1'); ?>" method="post">
                <input type="submit" value="weiter" class="submit" /><br class="clr" />
            </form>
             
            <?php 
            break;
    }?>
        

<?php
include(INCLUDEDIR."footer.inc.php");
?>