<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>


<h1>Special Thanks</h1>
<br/>
Unser besonderer Dank f�r die liebe und tatkr�ftige Unterst�tzung geht an
<br>
<ul>
    <li>
        Alexandra K. aus Wien
    </li>
    <li>
        Ingrid K. aus Wien
    </li>
    <li>
        Markus L. aus D�rnbach - <?php echo $l->makeLink("www.lenna.at","http://www.lenna.at","","_blank");?>
    </li>
    <li>
        Alexandra H. aus Wels
    </li>
    <li>
        Patrizia F. aus Altheim
    </li>
    <li>
        Justina W. aus Linz
    </li>
</ul>

Des weiteren m�chten wir uns auch noch bei unseren Projektpartnern bedanken, 
die uns attraktive Preise zum Verlosen an unsere Besucher zur Verf�gung gestellt haben.
<ul>
    <li>
        Hotel Elisabethpark - <?php echo $l->makeLink("www.elisabethpark.at","http://www.elisabethpark.at","","_blank");?>

    </li>
    <li>
        together - <?php echo $l->makeLink("www.firmentraining.at","http://www.firmentraining.at","","_blank");?>

    </li>
</ul>
Last but not least m�chten wir auch der <?php echo $l->makeLink("nb-cooperation","http://www.nb-cooperation.de","","_blank");?> f�r die technische Unterst�tzung herzlichst danken!


<?php include(INCLUDEDIR."footer.inc.php");?>