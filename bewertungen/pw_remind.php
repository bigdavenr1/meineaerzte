<?PHP
include ("../inc/config.php");

// Fehlervariable l�schen
unset($_SESSION['err']);

// pr�fen ob Formular abgesendet wurde und kein Fehler besteht
if ($_POST['anfordern'] && !$_SESSION['err'])
{
    // Mail-Klasse laden
    include(INCLUDEDIR."std/email.class.php");
    
    // Wenn das eMail-Feld nicht ausgef�llt ist oder keine g�ltige eMail eingetragen wurde.
    if (trim($_POST['email'])=="" || !isMail($_POST["email"]))
    {
        $_SESSION['err'].="Sie haben vergessen eine eMail-Adresse einzugeben! Oder Ihre Eingabe ist nicht g�ltig!<br/><br/> Sollten Sie als Arzt noch keine eMail-Adresse eingetragen haben, so beachten Sie bitt eden Hinweis unten auf dieser Seite!";
    }
   
    // Wenn eMail-Adresse g�ltig
    else
    {
        // Datensatz holen
        $benutzerfac=new Benutzer();
        $benutzerfac->getByMail($_POST['email']);
        // Wenn Datensatz gefunden
        if ($benutzer=$benutzerfac->getElement())
        {
            // Wenn gefundener Datensatz aktiv ist
            if ($benutzer->aktiv!=0)
            {
                // zuf�lliges PAsswort erstellen
                $newpass=generateRandomKey(8);
                
                // Pr�fen ob Typ Unternehmer
                if ($benutzer->typ=="unternehmen")
                {
                    // unternehmerdaten holen
                    $datenfac=new Daten();
                    $datenfac->getByMail($_POST['email']);
                    // pr�fen ob Unternehmerdaten vorhanden sind
                    if ($daten=$datenfac->getElement())
                    {
                        // Passwort in den Unternhemerdaten aktualisieren 
                        $datenfac->update("pass_clear='".$newpass."'");
                    }
                    
                    // Wenn Unternehmerprofil nicht vorhanden ist
                    else
                    {
                        $_SESSION['err'].="Ihr Unternehmerprofil wurde nicht gefunden. Bitte wenden Sie sich an den Administrator!";
                    }
                }
                // Pr�fen ob Fehlervariable leer
                if (!$_SESSION['err'])
                {
                    // Benutzertabelle updaten
                    $benutzerfac->update("pass='".md5($newpass.$benutzer->salt)."'");
                    
                    // Mail versenden
                    $msg = "Hallo ".$benutzer->titel."  ".$benutzer->vorname." ".$benutzer->name.chr(10);
                    $msg .= chr(10);
                    $msg .= "Es wurde f�r Ihr Profil ein neues Passwort angefordert. Dieses wurde aus Sicherheitsgr�nden nur an diese eMail gesendet. Sie k�nnen dieses neue Passwort einfach in Ihrem gesch�tzten bereich �ndern.".chr(10).chr(10);
                    $msg .= "Ihr neues Passwort lautet: ".$newpass.chr(10).chr(10);
                    $msg .= 'Ihr meineaerzte-Team'.chr(10).chr(10);
                    $msg .='Diese eMail wurde automatisch erzeugt. Bitte antworten Sie nicht auf diese eMail, da dies nicht verarbeitet wird.';
                                                               
                    $emailfac = new Email(1);
                    $emailfac->setFrom("newpasswort@meineaerzte.at");
                    $emailfac->setTo($benutzer->email);
                    $emailfac->setSubject("neues Passwort f�r meineaerzte.at");
                    $emailfac->setContent($msg);
                    $emailfac->sendMail();
                    
                    header("Location:".WEBDIR."inc/msg.php?msg=newpass");
                }
            }
            // Wenn gefundener Datensatz inaktiv ist
            else $_SESSION['err'].="Ihr Profil ist derzeit inaktiv. Entweder wurden Sie gesperrt oder Sie haben Ihre eMail-Adresse noch nicht best�tigt!";
        }
        // Wenn kein Datensatz gefunden
        else $_SESSION['err'].="Ihre eMail-Adresse wurde nicht in unserem System gefunden!";
   }
} 

// Wenn noch nicht Anforder gedr�ckt wurde oder ein fehler vorhanden ist
if (!$_POST['anfordern'] || $_SESSION['err']) 
{
    include(INCLUDEDIR."header.inc.php");
    ?>
    <h1>Passwort erneuern</h1>
    Sie k�nnen hier Ihr Passwort erneuern lassen. Nach Eingabe Ihrer eMail-Adresse erhalten Sie das neue Passwort per eMail zugesandt. Dieses k�nnen Sie in Ihrem gesch�tzten bereich leicht wieder ver�ndern.<br/><br/>
    <?php if ($_SESSION['err'] ) echo '<span class="err">'.$_SESSION['err'].'</span><br/><br/>';?>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
    <fieldset>
    <label>eMail-Adresse</label><input type="text" name="email"/>
    </fieldset>
    <br/>
    <input type="submit" name="anfordern" value="Passwort anfordern" class="submit"/>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php");
}
?>
