<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Forenliste
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>
<script src="<?php echo WEBDIR;?>script/addon.js" language="javascript" type="text/javascript"></script>
<?php
// Wenn kein Forum ausgew�hlt ist, dann allgemeine �bersicht w�hlen
if (!$_GET["id"])
{?>
    
    <div class="headline"><div class="right"><?php echo $l->makeLink("Forumsverstoss melden",WEBDIR."bewertungen/kontakt.php?typ=forum");?></div>
        <?php
         echo $l->makeLink("Forum", "./forum.view.php");
        ?>
    </div>
    <br />
    <div style="float:left;">
    <?php 
                 
    // Gruppen einlesen
    $mforumfac = new Forum();
    $mforumfac->getByTyp(1);
    while ($mforum = $mforumfac->getElement())
    {
        
        $t=1;
        unset($linkstr);
        unset($allbeitrag);
        unset($alltopic);
        
        // Forenthemen auslesen
        $forumfac = new Forum();
        //$forumfac->getByKategorie($mforum->kategorie);
        $forumfac->getByKategorie($mforum->id);         // Referenz Id verwenden
        while ($forum = $forumfac->getElement())
        {
            // Stylesheet
            $class="";
            $t++;
            if ($t%2)$class='class="td1"';
            
            // Themen z�hlen
            $forummsgfac = new ForumMsg();
            //$forummsgfac->getByForumId($forum->id);
            $forummsgfac->getOrderByForumId($forum->id);             // sortiert nach Datum
            $beitrag = $forummsgfac->getElementCount();
            $allbeitrag += $beitrag; 
            
            // Datum und Benutzer zusammenbasteln                       
            $last = $forummsgfac->getElement();                       
            $lastdatum = explode(" ", $last->datum);
            $stringdatum   = usaToGer($lastdatum[0]).' '.$lastdatum[1].'<br />';
            
            $benutzerfac = new Benutzer();
            $benutzerfac->getById($last->user_id);                
           
            $stringdatum .= '<span style="font-size:10px; font-style:italic;">von ';
            if ($benutzer = $benutzerfac->getElement()) 
            {
                if($benutzer->typ =="benutzer")
                {
                    $stringdatum .= $l->makeLink($benutzer->nickname, WEBDIR."bewertungen/userprofil.php?id=".$benutzer->id,$benutzer->anrede);
                }
                if($benutzer->typ =="unternehmen")
                {
                    $datenfac = new Daten();
                    $datenfac->getByMail($benutzer->email);
                    if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($benutzer->email);
                    $daten = $datenfac->getElement();
                    $stringdatum .= $l->makeLink($daten->titel." ".$daten->vorname." ".$daten->famname, WEBDIR."bewertungen/showeintrag.php?id=".$daten->id);
                }
                if($benutzer->typ =="admin")
                {
                    $stringdatum .= '<span class="err"><b>admin</b></span>';
                }
            }
            else $stringdatum .= "Gast";
            $stringdatum .= '</span>';
            
            // Topics z�hlen            
            $forummsgfac->getTopicByForumId($forum->id);
            $topic = $forummsgfac->getElementCount();
            $alltopic += $topic;
            
            // Links zusammenbasteln            
            $linkstr .='<tr '.$class.'>';
            $linkstr .='<td style="text-align:left;">'.$l->makeLink($forum->name, $_SERVER["PHP_SELF"]."?id=".$forum->id).'</td><td>'.$stringdatum.'</td><td>'.$topic.'</td><td>'.$beitrag.'</td><td>';
            
            // Adminlinks
            if ($_SESSION["user"]->typ == "admin" )
            {
                $linkstr .= "&nbsp;";
                $linkstr .= $l->makeLink($icon_edit_small, WEBDIR."admin/forum/forum.edit.php?id=".$forum->id."&amp;mode=forum", "");
                $linkstr .= $l->makeLink($icon_delete_small, WEBDIR."admin/forum/forum.delete.php?id=".$forum->id."&amp;mode=forum", "");
                $linkstr .= "</td></tr>";
            }
        }
        ?>
        <table class="hauptforen" style=" border:1px dotted #000;width:605px;text-align:center;">
            <tr>
                <td class="td1" colspan="3" style=" height:12px; padding-top:5px; font-weight:bold;text-align:left;">
                    <?php 
                    echo htmlspecialchars($mforum->name);
                    // Adminlinks            
                    if ($_SESSION["user"]->typ == "admin")
                    {
                         echo "&nbsp;";
                         echo $l->makeLink($icon_edit_small, WEBDIR."admin/forum/forum.edit.php?id=".$mforum->id."&amp;mode=forum", "").'&nbsp;';
                         echo $l->makeLink($icon_arrow_down_small, WEBDIR."admin/forum/forum.update.php?id=".$mforum->id."&amp;mode=sortup", "").'&nbsp;';
                         echo $l->makeLink($icon_arrow_up_small, WEBDIR."admin/forum/forum.update.php?id=".$mforum->id."&amp;mode=sortdown", "").'&nbsp;';
                         echo $l->makeLink($icon_delete_small, WEBDIR."admin/forum/forum.delete.php?id=".$mforum->id."&amp;mode=forum", "");
                    }
                    echo '<br /><span style="font-weight:normal; font-size:11px; font-style:italic;">'.htmlspecialchars($mforum->beschreibung).'</span>';
                    ?>
                </td>
                <td class="td1" style=" height:12px; padding-top:5px; font-size:10px; text-align:right;">
                    (Themen: <?php echo $alltopic;?><br/> Beitr�ge: <?php echo $allbeitrag;?>)
                </td>
            </tr>
            <tr>
               <th style="width:250px">
                   Forum
               </th>
               <th >
                   letzter Beitrag
               </th>
               <th style="width:65px">
                   Themen
               </th>
               <th style="width:65px">
                   Beitr�ge
               </th>
               <?php  if($_SESSION["user"]->typ == "admin" )
                      {?>
                       <th>
                        Links
                       </th>
               <?php  } ?>
            </tr>
            <?php echo $linkstr;?>
        </table>
        <br />
        <?php 
    }

    // Adminlink setzen
    if ($_SESSION["user"]->typ == "admin")
    {
        echo "<br />".$l->makeLink($icon_edit." Neues Haupt - oder Unterforum er�ffnen", WEBDIR."admin/forum/forum.edit.php?mode=forum");
    }
    ?>
    </div>
<?php
}
 
// Wenn Thema ausgew�hlt, aber noch kein Topic
if ($_GET["id"] && !$_GET["rid"])
{
    ?>
    <div class="headline"><div class="right"><?php echo $l->makeLink("Forumsverstoss melden",WEBDIR."bewertungen/kontakt.php?typ=forum");?></div>
        <?php
        // Forumname auslesen
        $forumfac = new Forum();
        $forumfac->getById($_GET["id"]);
        $forum = $forumfac->getElement();
            
        echo $l->makeLink("Forum", "./forum.view.php")." &raquo; ".$forum->name;
        ?>
       
    </div>
    <br />
    <div style="float:left;">
        <table class="forum" cellpadding="2" cellspacing="1" style="width:605px;;border:1px dotted #000;">
            <tr>
                <td class="td1" colspan="4">
                    Themen im Forum : <?php echo $forum->name;?>
                </td>
            </tr>
            <tr>
                <th class="header_l">Thema</th>
                <th class="header_r">Letzter Beitrag</th>
                <th class="header_m">Antworten</th>
                <th class="header_m">Aufrufe</th>
                <?php if($_SESSION["user"]->typ == "admin" || $_SESSION["rechte"]["forum"]) echo "<th class='header_m'>Links</th>";  ?>
            </tr>
            
            <?php
            // Threads und Messages auflisten
            $forummsgfac = new ForumMsg(20, "0");
            $forummsgfac->getTopicByForumId($_GET["id"]);
            $t=1;
            while ($fmsg = $forummsgfac->getElement())
            {
                $class="";
                $t++;
                if ($t%2) $class='class="td1"';
                $forummsgfac2 = new ForumMsg();
                $forummsgfac2->getByForumIdAndSuperIdLast($_GET["id"], $fmsg->super_id);
                $forummsg2 = $forummsgfac2->getElement();
                $antworten = $forummsgfac2->getElementCount()-1;

                $benutzerfac = new Benutzer();
                $benutzerfac->getById($fmsg->user_id);
                
                
                echo '<tr>';
                    echo '<td '.$class.'>';
                    if ($_SESSION["user"]->typ == "admin" || $_SESSION["rechte"]["forum"][$_GET['id']]) // Admin oder Forumsrecht f�r Benutzer
                    {
                        echo $l->makeLink($icon_delete, WEBDIR."admin/forum/forum.delete.php?id=".$_GET["id"]."&amp;rid=".$fmsg->super_id."&amp;mode=topic", "");
                    }
                    
                    // Threadtitel / Erstellt
                    echo $l->makeLink($fmsg->titel, $_SERVER["PHP_SELF"]."?id=".$_GET["id"]."&amp;rid=".$fmsg->super_id."&amp;offset0=".$_GET["offset0"])."<br>";
                    echo '<span style="font-size:10px; font-style:italic;">erstellt von ';
                    if ($benutzer = $benutzerfac->getElement())
                    {
                        if($benutzer->typ =="benutzer")
                        {
                            echo $l->makeLink($benutzer->nickname, WEBDIR."bewertungen/userprofil.php?id=".$benutzer->id,$benutzer->anrede);
                        }
                        if($benutzer->typ =="unternehmen")
                        {
                            $datenfac = new Daten();
                            $datenfac->getByMail($benutzer->email);
                            if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($benutzer->email);
                            $daten = $datenfac->getElement();
                            echo $l->makeLink($daten->titel." ".$daten->vorname." ".$daten->famname, WEBDIR."bewertungen/showeintrag.php?id=".$daten->id);
                        }
                        if($benutzer->typ =="admin")
                        {
                            echo "<span class='err'>admin</span>";
                        }
                    }
                    else echo "Gast";
                    echo '</span></td>';
                    
                    $benutzerfac = new Benutzer();
                    $benutzerfac->getById($forummsg2->user_id);
                    
                    $datum = explode(" ", $forummsg2->datum);
                    
                    echo '<td '.$class.'>'.usaToGer($datum[0]).' '.$datum[1].'<br/>';
                    echo '<span style="font-size:10px; font-style:italic;">von ';
                    if ($benutzer = $benutzerfac->getElement())
                    {
                        if($benutzer->typ =="benutzer")
                        {
                            echo $l->makeLink($benutzer->nickname, WEBDIR."bewertungen/userprofil.php?id=".$benutzer->id,$benutzer->anrede);
                        }
                        
                        if($benutzer->typ =="admin")
                        {
                            echo "<span class='err'>admin</span>";
                        }
                        
                        if($benutzer->typ =="unternehmen")
                        {
                            $datenfac = new Daten();
                            $datenfac->getByMail($benutzer->email);
                            if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($benutzer->email);
                            $daten = $datenfac->getElement();
                            echo $l->makeLink($daten->titel." ".$daten->vorname." ".$daten->famname, WEBDIR."bewertungen/showeintrag.php?id=".$daten->id);
                        }
                    } 
                    else echo "Gast";
                    echo '</span></td>';
                    
                    echo '<td style="text-align:center" '.$class.'>'.$antworten.'</td>';
                    echo '<td style="text-align:center" '.$class.'>'.count(explode("|", $fmsg->aufrufe)).'</td>';
                    if($_SESSION["user"]->typ == "admin" || $_SESSION["rechte"]["forum"][$_GET['id']])
                    {
                      echo '<td style="text-align:center" '.$class.'>'.$l->makeLink($icon_delete_small, WEBDIR.'admin/forum/forum.delete.php?id='.$_GET["id"].'&amp;rid='.$fmsg->super_id.'&amp;mode=topic', "").'<br />';
                      echo $l->makeLink($icon_edit_small, WEBDIR.'admin/forum/forum.edit.php?id='.$_GET["id"].'&amp;rid='.$fmsg->super_id.'&amp;mode=msg', "").'</td>';
                    }
                echo '</tr>';
            }
            ?>
    
        </table>
    
        <br/>
        
        <div class="contentboxsmall">
            
            <div class="contentboxsmalltitel">Seiten</div>
            
            <?php echo $forummsgfac->getHtmlNavi("std", "&amp;id=".$_GET["id"]); ?>
            
        </div>
    </div>
    <br class="clr"/>
        <?php
        echo $l->makeLink("Zur�ck", WEBDIR."bewertungen/forum.view.php", "backlink");
        if ($_SESSION["user"]) echo $l->makeLink('Neues Topic', WEBDIR."user/forum/forum.edit.php?id=".$_GET["id"]."&amp;mode=newmsg", "backlink");
    }
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Eintr�ge zum Thema
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    if ($_GET["id"] && $_GET["rid"])
    {   
        //
        // Update Aufrufe des Topics
        //
        
        
        if ($_SESSION["user"])
        {
            $forummsgfac = new ForumMsg();
            $forummsgfac->getByForumIdAndSuperId($_GET["id"], $_GET["rid"]);
            $forummsg = $forummsgfac->getElement();
            if($forummsg) // nicht falsche rid
            {
                       
                $userklick = explode("|", $forummsg->aufrufe);
            
                if (!in_array($_SESSION["user"]->id, $userklick))
                {
                    array_push($userklick, $_SESSION["user"]->id);
                    $forummsgfac->update("aufrufe='".implode("|", $userklick)."'");
                }
            }
            else $falscherid = "<h1>Gesuchter Eintrag ist nicht vorhanden!</h1>";
        }    
        
        
        //
        // Zeige Eintr�ge zum Topic
        //
        ?>
        
        
        <div class="contentboxsmall">
            
            <div class="headline"><div class="right"><?php echo $l->makeLink("Forumsverstoss melden",WEBDIR."bewertungen/kontakt.php?typ=forum");?></div>
            
            <?php
            $forumfac = new Forum();
            $forumfac->getById($_GET["id"]);
            $forum = $forumfac->getElement();
            
            $forummsgfac = new ForumMsg();
            $forummsgfac->getTopicByForumIdAndSuperId($_GET["id"], $_GET["rid"]);
            $forummsg = $forummsgfac->getElement();
            if($forummsg)
            {
                echo $l->makeLink("Forum", "./forum.view.php")." &raquo; ".$l->makeLink($forum->name, "./forum.view.php?id=".$_GET["id"]."&amp;offset0=".$_GET["offset0"])." &raquo; ".$forummsg->titel;
            }
            ?>
            </div>
        </div>
        
        
       
        <br />
        
        <table cellpadding="0" cellspacing="1" class="eintrag" style="border:1px dotted #000;width:605px;;border-bottom:none" >
        
        
        <?php
        $forummsgfac = new ForumMsg(10, "1");
        $forummsgfac->getByForumIdAndSuperId($_GET["id"], $_GET["rid"]);
        $t=0;
        $nummer = 0;
        while ($fmsg = $forummsgfac->getElement())
        {
           $class="";
           $t++;
           $nummer ++;
            if ($t%2) $class='class="td1"';
            
            $benutzerfac = new Benutzer();
            $benutzerfac->getById($fmsg->user_id);
            if ($benutzer = $benutzerfac->getElement())
            {
            

            ?>
        
            <tr>
                
                <td rowspan="2" <?php echo $class ?> valign="top" style="border-bottom:1px dotted #000;width:120px;">
                
                    <?php

                    echo '<br class="clr"/>';
                    
                    //echo $l->makeLink($benutzer->nickname, WEBDIR."bewertungen/userprofil.php?id=".$benutzer->id,$benutzer->anrede, ($benutzer->geschlecht=="m"?"mcolor":"wcolor"))."<br/>";
                    if($benutzer->typ =="benutzer" )
                    {
                        
                        echo $l->makeLink($benutzer->nickname, WEBDIR."bewertungen/userprofil.php?id=".$benutzer->id,$benutzer->anrede, ($benutzer->geschlecht=="m"?"mcolor":"wcolor"))."<br class='clr'/>";
                        $rechte = unserialize($benutzer->rechte);
                        if($rechte["forum"][$_GET["id"]]) echo "<b>Moderator</b><br />";
                                              
                        // pr�fen ob Bild vorhanden
                        if ($benutzer->bild!="")
                        {
                            // Grafiktyp auslesen
                            $typ=getimagesize(LOCALDIR."images/user/".$benutzer->bild) ;
                          
                            // je nach typ Endung bestimmen   
                            if ($typ[2]==1) $endung=".gif";
                            if ($typ[2]==2) $endung=".jpg"; 
                                
                            // Reinnamen ohne Endung
                            $name=basename($benutzer->bild,$endung);
                                
                            // Gr��e des Thumbnails auslesen
                            $size=getimagesize(LOCALDIR."images/user/".$name."_thumb".$endung);
                            
                            // Gr��e pr�fen und eventuell verkleinern
                            // breite 80 h�he 80
                            if ($size[0] > $size[1])
                            {
                                $thumbBreite = 80;
                                $thumbHoehe = (80*$size[1])/$size[0];
                            }
                            else
                            {
                                $thumbHoehe = 80;
                                $thumbBreite= (80*$size[0])/$size[1];
                            }
                                                  
                                
                            // HTML Bild schreiben
                            echo '<img src="'.WEBDIR.'images/user/'.$name.'_thumb'.$endung.'" width="'.$thumbBreite.'" height="'.$thumbHoehe.'" style="border:1px dotted #000;float:left;margin-right:5px;" alt="" />';
                        }
                    }
                    if($benutzer->typ =="admin")
                    {
                        echo "<span class='err'><b>admin</b></span>";
                    } 
                    
                    if($benutzer->typ =="unternehmen")
                    {
                        $datenfac = new Daten();
                        $datenfac->getByMail($benutzer->email);
                        if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($benutzer->email);
                        $daten = $datenfac->getElement();
                        echo $l->makeLink($daten->titel." ".$daten->vorname." ".$daten->famname, WEBDIR."bewertungen/showeintrag.php?id=".$daten->id)."<br />";
                    }
                    
                    
                    $forummsgfac2 = new ForumMsg();
                    $forummsgfac2->getByUser($benutzer->id);
                    
                    echo "<br />Beitr�ge: ".$forummsgfac2->getElementCount()."<br />";
                    echo "<br />";
                    ?>
                    
                </td>
                <?php
                } 
                else
                {
                ?>
                    <tr>
                
                <td rowspan="2" <?php echo $class ?> valign="top" style="border-bottom:1px dotted #000">
                    Gast
                </td>
                  
                <?php
                }
                ?>
                
                <td class="headline" style="width:250px;" valign="top">
                
                    <?php
                    echo "#".$nummer."&nbsp;&nbsp;";  // statt $fmsg->id
                    
                    $datum = explode(" ", $fmsg->datum);
                    echo usaToGer($datum[0])." - ".$datum[1];
                    ?>
                    
                </td>
                
                
                <td class="headline" valign="top" align="right" >
                
                    <?php
                    if ($_SESSION["user"])
                    {
                        if ($_SESSION["user"]->id == $fmsg->user_id && $_SESSION['user']->typ!="admin" && !$_SESSION["rechte"]["forum"][$_GET['id']])
                        {
                        /*if ($_GET["mode"] == "msgedit" && $_GET["mid"] == $fmsg->id)
                            echo $l->makeLink($icon_edit_small."&nbsp;normal", $_SERVER["PHP_SELF"]."?id=".$_GET["id"]."&amp;rid=".$fmsg->super_id."&amp;offset0=".$_GET["offset0"]."&amp;offset1=".$_GET["offset1"])."&nbsp;";
                        else*/
                            echo $l->makeLink($icon_edit_small."&nbsp;bearbeiten", $_SERVER["PHP_SELF"]."?id=".$_GET["id"]."&amp;rid=".$fmsg->super_id."&amp;mid=".$fmsg->id."&amp;mode=msgedit&amp;offset0=".$_GET["offset0"]."&amp;offset1=".$_GET["offset1"])."&nbsp;";
                        }
                        echo $l->makeLink($icon_edit_small."&nbsp;zitieren", WEBDIR."user/forum/forum.edit.php?id=".$_GET["id"]."&amp;rid=".$_GET["rid"]."&amp;mode=msg&amp;zitat=".$fmsg->id)."&nbsp;";
                    }

                    if ($_SESSION["user"]->typ == "admin" || $_SESSION["rechte"]["forum"][$_GET['id']])
                    {
                        echo $l->makeLink($icon_edit_small."&nbsp;bearbeiten", $_SERVER["PHP_SELF"]."?id=".$_GET["id"]."&amp;rid=".$fmsg->super_id."&amp;mid=".$fmsg->id."&amp;mode=msgedit&amp;offset0=".$_GET["offset0"]."&amp;offset1=".$_GET["offset1"])."&nbsp;";
                        echo $l->makeLink($icon_delete_small."&nbsp;l�schen", WEBDIR."admin/forum/forum.delete.php?id=".$_GET["id"]."&amp;rid=".$fmsg->super_id."&amp;mid=".$fmsg->id."&amp;mode=msg&amp;offset0=".$_GET["offset0"]."&amp;offset1=".$_GET["offset1"])."&nbsp;";
                    }
                    ?>
                    
                </td>
                
            </tr>
            
            <tr >
                <td colspan="2" <?php echo $class ?> valign="top" style="border-bottom:1px dotted #000">
                
                    <?php
                    if ($_GET["mode"] == "msgedit" && $_GET["mid"] == $fmsg->id && ($_SESSION["user"]->id == $fmsg->user_id || $_SESSION["user"]->typ == "admin" || $_SESSION["rechte"]["forum"][$_GET["id"]]))
                    {
                        ?>
                        <form action="<?php echo $l->makeFormLink(WEBDIR.'user/forum/forum.update.php'); ?>" method="post" class="forum">
                        
                        <input type="hidden" name="mode" value="msg">
                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                        <input type="hidden" name="mid" value="<?php echo $_GET['mid'];?>">
                        <input type="hidden" name="altid" value="<?php echo $fmsg->user_id;?>">
                        <input type="hidden" name="rid" value="<?php echo $_GET['rid'];?>">
                        <input type="hidden" name="date" value="<?php echo $fmsg->datum;?>">
                        <input type="hidden" name="offset0" value="<?php echo $_GET['offset0'];?>">
                        <input type="hidden" name="offset1" value="<?php echo $_GET['offset1'];?>">
                        <input type="hidden" name="edittimes" value="<?php echo $fmsg->edit_times;?>">
                        <input type="hidden" name="titel" value="<?php echo $fmsg->titel;?>">
                        
                        <?php 
                        $htmlarea = new HtmlArea("area");
                        echo $htmlarea->getHtmlButtons();
                        ?>
                        
                        <textarea id="area" class="internedit" name="text"><?php echo br2nl(stripslashes($fmsg->text));?></textarea>
                        
                        <br /><br class="spacer" />
                        
                        <input type="submit" value="speichern" class="submit">
                        
                        <br class="clr" /><br class="spacer" />
                        
                        </form>
                        <?php
                    }
                    else
                    {
                        
                        $tParser = new TextParser($fmsg->text);
                        $tParser->parse();
                        echo wordwrap(stripslashes($tParser->getParsedText()), 70, "\n", true);
                    }
                    ?>
                    
                    <br /><br />
                    <?php if ($fmsg->edit_times)
                    {
                        $datum = explode(" ",$fmsg->edit_datum);    
                        ?>
                        
                        <span style="font-size:10px;"><b><?php echo $fmsg->edit_times;?></b> mal bearbeitet. Zuletzt: <?php echo usaToGer($datum[0])." - ".$datum[1];?></span>
                        <?php 
                    }
                    ?>
                    
                    
                </td>
            </tr>

            
        <?php
        }   // end while
        ?>
        
        
        </table>
        <br />
        
        <div class="contentboxsmall">
            <?php
            if(isset($falscherid))
            {
                echo $falscherid;
            }
            else
            {
            ?>
            <div class="contentboxsmalltitel">Seiten</div>
            <?php
                echo $forummsgfac->getHtmlNavi("std", "&amp;id=".$_GET["id"]."&amp;rid=".$_GET["rid"]."&amp;offset0=".$_GET["offset0"]); 
            }
            ?>
        </div>
        

        
        <?php
        if(!isset($_SESSION['user'])) echo '<br />Hinweis: Sie m�ssen zum Schreiben im Forum eingeloggt sein!';
            echo $l->makeLink("Zur�ck", WEBDIR."bewertungen/forum.view.php?id=".$_GET["id"]."&amp;offset0=".$_GET["offset0"], "backlink");
        if($_SESSION['user'] && !isset($falscherid))echo $l->makeLink('Antworten', WEBDIR."user/forum/forum.edit.php?id=".$_GET["id"]."&amp;rid=".$_GET["rid"]."&amp;mode=msg", "backlink");  
        ?>

        <br class="clr" /><br /><br />
    
    
    <?php 
    }
    
include(INCLUDEDIR."footer.inc.php");
?>