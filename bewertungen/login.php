<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// login/login.php - Schreibt die $_SESSION["user"] und den Eintrag in die Onlineliste
//////////////////////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");     
    
// Benutzer wird angemeldet und in $_SESSION["user"] geschrieben
//////////////////////////////////////////////////////////////////////////////////////////

if (!$_GET["check"])
{
    unset($_SESSION["user"]);
            
    $benutzerfac = new Benutzer();
    //$benutzerfac->getByNick(htmlentities(strip_tags($_POST['name'])));
    $benutzerfac->getByMail(htmlentities(strip_tags($_POST['name'])));
    $testbenutzer = $benutzerfac->getElement();
	
    if(!$testbenutzer)// kein Datensatz f�r Emailadress
	{
	    header("Location: ".WEBDIR."inc/msg.php?msg=wronglogin");
	}
    
    $benutzerfac->getByLogin(htmlentities(strip_tags($_POST['name'])), md5(htmlentities(strip_tags($_POST['pass'])).$testbenutzer->salt));
        
    if ($benutzer = $benutzerfac->getElement())
    {
    
        if ($benutzer->aktiv) // Benutzeraktivierung pr�fen
        {
            $benutzer->pass = "korrekt";                // f�r Sessionvariable �berschreiben
            $benutzer->salt = "vorhanden";              // Salt �berschreiben
            $benutzerfac->insertIntoSession($benutzer);
            $data[] = $online_id;                       // id
            $data[] = $benutzer->id;                    // user_id
            $data[] = time();                           // time_login
            $data[] = "online";                         // status
            $data[] = "";                               // location
            $data[] = "";                               // location_ident
                    
            header("Location: ".$_SERVER["PHP_SELF"]."?".SID."&sv=1&check=1");
        }
        
        else
        {
            if($benutzer->typ == 'werbung')
            {
                header("Location: ".WEBDIR."inc/msg.php?msg=promoinactiv");
            }
            else  header("Location: ".WEBDIR."inc/msg.php?msg=inactiv");
        }
    }
    
    else
    {
        header("Location: ".WEBDIR."inc/msg.php?msg=wronglogin");
    }
}

else
{ 
    // $_SESSION - variable schreiben mit Status
    if($_SESSION['user']->typ=="unternehmen")
    {
        $datenfac = new Daten();
        $datenfac->getByMail($_SESSION['user']->email);
        if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
        if($daten = $datenfac->getElement())
        {
            $_SESSION['ustatus']= $daten->status;
        }
    }  
    // COOKIE - CHECK
    if ($_COOKIE[session_name()])
    {
        if ($_SESSION['user']->typ=="admin") header("Location: ".$l->makeUrl(WEBDIR."admin/index.php")); 
        if ($_SESSION['user']->typ=="unternehmen") header("Location: ".$l->makeUrl(WEBDIR."unternehmen/index.php"));
        if ($_SESSION['user']->typ=="benutzer") header("Location: ".$l->makeUrl(WEBDIR."user/index.php")); 
        if ($_SESSION['user']->typ=="werbung") header("Location: ".$l->makeUrl(WEBDIR."werbekunden/index.php"));    
    }
            
    else 
    {
        include(INCLUDEDIR."header.inc.php");
        ?>
        Hinweis: Du hast in deinem Browser Cookies deaktiviert.<br><br>
        Hier gehts <a href="<?php echo $l->makeFormLink(WEBDIR.'users/index.php'); ?>">weiter...</a> 
        <?php
        include(INCLUDEDIR."footer.inc.php");
    }
}    
?>