<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>


<h1>FAQ (H&auml;ufig gestellte Fragen)</h1>
<ul>
	<li><b><a href="#1">Was ist das Ziel dieser Website?</a></b></li>
	<li><b><a href="#2">Wie bewerte ich meinen Arzt?</a></b></li>
	<li><b><a href="#3">Kann jeder hier seinen Arzt bewerten?</a></b></li>
	<li><b><a href="#4">Wie formuliere ich meine Kommentare?</a></b></li>
	<li><b><a href="#5">Kann mein Arzt sehen, dass ich Ihn bewertet habe?</a></b></li>
	<li><b><a href="#6">Wie viele Bewertungen f&uuml;r eine Praxis sind n&ouml;tig, um einen Durchschnitt zu ermitteln?</a></b></li>
	<li><b><a href="#7">Wie ernst sollte ich die abgegebenen Bewertungen hier nehmen?</a></b></li>
	<li><b><a href="#8">Warum wird meine Bewertung nicht angezeigt?</a></b></li>
	<li><b><a href="#9">Ich bin Arzt! Was hab ich auf diesem Portal f&uuml;r M&ouml;glichkeiten und wie kann ich mein Profil selbst verwalten und aktualisieren?</a></b></li>
	<li><b><a href="#10">Kann ich auch euch bewerten?</a></b><br>
&nbsp;</li>
	</ul>
	<p><b>Was ist das Ziel dieser Webseite?<a name="1"></a></b><br/><br/>Das Portal www.meineaerzte.at ist ausschlie&szlig;lich von Patienten f&uuml;r Patienten gedacht.<br/>Wir versuchen, f&uuml;r unsere Besucher relevante Daten aufzubereiten und Ihnen hier bereit zu stellen.<br/>Wir sind auch sehr an Ihrer pers&ouml;nlichen Meinung zu unserem Projekt interessiert und freuen uns &uuml;ber Lob, Anregungen und Kritik!<br/>Benutzen Sie bitte hierzu unser <?php echo $l->makeLink("Kontaktformular",WEBDIR."bewertungen/kontakt.php");?>! 
	<br/><br/><b>Wie bewerte ich meinen Arzt?<a name="2"></a></b><br/><br/>Nehmen Sie sich nach Ihrem Arztbesuch die Zeit und versuchen Sie, Ihren Arzt so fair und sachlich wie m&ouml;glich zu beurteilen.<br/>Schreiben Sie hier sowohl Ihre positiven als auch Ihre negative Eindr&uuml;cke.<br/>Bewerten Sie einen Arzt, mit dem Sie nicht zufrieden waren nicht unmittelbar nach Ihrem Besuch!<br/>Machen Sie sich bitte erst Gedanken dar&uuml;ber, was Sie wirklich gest&ouml;rt hat und versuchen Sie, dies in konstruktiver Kritik zu &auml;u&szlig;ern.<br/>
	<br/>Nur so kann ein Arzt gegebenenfalls seine Leistungen verbessern!<br/>Auch wenn eine Beschwerde leichter f&auml;llt, vergessen Sie auch auf das Lob nicht ganz! 
	<br/><br/><b>Kann jeder hier seinen Arzt bewerten?<a name="3"></a></b><br/>
	<br/>Grunds&auml;tzlich steht dieses Portal jedem Patienten kostenlos zur Verf&uuml;gung!<br/>Als unregistrierter Nutzer haben Sie die M&ouml;glichkeit, in unserem Forum zu lesen, die Suchfunktion zu nutzen und &Auml;rzte per vordefinierten Fragebogen zu bewerten.<br/>Als registrierter Nutzer haben Sie zudem die M&ouml;glichkeit in unserem Forum Beitr&auml;ge zu schreiben, sowie ein pers&ouml;nlich formuliertes Bewertungskommentar abzugeben.<br/>
	<br/><b>Wie formuliere ich meine Kommentare?<a name="4"></a></b><br/><br/>Bitte formulieren Sie Ihre Gedanken m&ouml;glichst sachlich.<br/>Gebrauchen Sie keine Kraftausdr&uuml;cke und stellen Sie bitte keine Vermutungen an.<br/>Schreiben Sie ausschlie&szlig;lich das, was Sie pers&ouml;nlich erlebt und empfunden haben.<br/>Sie haben die M&ouml;glichkeit, auf diesem Portal 
	<b>Ihre eigene Meinung</b> kund zu tun, beachten Sie aber bitte den schmalen Grad zwischen eigener Meinung und Rufmord.<br/>Versuchen Sie bitte m&ouml;glichst Formulierungen wie "Meiner Meinung nach" oder "Ich denke" zu verwenden.<br/>
	<br/><b>Kann mein Arzt sehen, dass ich Ihn bewertet habe?<a name="5"></a></b><br/>
	<br/>Nein! Die Bewertung erfolgt entweder anonym, oder unter einem von Ihnen frei gew&auml;hltem Nicknamen.<br/>Beachten Sie jedoch bitte, das Ihre pers&ouml;nlichen Daten - die Sie bei Ihrer Registration angeben - in unserem System gespeichert werden.<br/>Wir behalten uns au&szlig;erdem vor, diese im Falle von Missbrauch an die Beh&ouml;rden weiter zu geben.<br/>
	<br/>Registrierte User k&ouml;nnen auf Wunsch Ihre Eckdaten wie Alter, Geschlecht, PLZ, Homepage und Email Adresse, sowie ein Profilfoto anzeigen lassen, um so mehr Transparenz in das System zu bringen.
	<br/><br/><b>Wie viele Bewertungen f&uuml;r eine Praxis sind n&ouml;tig, um einen Durchschnitt zu ermitteln?<a name="6"></a></b></br>
	<br/>Um einen wirklichen Durchschnitt zu ermitteln ist nat&uuml;rlich mehr als eine Bewertung erforderlich.<br/>Wir m&ouml;chten Ihnen aber auch eine einzelne Bewertung nicht vorenthalten, deshalb wird Ihnen diese ebenfalls in der &Uuml;bersicht angezeigt.<br/>Bedenken Sie jedoch bitte, dass s&auml;mtliche in diesem Portal abgegebene Meinungen subjektiv sind und somit nicht unbedingt den Tatsachen entsprechen.<br/>
	<br/><b>Wie ernst sollte ich die abgegebenen Bewertungen hier nehmen?<a name="7"></a></b></br/>
	<br/>Bitte bedenken Sie, dass die hier abgegebenen Bewertungen subjektive Einsch&auml;tzungen sind.<br/>Jeder Mensch hat andere Bed&uuml;rfnisse und sieht die Welt mit anderen Augen.<br/>Lassen Sie sich durch einzelne negative Bewertungen nicht beeinflussen!<br/>
	<br/>Machen Sie sich selbst ein Bild!<br/><br/><b>Warum wird meine Bewertung nicht angezeigt?<a name="8"></a></b><br/>
	<br/>Um keine Beleidigungen oder falsche Tatsachenbehauptungen zu 
    ver�ffentlichen, �berpr�fen wir von Zeit zu Zeit die abgegebenen Kommentare. <br>
    Auch hat bei uns jeder User die M�glichkeit, Kommentare zu melden!<br>
    Wird Ihre abgegebene Bewertung nicht angezeigt, kann dies folgende Gr�nde haben:
    <br>
    <br>
    <li>Wir haben Bedenken, Ihr Kommentar freigeschaltet zu lassen!<br>
    &nbsp;&nbsp;&nbsp;&nbsp; In diesem Fall werden wir uns gegebenenfalls per Email mit Ihnen in Kontakt setzen.</li><br>
  
<li>    Beachten Sie, das es eine Vorschaufunktion gibt. <br>
&nbsp;&nbsp;&nbsp;&nbsp; Haben Sie vielleicht 
    vergessen, Ihre Bewertung danach abzuschicken?</li>    <br>  <br> 
    Sollte keiner dieser Punkte zutreffen, setzten Sie sich bitte �ber unser 
    Kontaktformular mit uns in Verbindung!<br/>


<b><br>
	Ich bin Arzt! Was hab ich auf diesem Portal f&uuml;r M&ouml;glichkeiten und wie kann ich mein Profil selbst verwalten und aktualisieren?<a name="9"></a></b><br/><br/>
Nat&uuml;rlich k&ouml;nnen Sie Ihr Profil hier bei uns <b>kostenfrei</b> selbst verwalten und erweitern.<br/>
Sie haben die M&ouml;glichkeit, Ihre "Unternehmensdaten" zu vervollst&auml;ndigen, Suchbegriffe zu definieren, Fotos hochzuladen sowie Ihre Ordinationszeiten online zu stellen.<br/>
Schreiben Sie uns hierzu einfach per Email (von der offiziellen Email Adresse Ihrer Praxis) oder am Postweg eine kurze Anfrage, die Ihren vollen Namen und Ihre Anschrift enthalten.<br/>
Gerne senden wir Ihnen nach &Uuml;berpr&uuml;fung Ihrer Angaben die Zugangsdaten f&uuml;r Ihr Ordinationsprofil zu.<br/>
Alle von uns angebotenen Grundfunktionen stehen Ihnen <b>kostenfrei und ohne Verpflichtungen</b> zur Verf&uuml;gung.<br/>
Auf Wunsch gibt es auch die M&ouml;glichkeit einer Premium Mitgliedschaft, mit der Sie uns zum einen helfen, dieses Portal am Leben zu erhalten und welche zum anderen auch viele Vorteile f&uuml;r Sie bietet.<br/>
N&auml;here Infos hierzu finden Sie in Ihrem Ordinationsprofil.<br/><br/>
<b>Kann ich auch euch bewerten?<a name="10"></a></b><br/><br/>
Nat&uuml;rlich k&ouml;nnen und sollen Sie das auch. Nur so k&ouml;nnen wir unseren Service laufend verbessern und an Ihre Bed&uuml;rfnisse anpassen. Nutzen Sie hierzu bitte unseren <?php echo $l->makeLink("Fragebogen",WEBDIR."bewertungen/zufriedenheitsformular.php");?><?php include(INCLUDEDIR."footer.inc.php");?>