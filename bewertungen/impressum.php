<?PHP
include ("../inc/config.php");
include(INCLUDEDIR."header.inc.php");?> 
<h1>Impressum</h1>
<br/>
<b>Anschrift:</b><br/>
Langer Oliver<br/>
Projekt www.meineaerzte.at<br/>
Schubertstrasse 41<br/>
A-4600 Wels<br/>
<br/>
Ansprechpartner: Langer Oliver<br/>
<br/>
<b>T:</b> 0676/4192287<br/>
<b>E:</b> info@meineaerzte.at<br/>
<b>H:</b> www.meineaerzte.at<br/>
<br/>
<h1>Inhalt des Onlineangebotes</h1>
<br/>
Der Autor �bernimmt keinerlei Gew�hr f�r die Aktualit�t, Korrektheit, Vollst�ndigkeit oder Qualit�t der bereitgestellten Informationen. Haftungsanspr�che gegen den Autor, welche sich auf Sch�den materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollst�ndiger Informationen verursacht wurden, sind grunds�tzlich ausgeschlossen, sofern seitens des Autors kein nachweislich vors�tzliches oder grob fahrl�ssiges Verschulden vorliegt.
<br/><br/>
Alle Angebote sind freibleibend und unverbindlich. Der Autor beh�lt es sich ausdr�cklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ank�ndigung zu ver�ndern, zu erg�nzen, zu l�schen oder die Ver�ffentlichung zeitweise oder endg�ltig einzustellen.
Verweise und Links
<br/><br/>
Bei direkten oder indirekten Verweisen auf fremde Webseiten ("Hyperlinks"), die au�erhalb des Verantwortungsbereiches des Autors liegen, w�rde eine Haftungsverpflichtung ausschlie�lich in dem Fall in Kraft treten, in dem der Autor von den Inhalten Kenntnis hat und es ihm technisch m�glich und zumutbar w�re, die Nutzung im Falle rechtswidriger Inhalte zu verhindern.
<br/><br/>
Der Autor erkl�rt hiermit ausdr�cklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die aktuelle und zuk�nftige Gestaltung, die Inhalte oder die Urheberschaft der verlinkten/verkn�pften Seiten hat der Autor keinerlei Einfluss. Deshalb distanziert er sich hiermit ausdr�cklich von allen Inhalten aller verlinkten /verkn�pften Seiten, die nach der Linksetzung ver�ndert wurden. Diese Feststellung gilt f�r alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie f�r Fremdeintr�ge in vom Autor eingerichteten G�steb�chern, Diskussionsforen, Linkverzeichnissen, Mailinglisten und in allen anderen Formen von Datenbanken, auf deren Inhalt externe Schreibzugriffe m�glich sind. F�r illegale, fehlerhafte oder unvollst�ndige Inhalte und insbesondere f�r Sch�den, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der �ber Links auf die jeweilige Ver�ffentlichung lediglich verweist.
Urheber- und Kennzeichenrecht
<br/><br/>
Der Autor ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, von ihm selbst erstellte Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zur�ckzugreifen. Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte gesch�tzten Marken- und Warenzeichen unterliegen uneingeschr�nkt den Bestimmungen des jeweils g�ltigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigent�mer. Allein aufgrund der blo�en Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter gesch�tzt sind!
<br/><br/>
Das Copyright f�r ver�ffentlichte, vom Autor selbst erstellte Objekte bleibt allein beim Autor der Seiten. Eine Vervielf�ltigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdr�ckliche Zustimmung des Autors nicht gestattet.
Datenschutz
<br/><br/>
Sofern innerhalb des Internetangebotes die M�glichkeit zur Eingabe pers�nlicher oder gesch�ftlicher Daten (E-Mail-Adressen, Namen, Anschriften) besteht, so erfolgt die Preisgabe dieser Daten seitens des Nutzers auf ausdr�cklich freiwilliger Basis. Die Nutzung der im Rahmen des Impressums oder vergleichbarer Angaben ver�ffentlichten Kontaktdaten wie Postanschriften, Telefon- und Faxnummern sowie Emailadressen durch Dritte zur �bersendung von nicht ausdr�cklich angeforderten Informationen ist nicht gestattet. Rechtliche Schritte gegen die Versender von sogenannten Spam-Mails bei Verst�ssen gegen dieses Verbot sind ausdr�cklich vorbehalten.
Rechtswirksamkeit dieses Haftungsausschlusses
<br/><br/>
Dieser Haftungsausschluss ist als Teil des Internetangebotes zu betrachten, von dem aus auf diese Seite verwiesen wurde. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollst�ndig entsprechen sollten, bleiben die �brigen Teile des Dokumentes in ihrem Inhalt und ihrer G�ltigkeit davon unber�hrt.
<br/><br/>
<h1>INFORMATIONEN ZUR DATENSICHERHEIT</h1>
<br/>
<b>Einverst�ndniserkl�rung</b>
<br/><br/>
Diese Web-Site wird von Langer Oliver (Betreiber) verwaltet. Indem Sie auf sie zugreifen oder sie in einer anderen Form nutzen, erkl�ren Sie sich mit den Bedingungen der Richtlinien f�r den Datenschutz wie unten aufgef�hrt einverstanden. Sollten Sie mit den Richtlinien nicht einverstanden sein, bitten wir Sie, diese Web-Site nicht abzurufen oder in irgendeiner anderen Form zu nutzen.
Dateninfostelle
<br/><br/>
Infos, Beschwerden, Datenausk�nfte unter: info@meineaerzte.at
Umgang mit pers�nlichen Angaben
<br/><br/>
Bei bestimmten Aktionen auf dieser Site werden Sie vom Betreiber gebeten, Angaben zu Ihrer Person auf daf�r bereitgestellten Web-Formularen zu machen. Es steht in v�llig frei, dieser Aufforderung nachzukommen. Sollten Sie sich aber daf�r entscheiden, kann der Betreiber u. U. von Ihnen die Zurverf�gungstellung pers�nlicher Angaben (Name, E-Mail-Adresse, Postanschrift u. �.) verlangen.
<br/><br/>
Wenn Sie Ihre pers�nlichen Informationen an den Betreiber �bermitteln, geben Sie Ihr Einverst�ndnis, dass der Betreiber und vertrauensw�rdige Gesch�ftspartner Ihr Kundenprofil speichern und weiterverarbeiten d�rfen.
<br/><br/>
Diese Angaben gehen beim Betreiber ein und werden dort gespeichert, um die Inanspruchnahme von Diensten der Web-Site zu erm�glichen.
<br/><br/>
Der Betreiber anerkennt in vollem Umfang die Wichtigkeit eines sorgsamen Umgangs mit Ihren Angaben. Wenn Sie gegen eine Verwendung dieser Angaben zum Zwecke der Unterrichtung von anderen Produkten und besonderen Angeboten sind und den Betreiber bei der Bereitstellung Ihrer pers�nlichen Daten davon in Kenntnis setzen, kommt der Betreiber diesem Wunsch nach.
<br/><br/>
Sobald Sie Ihre pers�nlichen Daten im Zusammenhang mit einer bestimmten Aktion auf der Web-Site an den Betreiber weitergeben, erkl�ren Sie sich damit einverstanden, dass zum Zwecke der Anwicklung der Aktion und der Aktualisierung damit verbundener Datenbest�nde der Betreiber sowie autorisierte Partner diese Daten �bermitteln, speichern und verarbeiten.
<br/><br/>
<h1>COPYRIGHT</h1>
<br/>
Der Inhalt dieser Seiten ist grunds�tzlich urheberrechtlich gesch�tzt. Bei �ber den pers�nlichen Gebrauch hinausgehender Verwendung (Ver�ffentlichung, Vervielf�ltigung, Weitergabe an Dritte, Abdruck in Medien) ist www.meineaerzte.at als Urheber zu zitieren. 
Alle Informationen erfolgen nach bestem Wissen und Gewissen sowie aktuellem Kenntnisstand. F�r Fehler kann aber keine Haftung �bernommen werden. In diesem Zusammenhang wird darauf verwiesen, dass das gesamte Informationsangebot laufend aktualisiert wird. 
<br/><br/>

<?php
include(INCLUDEDIR."footer.inc.php");
?>
