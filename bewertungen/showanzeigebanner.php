<?php
////////////////////////////////////////////////////////////////////////////////
// bewertungen/schowanzeigebanner.php - update anzeigebanner->klick
//          - weiterleiten auf anzeigebanner->homepage
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");

// Wenn eine ID �bergeben wurde
if ($_GET['id'] && is_numeric($_GET['id']))
{
    // Datensatz holen
    $anzeigebannerfac = new Anzeigebanner();
    $anzeigebannerfac->getById(htmlentities(trim($_GET['id']))); 
    $anzeigebanner= $anzeigebannerfac->getElement();
        
    // Statistik aktualisieren
    $anzeigebannerfac->update(" `klick`='".($anzeigebanner->klick+1)."'");
    
    //header('Location: http://www.example.com/');
    if (ereg("http://",$anzeigebanner->homepage)) header('Location:'.$anzeigebanner->homepage);
    else header('Location: http://www.'.$anzeigebanner->homepage);
}
else
{
    include(INCLUDEDIR."header.inc.php");
    echo "Fehlerhafter Seitenaufruf!";
    include(INCLUDEDIR."footer.inc.php");
}

?>