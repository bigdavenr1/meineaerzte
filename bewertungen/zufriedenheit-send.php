<?php
////////////////////////////////////////////////////////////////////////////////
// zufriedenheit-send.php - Auswertung der Daten von zufriedenheitsformular.php
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."std/email.class.php");          // eMail-class

unset ($_SESSION['err']);

// wenn abgesendet wurde
if ($_POST['Zufriedenheitabsenden'])
{
   if(!$_SESSION['user'])
   {
       if(!$_POST['name'] || strlen($_POST['name']) < 2) $_SESSION['err'] .= "Ihr Name ist zu kurz (min 2 Zeichen)<br />";
   }
   
   if(!$_SESSION['err'])
   {
       // Zufriedenheitsmerkmale holen
       $zufriedenheitfac=new Zufriedenheit();
       $zufriedenheitfac->getPunkteWithKat();
       $katname=array();
    
       // Zufriedenheitsmerkmale mit POST-Inhalt abgleichen
       while($zufriedenheitspunkt= $zufriedenheitfac->getElement())
       {
           // Wenn Felder vergessen wurde
           if (!$_POST[$zufriedenheitspunkt->id] ) $_SESSION['err']= "Sie m�ssen eine vollst�ndige Zufriedenheit-Bewertung abgeben!<br/>";
           $zufriedenheitspunkte[$zufriedenheitspunkt->id]=$_POST[$zufriedenheitspunkt->id];
       }
   }
   
   // Wenn kein fehler aufgetreten ist und das Formular abgesendet wurde
   if (!$_SESSION['err'])
   {
       // Schauen ob registrierter oder freier User
       if ($_SESSION['user'])
       {
            $user= $_SESSION['user']->id;
            $name= $_SESSION['user']->name;
       }
       else 
       {
            $user= $_SERVER['REMOTE_ADDR'];
            $name= $_POST['name'];
       }
       
       // Neue Zufriedenheits-Bewertung dem Array hinzuf�gen
       $zbewertung[]="";                                        // id
       $zbewertung[]=$user;                                     // userid
       $zbewertung[]=$name;                                     // name
       $zbewertung[]=serialize($zufriedenheitspunkte);          // bewertung
       $zbewertung[]=$_POST['kommentar'];                       // kommentar
       if (!$_POST['kommentar'] || $_POST['kommentar']=="") $zbewertung[]="1";
       else $zbewertung[]="0";                                  // aktiv
       $zbewertung[]=time();                                    // datum
       
        // Zufriedenheit speichern
        $zufriedenheitsfac= new Zufriedenheit();
        $zufriedenheitsfac->getByUserId($user);                 // mit userid pr�fen
        if (!$bewertungsvar = $zufriedenheitsfac->getElement())
        {
            $zufriedenheitsfac->write($zbewertung);
            // eMail versenden
            $msg = "Hallo Herr Langer".chr(10);
            $msg .= chr(10);
            $msg .= "Eine Zufriedenheitsbewertung erfolgte am ".date("d.m.Y",time()).", anbei die Daten:".chr(10).chr(10);
            $msg .= "Bewertername: ".$name.chr(10);
            if (!$_SESSION['user']) $msg .= "nicht eingeloggt IP :".$_SERVER['REMOTE_ADDR'].chr(10).chr(10);
            else $msg .= "eingeloggter User mit ID :".$_SESSION['user']->id.chr(10);
            
            $zufriedenheitfac = new Zufriedenheit();
            $zufriedenheitfac->getPunkteWithKat();
            $katname="";
                        
            while($bewertung= $zufriedenheitfac->getElement())
            {
          
              if ($katname != $bewertung->katname)
              {
                $msg .= chr(10).$bewertung->katname.': '.chr(10);
                $msg .= "==============================================".chr(10);
                $katname = $bewertung->katname;
              }
                $msg .= $bewertung->name.' = '.$_POST[$bewertung->id].' Punkte'.chr(10);
            }
            

            if($_POST['kommentar']) $msg .= chr(10)."Kommentar: ".$_POST['kommentar'];

                           
            $emailfac = new Email(1);
            $emailfac->setFrom($CONST_MAIL['from']);
            $emailfac->setTo($CONST_MAIL['an']);
            $emailfac->setSubject("Zufriedenheitsbewertung");
            $emailfac->setContent($msg);
            //echo nl2br($msg);
            $emailfac->sendMail();
            
            header ("LOCATION:".WEBDIR."inc/msg.php?msg=zufriedenheit_ok");
        }
       
        else 
        {
            if(is_numeric($bewertungsvar->userid) && (time()-$bewertungsvar->datum)<604800)
            {
                $_SESSION['err'].="Sie haben die Seite bereits bewertet. Info f�r registrierte User, bitte geben sie uns bis zur Neubewertung 7 Tage Zeit zur evt. Umgestaltung.<br/>";
            }  
            else if(is_numeric($bewertungsvar->userid) && (time()-$bewertungsvar->datum)>604800)
            {
                $zufriedenheitsfac->write($zbewertung);
                // eMail versenden
                $msg = "Hallo Herr Langer".chr(10);
                $msg .= chr(10);
                $msg .= "Eine Zufriedenheitsbewertung erfolgte am ".date("d.m.Y",time()).", anbei die Daten:".chr(10).chr(10);
                $msg .= "Bewertername: ".$name.chr(10);
                if (!$_SESSION['user']) $msg .= "nicht eingeloggt IP :".$_SERVER['REMOTE_ADDR'].chr(10).chr(10);
                else $msg .= "eingeloggter User mit ID :".$_SESSION['user']->id.chr(10);
                
                $zufriedenheitfac = new Zufriedenheit();
                $zufriedenheitfac->getPunkteWithKat();
                $katname="";
                            
                while($bewertung= $zufriedenheitfac->getElement())
                {
              
                  if ($katname != $bewertung->katname)
                  {
                    $msg .= chr(10).$bewertung->katname.': '.chr(10);
                    $msg .= "==============================================".chr(10);
                    $katname = $bewertung->katname;
                  }
                    $msg .= $bewertung->name.' = '.$_POST[$bewertung->id].' Punkte'.chr(10);
                }
                
    
                if($_POST['kommentar']) $msg .= chr(10)."Kommentar: ".$_POST['kommentar'];
    
                               
                $emailfac = new Email(1);
                $emailfac->setFrom($CONST_MAIL['from']);
                $emailfac->setTo($CONST_MAIL['an']);
                $emailfac->setSubject("Zufriedenheitsbewertung");
                $emailfac->setContent($msg);
                $emailfac->sendMail();
                
                header ("LOCATION:".WEBDIR."inc/msg.php?msg=zufriedenheit_ok");
            }
            else{$_SESSION['err'].="Sie haben die Seite bereits bewertet. Mehrfachbewertungen sind nicht erw�nscht!<br/>";}
        }
    }
    
    // wenn Fehler zur�ck zur Formularseite
   if ($_SESSION['err'])
   {
        header ("LOCATION:".WEBDIR."bewertungen/zufriedenheitsformular.php");
   }
}

?>

