<?php
////////////////////////////////////////////////////////////////////////////////
// kontakt.php - Kontaktformular mit unterschiedlichen Header und Betreffzeile
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");

include(INCLUDEDIR."std/email.class.php");
include(INCLUDEDIR."header.inc.php");
?>
<h1>Kontaktformular</h1><br/>
<?php
unset ($_SESSION['err']);

if($_GET['typ']== 'forum' || $_POST['betreff']== 'forum' ) $betreff = 'Eintrag im Forum';
elseif($_GET['typ']== 'profil' || $_POST['betreff']== 'profil') $betreff = 'Userprofil';
elseif($_GET['typ']== 'show' || $_POST['betreff']== 'show') $betreff = 'Ordinationsprofil';
elseif($_GET['typ']== 'bewertung' || $_POST['betreff']== 'bewertung') $betreff = 'Bewertung';
elseif($_GET['typ']== 'search' || $_POST['betreff']== 'search') $betreff = 'Suchseite';
elseif($_POST['betreff']== 'kritik') $betreff = 'Kritik / W�nsche';
elseif($_POST['betreff']== 'firma') $betreff = 'Ordinationseintrag';
elseif($_POST['betreff']== 'bewertungsbogen') $betreff = 'Bewertungsbogen';
elseif($_POST['betreff']== 'allgemein') $betreff = 'Allgemeine Frage';
else $betreff = 'allgemein';

if($_POST['kontaktieren'])
{
    // Anliegen pr�fen und versenden
    if(!$_POST['anliegen']) $_SESSION['err'] .= 'kein Anliegen eingetragen!<br />';
    if($_POST['anliegen'] && strlen(trim($_POST['anliegen']))<10) $_SESSION['err'] .= 'Sie brauchen f�r Ihr Anliegen weniger als 10 Zeichen?<br />';
    if(!$_POST['name'] || $_POST['name']=="") $_SESSION['err'] .="Bitte geben Sie Ihren Namen an!<br/>";
    if(!$_POST['email'] || $_POST['email']=="") $_SESSION['err'] .="Bitte geben Sie Ihre eMail-Adresse an!<br/>";
    
    if(!$_SESSION['err']) // kein Pr�ffehler
    {
       // eMail an Langer versenden
      $msg = "Hallo Herr Langer".chr(10);
      $msg .= chr(10);
      $msg .= "Das Kontaktformular wurde am ".date("d.m.Y",time())." abgesendet:".chr(10).chr(10);
      $msg .= "gesendet wurde es von: ".$_POST['name']." / ".$_POST['email'].chr(10).chr(10);
      $msg .= "betrifft :".$betreff.chr(10).chr(10);
      $msg .= strip_tags($_POST['anliegen']);
      $emailfac = new Email(1);
      $emailfac->setFrom($_POST['email']);
      $emailfac->setTo($CONST_MAIL['an']);
      $emailfac->setSubject($betreff);
      $emailfac->setContent($msg);
      //echo nl2br($msg);
      $emailfac->sendMail();
      // Ende Email an Langer
      
      
      echo "Ihr Anliegen wurde erfolgreich abgesendet!<br/><br/>Wir werden diese Anfrage schnellstm�glich bearbeiten uns uns gegebenenfalls mit Ihnen in Verbindung setzen.<br/><br/>Ihr meineaerzte.at-Team";
    }
}
?>
<h1><?php echo $titel; ?></h1>
<?php
    if ($_SESSION['err']) {echo '<span class="err">'.$_SESSION['err'].'</span><br/>';}

if(!$_POST['kontaktieren'] || $_SESSION['err'])
{
?>
<form action="<?php if($_GET['typ'])echo 'kontakt.php?typ='.$_GET['typ'];else echo 'kontakt.php'; ?>" method="post">
    <fieldset>
    <legend>Kontakt aufnehmen</legend>
    <label>
         Name*
    </label>
        <input type="text" name="name"/><br class="clr"/>
        <label>
         eMail*
    </label>
        <input type="text" name="email"/><br class="clr"/>
    <label>
         Betreff
    </label>
    <select name="betreff" id="betreff">
        
        <option value="allgemein" <?php if($betreff == "allgemein") echo 'selected="selected"'?>>Allgemeine Frage</option>
        <option value="profil" <?php if($_GET['typ'] == "profil") echo 'selected="selected"';  ?>>Userprofil</option>
        <option value="forum" <?php if($_GET['typ'] == "forum") echo 'selected="selected"';?> >Eintrag im Forum</option>
        <option value="show" <?php if($_GET['typ'] == "show") echo 'selected="selected"';  ?>>Ordinationsprofil</option>
        <option value="search" <?php if($_GET['typ'] == "search") echo 'selected="selected"';?> >Suchseite</option>
        <option value="bewertung" <?php  if($_GET['typ'] == "bewertung") echo 'selected="selected"'; ?>>Bewertung</option>
        <option value="bewertungsbogen" <?php if($_POST['betreff'] == "bewertungsbogen") echo 'selected="selected"';  ?>>Bewertungsbogen</option>
        <option value="firma" <?php if($_POST['betreff'] == "firma") echo 'selected="selected"'; ?>>Ordinationseintrag</option>
        <option value="kritik" <?php if($_POST['betreff'] == "kritik") echo 'selected="selected"';?>>Kritik / W�nsche</option>
        
    </select><br class="clr"/>
    
    <label>
         Anliegen*
    </label>
        <textarea name="anliegen" rows="10" cols="10"></textarea>
    </fieldset>

    <br/>
    <input type="submit" class="submit" name="kontaktieren" value="Anliegen absenden" /><br/>
     <br/>
     Alle mit * gekennzeichneten Felder sind Pflichfelder. Dieser Dienst ist kostenlos. meineaerzte.at behandelt Ihre Daten streng vertraulich und gibt diese nicht weiter.<br/><br/>
</form>
<?php
}

include(INCLUDEDIR."footer.inc.php");

?>