<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// Wenn eine ID �bergeben wurde
if ($_GET['id'])
{
    // Datensatz holen
    $datenfac = new Daten();
    $datenfac->getById($_GET['id']); 
    $daten= $datenfac->getElement();
    
    // Wenn Datensatz vorhanden und aktiv
    if($daten->aktiv == "1")
    {?>
        <script type="text/javascript" src="<?php echo WEBDIR;?>script/prototype.js"></script>
        <script type="text/javascript" src="<?php echo WEBDIR;?>script/scriptaculous.js?load=effects,builder"></script>
        <script type="text/javascript" src="<?php echo WEBDIR;?>script/lightbox.js"></script>
        <h1>Detailansicht - "<?php echo $daten->name.' - '.$daten->titel.' '.$daten->famname.', '.$daten->vorname ;?>"</h1>
        <div class="headline"><div class="right"><?php echo $l->makeLink("Firmfehler melden",WEBDIR."bewertungen/kontakt.php?typ=show");?></div></div>
        <br />
        <div class="td1" style="padding:5px;border:1px dotted #000;">
            <div style="float:left;margin-right:10px;">
                <?php 
                // Bildinfos aus DB auslesen
                if (is_array(unserialize($daten->bild)))$bild=unserialize($daten->bild);
                else $bild=array();
    
                // pr�fen ob Bild vorhanden
                if (sizeof($bild)!=0)
                {
                    // Grafiktyp auslesen
                    $typ=getimagesize(LOCALDIR."images/unternehmen/".$bild[0]['url']) ;
                  
                    // je nach typ Endung bestimmen   
                    if ($typ[2]==1) $endung=".gif";
                    if ($typ[2]==2) $endung=".jpg"; 
                        
                    // Reinnamen ohne Endung
                    $name=basename($bild[0]['url'],$endung);
                        
                    // Gr��e des Thumbnails auslesen
                    $size=getimagesize(LOCALDIR."images/unternehmen/".$name."_thumb".$endung);
                        
                    // HTML Bild schreiben
                    echo $l->makeLink('<img src="'.WEBDIR.'images/unternehmen/'.$name.'_thumb'.$endung.'" width="'.$size[0].'" height="'.$size[1].'" style="border:1px dotted #000;float:left;margin-right:5px;" alt="" />',WEBDIR."images/unternehmen/".$name.$endung,"","_blank",'rel="lightbox" title="'.$bild[0]['bildkommentar'].'"');
                }
                // Wenn kein Bild vorhanden, dann no-image-Bild hinein
                else echo '<img src="'.WEBDIR.'images/nopicture.gif" width="125" height="125" alt="" style="border:1px dotted #000;float:left;margin-right:5px;" />';
                echo '<br class="clr"/>';
                
                // Wenn Premiumstatus OK
                if ($daten->status=="P")
                {
                    // Wenn Gr��e des Bild-Arrays !=0
                    if (sizeof($bild)!=0)
                    {
                        // Bilder (Thumbnails) hochz�hlen
                        for ($x=1;$x<=(sizeof($bild)-1);$x++)
                        {
                            // Umbruch nach 2 Thumnails
                            if ($x==3)echo '<br class="clr"/>';
                            
                            // Grafiktyp auslesen
                            $typ=getimagesize(LOCALDIR."images/unternehmen/".$bild[$x]['url']) ;
                      
                            // je nach typ Endung bestimmen   
                            if ($typ[2]==1) $endung=".gif";
                            if ($typ[2]==2) $endung=".jpg"; 
                            
                            // Reinnamen ohne Endung
                            $name=basename($bild[$x]['url'],$endung);
                            
                            // Gr��e des Thumbnails auslesen
                            $size=getimagesize(LOCALDIR."images/unternehmen/".$name."_thumb".$endung);
                            
                            // HTML Bild schreiben
                            echo '<div style="width:50px;height:50px;border:1px dotted #000;float:left;margin:6px;">';
                            echo $l->makeLink('<img src="'.WEBDIR.'images/unternehmen/'.$name.'_thumb'.$endung.'" width="'.number_format(($size[0]/2.5),0,'','').'" height="'.number_format(($size[1]/2.5),0,'','').'" style="margin-top:'.number_format(((50-($size[1]/2.5))/2),0,'','').'px;margin-left:'.number_format(((50-($size[0]/2.5))/2),0,'','').'px" alt="'.$bild[$x]['bildkommentar'].'" title="'.$bild[$x]['bildkommentar'].'" />',WEBDIR."images/unternehmen/".$name.$endung,"","_blank",'rel="lightbox" title="'.$bild[$x]['bildkommentar'].'"');
                            echo '</div>';
                        }
                    }
                }
                ?>  
                
                </div>
                <div>
                    <b><?php echo $daten->name;?><br/><?php echo $daten->titel;?> <?php echo $daten->famname;?>, <?php echo $daten->vorname;?>  </b><br/><br/>
                    <div style="float:left;width:160px;">
                        <?php echo $daten->strasse;?><br/>
                        <?php echo $daten->plz.' '.$daten->ort;?><br/><br/>
                        <?php echo $daten->land;?><br/>
                    </div>
                    <div style="width:160px;float:left;">
                        Tel: <?php echo $daten->tel;?><br/>
                        <?php if ($daten->fax!='') echo 'Fax: '.$daten->fax.'<br/>';?>
                        <br/>
                        <?php echo $daten->email;?><br/>
                        <?php echo $daten->homepage;?>
                    </div>
                    <br class="clr" />
                </div>
                <div>
                <br class="clr" />
                <hr/><br/>
                <div class="headline">Ordinationszeiten</div>
                <table>
                
                <?php
                $t=0;
                for ($i=1;$i<8;$i++)
                {
                    $ordzeitenfac = new Ordzeiten();
                    $ordzeitenfac->getByFirmIdAndDay($daten->id,$i);
                    $counttimes = $ordzeitenfac->getElementCount();
                    if ($ordzeit = $ordzeitenfac->getElement())
                    {
                        $t++;
                        echo "<tr>";
                        echo "<td>".$ordzeitenfac->parseDay($i,"long")."</td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timestart)."</td><td> - </td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timeend)."</td>".chr(10);
                        if ($counttimes>1)
                        {
                            $ordzeit = $ordzeitenfac->getElement();
                            echo "<td> und </td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timestart)."</td><td> - </td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timeend)."</td>";
                        }  
                        else echo '<td colspan="4"></td>';
                        echo "<td> - ".$ordzeit->memo."</tr>";
                        echo "</tr>";
                    }
                }
                if ($t=0) echo "<tr><td>Keine Ordinationszeiten hinterlegt</td></tr>";   
               ?>  
                </table>
                </div>
                <br class="clr"/>
                <?php 
                // Wenn Premiumstatus OK
                if ($daten->status=="P" && $daten->beschreibung!="") {?>
                <br/>
                <hr/>
                <br/>
                <u><b>Beschreibung</b></u><br/><br/>    
                <?php
                    // Beschreibung schrieben 
                    echo nl2br($daten->beschreibung); 
                } ?>
            </div>
            <br class="clr" />
            <br/>
            
            <?php
            // Bewertungsdaten holen
                $bewertungfac=new Bewertung();
                $bewertungfac->getByUid($daten->id);
                $z=0;
            
            // Wenn keine bestimmte Bewertung gew�hlt wurde
            if (!$_GET['bid'])
            {?>
                <h1>Bewertungen zu "<?php echo $daten->name.' - '.$daten->titel.' '.$daten->famname.', '.$daten->vorname ;?>" <a name="bewertung">&nbsp;</a></h1>
                <?php
                
                while ($bewertung = $bewertungfac->getElement())
                {
                    // Bewertungsarray()
                    $bewertungen=array();
                    $z++;
                    // Bewertung auslesen 
                    $bewertungen[]=unserialize($bewertung->bewertung);
                    
                    // Gr��e des Bewertungsarrays bestimmen         
                    $size_bewertung=sizeof($bewertungen);
                                
                    // Wenn zum Bewerten Freigegeben
                    if ($daten->bewertungsstatus=="A")
                    {
                        // Wenn KEINE Bewertung vorhanden ist
                        if ($size_bewertung==0)
                        {
                            echo 'Noch keine Bewertungen zu diesem Eintrag vorhanden<br/><br/>';
                        }
                                
                        // Wenn mindestens eine Bewertung vorhanden ist
                        else
                        {
                            $punktebewertung=0;
                                     
                            //Bewertungspunkte addieren 
                            for ($x=0;$x<$size_bewertung;$x++)
                            {
                                $punktebewertung=$punktebewertung+(array_sum($bewertungen[$x])/sizeof($bewertungen[$x]));
                            }
                                       
                            // Quersumme bilden
                            $gesamtbewertung=$punktebewertung/$size_bewertung;
                            if ($z%2) $class='class="td1"';
                            else $class='';
                            echo '<div '.$class.' style="padding:4px;border-bottom:1px dotted #000;">';
                            echo 'Bewertung von ';
                            
                            // User auslesen wenn benutzerid nummerisch
                            if (is_numeric($bewertung->userid))
                            {
                                $benutzerfac=new Benutzer();
                                $benutzerfac->getById($bewertung->userid);
                                
                                // Wenn Benutzer vorhanden              
                                if ($benutzer=$benutzerfac->getElement()) 
                                {
                                    echo $l->makeLink($benutzer->nickname.' (ID: '.$benutzer->id.')',WEBDIR.'bewertungen/userprofil.php?id='.$bewertung->userid,$benutzer->anrede);
                                }
                                
                                // Wenn Benutzer nicht vorhanden
                                else echo '(unregistrierter User)'; 
                            }
                        
                            // Wenn NutzerID nicht nummerisch (weil IP gespeichert)
                            else echo '(unregistrierter User)';
        
                            // Wenn ordentliches Datum gespeichert wurde (Unix-Timestamp)
                            if (is_numeric($bewertung->datum))
                            {
                                echo ' vom '.date("d.m.Y",$bewertung->datum);
                            }
                            echo '<br class="clr"/>';
                                           
                            // Ausgabe der Bewertung
                            for ($r=1;$r<=5;$r++)
                            {
                                if ($r<=round($gesamtbewertung,0))
                                {?>
                                    <img src="<?php echo WEBDIR;?>images/icons/star.gif" alt="Stern-bewertet-<?php echo $r;?>"/>
                                <?php 
                            }
                        
                            else
                            {?>
                                <img src="<?php echo WEBDIR;?>images/icons/star_grey.gif" alt="Stern-unbewertet-<?php echo $r;?>"/>
                            <?php
                            }
                        }
                        
                        // Wenn Bewertungskommentar freigegeben, eine Userid gespeichert wurde und der Kommentar nicht leer ist                
                        if (($bewertung->aktiv==1 || $bewertung->aktiv==2 ) && is_numeric($bewertung->userid) && $bewertung->kommentar!='')
                        {
                            echo '<br/>';
                            echo substr(nl2br($bewertung->kommentar),0,150)." ...";
                            echo '<br/>';
                        }
                    
                        // Wenn Bewertungskommentar NICHT freigegeben, eine Userid gespeichert wurde und der Kommentar nicht leer ist   
                        else if ($bewertung->aktiv==0 && is_numeric($bewertung->userid) && $bewertung->kommentar!='')
                        {   
                            echo '<br/>';
                            echo "Bewertungskomnmentar noch nicht freigeschaltet";
                            echo '<br/>';
                        }
    
                        echo "<br/>".$l->makeLink('<b>[ Details dieser Bewertung ansehen ]</b>',WEBDIR.'bewertungen/showeintrag.php?bid='.$bewertung->id.'&amp;id='.$daten->id);
                        echo '</div>';
                    }
                }
                            
                // Wenn nicht zum Bewerten Freigegeben
                else echo 'Dieser Eintrag ist noch nicht zur Bewertung freigegeben!<br/><br/> Bitte versuchen Sie es sp�ter nocheinmal!';
            }
        }   
        else 
        {
            echo "<h1>Details zu einer Bewertung</h1>";
            ?>
            <div class="headline"><div class="right"><?php echo $l->makeLink("Bewertungsfehler melden",WEBDIR."bewertungen/kontakt.php?typ=bewertung");?></div></div>
            <?php
            $k=1;
            $x=0;
            // Bewertungspunkte auslesen
            $datenfac->createOwnQuery("SELECT * , katbewertung.name AS katname FROM  katbewertung,bewertungspunkte WHERE bewertungspunkte.kat = katbewertung.id ORDER BY katbewertung.id" );
            $katname=array();
              
            // Bewertung mit der ID auslesen
            $bewertungfac->getById($_GET['bid']);
            $bewertungsdaten = $bewertungfac->getElement();
            $bewertungen=unserialize($bewertungsdaten->bewertung);
                
            echo 'Bewertung von ';
                            
            // User auslesen wenn benutzerid nummerisch
            if (is_numeric($bewertungsdaten->userid))
            {
                $benutzerfac=new Benutzer();
                $benutzerfac->getById($bewertungsdaten->userid);
                                
                // Wenn Benutzer vorhanden              
                if ($benutzer=$benutzerfac->getElement()) 
                {
                    echo $l->makeLink($benutzer->nickname.' (ID: '.$benutzer->id.')',WEBDIR.'bewertungen/userprofil.php?id='.$benutzer->id,$benutzer->anrede);
                }
                                
                // Wenn Benutzer nicht vorhanden
                else echo '(unregistrierter User)'; 
            }
            
            // Wenn NutzerID nicht nummerisch (weil IP gespeichert)
            else echo '(unregistrierter User)';
    
            // Wenn ordentliches Datum gespeichert wurde (Unix-Timestamp)
            if (is_numeric($bewertungsdaten->datum))
            {
                echo ' vom '.date("d.m.Y",$bewertungsdaten->datum);
            }
            echo '<br class="clr"/>';
            
            // Einzelne Bewertungspunkte durchegehen    
            while($bewertung= $datenfac->getElement())
            {
                // Gruppenwechsel zu Kategoriename der Bewertungen
                if (!$katname[$bewertung->kat])
                { 
                    $x++;
                    // Kategoriename in Array speichern
                    $katname[$bewertung->kat]['name']=$bewertung->katname;
                    
                    // Anzahl f�r Kategorie auf 0 setzen
                    $katname[$bewertung->kat]['anzahl']=0;
                }
                
                // Wenn Bewertung f�r diesen Punkt vorhanden ist
                if ($bewertungen[$bewertung->id])
                {
                    // Bewertungsname als String in Array speichern
                    $katname[$bewertung->kat]['string'].='<label style="width:200px;margin-top:0px;padding-top:0px;">'.$bewertung->name.'</label>';
                    
                    // Ausgabe der Bewertung (Hochz�hlen der Punkte und mit eingetragener BEwertung abgleichen
                    for ($r=1;$r<=5;$r++)
                    {
                        // Sterne in Array speichern
                        if ($r<=$bewertungen[$bewertung->id])
                        {
                            $katname[$bewertung->kat]['string'].='<img src="'.WEBDIR.'images/icons/star.gif" alt="Stern-bewertet-'.$r.'"/>';
                        }
            
                        else
                        {
                           $katname[$bewertung->kat]['string'].='<img src="'.WEBDIR.'images/icons/star_grey.gif" alt="Stern-unbewertet-'.$r.'"/>';
                       
                        }
                    }
                    // Absatz in Array speichern
                    $katname[$bewertung->kat]['string'].= '<br class="clr"/>';
                }
                // Kategoriepunkte hinzuf�gen   
                $katname[$bewertung->kat]['punkte']=$katname[$bewertung->kat]['punkte']+$bewertungen[$bewertung->id];
                
                // Anzahl um eins erh�hen  
                $katname[$bewertung->kat]['anzahl']=$katname[$bewertung->kat]['anzahl']+1;        
            }
            
            // Katname Array auslesen
            for ($p=1;$p<=sizeof($katname);$p++)
            {
                echo '<h2><div style="float:right">';
                // Durchschnitt errechnen
                $gesamt=$katname[$p]['punkte']/$katname[$p]['anzahl'];
                
                // Ausgabe der Sterne f�r Kategoriedurchschnitt
                for ($r=1;$r<=5;$r++)
                {
                    if ($r<=round($gesamt,0))
                    {
                        echo '<img src="'.WEBDIR.'images/icons/star.gif" alt="Stern-bewertet-'.$r.'"/>';
                    }
            
                    else
                    {
                       echo '<img src="'.WEBDIR.'images/icons/star_grey.gif" alt="Stern-unbewertet-'.$r.'"/>';
                     
                    }
                }
                echo '</div>'.$katname[$p]['name'].'</h2>';
                
                // restlichen String ausgeben
                echo $katname[$p]['string'];
            }
            
            if (($bewertungsdaten->aktiv==1 || $bewertungsdaten->aktiv==2 ) && is_numeric($bewertungsdaten->userid) && $bewertungsdaten->kommentar!='')
            {
                  echo '<br/><div class="td1">';
                  echo '<b>Bewertungskommentar</b><br/>';
                  echo nl2br($bewertungsdaten->kommentar);
                  
                  if ($bewertungsdaten->aktiv==1 && $bewertungsdaten->antwort != "")
                  {
                      echo '<span style="font-style:italic"><b>Antwort:<br></b>'.$bewertungsdaten->antwor.'</span>';
                  }
                  echo '</div>';
            }
           echo '<br class="clr"/><br/>'.$l->makeLink('<b>[ zur�ck zur Profilansicht ]</b>',WEBDIR."bewertungen/showeintrag.php?id=".$daten->id);
        }                       
    }
    // Wenn kein Datensatz gefunden wurde
    else 
    {
        echo "<h1>Artikel nicht gefunden</h1>Der von Ihnen aufgerufenen Artikel existiert nicht<br /><br />";
    }
}

// Wenn keine ID �bertragen wurde
else
{
    echo "Kein Artikel gew�hlt!";
}
    
include(INCLUDEDIR."footer.inc.php");
?>