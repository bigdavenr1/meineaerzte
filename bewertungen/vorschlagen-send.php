<?php 
include("../inc/config.php");
include(INCLUDEDIR."std/email.class.php");          // eMail-class

$datenfac = new Daten();

// Fehlervariable l�schen
unset ($_SESSION["err"]);
    
    // Wenn eMail-Adresse eingetragen wurde
    if($_POST['mail'] && trim($_POST['mail']) != '')
    {
        $datenfac->getByMail($_POST['mail']);
        if($daten = $datefac->getElement())
        {
            $err .= "F�r diese Emailadresse gibt es schon einen Arzt.<br />";
        }
    }

     // Wenn eMail-Adresse ge�ndert wurde
    /*if ($daten->email!=$_POST['email'])
    {
        // email auf Dopplung pr�fen
        $benutzerfac = new Benutzer();
        $benutzerfac->getByMail($_POST["email"]);
        
        // Wenn eMail-Doppelt dann Fehlermeldung
        if ($benutzerfac->getElementCount() && $_POST["email"]!="") $err .= "Diese Emailadresse gibt es schon.<br />";
    }*/

    // Wenn Dopplungspr�fung keinen Fehler ergeben hat    
    if (!$err)
    {
        $newpass=generateRandomKey(8);
        $kunr=generateRandomKey(8);
        
        // Array f�r Datenspeicherung
        $data[]="";                                                                 //Frei f�r ID
        $data[]=$_POST['name'];                                                     //Praxistyp
        $data[]=$_POST['email'];                                                    //eMail
        $data[]="2";                                                                 //aktiv
        $data[]=$_POST['vorname'];                                                  //Vorname
        $data[]=$_POST['famname'];                                                  //Nachname
        $data[]=$_POST['strasse'];                                                  //Strasse
        $data[]=$_POST['plz'];                                                      //PLZ
        $data[]=$_POST['ort'];                                                      //Ort
        $data[]=$_POST['land'];                                                     //Land
        $data[]=$_POST['tel'];                                                      //Telefon
        $data[]=$_POST['fax'];                                                      //Fax
        $data[]=$_POST['homepage'];                                                 //Homepage
        $data[]="N";                                                                //Free oder Premium
        $data[]="";                                                                 //Hochgeladenes Bild
        $data[]="";                                                                 //Bechreibung
        $data[]="";                                                                 //Zahlungsweise
        $data[]="";                                                                 //Kontoinhaber
        $data[]="";                                                                 //Konto
        $data[]="";                                                                 //BLZ
        $data[]="";                                                                 //Bank
        $data[]="";                                                                 //Kreditkarteninhaber
        $data[]="";                                                                 //Kartentyp
        $data[]="";                                                                 //Gueltigkeit
        $data[]="";                                                                 //Kartennumer
        $data[]=$newpass;                                                           //Clearpasswort
        $data[]="";                                                                 //Fachgebiete
        $data[]=$_POST['titel'];                                                    //Titel
        $data[]="";                                                                 //Suchbegrife
        $data[]=time();                                                             //Anlagedatum
        $data[]="";                                                                 //Premiumdatum
        $data[]="I";                                                                //Freigabe f�r Bewertung
        $data[]=$kunr;                                                              //Kundennummer
        $data[]="";                                                                 //Klaerung
        $data[]="";                                                                 //Schlagworte
        $data[]=$_POST['anrede'];                                                   //Anrede
        $datenfac->write($data); 
        
        if ($_POST['email']) $login=$_POST['email'];
        else $login=$kunr;
        
        // Array f�r Benutzerdatenbank
        $salt=generateRandomKey(8);
        $benutzerdata[]="";
        $benutzerdata[]=$_POST['anrede'];
        $benutzerdata[]=$_POST['vorname'];
        $benutzerdata[]=$_POST['famname'];
        $benutzerdata[]=md5($newpass.$salt);
        $benutzerdata[]=$salt;
        $benutzerdata[]="2";
        $benutzerdata[]="unternehmen";
        $benutzerdata[]=$login;
        $benutzerfac= new Benutzer();
        $benutzerfac->write($benutzerdata);
        
        
        // eMail an Langer versenden
        $msg = "Hallo Herr Langer".chr(10);
        $msg .= chr(10);
        $msg .= "Eine neuer Arzt wurde am ".date("d.m.Y",time()).", vorgeschlagen:".chr(10).chr(10);
        
        $msg .= "Name : ".$_POST['famname'].chr(10).chr(10);
        
        if($_POST['email']) $msg .= "EMail: ".$_POST['email'];
        else $msg .= "KundenNr: ".$kunr;
    
        $emailfac = new Email(1);
        $emailfac->setFrom($CONST_MAIL['from']);
        $emailfac->setTo($CONST_MAIL['an']);
        $emailfac->setSubject("Neuer Arzt");
        $emailfac->setContent($msg);
        //echo nl2br($msg);
        $emailfac->sendMail();
        // End eMail an Langer
        
        include(INCLUDEDIR."header.inc.php");
        ?>
        
        <h1> Eintrag erfolgreich</h1>
        Der von Ihnen eingetragene Arzt wurde erfolgreich in unserer Datenbank aufgenommen. Nach einer Pr�fung wird dieser Arzt zur Bewertung freigeschaltet.
    <?php
        include(INCLUDEDIR."footer.inc.php");
    }
    else
    { 
        $_SESSION['err']=$err;
        header("Location:vorschlagen.php?sv=1");
    }
?>
