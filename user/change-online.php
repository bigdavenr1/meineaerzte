<?php
////////////////////////////////////////////////////////////////////////////////
// user/change-online.php - Zeigt Profilformular
////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// Datensatz einlesen
$benutzerfac = new Benutzer(1);
$benutzerfac->getById($_SESSION['user']->id);
$benutzer = $benutzerfac->getElement();
   
// Freigabeoptionen einlesen
$freigabe=array();
if (is_array(unserialize($benutzer->freigabe))) $freigabe=unserialize($benutzer->freigabe);
if ($freigabe['plz']) $selected_plz='checked="checked"';
if ($freigabe['ort']) $selected_ort='checked="checked"';
if ($freigabe['email']) $selected_email='checked="checked"';
if ($freigabe['homepage']) $selected_homepage='checked="checked"';
if ($freigabe['geburtstag']) $selected_alter='checked="checked"';

// Newsletter einlesen
$newsletterfac = new Newsletter();
$newsletterfac->getByEmail($_SESSION['user']->email);
if($newsletter = $newsletterfac->getElement()) $selected_newsletter='checked="checked"';

?>

<h1>Benutzer-Details bearbeiten</h1>
<br />
<?php if ($_SESSION['err']) echo '<span class="err">'.$_SESSION['err'].'</span><br/>';?>
<form action="<?php echo $l->makeFormLink(WEBDIR.'user/change-online-update.php?id='.$_SESSION['user']->id);?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Profilbild bearbeiten</legend>
        <?php
        // Wenn kein Profilbild hochgeladen wurde
        if($benutzer->bild=="") echo 'Kein Profilbild hochgeladen';
        
        // wenn ein profilbild hochgeladen wurde
        else 
        {
            // Grafiktyp auslesen
            $typ=getimagesize(LOCALDIR."images/user/".$benutzer->bild) ;
  
            // je nach typ Endung bestimmen   
            if ($typ[2]==1) $endung=".gif";
            if ($typ[2]==2) $endung=".jpg"; 
        
            // Reinnamen ohne Endung
            $name=basename($benutzer->bild,$endung);
        
            // Gr��e des Thumbnails auslesen
            $size=getimagesize(LOCALDIR."images/user/".$name."_thumb".$endung);
  
            // HTML Bild schreiben 
            ?>
            <img src="<?php echo WEBDIR;?>images/user/<?php echo $name;?>_thumb<?php echo $endung;?>" height="<?php echo $size[1]; ?>" width="<?php echo $size[0]; ?>" alt="Bild von <?php echo $benutzer->nickname?>"/>
            <input type="checkbox" name="bildloeschen" class="selectbox" /> Bild l�schen  <br class="clr"/>

        <?php   
        }
        ?>
        <br class="clr" />
        <hr/>
        <br/>
         <label for="bildneu">
            neues Bild hochladen:
        </label>
        <input id="bildneu" name="bildneu" type="file" /><br class="clr"/>
    </fieldset>
    <br/>
    <fieldset>
        <legend>Anzeigeigenschaften �ndern</legend>
          Sie k�nnen entscheiden welche Daten in Ihrem Profil angezeigt werden, und welche nicht!<br/>
        <table>
            <tr>
                <th>
                    Feld
                </th>
                <th style="width:100px">
                    Sichtbarkeit
                </th>
            </tr>
            <tr>
                <td>
                   PLZ
               </td>
               <td align="center">
                    <input name="freigabe[plz]" type="checkbox" <?php echo $selected_plz;?> class="selectbox" />                        
               </td>
           </tr>
           <tr>
               <td>
                   Ort
               </td>
               <td align="center">
                    <input name="freigabe[ort]" type="checkbox" <?php echo $selected_ort;?> class="selectbox" />    
                    
               </td>
           </tr>
           <tr>
               <td >
                   eMail
               </td>
               <td align="center">
                 <input name="freigabe[email]" type="checkbox" <?php echo $selected_email;?> class="selectbox" />    
                    
               </td>
           </tr>
           <tr>
               <td >
                   Alter
               </td>
               <td align="center">
                 <input name="freigabe[geburtstag]" type="checkbox" <?php echo $selected_alter;?> class="selectbox" />    
                    
               </td>
           </tr>
           <tr>
               <td>
                   Homepage
               </td>
               <td align="center">
                   
                   <input name="freigabe[homepage]" type="checkbox" <?php echo $selected_homepage;?> class="selectbox" />     
               </td>
           </tr>
       </tr>
   </table>
    </fieldset>
    <br/>
    <fieldset>
    <legend>Newsletter bestellen / abbestellen</legend>
    <input name="newsletter" type="checkbox" <?php echo $selected_newsletter;?> class="selectbox" />     

    </fieldset>
 <input type="submit" value="Daten speichern" class="submit" />

</form><br /><br />



<?php 

include(INCLUDEDIR."footer.inc.php");

?>



