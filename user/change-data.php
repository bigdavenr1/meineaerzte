<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

    $benutzerfac = new Benutzer(1);
    $benutzerfac->getById($_SESSION['user']->id);
    $benutzer = $benutzerfac->getElement();
?>

<h1>Benutzer-Details bearbeiten</h1>
<br /><?php
if ($_SESSION["err"])
{
    echo '<div class="err">'.$_SESSION["err"].'</div><br />';
    unset($_SESSION["err"]);
}?>
<form action="<?php echo $l->makeFormLink(WEBDIR.'user/change-data-update.php?id='.$_SESSION['user']->id);?>" method="post">
   <fieldset>
   <legend>pers�nliche Daten</legend>
    <label style="margin-top:0px">
        ID:
    </label>
   <?php echo $benutzer->id; ?>
    <br class="clr" />
    <label style="margin-top:0px">
        Anrede:
    </label>
    <?php echo $benutzer->anrede; ?>
    <br class="clr" />
    <label style="margin-top:0px">
        Titel:
    </label>
    <?php echo $benutzer->titel; ?>
    <br class="clr" />
     <label style="margin-top:0px">
        Vorname:
    </label>
    <?php echo $benutzer->vorname; ?>
    <br class="clr" />
    <label style="margin-top:0px">
        Name:
    </label>
    <?php echo $benutzer->name; ?>
    <br class="clr" />
    <label style="margin-top:0px">
        Geburtstag:
    </label>
    <?php echo $benutzer->geburtstag; ?>
    <br class="clr" />
    <label style="margin-top:0px">
        Nickname:
    </label>
    <?php echo $benutzer->nickname; ?>
    <br class="clr" />
    
    <label>
        Strasse
    </label>
    <input type="text" name="strasse" value="<?php echo $benutzer->adresse; ?>" />
    <br class="clr" />
    
    <label >
        PLZ / Ort:
    </label>
    <input type="text" name="plz" value="<?php echo $benutzer->plz; ?>" class="plz" /><input type="text" name="ort" value="<?php echo $benutzer->ort; ?>" class="ort" />
    <br class="clr" />
    
    <label>
        Land
    </label>
        <input type="text" name="land" value="<?php echo $benutzer->land; ?>" />
        
        <br class="clr" />

        <label>
        Homepage
    </label>
        <input type="text" name="homepage" value="<?php echo $benutzer->homepage; ?>" />
        
        <br class="clr" />
    <label >
        eMail:
    </label>
        <input type="text" name="email" value="<?php echo $benutzer->email; ?>" /><br class="clr" />
        <label>&nbsp;</label>
        <i>Neue eMail-Adressen m�ssen erneut verifiziert werden!</i>
    <br class="clr" /><br />
    </fieldset>
    <br/>
    <fieldset>
    <legend>Passwort �ndern</legend>
     <label>
        neues Passwort
    </label>
        <input type="password" name="pass"  />
    <br class="clr"/>
    <label>
        Passwort wiederholen
    </label>
        <input type="password" name="repass"  />
    </fieldset>
        <br class="clr" />
<br />
 <input type="submit" value="Daten speichern" class="submit" />
</form><br /><br />

<?php 
include(INCLUDEDIR."footer.inc.php");
?>

