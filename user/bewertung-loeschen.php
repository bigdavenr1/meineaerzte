<?php
////////////////////////////////////////////////////////////////////////////////
// user/bewertung-loeschen.php - Zeigt abgegebene Bewertungen als Tabelle
////////////////////////////////////////////////////////////////////////////////


include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>

<h1>Bewertungen zur�ckziehen</h1>
<br/>
<br/>
Ihre bisher abgegebenen Bewertungen:<br/><br/>
<?php


    $kommentarfac = new Bewertung(25);
    $kommentarfac->getByUser($_SESSION['user']->id);
    
?>
<br class="clr" /><br />
<table style="width:100%">
    <tr>
        <th style="width:150px">
            Bewertung
        </th>
        <th >
            Kommentar               
        </th>
        <th>
            f�r Eintrag
        </th>
        <th>
            Datum
        </th>
        <th>
            Status
        </th>
        <th style="width:50px;">
            
        </th>
    </tr>
    
    
<?php 
$t=0;
while ($kommentar = $kommentarfac->getElement())
{ 
    $t++;
    ?>
    <tr>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php
            // Bewertungsarray()
            $bewertungen=array();
 
            // Bewertung auslesen 
            $bewertungen[]=unserialize($kommentar->bewertung);
                    
            // Gr��e des Bewertungsarrays bestimmen         
            $size_bewertung=sizeof($bewertungen);
                                
            $punktebewertung=0;
                                     
            //Bewertungspunkte addieren 
            for ($x=0;$x<$size_bewertung;$x++)
            {
                $punktebewertung=$punktebewertung+(array_sum($bewertungen[$x])/sizeof($bewertungen[$x]));
            }
                                       
            // Quersumme bilden
            $gesamtbewertung=$punktebewertung/$size_bewertung;
             
            // Ausgabe der Bewertung
            for ($r=1;$r<=5;$r++)
            {
                if ($r<=round($gesamtbewertung,0))
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star.gif" alt="Stern-bewertet-<?php echo $r;?>"/>
                <?php 
                }
                
                else
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star_grey.gif" alt="Stern-unbewertet-<?php echo $r;?>"/>
                <?php
                }
           }
           ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php echo substr($kommentar->kommentar,0,200);?>...
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($kommentar->unternehmen.'<br/>'.$kommentar->name.', '.$kommentar->vorname,WEBDIR."bewertungen/showeintrag.php?id=".$kommentar->unternehmenid); ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php if (is_numeric($kommentar->datum))  echo date("d.m.Y",$kommentar->datum); ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php if ($kommentar->aktiv == 1)  echo "best�tigt"; else echo "unbest�tigt"; ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?> style="text-align:center">
      
           <?php  echo $l->makeLink($icon_delete_small,WEBDIR."user/bewertung-loeschen-update.php?id=".$kommentar->id);?>
       </td>
   </tr>

<?php } ?>
</table>

<br /><br />

<?php if ($t==0) echo "Keine abgegebenen Bewertungen vorhanden";
else {

?>


<div class="contentboxsmall" style="text-align:left;">
        Seiten: <?php echo $kommentarfac->getHtmlNavi("std", "&amp;id=".$benutzer->id."&amp;rid=".$_GET["rid"]);?>
    </div>
<?php 
}

include(INCLUDEDIR."footer.inc.php");
?>

