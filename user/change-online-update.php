<?php
include("../inc/config.php");

////////////////////////////////////////////////////////////////////////////////
// change-online-update.php �ndert die Anzeigeoptionen und das Porfilbild
////////////////////////////////////////////////////////////////////////////////

// Alte Fehlermeldungen l�schen
unset($_SESSION['err']);

$benutzerfac = new Benutzer();
$benutzerfac->getById($_SESSION['user']->id);
$benutzer = $benutzerfac->getElement();
$bildname=$benutzer->bild;

// Wenn H�kchen bei "Bild l�schen"
if($_POST['bildloeschen'])
{
    // Grafiktyp auslesen
    $typ=getimagesize(LOCALDIR."images/user/".$benutzer->bild) ;
      
    // je nach typ Endung bestimmen   
    if ($typ[2]==1) $endung=".gif";
    if ($typ[2]==2) $endung=".jpg"; 
            
    // Bild und Thumb l�schen
    unlink(LOCALDIR."images/user/".$benutzer->bild);
    unlink(LOCALDIR."images/user/".basename($benutzer->bild,$endung)."_thumb".$endung);
            
    $bildname="";
} 

// Wenn neues Bild hochgeladen wurde   
if ($_FILES['bildneu']['name'] != "")
{          
    // Einlesen der Bildinformationen
    $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);

    // �berpr�fung des Bildtyps
    if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
    {
        // Altes Bild l�schen
        if ($benutzer->bild!="" && file_exists(LOCALDIR."images/user/".$benutzer->bild))
        {
            // Grafiktyp auslesen
            $typ=getimagesize(LOCALDIR."images/user/".$benutzer->bild) ;
          
            // je nach Typ Endung bestimmen   
            if ($typ[2]==1) $endung=".gif";
            if ($typ[2]==2) $endung=".jpg"; 
                
            // Bild und Thumb l�schen
            if (file_exists(LOCALDIR."images/user/".$benutzer->bild)) unlink(LOCALDIR."images/user/".$benutzer->bild);
            if (file_exists(LOCALDIR."images/user/".basename($benutzer->bild,$endung)."_thumb".$endung)) unlink(LOCALDIR."images/user/".basename($benutzer->bild,$endung)."_thumb".$endung);
        }
        
        // Filetyp des neuen Bildes bestimmen
        if ($filetyp[2]==2) $type = '.jpg';
        if ($filetyp[2]==1) $type = '.gif';
        
        // neues Bild hochladen
        $key=generateRandomKey(10);
        $pic = $benutzerfac->writePic($_FILES['bildneu']["tmp_name"],$key,$type);
        $bildname=$key.$type;       
    }

    //Wenn falscher Bildtyp 
    else 
    {    
       $_SESSION['err'] .= "Ihr Bild hat einen falschen Bildtyp! Es sind nur JPG oder GIF Bilder erlaubt! Das BIld wurde nicht hochgeladen. Alle anderen �nderungen wurden �bernommen!";
    }
}

// Benutzer-Datensatz updaten
$benutzerfac->update("bild='".$bildname."', freigabe='".serialize($_POST['freigabe'])."'");

// Newsletter pr�fen
$newsletterfac = new Newsletter();
$newsletterfac->getByEmail($_SESSION['user']->email);
// in Tabelle vorhanden
if($newsletter = $newsletterfac->getElement()) 
{
    // Email austragen
    if(!$_POST['newsletter'])
    {
        $newsletterfac->deleteElement();
    }
    
}
else
{   
    // Email eintragen
    if($_POST['newsletter'])
    {
        $newsletterdata[] = '';                         // Id
        $newsletterdata[] = $_SESSION['user']->email;   // Email
        $newsletterfac->write($newsletterdata);
    }
} 

header("Location:change-online.php?sv=1");
?>