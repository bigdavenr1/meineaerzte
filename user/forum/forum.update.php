<?php
////////////////////////////////////////////////////////////////////////////////
// forum.update.php - Verwaltung der Einträge
////////////////////////////////////////////////////////////////////////////////

include("../../inc/config.php");

if ($_SESSION["user"])
{
    unset($_SESSION["post"]);
    unset($err);
    // Topic erstellen
    ////////////////////////////////////////////////////////////////////////////
    
    if ($_POST["mode"] == "newmsg")
    {
        if (!$_POST["titel"]) $err .= "Dein Thema brauch einen Titel<br>";
        if (!$_POST["text"]) $err .= "Dein Thema brauch einen Text<br>";
        if (!$_POST["id"]) $err .= "Es ist kein Forum ausgewählt<br>";
        
        if (!$err)
        {

            $forummsgfac = new ForumMsg();
            $data[] = "";                                                           // id
            $data[] = $_POST["id"];                                                 // forum_id
            $data[] = $_SESSION["user"]->id;                                        // user_id
            $data[] = md5($_SESSION["user"]->id.$_POST["id"].date("Y-m-d H:i:s"));  // super_id
            $data[] = date("Y-m-d H:i:s");                                          // datum
            $data[] = date("Y-m-d H:i:s");                                          // last_datum
            $data[] = "";                                                           // edit_datum
            $data[] = "";                                                           // edit_times
            $data[] = addslashes(trim(strip_tags($_POST["titel"])));                // titel
            $data[] = nl2br(addslashes(trim(strip_tags($_POST["text"]))));          // text
            $data[] = $_SESSION["user"]->id;                                        // aufrufe
            $forummsgfac->write($data);
            
        
            $_SESSION["punkte"] = "topic";
        
            header("Location: ".WEBDIR."bewertungen/forum.view.php?id=".$_POST["id"]."&sv=1");
        }
        else
        {
            $_SESSION["err"] = $err;
            $_SESSION["post"] = $_POST;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }    
    } // end if ($_POST["mode"] == "newmsg")
    
    // Antwort erstellen
    //////////////////////////////////////////////////////////////////////////////////////////////
    
    if ($_POST["mode"] == "msg")
    {
        if (!$_POST["text"]) $err .= "Ihr Thema brauch einen Text<br>";
        if (!$_POST["id"]) $err .= "Es ist kein Forum ausgewählt<br>";
        if (!$_POST["rid"]) $err .= "Es ist kein Topic ausgewählt<br>";
        
        
        if (!$err)
        {
            $modstring = "";
            if(isset($_POST["mid"]))    // bearbeiteter Beitrag
            {
                //$modstring = "<br /> zuletzt überarbeitet vom ";//.date("Y-m-d H:i:s");
                $userid = $_POST["altid"];
            }
            else $userid = $_SESSION["user"]->id;
            
            if(isset($_POST["altid"]))
            {
               if($_POST["altid"] != $_SESSION["user"]->id)
               {
                    //$modstring .= " [Modorator!]";
                    $userid = $_SESSION["user"]->id;
               }
               //else $modstring .= " User ";
            }

            $forummsgfac = new ForumMsg();
            $data[] = $_POST["mid"];                                                // id
            $data[] = $_POST["id"];                                                 // forum_id
            $data[] = $userid;                                                      // user_id
            $data[] = $_POST["rid"];                                                // super_id
            $data[] = $_POST["date"]?$_POST["date"]:date("Y-m-d H:i:s");            // datum
            
            
            if ($_POST["mid"])
            {
                $forummsgfac->getbyId($_POST["mid"]);
                $oldmsg=$forummsgfac->getElement();
                if ($oldmsg->titel!="")
                {
                    $data[] = $oldmsg->last_datum;                                      // last_datum
                }
                
                else $data[] = "";
                $data[] = date("Y-m-d H:i:s");                                      // edit_datum
                $data[] = $_POST["edittimes"]+1;                                    // edit_times
            }
            else
            {
                $data[] = "";
                $data[] = "";                                                       // edit_datum
                $data[] = "";                                                       // edit_times
            }
            
            $data[] = $_POST["titel"];                                              // titel
            $data[] = nl2br(addslashes(trim(strip_tags($_POST["text"])))).$modstring;          // text
            $data[] = $_POST["aufrufe"];                                            // aufrufe
            $forummsgfac->write($data);
            
            
            if (!$_POST["mid"])
            {
                $fmfac = new ForumMsg();
                $fmfac->getTopicByForumIdAndSuperId($_POST["id"], $_POST["rid"]);
                $fmfac->update("last_datum='".date("Y-m-d H:i:s")."'");
            }
            
            
            $_SESSION["punkte"] = "eintrag";
            
            header("Location: ".WEBDIR."bewertungen/forum.view.php?id=".$_POST["id"]."&rid=".$_POST["rid"]."&sv=1&offset0=".$_POST["offset0"]."&offset1=".$_POST["offset1"]);
        }
        else
        {
            $_SESSION["err"] = $err;
            $_SESSION["post"] = $_POST;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }    
    } // if ($_POST["mode"] == "msg")
}

else
{
    header("Location: ".WEBDIR."inc/msg.php?msg=login");
}

?>
