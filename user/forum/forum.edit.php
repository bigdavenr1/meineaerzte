<?php
////////////////////////////////////////////////////////////////////////////////
// forum.edit.php - Zum Bearbeiten des Forums / Eintr�ge
////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
?>
 <script src="<?php echo WEBDIR;?>script/addon.js" language="javascript" type="text/javascript"></script>
<?php
if ($_SESSION["user"])
{
       // Fehlerbox
        ////////////////////////////////////////////////////////////////////////
         if ($_SESSION["err"])
        {?>
            <p class="contentboxheader">Hier stimmt was nicht</p>
            <div class="contentbox">
                <?php echo $_SESSION["err"]; unset($_SESSION["err"]); ?>
            </div>
        <?php }
    
        // Eingabemasken
        ////////////////////////////////////////////////////////////////////////
        if ($_GET["mode"] == "msg" || $_GET["mode"] == "newmsg")
        {
        ?>
            <p class="contentboxheader"><?php echo ($_GET["mode"]=="msg"?"Beitrag schreiben":"Neues Topic erstellen");?></p>
            <br />
                <form action="<?php echo $l->makeFormLink('./forum.update.php'); ?>" method="post" class="forum">
                    <input type="hidden" name="mode" value="<?php echo $_GET['mode'];?>" />
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
                    <input type="hidden" name="rid" value="<?php echo $_GET['rid'];?>" />
                    <table cellpadding="0" cellspacing="0" class="eintrag">
                        <tr> 
                            <th rowspan="2" class="miniprofil" valign="top">
                                  <?php
                                  $benutzerfac = new Benutzer();
                                  $benutzerfac->getById($_SESSION["user"]->id);
                                  $benutzer = $benutzerfac->getElement();
                                  
                                  $dateifac = new Datei("file");
                                  $dateifac->getByRefAndGroup($benutzer->id, "profil");
                                  
                                  
                                  /*echo '<p class="thumbbox">';
                                  if ($datei = $dateifac->getElement())
                                  {
                                      $thumbfac = new Thumb($dateifac->getDatei($datei), "files/thumb/", $CONST_THUMB['width'], $CONST_THUMB['height']);
                                      $thumbfac->write();
                                      $bildsize = getimagesize($_SERVER["DOCUMENT_ROOT"].$thumbfac->getDatei());
                                      echo $l->makeLink('<img src="'.$thumbfac->getDatei().'" alt="'.$datei->name.'" style="margin-top:'.round(((60 - $bildsize[1]) / 2)).'px;" />', WEBDIR."bewertungen/userprofil.php?id=".$benutzer->id); 
                                  }
                                  else 
                                      echo $l->makeLink('<img src="'.WEBDIR.'images/head_who_thumb.gif" alt="'.$benutzer->name.'" />', WEBDIR."bewertungen/userprofil.php?id=".$benutzer->id);
                                  echo '</p><br class="clr" />';
                                  */
                                  echo $benutzer->nickname."<br />";
                                  
                                  $forummsgfac2 = new ForumMsg();
                                  $forummsgfac2->getByUser($benutzer->id);
                                  
                                  echo "Beitr�ge: ".$forummsgfac2->getElementCount()."<br />";
                                  ?>
                            </th>
                            <th class="headline">
                                  <?php
                                  if ($_GET["mode"] == "newmsg")
                                      echo 'Titel:<br /><input type="text" name="titel" />';
                                  else
                                      echo date("d.m.Y H:i");
                                  ?>
                            </th>
                        </tr> 
                        <tr> 
                            <td class="text">
                                <?php 
                                $htmlarea = new HtmlArea("area");
                                echo $htmlarea->getHtmlButtons();
                                ?>
                            
                                <textarea id="area" name="text" rows="0" cols="0"><?php if ($_GET["zitat"]) echo '[quote msg='.$_GET["zitat"].']'.chr(10);?></textarea>
                            </td>
                            
                        </tr>
                    
                    </table>
                    <br />
                    <?php echo $l->makeLink("Zur�ck", WEBDIR."bewertungen/forum.view.php?id=".$_GET["id"]."&amp;rid=".$_GET["rid"], "backlink");?>
                    <input type="submit" value="Absenden" class="submit" /><br class="clr" />
                </form>
        <?php 
        }// end if ($_GET["mode"] == "msg" || $_GET["mode"] == "newmsg")

}// end if ($_SESSION["user"])

include(INCLUDEDIR."footer.inc.php");
?>