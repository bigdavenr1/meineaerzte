<?php
////////////////////////////////////////////////////////////////////////////////
// forum.delete.php - Zum L�schen eines Beitrags
////////////////////////////////////////////////////////////////////////////////
include("../../inc/config.php");

if ($_SESSION["user"])
{
    if ($_POST["submit"])
    {
        if ($_POST["mode"] == "msg")
        {
            $forummsgfac = new ForumMsg();
            $forummsgfac->getById($_POST["mid"]);
            $forummsgfac->getElement();
            $forummsgfac->deleteElement();
        
            header("Location: ".WEBDIR."bewertungen/forum.view.php?sv=1&id=".$_POST["id"]."&rid=".$_POST["rid"]."&offset0=".$_POST["offset0"]."&offset1=".$_POST["offset1"]);
        }
    }
    //  Sicherheitsabfrage
    //////////////////////////////////////////////////////////////////////////////////////////////
 
    include(INCLUDEDIR."header.inc.php");
    
    if ($_GET["mode"] == "msg") 
    { ?>
            <p class="contentboxheader">L�schen</p>
            <div class="contentbox">
                <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
                    
                    <input type="hidden" name="mode" value="<?php echo $_GET['mode'];?>">
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                    <input type="hidden" name="rid" value="<?php echo $_GET['rid'];?>">
                    <input type="hidden" name="mid" value="<?php echo $_GET['mid'];?>">
                    <input type="hidden" name="offset0" value="<?php echo $_GET['offset0'];?>">
                    <input type="hidden" name="offset1" value="<?php echo $_GET['offset1'];?>">
                    
                    Soll dieser Beitrag wirklich gel�scht werden?<br><br>
                    
                    <?php echo $l->makeLink("Zur�ck", WEBDIR."bewertungen/forum.view.php?id=".$_GET["id"]."&amp;rid=".$_GET["rid"], "backlink");?>
                    <input type="submit" value="L�schen" name="submit" class="submit"><br class="clr">
                </form>
            </div>
    
    <?php 
    } 
    include(INCLUDEDIR."footer.inc.php");
}
else
{
    include(INCLUDEDIR."header.inc.php");
    echo "Nicht eingeloggt!";    
    include(INCLUDEDIR."footer.inc.php");
}   
?>
    
    



