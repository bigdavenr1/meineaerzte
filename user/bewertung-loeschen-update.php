<?php
////////////////////////////////////////////////////////////////////////////////
// user/bewertung-loeschen-update.php - L�scht datensatz aus Tabelle bewertungen
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."std/email.class.php");          // eMail-class

if ($_POST["mode"] == "delete_yes")
{
    $kommentarfac = new Bewertung();
    $kommentarfac->getById($_POST['id']);
    $kommentar = $kommentarfac->getElement();
    $kommentarfac->deleteElement();
    
    // eMail an Langer versenden
    // Daten f�r Mailtext hohlen
    $benutzerfac = new Benutzer();
    $benutzerfac->getById($kommentar->userid);
    $benutzer = $benutzerfac->getElement();
    
    $unternehmenfac = new Daten();
    $unternehmenfac->getById($kommentar->unternehmenid);
    $unternehmen = $unternehmenfac->getElement();
    
    // Mailtext
    $msg = "Hallo Herr Langer".chr(10).chr(10);
    $msg .= "Zeit ".date("d.m.Y",time()).chr(10);
    $msg .= $benutzer->anrede." ".$benutzer->name." ".$benutzer->vorname." hat die Bewertung f�r ".chr(10);
    $msg .= $unternehmen->famname." ".$unternehmen->name." zur�ckgezogen.";

    $emailfac = new Email(1);
    $emailfac->setFrom($CONST_MAIL['from']);
    $emailfac->setTo($CONST_MAIL['an']);
    $emailfac->setSubject("Bewertung zurueckgezogen");
    $emailfac->setContent($msg);
    //echo nl2br($msg);
    $emailfac->sendMail();
    // End eMail an Langer
    
    header("Location: ./bewertung-loeschen.php?sv=1");
}

include(INCLUDEDIR."header.inc.php");?>

<h1>Bewertungen zur�ckziehen</h1>
<br/>

Wollen Sie folgende Bewertung wirklich zur�ckziehen? Dieser Vorgang kann nicht r�ckg�ngig gemacht werden!<br/><br/>
<form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
<input type="hidden" name="mode" value="delete_yes"/>
<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"/>
<?php


    $kommentarfac = new Bewertung(25);
    $kommentarfac->getById($_GET['id']);
    
?>

<table style="width:100%">
    <tr>
        <th style="width:150px">
            Bewertung
        </th>
        <th >
            Kommentar
        </th>
        <th style="width:100px">
            Datum
        </th>


    </tr>
    
    
<?php 
$t=0;
if ($kommentar = $kommentarfac->getElement())
{

    ?>
    <tr>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php
            // Bewertungsarray()
            $bewertungen=array();
 
            // Bewertung auslesen 
            $bewertungen[]=unserialize($kommentar->bewertung);
                    
            // Gr��e des Bewertungsarrays bestimmen         
            $size_bewertung=sizeof($bewertungen);
                                
            $punktebewertung=0;
                                     
            //Bewertungspunkte addieren 
            for ($x=0;$x<$size_bewertung;$x++)
            {
                $punktebewertung=$punktebewertung+(array_sum($bewertungen[$x])/sizeof($bewertungen[$x]));
            }
                                       
            // Quersumme bilden
            $gesamtbewertung=$punktebewertung/$size_bewertung;
             
            // Ausgabe der Bewertung
            for ($r=1;$r<=5;$r++)
            {
                if ($r<=round($gesamtbewertung,0))
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star.gif" alt="Stern-bewertet-<?php echo $r;?>"/>
                <?php 
                }
                
                else
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star_grey.gif" alt="Stern-unbewertet-<?php echo $r;?>"/>
                <?php
                }
           }
           ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php echo substr($kommentar->kommentar,0,200);?>...
       </td>

       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php if (is_numeric($kommentar->datum))  echo date("d.m.Y",$kommentar->datum); ?>

       </td>

   </tr>

</table>

<br /><br />

 <input type="submit" value="L�schen" name="submit" class="submit left" />                    <?php echo $l->makeLink("Zur�ck", "./bewertung-loeschen.php", "backlink");?>
 </form>
<?php 

}
else 
{
    echo "Keine neuen Kommentare vorhanden";
}

include(INCLUDEDIR."footer.inc.php");
?>

