<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");

include(INCLUDEDIR."std/email.class.php");

// Fehlerpr�fung
if (!$_POST["strasse"]) $err .= "Du hast das Feld 'Strasse' vergessen<br>";
if (!$_POST["plz"]) $err .= "Du hast das Feld 'PLZ' vergessen<br>";
if (!$_POST["ort"]) $err .= "Du hast das Feld 'Ort' vergessen<br>"; 
if (!$_POST["email"]) $err .= "Du hast das Feld 'eMail' vergessen<br>";
if ($_POST["pass"] != $_POST['repass']) $err .= "Das Passwort und die Widerholung m�ssen �bereinstimmen<br>";
$_POST['pass']=trim($_POST["pass"]);
if ($_POST['pass']!="")
{
    if ($_POST["pass"] && strlen($_POST["pass"]) < 6) $err .= "Ihr Passwort ist zu kurz (min 6 Zeichen)<br />";
}

// Wenn kein Fehler    
if (!$err)
{
    // Daten holen
    $benutzerfac = new Benutzer();
    $benutzerfac->getById($_SESSION['user']->id);
    $benutzer = $benutzerfac->getElement();
    
    $salt = generateRandomKey(8); // salt als Variable erzeugen
    // DAten updaten
    $benutzerfac->update("adresse='".$_POST['strasse']."', plz='".$_POST['plz']."', ort='".$_POST['ort']."', vorwahl='".$_POST['vorwahl']."', fax='".$_POST['fax']."', land='".$_POST['land']."', homepage='".$_POST['homepage']."', tel='".$_POST['telefon']."'");
    
    // Wenn Passwort ver�ndert wurde    
    if ($_POST['pass'])
    {
        $benutzerfac->update("pass='".md5($_POST['pass'].$salt)."',  salt='".$salt."'" );
    }
    
    // Wenn KEINE neue eMailadresse angegeben ist
    if ($_POST['email']==$benutzer->email)
    {
        include(INCLUDEDIR."header.inc.php");
        echo '<h1>�nderung erfolgreich</h1>Die Benutzerdaten wurden erfolgreich ge�ndert<br /><br />'.$l->makeLink("zur�ck zur �berischt",WEBDIR."user/change-data.php");
    }  
    
    // Wenn keine neue eMailadresse angegeben ist
    else
    {
        $benutzerfac = new Benutzer();
        $benutzerfac->getByMail($_POST["email"]);
        
        // Pr�fung auf Dopplung
        if ($benutzerfac->getElementCount()!=0) 
        {
            $err = "Diese Emailadresse gibt es schon.<br />";
        }
        
        // Wenn eMail-Adresse noch nicht existiert    
        else
        {
            // benutzerdaten nochmal holen
            $benutzerfac->getById($_SESSION['user']->id);
            $key=generateRandomKey(30);
            $benutzerfac->update("aktiv='0', aktivierungskey='".$key."', email='".$_POST['email']."'");
            
            // Mail versenden
            $msg = "Hallo ".$benutzer->vorname." ".$benutzer->name.chr(10);
            $msg .= chr(10);
            $msg .= "Jetzt sind Sie nur noch einen Klick davon entfernt, Ihre neue eMail-Adresse zu best�tigen. Der folgende Link reaktiviert einmalig Ihr Profil. Diese Mail ist automatisch generiert, deshalb bitte nicht auf diese eMail antworten.".chr(10).chr(10);
            $msg .= "Viel Spass!".chr(10).chr(10);
            $msg .= "http://www.meineaerzte.at/signin/activate.php?key=".$key.chr(10);
                                  
            $emailfac = new Email();
            $emailfac->setFrom($CONST_MAIL['from']);
            $emailfac->setTo($_POST['email']);
            $emailfac->setSubject("Account - Best�tigung der ge�nderten eMail-Adresse - �rztebewertung");
            $emailfac->setContent($msg);
            $emailfac->sendMail();
            
            // ausloggen
            session_destroy();
            
            // weiterleiten        
            header("Location:".WEBDIR."inc/msg.php?msg=newmail");
        }
    }
}

// Wenn fehler aufgetaucht sind           
if ($err) 
{      
    $_SESSION["err"] = $err; 
    header("Location: ./change-data.php?sv=1");
}

include(INCLUDEDIR."footer.inc.php");
?>