<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");?>
<h1>Willkommen im Userbereich</h1><br/>
Sie haben sich erfolgreich eingeloggt Sie k�nnen nun Ihre pers�nlichen Daten �ndern und erweiterte Bewertungen zu Eintr�gen abgeben.<br/><br/>
Eine wiederholte Bewertung eines Eintrages kann erst nach einer Wartezeit von 7 Tagen erfolgen. Den Status Ihrer Bewertungen sehen Sie in der Tabelle unten.
<br/>
<ul>
   <li>
       <?php echo $l->makeLink("[ pers�nliche Daten �ndern ]",WEBDIR."user/change-data.php");?>
   </li>
     <li>
       <?php echo $l->makeLink("[ Profildaten �ndern ]",WEBDIR."user/change-online.php");?>
   </li>
   <li>
       <?php echo $l->makeLink("[ Im Foren schreiben / Eigene Beitr�ge �ndern ]",WEBDIR."bewertungen/forum.view.php");?>
   </li>
   <li>
       <?php echo $l->makeLink("[ Arzteintrag suchen ]",WEBDIR."bewertungen/search.php?mod=searchplus");?>
   </li>
   <li>
       <?php echo $l->makeLink("[ Bewertung zur�ckziehen ]",WEBDIR."user/bewertung-loeschen.php");?>
   </li>
</ul>

<br/>
<hr />

<h1>folgende Eintr�ge wurden bereits von Ihnen bewertet</h1>

<table style="width:100%">    
    <tr>
        <th style="width:150px">Bewertung</th>
        <th >Kommentar</th>
        <th>f�r Eintrag</th>
        <th>Datum</th>
        <th>Status</th>
    </tr>
<?php 

$datenfac = new Bewertung(15);
$datenfac->getByUser($_SESSION['user']->id);

$t=0;
while ($daten = $datenfac->getElement())
{
   // pr�fung ob Unternehmen vorhanden
   if ($daten->unternehmen=="" && $daten->name=="" && $daten->vorname=="")
   {
      // Keine Aktion
   }
   //Wenn Unternehmen vorhanden
   else 
   {
    $t++;


    
?>
   <tr>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php
            // Bewertungsarray()
            $bewertungen=array();
 
            // Bewertung auslesen 
            $bewertungen[]=unserialize($daten->bewertung);
                    
            // Gr��e des Bewertungsarrays bestimmen         
            $size_bewertung=sizeof($bewertungen);
                                
            $punktebewertung=0;
                                     
            //Bewertungspunkte addieren 
            for ($x=0;$x<$size_bewertung;$x++)
            {
                $punktebewertung=$punktebewertung+(array_sum($bewertungen[$x])/sizeof($bewertungen[$x]));
            }
                                       
            // Quersumme bilden
            $gesamtbewertung=$punktebewertung/$size_bewertung;
             
            // Ausgabe der Bewertung
            for ($r=1;$r<=5;$r++)
            {
                if ($r<=round($gesamtbewertung,0))
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star.gif" alt="Stern-bewertet-<?php echo $r;?>"/>
                <?php 
                }
                
                else
                {?>
                    <img src="<?php echo WEBDIR;?>images/icons/star_grey.gif" alt="Stern-unbewertet-<?php echo $r;?>"/>
                <?php
                }
           }
           ?>
 
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
       <?php if ($daten->kommentar!="") echo substr($daten->kommentar,0,150).'...'; ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php echo $l->makeLink($daten->unternehmen.'<br/>'.$daten->name.', '.$daten->vorname,WEBDIR."bewertungen/showeintrag.php?id=".$daten->unternehmenid); ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
            <?php if (is_numeric($daten->datum))  echo date("d.m.Y",$daten->datum); ?>
       </td>
       <td <?php if ($t%2) echo 'class="td1"'; ?>>
           <?php if ($daten->aktiv == 1)  echo "best�tigt"; else echo "unbest�tigt"; ?>
       </td>
       
   </tr>



<?php } }?>

</table>
<br />


<?php if ($t==0)
{
   echo 'noch keine Bewertung abgegeben!';
}

else {
?>




<div class="contentboxsmall" style="text-align:left;">
        Seiten: <?php echo $datenfac->getHtmlNavi("std", "&amp;id=".$benutzer->id."&amp;rid=".$_GET["rid"]);?>
    </div>

    
    
    

<?php }

echo '<br/><br/>Haben Sie eine Bewertung abgegeben, welche hier nicht angezeigt wird?<br/>In unseren '.$l->makeLink('F.A.Q.',WEBDIR."bewertungen/faq.php").' erfahren Sie warum!';

 include(INCLUDEDIR."footer.inc.php");?>