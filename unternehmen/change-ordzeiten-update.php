<?php
////////////////////////////////////////////////////////////////////////////////
// change-ordzeiten-update.php - verarbeiten der Formulardaten 
//                               f�r Ordzeiten aus change-ordzeiten-edit.php
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");

$ordzeitenfac = new Ordzeiten();

//print_r($_POST);
// Datenpr�fung
unset($_SESSION['err']);
// Tage durchgehen auf Datenkonsistenz pr�fen
for($i=1;$i<8;$i++)
{
    if($_POST['day'.$i])
    {
        if($_POST['time1'.$i] == '') $_SESSION['err'] .= "Die Startzeit am ".$ordzeitenfac->parseDay($i,"long")." wurde nicht gesetzt!<br />";
        if($_POST['time2'.$i] == '') $_SESSION['err'] .= "Die Endzeit am ".$ordzeitenfac->parseDay($i,"long")." wurde nicht gesetzt!<br />";
        if($_POST['time3'.$i] != '' && $_POST['time4'.$i] == '' ) $_SESSION['err'] .= "Die zweite Endzeit am".$ordzeitenfac->parseDay($i,"long")." wurde nicht gesetzt!<br />";

        if($_POST['time2'.$i]<$_POST['time1'.$i]) $_SESSION['err'] .= "Die Endzeit am ".$ordzeitenfac->parseDay($i,"long")." muss nach der Startzeit liegen!<br />";
        if($_POST['time3'.$i] != '' && ($_POST['time3'.$i]<$_POST['time2'.$i])) $_SESSION['err'] .= "Die zweite Startzeit am ".$ordzeitenfac->parseDay($i,"long")." muss nach der ersten Endzeit liegen!<br />";
        if($_POST['time3'.$i] != '' && ($_POST['time4'.$i]<$_POST['time3'.$i])) $_SESSION['err'] .= "Die zweite Endzeit am ".$ordzeitenfac->parseDay($i,"long")." muss nach der zweiten Startzeit liegen!<br />";
    }
}

if(!isset($_SESSION['err']))
{
    // unternehmen Id hohlen mit user mail
    $datenfac = new Daten();
    $datenfac->getByMail($_SESSION['user']->email);
    if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
    $firma = $datenfac->getElement();
    
    if($firma)
    {
        $ordzeitenfac->getByFirmId($firma->id);
        $count = $ordzeitenfac->getElementCount();
        if($count > 0)
        {
            // alte Datens�tze l�schen
            while($ordzeit = $ordzeitenfac->getElement())
            {
                $ordzeitenfac->deleteElement();
            }
        }
    }
    else $_SESSION['err'] .= "Kein Unternehmen gefunden";
}

if(!isset($_SESSION['err']))
{
    // neuer Datensatz bzw. neue Datens�tze schreiben
    for($i=1;$i<8;$i++)
    {
        if($_POST['day'.$i])
        {
            unset($odata);
            unset($ordzeitenfac1);
            $ordzeitenfac1 = new Ordzeiten(); 
            $odata[] = "";                       // id
            $odata[] = $firma->id;               // firmid
            $odata[] = $i;                       // daystart
            $odata[] = $i;                       // dayend
            $odata[] = $_POST['time1'.$i];       // timestart
            $odata[] = $_POST['time2'.$i];       // timeend
            $odata[] = $_POST['memo'.$i];        // timeend
            $ordzeitenfac1->write($odata);
        }
        if($_POST['day'.$i] && $_POST['time3'.$i] != '')
        {  
            unset($o2data);
            unset($ordzeitenfac2);
            $ordzeitenfac2 = new Ordzeiten(); 
            $o2data[] = "";                       // id
            $o2data[] = $firma->id;               // firmid
            $o2data[] = $i;                       // daystart
            $o2data[] = $i;                       // dayend
            $o2data[] = $_POST['time3'.$i];       // timestart
            $o2data[] = $_POST['time4'.$i];       // timeend
            $o2data[] = $_POST['memo'.$i];        // timeend
            $ordzeitenfac2->write($o2data);
        }
    }
    header("Location:./change-ordzeiten.php?sv=1");
} 
else header("Location:./change-ordzeiten-edit.php?sv=1");
?>

