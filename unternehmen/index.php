<?php
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

if ($_SESSION["ustatus"] == "N")
{?>
    <h1>
        Willkommen im Mitgliederbereich
    </h1>
    <?php if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);} ?>

    <p>Hier die AGB .................................<br/><br/>
       Mit dem Versenden des Formulars erkl�ren Sie sich mit den AGB einverstanden und sind f�r die Richtigkeit und Pflege Ihrer Daten selbst zust�ndig. Nach Best�tigung der AGB werden Sie automatisch auf Ihre Datenseite weitergeleitet.</p>
    <form action="<?php echo WEBDIR;?>unternehmen/zustimmen.php" method="post"  >
        <label for="zustimmen">
            Zustimmen
        </label>
        <input type="checkbox" id="zustimmen" name="zustimmen" value="Z" class="selectbox" />
        <input type="submit" value="Zustimmen" class="submit" /><br class="clr" />
    </form>
    
<?php 
}
else
{
?>
<h1>Willkommen im Mitgliederbereich</h1>
Sie haben sich erfolgreich eingeloggt und k�nnen nun Ihr Profil bearbeiten, Bilder hochladen, den Premiumstatus erlangen, Und als Premiummitglied Antworten zu Bewertungen verfassen..
<ul>
    <li><?php echo $l->makeLink("pers�nliche Daten bearbeiten",WEBDIR."unternehmen/change-data.php");?></li>
    <li><?php echo $l->makeLink("Profilbilder bearbeiten",WEBDIR."unternehmen/change-pictures.php");?></li>
    <li><?php echo $l->makeLink("Suchw�rter und Fachgebiete bearbeiten",WEBDIR."unternehmen/change-fachgebiete.php");?></li>
    <li><?php echo $l->makeLink("Beschreibung �ndern",WEBDIR."unternehmen/change-beschreibung.php");?></li>
    <li><?php echo $l->makeLink("�ffnungszeiten bearbeiten",WEBDIR."unternehmen/change-ordzeiten.php");?></li>
    <li><?php echo $l->makeLink("Erhaltene Bewertungen",WEBDIR."unternehmen/bewertungen.show.php");?></li>
    <li><?php echo $l->makeLink("Auf Bewertungskommentare antworten",WEBDIR."unternehmen/bewertungen.view.php");?></li>
    <?php  if(!$_SESSION['ustatus'] == "A" && !$_SESSION['ustatus'] == "P" ) echo '<li>'.$l->makeLink("Premiumupdate",WEBDIR."unternehmen/premiumanmeldung.php").'</li>'; ?>

</ul>
<br /><br />
<hr />
<br />
<?php
}

 include(INCLUDEDIR."footer.inc.php");?>
