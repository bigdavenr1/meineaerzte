<?php
////////////////////////////////////////////////////////////////////////////////
// unternehmen/change-ordzeiten-edit.php zeigt das Formular zur Änderung von Unternehmen an
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

$ordzeitenfac = new Ordzeiten();

// unternehmen Id hohlen mit user mail
$datenfac = new Daten();
$datenfac->getByMail($_SESSION['user']->email);
if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);

if ($firma = $datenfac->getElement())  $ordzeitenfac->getByFirmId($firma->id);
else $_SESSION['err'] .= "Für Ihr Login Kein Unternehmen gefunden<br />";

?>
<h1>Öffnugszeiten eingeben</h1>
<br />
<?php 
if ($_SESSION['err']) 
{
    echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; 
    unset($_SESSION['err']);
}
?>
<form action="<?php echo $l->makeFormLink(WEBDIR.'unternehmen/change-ordzeiten-update.php?fid='.$_GET["fid"].'&amp;id='.$_GET["id"].'&amp;mode='.$_GET["mode"]);?>" method="post" >
    <fieldset>
        <legend>
           Öffnungszeitenformular
        </legend>
        <table ><tr><th>Tage</th><th colspan="2">Vormittags</th><th colspan="2">Nachmittags</th><th>Ordination/Vereinbarung</th></tr>
        <?php
        for($i=1;$i<8;$i++)
        {
        ?>
         <tr><td <?php if ($i%2) echo 'class="td1"'; ?> >
            <label for="<?php echo 'day'.$i ?>" style="width:100px"><?php echo $ordzeitenfac->parseDay($i,"long"); ?></label>
            <?php 
                $ordzeitenfac->getByFirmIdAndDay($firma->id,$i);
                $counttimes = $ordzeitenfac->getElementCount();
                $ordzeit = $ordzeitenfac->getElement();
            ?>
            <input type="checkbox" id="<?php echo 'day'.$i; ?>" name="<?php echo 'day'.$i; ?>" value="<?php echo $ordzeitenfac->parseDay($i,"long"); ?>" style="width:50px" <?php if($ordzeit) echo "checked='checked'"?>/></td>
           <td <?php if ($i%2) echo 'class="td1"'; ?> >
              von<select id="<?php echo 'time1'.$i ?>" name="<?php echo 'time1'.$i ?>" style="width:75px">
              <option value =''>---</option>
              <?php  
                  for($j=1;$j<50;$j++)
                  {      
                      unset($teststring); 
                      if($ordzeit->timestart == $j) $teststring = "selected='selected'"; 
                      else $teststring = "";  
                      echo "<option value='".$j."'".$teststring." >".$ordzeitenfac->parseTime($j)."</option>";
                  }  
               ?>
              </select>
            </td>
            <td <?php if ($i%2) echo 'class="td1"'; ?> >
                bis<select id="<?php echo 'time2'.$i ?>" name="<?php echo 'time2'.$i ?>" style="width:75px">
                <option value =''>---</option>  
                  <?php  
                      for($k=1;$k<50;$k++)
                      {   
                          unset($teststring); 
                          if($ordzeit->timeend == $k) $teststring = "selected='selected'"; 
                          else $teststring = "";  
                          echo "<option value='".$k."'".$teststring." >".$ordzeitenfac->parseTime($k)."</option>";
                      }  
                   ?>
                  </select>
             </td>
             <td <?php if ($i%2) echo 'class="td1"'; ?> >
                von<select id="<?php echo 'time3'.$i ?>" name="<?php echo 'time3'.$i ?>" style="width:75px">
                <option value =''>---</option>
                <?php  
                    if($counttimes > 1) // zweiten datensatz holen
                        {
                            $ordzeit = $ordzeitenfac->getElement();
                        }
                    for($m=1;$m<50;$m++)
                    {
                        unset($teststring);
                        if($counttimes > 1)
                        {
                            if($ordzeit->timestart == $m) $teststring = "selected='selected'"; 
                            else $teststring = "";
                        }
                        echo "<option value='".$m."'".$teststring." >".$ordzeitenfac->parseTime($m)."</option>";
                    }  
                 ?>
              </select>
             </td>
             <td <?php if ($i%2) echo 'class="td1"'; ?> >
                bis<select id="<?php echo 'time4'.$i ?>" name="<?php echo 'time4'.$i ?>" style="width:75px">
                <option value =''>---</option>
                <?php  
                    for($n=1;$n<50;$n++)
                    {   
                        unset($teststring);
                        if($counttimes > 1)
                        {
                            if($ordzeit->timeend == $n) $teststring = "selected='selected'"; 
                            else $teststring = "";
                        }    
                        echo "<option value='".$n."'".$teststring." >".$ordzeitenfac->parseTime($n)."</option>";
                    }  
                 ?>
                </select>
             </td>
             <td <?php if ($i%2) echo 'class="td1"'; ?>>
                <input type="text" id="<?php echo 'memo'.$i; ?>" name="<?php echo 'memo'.$i; ?>" value="<?php echo $ordzeit->memo; ?>" />
             </td>
          </tr>  
        
        <?php
        }
        
        ?>
        </table>
     </fieldset>
    <br class="clr" /><br/>
    <input type="submit" value="Daten speichern" class="submit" />
    <?php echo $l->makeLink("Zurück zur Übersicht", "./change-ordzeiten.php", "backlink");?>
    <br class="clr"/><br/>
</form>
<?php

include(INCLUDEDIR."footer.inc.php");
?>

