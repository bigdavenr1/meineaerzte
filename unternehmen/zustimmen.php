<?php
////////////////////////////////////////////////////////////////////////////////
// unternehmen/zustimmen.php - Auswertung AGB-Formular
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");

// Fehlervariable l�schen
unset ($_SESSION["err"]);

// Fehlerpr�fung OHNE Email-Dopplungspr�fung
if($_SESSION['ustatus'] == 'N' && !$_POST['zustimmen'] ) $err .= "Sie m�ssen der Erkl�rung zustimmen<br />";

// Wenn kein Fehler ist
if (!$err)
{       
    $datenfac = new Daten();
    $datenfac->getByMail($_SESSION['user']->email);
    if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
    if($daten=$datenfac->getElement())
    {
        // update des Status
        $datenfac->update("status='F'");
        $_SESSION['ustatus']= "F";
        header("Location: ./change-data.php?sv=1");
    }
    else  $err .= "Fehler beim Datenaufruf!<br />";  
}
// Wenn die Pr�fung Fehler ergeben hat
else 
{
    $_SESSION["err"] = $err; 
    header("Location: ./index.php?sv=1");
}
?>