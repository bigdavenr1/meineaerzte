<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// unternehmen/edit.php zeigt das Formular zur �nderung von Unternehmen an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// daten holen
$datenfac = new Daten();
$datenfac->getByMail($_SESSION['user']->email);
if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
$daten = $datenfac->getElement();

// Bildarray �berpr�fen
if (is_array(unserialize($daten->bild))) $bilder=unserialize($daten->bild);
else $bilder=array();

// Status �berpr�fen und maximale Bildanzahl setzen
if ($daten->status=="P") $max=5;
else $max=1;

?>

<h1>Bilder bearbeiten</h1>

<br />
Hinweis: Das erste Bild ist Ihr Profilbild, welches in der Suchliste und als gro�es Vorschaubild in der Profilansicht erscheint. 
<br/><br/>
<?php if ($_SESSION['err']) echo '<span class="err">'.$_SESSION['err'].'</span><br/>';?>
<form action="<?php echo $l->makeFormLink(WEBDIR.'unternehmen/change-pictures-update.php');?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>
            Profilbild
        </legend>
        <?php
        // Wenn noch keine Bilder hochgeladen wurden
        if(sizeof($bilder)==0) echo '<img src="'.WEBDIR.'images/nopicture.gif" width="125" height="125" alt="" style="border:1px dotted #000;float:left;margin-right:5px;" />';
        
        // Wenn Bilder hochgeladen wurden
        else 
        {
            // Begrenzung wenn Free user
            if ($daten->status=="P") $maxsize=sizeof($bilder);
            else $maxsize=1;
            
            // Bildschleife
            for ($x=0; $x<$maxsize;$x++)
            {
                // Grafiktyp auslesen
                $typ=getimagesize(LOCALDIR."images/unternehmen/".$bilder[$x]['url']) ;
      
                // je nach typ Endung bestimmen   
                if ($typ[2]==1) $endung=".gif";
                if ($typ[2]==2) $endung=".jpg"; 
            
                // Reinnamen ohne Endung
                $name=basename($bilder[$x]['url'],$endung);
            
                // Gr��e des Thumbnails auslesen
                $size=getimagesize(LOCALDIR."images/unternehmen/".$name."_thumb".$endung);
      
                // HTML Bild schreiben 
                ?>
                <div <?php if ($x%2) echo 'class="td1"';?>>
                    <img src="<?php echo WEBDIR;?>images/unternehmen/<?php echo $name;?>_thumb<?php echo $endung;?>" height="<?php echo $size[1]; ?>" width="<?php echo $size[0]; ?>" alt="Bild von <?php echo $daten->name." ".$daten->famname?>"/>
                    <input type="checkbox" name="bildloeschen<?php echo $x;?>" class="selectbox" /> Bild l�schen  <br class="clr"/>
                    <label>Bildkommentar</label><input type="text" name="bildkommentar[<?php echo $x;?>]" id="bildkommentar" value="<?php echo $bilder[$x]['bildkommentar'];?>"/>
                </div> 
        <?php
            }
        }
        ?>
        <br class="clr"/><br/><hr/><br/>
        
        <?php 
        // Wenn maximale Bildanzahl noch nicht erreicht wurde
        if (sizeof($bilder)<$max) 
        { ?>
            <label for="bild">
                weiteres Bild hinzuf�gen:
            </label>
            <input id="bildneu" name="bildneu" type="file" /><br class="clr">
            <label for="bild">
                Bildkommentar
            </label>
            <input id="bildkommentarneu" name="bildkommentarneu" type="text" />
        <?php 
        }
        
        // wenn maximale Bildanzahl erreicht wurde 
        else
        {
            echo '<span class="err">Maximale Anzahl an Bildern erreicht!</span>';
            
            // Wenn Free-User
            if ($daten->status!="P") echo '<span class="err">Sie sind Free-User<br/><br/>Als Premiumuser k�nnen Sie bis zu 5 Bilder uploaden! Melden Sie sich noch heute als Premiumuser an und profitieren Sie von den besseren Darstzellungsm�glichkeiten. Als premiumuser haben Sie au�erdem die M�glichkeit eine Beschreibung zu hinterlassen, Bis zu 10 Suchbegriffe einzugeben und Sie werden bei der Suche auf den ersten Pl�tzen angezeigt.</span>';
        }
        ?>
    </fieldset>
    <br class="clr" /><br/>
    <input type="submit" value="Daten speichern" class="submit" />
</form>
    
<?php
include(INCLUDEDIR."footer.inc.php");
?>