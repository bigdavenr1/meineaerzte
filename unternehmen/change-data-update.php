<?php

////////////////////////////////////////////////////////////////////////////////
// unternehmen/change-data-update.php - verarbeiten der Formulardaten 
////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");

include(INCLUDEDIR."std/email.class.php");

// Fehlervariable l�schen
unset ($_SESSION["err"]);

// Fehlerpr�fung OHNE Email-Dopplungspr�fung
if (!$_POST["famname"]) $err .= "Sie haben das Feld 'Nachname' vergessen<br />";
if (!$_POST["vorname"]) $err .= "Sie haben das Feld 'Vorname' vergessen<br />";
if (!$_POST["strasse"]) $err .= "Sie haben das Feld 'Strasse' vergessen<br />";
if (!$_POST["plz"]) $err .= "Sie haben das Feld 'PLZ' vergessen<br />";
if (!$_POST["ort"]) $err .= "Sie haben das Feld 'Ort' vergessen<br />";
if (!$_POST["land"]) $err .= "Sie haben das Feld 'Land' vergessen<br />";
if (!$_POST["tel"]) $err .= "Sie haben das Feld 'Telefon' vergessen<br />";   
if (!$_POST["email"]) $err .= "Sie haben das Feld 'eMail' vergessen<br />";
if ($_POST["email"] && !isMail($_POST["email"])) $err .= "Ihre Emailadresse ist nicht g�ltig<br />";
if ($_POST["pass"])
{   
    if($_POST["pass"] != $_POST["repass"])
    {
        $err .= "Ihr neues Passwort stimmt nicht mit der Passwortwiederholung �berein<br />";
    }
}


if(($_SESSION['ustatus'] == 'N' || $_SESSION['ustatus'] == 'NOFIRMA') && !$_POST['zustimmen'] ) $err .= "Sie m�ssen der Erkl�rung zustimmen<br />";

// Wenn kein Fehler ist
if (!$err)
{       
    $datenfac = new Daten();
    $datenfac->getByMail($_SESSION['user']->email);
    if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
    $daten=$datenfac->getElement();
    
    // Wenn eMail-Adresse ge�ndert wurde
    if ($daten->email!=$_POST['email'])
    {
        // email auf Dopplung pr�fen
        $benutzerfac = new Benutzer();
        $benutzerfac->getByMail($_POST["email"]);
        
        // Wenn eMAil-Doppelt dann Fehlermeldung
        if ($benutzerfac->getElementCount() && $_POST["email"]!="") $err .= "Diese Emailadresse gibt es schon.<br />";
    }

    // Wenn Dopplungspr�fung keinen Fehler ergeben hat    
    if (!$err)
    {
        // Holen der Daten
        if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
    
        // update der Daten
        if($_POST['pass'])
        {
            $datenfac->update("titel='".mysql_real_escape_string($_POST['titel'])."', 
                  anrede='".mysql_real_escape_string($_POST['anrede'])."', 
                famname='".mysql_real_escape_string($_POST['famname'])."', 
                vorname='".mysql_real_escape_string($_POST['vorname'])."', 
                strasse='".mysql_real_escape_string($_POST['strasse'])."', 
                    plz='".mysql_real_escape_string($_POST['plz'])."', 
                    ort='".mysql_real_escape_string($_POST['ort'])."', 
                   land='".mysql_real_escape_string($_POST['land'])."', 
             pass_clear='".mysql_real_escape_string($_POST['pass'])."', 
                    tel='".mysql_real_escape_string($_POST['tel'])."', 
                    fax='".mysql_real_escape_string($_POST['fax'])."', 
                  email='".mysql_real_escape_string($_POST['email'])."', 
               homepage='".mysql_real_escape_string($_POST['homepage'])."',
               klaerung='".mysql_real_escape_string($_POST['klaerung'])."'");
        
        }
        else
        {
            $datenfac->update("titel='".mysql_real_escape_string($_POST['titel'])."', 
                  anrede='".mysql_real_escape_string($_POST['anrede'])."', 
                famname='".mysql_real_escape_string($_POST['famname'])."', 
                vorname='".mysql_real_escape_string($_POST['vorname'])."', 
                strasse='".mysql_real_escape_string($_POST['strasse'])."', 
                    plz='".mysql_real_escape_string($_POST['plz'])."', 
                    ort='".mysql_real_escape_string($_POST['ort'])."', 
                   land='".mysql_real_escape_string($_POST['land'])."', 
                    tel='".mysql_real_escape_string($_POST['tel'])."', 
                    fax='".mysql_real_escape_string($_POST['fax'])."', 
                  email='".mysql_real_escape_string($_POST['email'])."', 
               homepage='".mysql_real_escape_string($_POST['homepage'])."',
               klaerung='".mysql_real_escape_string($_POST['klaerung'])."'");
        }
        
        // Wenn eMail, Name , Vorname , Titel ,Anrede oder Passwort ge�ndert wurde           
        if ($daten->email!=$_POST['email'] || $daten->vorname != $_POST['vorname'] || $daten->famname != $_POST['famname'] || $daten->anrede != $_POST['anrede'] || $daten->titel != $_POST['titel'] || $daten->pass_clear != $_POST['pass'])
        {
            // Abfrage was in der Logintabelle steht (email oder Kundennummer)
            if ($daten->email != "") $ref=$daten->email;
            else $ref=$daten->kundennummer;         
        
            // Update der Logintabelle
            $benutzerfac= new Benutzer();
            if($_POST['pass'])
            {
                $salt=generateRandomKey(8); // salt
                $pass = md5($_POST["pass"].$salt);      // pass mit salt
                $benutzerfac->update("email='".$_POST['email']."', vorname='".$_POST['vorname']."' , name='".$_POST['famname']."'  , pass='".$pass."'  , salt='".$salt."'  , anrede='".$_POST['anrede']."'  , titel='".$_POST['titel']."' WHERE email='".$ref."'");
            }
            else
            {
                $benutzerfac->update("email='".$_POST['email']."', vorname='".$_POST['vorname']."' , name='".$_POST['famname']."'  , anrede='".$_POST['anrede']."'  , titel='".$_POST['titel']."' WHERE email='".$ref."'");
            }
            // neue Email in $_SESSION - Variable
            if($_POST['email']) $_SESSION['user']->email = $_POST['email'];       
        }
      
        include(INCLUDEDIR."header.inc.php");                      
        echo '<h1>�nderung erfolgreich</h1>Die Benutzerdaten wurden erfolgreich ge�ndert<br /><br />'.$l->makeLink("zur�ck zur �bersicht",WEBDIR."unternehmen/change-data.php");
        include(INCLUDEDIR."footer.inc.php");
    }
    
    // Wenn Dopllungspr�fung einen fehler ergeben hat
    else 
    {
        $_SESSION["err"] = $err; 
        header("Location: change-data.php?sv=1");
    }
}

// Wenn die Pr�fung (ohne Dopplungspr�fung) Fehler ergeben hat
else 
{
    $_SESSION["err"] = $err; 
    header("Location: change-data.php?sv=1");
}
?>