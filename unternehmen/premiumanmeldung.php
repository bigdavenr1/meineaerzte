<?php
////////////////////////////////////////////////////////////////////////////////
// unternehmen/premiumanmeldung.php ->
//                    zeigt das Formular zur Premiumanmeldung von Unternehmen an
// Anmeldeformular - Schritt 0 = Einleitung
//                 - Schritt 1 = AGB
//                 - Schritt 2 = Zahlungsart
//                 - Schritt 3 = Best�tigung
////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

/*echo "<pre>"; print_r($_GET); print_r($_POST); print_r($_SESSION['user']); echo "</pre>";*/

// Errors
////////////////////////////////////////////////////////////////////////////////

if (!$_GET["step"] && !$_SESSION["premium"])//  $_GET["step"] <= 4 && 
{
    $_GET["step"] = 0;
}

if ($_SESSION["err"] && $_GET["step"] >= 1)
{
    echo '<div class="err">'.$_SESSION["err"].'</div>';
    unset($_SESSION["err"]);
}

if ($_SESSION["msg"] && $_GET["step"] >= 1)
{
    echo '<div class="ok">'.$_SESSION["msg"].'</div>';
    unset($_SESSION["msg"]);
}
// Formulare
////////////////////////////////////////////////////////////////////////////////
 
    switch ($_GET["step"]) 
    {
        case 1:?>
            <h1>Anmelden - Schritt 1</h1>
            Weiterhin ist es wichtig, das Sie mit unseren AGB einverstanden sind.
            <br /><br /><br />
            <div class="textarea" style="width:495px;height:250px;overflow:auto;">
            <h1>AGB</h1>
            
            Hier die AGB .... �1 ... usw.
            </div>
            <br /><br />
            
            <form action="<?php echo $l->makeFormLink('premium.update.php?step=2'); ?>" method="post">    
                <input type="checkbox" name="agb" class="selectbox" style="width:25px;"><label style="width:400px;">Ich habe die AGB gelesen und bin damit einverstanden.</label><br class="clr" />
                
                <br />
                <?php echo $l->makeLink("Zur�ck","premium.update.php?step=0", "backlink"); ?>
                <input type="submit" value="anmelden" class="submit">
            </form>
            
            <?php break; 
               
        case 2:?>
            <h1>Anmelden - Schritt 2</h1>
            <br />
            <form action="<?php echo $l->makeFormLink('premium.update.php?step=3'); ?>" method="post" class="left" style="margin-right:50px;">
            <fieldset>
            <legend>Bitte w�hlen Sie die Zahlungsart aus</legend>
            <label for="zahlungsweise">
            Zahlungsweise
            </label>
            <select name="zahlungsweise" id="zahlungsweise">
                <option value="">&nbsp;</option>
                <option value="Vorab" <?php if ($_SESSION['premium']['zahlungsweise']=="Vorab") echo 'selected="selected"';?>>Vorab �berweisung</option>
                <option value="Nachnahme" <?php if ($_SESSION['premium']['zahlungsweise']=="Nachnahme") echo 'selected="selected"';?>>Nachnahme</option>
                <option value="Rechnung" <?php if ($_SESSION['premium']['zahlungsweise']=="Rechnung") echo 'selected="selected"';?>>Rechnung</option>
            </select>
            <br class="clr" /> 
            </fieldset>
                <?php echo $l->makeLink("Zur�ck", $_SERVER['PHP_SELF'], "backlink"); ?><input type="submit" value="Weiter" class="submit" />
            </form>
            <br class="clr" />
            
            <?php break; 
        case 3:?>
            <h1>Best�tigung der Anmeldung</h1>
            <br />
                <?php  
                if($_SESSION['premium']['zahlungsweise']=="Vorab")
                {
                 echo 'Sie haben die Zahlungsart Vorabzahlung gew�hlt. Dazu erhalten Sie nachfolgend unsere     Kontodaten. Bitte geben Sie bei der �berweisung Ihre Kundennummer oder eMail-Adresse zur Zuordnung an.<br/><br />'; 
                 echo 'Kontoinhaber: '.$_SESSION['premium']['inhaber'].'<br />';
                 echo 'KontoNr: '.$_SESSION['premium']['konto'].'<br />';
                 echo 'BLZ: '.$_SESSION['premium']['blz'].'<br />';
                 echo 'Bank: '.$_SESSION['premium']['bank'].'<br />';
                 echo 'IBAN: '.$_SESSION['premium']['iban'].'<br />';
                 echo 'BIC: '.$_SESSION['premium']['bic'].'<br />';
                }?>
                <br/>
                Sie erhalten umgehend eine Best�tigungsmail mit den Daten der Premiumanmeldung.      
            <?php echo $l->makeLink("Zur�ck zur �bersicht",WEBDIR."unternehmen/index.php", "backlink"); ?>
            
            <?php 
                unset($_SESSION['premium']); // Sessionvariable nach Durchlauf l�schen
                break;
        default:?>
            <h1>Premiumanmeldung</h1>
            <br />
            Um die vollen Vorteile als Premiummittglied nutzen zu k�nnen m�ssen Sie sich hier anmelden.
            Sobald die daten gepr�ft wurden, werden Sie als Premiummitglied freigeschaltet.
            ...etc.
            <br /><br />
            <form action="<?php echo $l->makeFormLink('premiumanmeldung.php?step=1'); ?>" method="post">
                <input type="submit" value="weiter" class="submit" /><br class="clr" />
            </form>
             
            <?php 
            break;
    }
include(INCLUDEDIR."footer.inc.php");
?>

