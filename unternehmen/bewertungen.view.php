<?php
////////////////////////////////////////////////////////////////////////////////
// unternehmen/bewertungen.view.php zeigt Tabelle mit Bewertungen des Unternehmens
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// Wenn eingeloggt
if ($_SESSION['user']->typ == "unternehmen")
{
    // Firmen Id hohlen
    // Object initialisieren
    $datenfac = new Daten();
    $datenfac->getByMail($_SESSION['user']->email);
    //$datenfac->getById(11);
    $daten = $datenfac->getElement();
     
      // Bewertungsdaten holen
      $bewertungfac=new Bewertung();
      //$bewertungfac->getByUid($daten->id);
      $bewertungfac->getByUidAndKom($daten->id);
      $z=0;
      ?>
  
      <h1>Bewertungen zu "<?php echo $daten->name.' - '.$daten->titel.' '.$daten->famname.', '.$daten->vorname ;?>" <a name="bewertung">&nbsp;</a></h1>
      <table style="width:100%">
<tr><th>Von</th><th>Datum</th><th>Bewertung</th><th>Kommentar</th><th>Antwort</th><th>Link</th></tr>
      <?php
                while ($bewertung = $bewertungfac->getElement())
                {
                ?>
                <tr>
                <?php
                    // Bewertungsarray()
                    $bewertungen=array();
                    $z++;
                    // Bewertung auslesen 
                    $bewertungen[]=unserialize($bewertung->bewertung);
                    
                    // Gr��e des Bewertungsarrays bestimmen         
                    $size_bewertung=sizeof($bewertungen);
                                
                    // Wenn zum Bewerten Freigegeben
                    if ($daten->bewertungsstatus=="A")
                    {
                        // Wenn KEINE Bewertung vorhanden ist
                        if ($size_bewertung==0)
                        {
                            echo 'Noch keine Bewertungen zu diesem Eintrag vorhanden<br/><br/>';
                        }
                                
                        // Wenn mindestens eine Bewertung vorhanden ist
                        else
                        {
                            $punktebewertung=0;
                                     
                            //Bewertungspunkte addieren 
                            for ($x=0;$x<$size_bewertung;$x++)
                            {
                                $punktebewertung=$punktebewertung+(array_sum($bewertungen[$x])/sizeof($bewertungen[$x]));
                            }
                                       
                            // Quersumme bilden
                            $gesamtbewertung=$punktebewertung/$size_bewertung;
                            if ($z%2) $class='class="td1"';
                            else $class='';
                            echo '<td style="width:75px;"> ';
                            
                            // User auslesen wenn benutzerid nummerisch
                            if (is_numeric($bewertung->userid))
                            {
                                $benutzerfac=new Benutzer();
                                $benutzerfac->getById($bewertung->userid);
                                
                                // Wenn Benutzer vorhanden              
                                if ($benutzer=$benutzerfac->getElement()) 
                                {
                                    echo $benutzer->nickname.' (ID: '.$benutzer->id.')';
                                }
                                
                                // Wenn Benutzer nicht vorhanden
                                else echo '(unregistrierter User)'; 
                            }
                        
                            // Wenn NutzerID nicht nummerisch (weil IP gespeichert)
                            else echo '(unregistrierter User)';
                            
                            echo '</td><td>';
                            // Wenn ordentliches Datum gespeichert wurde (Unix-Timestamp)
                            if (is_numeric($bewertung->datum))
                            {
                                echo date("d.m.Y",$bewertung->datum);
                            }
                            echo '</td><td style="width:95px">';
                            // Ausgabe der Bewertung
                            for ($r=1;$r<=5;$r++)
                            {
                                if ($r<=round($gesamtbewertung,0))
                                {?>
                                    <img src="<?php echo WEBDIR;?>images/icons/star.gif" alt="Stern-bewertet-<?php echo $r;?>"/>
                                <?php 
                            }
                        
                            else
                            {?>
                                <img src="<?php echo WEBDIR;?>images/icons/star_grey.gif" alt="Stern-unbewertet-<?php echo $r;?>"/>
                            <?php
                            }
                            
                        }
                        echo '</td><td style="width:110px;">';
                        // Wenn Bewertungskommentar freigegeben, eine Userid gespeichert wurde und der Kommentar nicht leer ist                
                        if (($bewertung->aktiv==1 || $bewertung->aktiv==2 ) && is_numeric($bewertung->userid) && $bewertung->kommentar!='')
                        {
                            echo substr(strip_tags($bewertung->kommentar),0,150)." ...";
                        }
                        
                        // Wenn Bewertungskommentar NICHT freigegeben, eine Userid gespeichert wurde und der Kommentar nicht leer ist   
                        else if ($bewertung->aktiv==0 && is_numeric($bewertung->userid) && $bewertung->kommentar!='')
                        {   
                            echo "Bewertungskomnmentar noch nicht freigeschaltet";
                        }
                        echo '</td><td style="width:110px;">';
                            echo substr(strip_tags($bewertung->antwort),0,150)." ...";    
                        echo '</td><td>';
                        if ($bewertung->aktiv=="1" && $bewertung->antwort=="")
                        {
                            echo $l->makeLink('<b>[ Antworten ]</b>',WEBDIR.'unternehmen/bewertungen.edit.php?bid='.$bewertung->id.'&amp;id='.$daten->id);
                        }
                        else if ($bewertung->aktiv!="1")
                        {
                            echo "Freigabe fehlt";
                        }
                        else echo "Antwort vorhanden";
                        
                        echo '</td>';
                    }
                }
                // Wenn nicht zum Bewerten Freigegeben
                //else echo 'Dieser Eintrag ist noch nicht zur Bewertung freigegeben!<br/><br/> Bitte versuchen Sie es sp�ter nocheinmal!';
                ?>
                </tr> 
            <?php    
            }
            ?>
            </table>
      <?php
      // Wenn kein Datensatz gefunden wurde
      if($z ==0) 
      {
          echo "<br/>Keine Bewertung mit Kommentar f�r Ihre Unternehmens vorhanden<br />";
      }
} // end if ($_SESSION['user']->typ == "unternehmen")

include(INCLUDEDIR."footer.inc.php");
?>



