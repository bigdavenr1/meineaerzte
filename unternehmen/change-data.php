<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// unternehmen/edit.php zeigt das Formular zur �nderung von Unternehmen an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

$datenfac = new Daten();
$datenfac->getByMail($_SESSION['user']->email);
if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
$daten = $datenfac->getElement();

?>
<h1>pers�nliche Daten Bearbeiten</h1>
<br />
<?php if ($_SESSION['err']){ echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']); }?>
<form action="<?php echo $l->makeFormLink(WEBDIR.'unternehmen/change-data-update.php');?>" method="post" >
    <fieldset>
        <legend>
            Datenformular
        </legend>
        <?php
        if($_SESSION['ustatus'] == 'N' || $_SESSION['ustatus'] == 'NOFIRMA')
        {
        ?>
        <p>Mit dem Versenden des Formulars sind Sie f�r die Richtigkeit und Pflege Ihrer Daten selbst zust�ndig..."</p>
        <label for="zustimmen">
            Zustimmen
        </label>
        <input type="checkbox" id="zustimmen" name="zustimmen" value="Z" class="selectbox" />
        <br class="clr" />
        <?php
        }
        ?>
        <label for="titel">
            Titel
        </label>
        <input type="text" name="titel" id="titel" value="<?php echo $daten->titel;?>" />
        <br class="clr" />
        <label>Anrede</label>
        <select name="anrede" id="anrede">
         <option value="Herr" <?php if($daten->anrede == "Herr")echo 'selected='.'"selected"'; ?>>Herr</option>
         <option value="Frau" <?php if($daten->anrede == "Frau")echo 'selected='.'"selected"'; ?>>Frau</option>
         </select>
        <br class="clr" />
        <label for="famname">
            Nachname
        </label>
        <input type="text" name="famname" id="famname" value="<?php echo $daten->famname;?>" />
        <br class="clr" />
        <label for="vorname">
            Vorname
        </label>
        <input type="text" id="vorname" name="vorname" value="<?php echo $daten->vorname;?>" />
        <br class="clr" /><br/>
        <label for="strasse">
            Strasse
        </label>
        <input type="text" id="strasse" name="strasse" value="<?php echo $daten->strasse;?>" />
        <br class="clr" />
        <label for="plz">
            PLZ / Ort
        </label>
        <input type="text" id="plz" name="plz" value="<?php echo $daten->plz;?>" class="plz"/>
        <input type="text" id="ort" name="ort" value="<?php echo $daten->ort;?>" class="ort" alt="ort"/>
        <br class="clr" />
        <label for="land">
            Land
        </label>
        <input type="text" id="land" name="land" value="<?php echo $daten->land;?>" />
        <br class="clr" /><br/>
        <label for="tel">
            Telefon
        </label>
        <input type="text" id="tel" name="tel" value="<?php echo $daten->tel;?>" />
        <br class="clr" />
        <label for="fax">
            Fax
        </label>
        <input type="text" id="fax" name="fax" value="<?php echo $daten->fax;?>" />
        <br class="clr" /><br/>
        <label for="email">
            eMail
        </label>
        <input type="text" id="email" name="email" value="<?php echo $daten->email;?>" />
        <br class="clr" />
        <label>Neues Passwort</label><input type="password" name="pass"  maxlength="25" />
        <br class="clr" />
        <label>Neues P. best�tigen</label><input type="password" name="repass" maxlength="25" />
        <br class="clr" />
        <label for="homepage">
            Homepage
        </label>
        <input type="text" id="homepage" name="homepage" value="<?php echo $daten->homepage;?>" />
        <br class="clr" />
        <label for="klaerung">
            eMail zur Kl�rung freigeben
        </label>
        <input type="checkbox" id="klaerung" name="klaerung" value="A" <?php if($daten->klaerung == "A") echo "checked='checked'" ?> class="selectbox" />
        <br class="clr" />
    </fieldset>
    <br class="clr" /><br/>
    <input type="submit" value="Daten speichern" class="submit" />
    <br class="clr"/><br/>
</form>
<?php
include(INCLUDEDIR."footer.inc.php");
?>

