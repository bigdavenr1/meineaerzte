<?php

/////////////////////////////////////////////////////////////////////////////////////////////////
// unternehmen/edit.php zeigt das Formular zur �nderung von Unternehmen an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// Datensatz holen
$datenfac = new Daten();
$datenfac->getByMail($_SESSION['user']->email);
if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
$daten = $datenfac->getElement();
?>
<h1>Beschreibung bearbeiten</h1>
<br /> 
<?php
// Status �berpr�fen und bei NichPremium Meldung ausgeben
if ($daten->status!="P") echo '<span class="err">Sie sind Free-User<br/><br/>Die Eingabe einer Beschreibung ist Premiumusern vorbehalten!<br/><br/> Melden Sie sich noch heute als Premiumuser '.$l->makeLink("hier",WEBDIR."unternehmen/premiumanmeldung.php?&amp;id=".$_SESSION['user']->id).'an und profitieren Sie von den besseren Darstellungsm�glichkeiten. Als Premiumuser haben Sie au�erdem die M�glichkeit bis zu 10 Suchbegriffe einzugeben, bis zu 5 Profilbilder hochzuladen und Sie werden bei der Suche auf den ersten Pl�tzen angezeigt.</span>';

else
{?>
    <script src="<?php echo WEBDIR;?>script/addon.js" language="javascript" type="text/javascript"></script>
    <form action="<?php echo $l->makeFormLink(WEBDIR.'unternehmen/change-beschreibung-update.php');?>" method="post" >
        <fieldset>
            <legend>
                Beschreibung bearbeiten
            </legend>
            <label for="area">
                Beschreibung
            </label>
            <?php $htmlarea = new HtmlArea("area");
            echo $htmlarea->getHtmlButtons();?>
            <textarea name="text" cols="0" rows="0" id="area"><?php echo $daten->beschreibung;?></textarea>
        </fieldset>
        <br class="clr" /><br/>
        <input type="submit" value="Daten speichern" class="submit" />
        <br class="clr"/><br/>
    </form>
<?php
}
include(INCLUDEDIR."footer.inc.php");
?>