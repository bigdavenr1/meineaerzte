<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// change-ordzeiten-delete.php - L�schen von datens�tzen aus der ordzeitentabelle
//////////////////////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");

if(!($_SESSION['user']->typ == 'unternehmen'))
{
    header("Location: ../bewertungen/login.php");
}

if ($_POST["submit"])
{
    if ($_POST["mode"] == "delete_yes")
    {
        $ordzeitenfac = new Ordzeiten();
        $ordzeitenfac->getById($_POST["id"]);
        $ordzeitenfac->deleteElement();
        header("Location: ./change-ordzeiten.php?sv=1");
    }
}

//  Sicherheitsabfrage
//////////////////////////////////////////////////////////////////////////////////////////////
    
else
{
    include(INCLUDEDIR."header.inc.php");?>
    <h1>�ffungszeit l�schen</h1>
    <?php
    $ordzeitenfac = new Ordzeiten();
    $ordzeitenfac->getById($_GET["id"]);
    $ordzeit = $ordzeitenfac->getElement();?>
    <form action="<?php echo $l->makeFormLink($_SERVER['PHP_SELF']);?>" method="post">
        <fieldset>
            <legend>Soll diese �ffnungszeit wirklich gel�scht werden?</legend>
            <input type="hidden" name="mode" value="delete_yes" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
            �ffnungszeit: <b><?php echo $ordzeitenfac->parseDay($ordzeit->daystart).' - '.$ordzeitenfac->parseDay($ordzeit->dayend).' von: '.$ordzeitenfac->parseTime($ordzeit->timestart).' bis '.$ordzeitenfac->parseTime($ordzeit->timeend);?></b><br/><br />
        </fieldset>
        <br />
        <input type="submit" value="L�schen" name="submit" class="submit" />
        <?php echo $l->makeLink("Zur�ck", "./change-ordzeiten.php", "backlink");?>
    </form>
    <?php
    include(INCLUDEDIR."footer.inc.php"); 
} ?>