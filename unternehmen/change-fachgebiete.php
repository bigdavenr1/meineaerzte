<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// unternehmen/edit.php zeigt das Formular zur �nderung von Unternehmen an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// Datensatz holen
$datenfac = new Daten();
$datenfac->getByMail($_SESSION['user']->email);
if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
$daten = $datenfac->getElement();

// Fachgebiete-Array einlesen
if (is_array(unserialize($daten->fachgebiete))) $fachgebiete=unserialize($daten->fachgebiete);
else $fachgebiete=array();
// Suchbegriffe-Array einlesen
if (is_array(unserialize($daten->suchbegriffe))) $suchbegriffe=unserialize($daten->suchbegriffe);
else $suchbegriffe=array();
// Schlagworte-Array einlesen
if (is_array(unserialize($daten->schlagworte))) $schlagworte=unserialize($daten->schlagworte);
else $schlagworte=array();

// Status �berpr�fen und maximale Suchbegriffsanzahl setzen
if ($daten->status=="P") $max=10;
else $max=4;

?>
<h1>Suchw�rter und Fachgebiete bearbeiten</h1>
<?php
if ($_SESSION["err"]){ echo '<div class="err">'.$_SESSION["err"].'</div><br />'; unset($_SESSION["err"]);}?>
<br />
<form action="<?php echo $l->makeFormLink(WEBDIR.'unternehmen/change-fachgebiete-update.php');?>" method="post" >
    <fieldset>
        <legend>
            Suchbegriffe
        </legend>
        <?php
        for($x=0;$x<$max;$x++)
        {
            echo '<input type="text" name="suchbegriff[]" value="'.$suchbegriffe[$x].'"/>';
        }
        ?>
        <br class="clr"/><br/>
        <?php
        // Wenn Free-User
        if ($daten->status!="P") echo '<span class="err">Sie sind Free-User<br/>'.$l->makeLink("hier die Vorteile als Premiumuser",WEBDIR."bewertungen/premiumvorteile.php");
        if ($daten->status!="P" && $daten->status != "A") echo ' Melden Sie sich noch heute '.$l->makeLink("hier",WEBDIR."unternehmen/premiumanmeldung.php?&amp;id=".$_SESSION['user']->id).' als Premiumuser an.';
        if ($daten->status!="P") echo '<span>';
        ?>
    </fieldset>
    <br class="clr"/><br/>
    <fieldset>
        <legend>
            Fachgebiete --- bitte bis zu drei ausw�hlen
        </legend>
    <?php
        $kategoriefac = new Fach();
        $kategoriefac->getAll();
        while ($kategorien = $kategoriefac->getElement())
        {
            $checked="";
            if (in_array($kategorien->id,$fachgebiete)) $checked='checked="checked"';
            echo '<div style="width:190px;float:left"><input type="checkbox" class="selectbox" '.$checked.' name="fachgebiete['.$kategorien->id.']" value="'.$kategorien->id.'"/>'.$kategorien->name.'</div>';
        }
    ?>
        </fieldset>
    <br class="clr" /><br/>
    <?php 
    if($daten->status=="P")
    {
    ?>
    <fieldset>
        <legend>
            Schlagworte
        </legend>
        <?php
        for($i=1;$i<4;$i++)
        {
        ?>
        <select name="<?php echo 'schlagwort['.$i.']'; ?>" >
            <option value="">Bitte w�hlen</option>
            <?php 
            $schlagwortefac=new Schlagworte();
            $schlagwortefac->getAll();
            while ($swort=$schlagwortefac->getElement())
            {
                unset($selected); 
                if($schlagworte[$i] == $swort->schlagwort) $selected = 'selected="selected"'; 
                else $teststring = "";
                echo '<option value="'.$swort->schlagwort.'" '.$selected.'>'.$swort->schlagwort.'</option>';
            }     
            ?>
        </select>
        <?php
        }
        ?>
        </fieldset>
    <br class="clr" /><br/>
    <?php
    }
    ?>
    <input type="submit" value="Daten speichern" class="submit" />
    <br class="clr"/><br/>
</form>

<?php
include(INCLUDEDIR."footer.inc.php");
?>
