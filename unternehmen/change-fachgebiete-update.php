<?php

////////////////////////////////////////////////////////////////////////////////
// unternehmen/change-fachgebiete-update.php - Auswertung der Formulardaten
////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");

// Daten pr�fen
unset($err);
unset($_SESSION['err']);

//echo "<pre>"; print_r($_POST['fachgebiete']); echo "</pre>";

if($_POST['fachgebiete'])
{
   $count=count($_POST['fachgebiete']);
   if($count > 3) $err .= 'Sie haben zuviele Fachgebiete gew�hlt!<br />';
   //echo "ZAHL: ".$count;
}
if($err)
{
    $_SESSION['err'] .= $err;
    header("Location: change-fachgebiete.php?sv=1");
}
else
{
    // Holen der Daten
    $datenfac = new Daten();
    $datenfac->getByMail($_SESSION['user']->email);
    if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
    if(!$daten=$datenfac->getElement()) $err .= 'Unternehmen nicht gefunden!<br />';
}

if(!$err)
{
    // update der Daten
    $datenfac->update("fachgebiete='".serialize($_POST['fachgebiete'])."',
                      suchbegriffe='".serialize($_POST['suchbegriff'])."' ,
                      schlagworte='".serialize($_POST['schlagwort'])."'");

    include(INCLUDEDIR."header.inc.php");                      
    echo '<h1>�nderung erfolgreich</h1>Die Daten wurden erfolgreich ge�ndert<br /><br />'.$l->makeLink("zur�ck zur �berischt",WEBDIR."unternehmen/change-fachgebiete.php");
    include(INCLUDEDIR."footer.inc.php");
}
else
{
    header("Location: change-fachgebiete.php?sv=1");
}
?>