<?php
////////////////////////////////////////////////////////////////////////////////
// unternehmen/zustimmen.php - Auswertung AGB-Formular
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");

// Fehlervariable l�schen
unset ($_SESSION["err"]);
unset($err);

// Fehlerpr�fung OHNE Email-Dopplungspr�fung
if(trim($_POST['antwort']) == '' ) $err .= "Sie m�ssen etwas eingeben!<br />";

// Wenn kein Fehler ist
if (!$err)
{       
    $bewertungfac=new Bewertung();
    $bewertungfac->getById($_GET['bid']);
    if($bewertung=$bewertungfac->getElement())
    {
        // update der Antwort und des active status
        $bewertungfac->update("antwort='".mysql_real_escape_string($_POST['antwort'])."'");
        header("Location: ./bewertungen.view.php?sv=1");
    }
    else  $err .= "Fehler beim Datenaufruf!<br />";  
}
// Wenn die Pr�fung Fehler ergeben hat
if($err)
{
    $_SESSION["err"] = $err; 
    header("Location: ./bewertungen.edit.php?bid=".$_GET[bid]."&sv=1");
}
?>