<?php
////////////////////////////////////////////////////////////////////////////////
// unternehmen/bewertungen.view.php zeigt Tabelle mit Bewertungen des Unternehmens
////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

// Wenn eingeloggt
if ($_SESSION['user']->typ == "unternehmen")
{
    // Firmen Id hohlen
    $bewertungfac=new Bewertung();
    $bewertungfac->getById($_GET['bid']);
    $bewertung = $bewertungfac->getElement();
?>
<h1>Auf Kommentar antworten</h1><br />
<div class="headline">Kommentar: </div><div class="td1"><?php echo $bewertung->kommentar; ?></div><br />
<?php
    if ($_SESSION['err']) 
    {
        echo '<span class="err">'.$_SESSION['err'].'</span><br/>'; 
        unset($_SESSION['err']);
    }
?>
<form action="<?php echo $l->makeFormLink(WEBDIR.'unternehmen/bewertungen.update.php?bid='.$_GET['bid']);?>" method="post" >
    <fieldset>
        <legend>
            Antworten auf Kommentar
        </legend>
        <textarea name="antwort" cols="0" rows="0" id="antwort"></textarea>
    </fieldset>
    <br class="clr"/><br/>
    <input type="submit" value="Daten speichern" class="submit" />
<form>        
<?php
    
} 
include(INCLUDEDIR."footer.inc.php");
?>