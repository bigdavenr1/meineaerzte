<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// unternehmen/premium.update.php regelt das update der Unternehmenstabelle
//////////////////////////////////////////////////////////////////////////////////////////////////
include("../inc/config.php");
include(INCLUDEDIR."std/email.class.php");          // eMail-class

//////////////////////////////////////////////////////////////////////////////////////////////////
// Datensatz �ndern
//////////////////////////////////////////////////////////////////////////////////////////////////

// daten testen
unset($_SESSION['err']);        // alte Fehlermeldung l�schen
unset($_SESSION['msg']);        // alte Meldung l�schen
unset($_SESSION['premium']);    // alte Formulardaten l�schen
unset($err);

if($_SESSION['user'])
{
  $unternehmenfac = new Daten();
  $unternehmenfac->getByMail($_SESSION['user']->email);
  if ($unternehmenfac->getElementCount()==0)
  {
    $unternehmenfac->getByKuNr($_SESSION['user']->email);
  }
  
  if ($unternehmenfac->getElementCount()!=1)
  {
     $err = "Firma nicht gefunden<br />";
     $_SESSION['msg'] .= "F�r Sie ist kein Unternehmen in der Datenbank vorhanden!";
  }
  else
  {
      $daten = $unternehmenfac->getElement();
      //$_SESSION['msg'] .= "Daten f�r die Firma <b>".$daten->name."</b>";
  }
}
else
{
    $err .= "Nicht eingeloggt!<br />";
    header("Location:../bewertungen/login.php?msg=login"); 
}


switch ($_GET['step'])
{
  case 2:
        if(!$_POST['agb']) $err .= "Sie m�ssem die ABG durchlesen und zustimmen!<br />";
        else $_SESSION['premium']['agb'] = $_POST['agb'];
        
        break;
        
  case 3:
        if(!$_POST['zahlungsweise']) $err .= "Bitte Zahlungsweise ausw�hlen!<br />";
        else $_SESSION['premium']['zahlungsweise'] = $_POST['zahlungsweise'];
        
        if($_POST['zahlungsweise'])
        {
            if($_POST['zahlungsweise']=="Vorab") 
            { 
               $_SESSION['msg'] .= " Hinweis unten die Kontodaten f�r die �berweisung, werden auch noch per Email gesannt! <br />";
            }
        }
        // datensatz einlesen, wenn kein Fehler - insbesondere die Firma gefunden wurde
        if(!$err)
        {
          // Update der DB 
          $unternehmenfac->update("status='A', zahlungsweise='".mysql_real_escape_string($_POST['zahlungsweise'])."'");

          // Daten f�r die Mails hohlen
          $datum = time();
          
          // Mail an Langer
          unset($msg);
          $msg = "Hallo Herr Langer".chr(10);
          $msg .= chr(10);
          $msg .= "Firma ".$daten->name." hat sich am ".date("d.m.Y",$datum).", zum Premiumupdate angemeldet:".chr(10).chr(10);
          $msg .= "Firma mit Id ".$daten->id." / KundenNr ".$daten->kundennummer." / ".$daten->anrede." ".$daten->vorname." ".$daten->famname;
          $msg .= " hat sich angemeldet!".chr(10).chr(10);
          $msg .= "Gew�hlte Zahlungsweise: ".$_POST['zahlungsweise'].chr(10);
          
          $emailfac = new Email(1);
          $emailfac->setFrom($CONST_MAIL['from']);
          $emailfac->setTo($CONST_MAIL['an']);
          $emailfac->setSubject("Premiumanmeldung");
          $emailfac->setContent($msg);
          //echo nl2br($msg);
          $emailfac->sendMail();
          // End Mail Langer
          
          // Mail an Firma
          unset($msg);
          $msg = "Hallo ".$daten->anrede." ".$daten->vorname." ".$daten->famname.chr(10);
          $msg .= chr(10);
          $msg .= "Sie haben sich am ".date("d.m.Y",$datum)." f�r das Premiumupdate angemeldet:".chr(10).chr(10);
          $msg .= "Gew�hlte Zahlungsweise: ".$_POST['zahlungsweise'].chr(10);
          // bei Vorab Kontodaten
          /*    // kopiert aus Forum 28.05.2008
                IBAN: AT893468000008140550
                BIC: RZOOAT2L680
                Raiffeisenbank Wels
                K.Nr.: 8140550
                BLZ: 34680
                Konto Inhaber: Langer Oliver 
          */
          if($_POST['zahlungsweise']=="Vorab")
          {
              $msg .= "Kontoinhaber : Langer Oliver ".chr(10);
              $msg .= "Kontonummer : 8140550 ".chr(10);
              $msg .= "BLZ : 34680 ".chr(10);
              $msg .= "Bank : Raiffeisenbank Wels ".chr(10);
              $msg .= "IBAN : AT893468000008140550 ".chr(10);
              $msg .= "BIC : RZOOAT2L680 ".chr(10).chr(10);
              
              // Kontodaten in SESSION f�r anzeige auf premiumanmeldung.php
              $_SESSION['premium']['inhaber']= "Langer Oliver";
              $_SESSION['premium']['konto'] = "8140550";
              $_SESSION['premium']['blz']   = "34680";
              $_SESSION['premium']['bank']  = "Raiffeisenbank Wels";
              $_SESSION['premium']['iban']  = "AT893468000008140550";
              $_SESSION['premium']['bic']   = "RZOOAT2L680";
              
          }
          elseif($_POST['zahlungsweise']=="Nachnahme")
          {
              $msg .= "Ihnen werden die Unterlagen per Nachnahme umgehend zugesannt und Ihr Premiumstatus freigeschaltet!".chr(10).chr(10);
          }
          else $msg .= "Ihnen wird die Rechnung umgehend zugesannt, und nach Bezahlen wird Ihr Premiumstatus freigeschaltet!".chr(10).chr(10);
          
          $msg .= " Vielen Dank!".chr(10).chr(10);
        
          $emailfac = new Email(1);
          $emailfac->setFrom($CONST_MAIL['from']);
          $emailfac->setTo($daten->email);
          $emailfac->setSubject("User Anmeldung");
          $emailfac->setContent($msg);
          //echo nl2br($msg);
          $emailfac->sendMail();
          // End Mail Firma
          
          // Sessionstatus f�r loginmask �ndern
          $_SESSION['ustatus']="A";
          
          $_SESSION['msg'] .=" Premiumanmeldung erfolgreich gespeichert <br />";
            
          }
          break;
}

if($err) 
{
    $_SESSION['err']= $err;
    $_GET["step"]--;
    //$_SESSION['msg'] .= " unvollst�ndig! <br />";
    header("Location:premiumanmeldung.php?step=".$_GET["step"]."&sv=1");
}

header("Location:premiumanmeldung.php?step=".$_GET["step"]."&sv=1");
?>
