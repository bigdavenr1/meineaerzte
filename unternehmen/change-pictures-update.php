<?php

//////////////////////////////////////////////////////////////////////////////////////////////////
// foto.view.php - Zeigt die "Fotogalerie" an
//////////////////////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."std/email.class.php");

// Fehlervariable l�schen
unset ($_SESSION["err"]);

// Holen der Daten
$datenfac = new Daten();
$datenfac->getByMail($_SESSION['user']->email);
if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
$daten = $datenfac->getElement();

// Wenn Bilder vorhanden sind, dann als Array einlesen
if (is_array(unserialize($daten->bild))) $bildname=unserialize($daten->bild);
else $bildname=array();
    
// Bildkommentare einlesen;
$bildkommentar=$_POST['bildkommentar'];

/////////////////////////////////////////////////////////////////////////    
// vorhandene Bilder aktualisieren
///////////////////////////////////////////////////////////////////////// 
   
// Schauen ob Bilder gel�scht werden sollen
for ($x=5;$x>-1;$x--)
{
    // eventuell altes Bild l�schen
    if($_POST['bildloeschen'.$x])
    {
        // Grafiktyp auslesen
        $typ=getimagesize(LOCALDIR."images/unternehmen/".$bildname[$x]['url']) ;
      
        // je nach typ Endung bestimmen   
        if ($typ[2]==1) $endung=".gif";
        if ($typ[2]==2) $endung=".jpg"; 
            
        // Bild und Thumb l�schen
   	    unlink(LOCALDIR."images/unternehmen/".$bildname[$x]['url']);
        unlink(LOCALDIR."images/unternehmen/".basename($bildname[$x]['url'],$endung)."_thumb".$endung);
            
        // Bildurl und Name l�schen
        unset($bildname[$x]);
        unset($bildkommentar[$x]);
            
        // Arrays neu aufbauen und durchnummerieren
        $bildname=array_values($bildname);     
        $bildkommentar=array_values($bildkommentar);  
    }  
}
    
// Bildkommentare aktualisieren
for ($x=0;$x<sizeof($bildname);$x++)
{
    $bildname[$x]['bildkommentar']=$bildkommentar[$x];
}

/////////////////////////////////////////////////////////////////////////    
// neue Bilder hochladen
/////////////////////////////////////////////////////////////////////////     
   
if ($_FILES['bildneu'] != "") // Bild ausgew�hlt
{                        
    // �berpr�fung des Bildtyps
    $filetyp = getimagesize($_FILES['bildneu']['tmp_name']);
 
    // �berpr�fung des Bildtyps
    if ($filetyp[2]==1 || $filetyp[2]==2) // ==1 gif ==2 jpg
    {
        if ($filetyp[2]==2) $type = '.jpg';
        if ($filetyp[2]==1) $type = '.gif';
                   
        // neues Bild hochladen
        $key=generateRandomKey(10);
        $pic = $datenfac->writePic($_FILES['bildneu']["tmp_name"],$key,$type);
        $bildname[]= array("url"=>$key.$type,
                 "bildkommentar"=>$_POST['bildkommentarneu']);
    }
    
    // Bei falschem Bildtyp Fehlermeldung setzen
    else $_SESSION["err"]="Das Bild hat einen falschen Datentyp. Es sind nur GIF oder JPEG-Bilder erlaubt!"; 
}

// Datenbank aktualisieren
$datenfac->update("bild='".serialize($bildname)."'");
    
header("Location: change-pictures.php?sv=1");
?>