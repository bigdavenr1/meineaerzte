<?php
////////////////////////////////////////////////////////////////////////////////
// unternehmen/change-ordzeiten.php zeigt das Formular zur �nderung von Unternehmen an
////////////////////////////////////////////////////////////////////////////////

include("../inc/config.php");
include(INCLUDEDIR."header.inc.php");

$ordzeitenfac = new Ordzeiten();

if($_SESSION['user']->typ == 'unternehmen')
{
    // unternehmen Id hohlen mit user mail
    $datenfac = new Daten();
    $datenfac->getByMail($_SESSION['user']->email);
    if ($datenfac->getElementCount()==0) $datenfac->getByKuNr($_SESSION['user']->email);
    $firma = $datenfac->getElement();
    
    if($firma)
    {
        $ordzeitenfac->getByFirmId($firma->id);
        $count = $ordzeitenfac->getElementCount();
    }
    else $_SESSION['err'] = "F�r Ihr Login wurde Kein Unternehmen gefunden";
?>
<h1>Ordinationszeiten bearbeiten / anlegen</h1>
<?php if ($_SESSION['err']) {echo '<span class="err">'.$_SESSION['err'].'</span><br/>';unset($_SESSION['err']);}?>
<br />
<?php if($firma){ echo '<b>'.$l->makeLink($icon_neu_small." [ Ordinationszeiten �ndern ]",WEBDIR."unternehmen/change-ordzeiten-edit.php?mode=new","none").'</b>';} ?>
<br /><br />

        <table>
        <tr>
            <th>Tag</th>
            <th>von</th>
            <th></th>
            <th>bis</th>
            <th></th>
            <th>von</th>
            <th></th>
            <th>bis</th>
            <th>Hinweis</th>
        </tr>
<?php
                $t=0;
                for ($i=1;$i<8;$i++)
                {
                    $ordzeitenfac = new Ordzeiten();
                    $ordzeitenfac->getByFirmIdAndDay($firma->id,$i);
                    $counttimes = $ordzeitenfac->getElementCount();
                    if ($ordzeit = $ordzeitenfac->getElement())
                    {
                        $t++;
                        echo "<tr>";
                        echo "<td>".$ordzeitenfac->parseDay($i,"long")."</td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timestart)."</td><td> - </td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timeend)."</td>".chr(10);
                        if ($counttimes>1)
                        {
                            $ordzeit = $ordzeitenfac->getElement();
                            echo "<td> und </td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timestart)."</td><td> - </td><td style='text-align:right'>".$ordzeitenfac->parseTime($ordzeit->timeend)."</td>";
                        }  
                        else echo '<td colspan="4"></td>';
                        
                        echo "<td> - ".$ordzeit->memo."</tr>";
                    }
                }
                if ($t=0) echo "<tr><td>Keine Ordinationszeiten hinterlegt</td></tr>";   
               ?>  
        </table>
<?php
}// end if($_SESSION['user']->typ == 'unternehmen')   
include(INCLUDEDIR."footer.inc.php");
?>

